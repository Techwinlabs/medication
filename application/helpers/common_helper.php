<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Encode Response to json
function json_output($statusHeader,$response)
{
	$ci =& get_instance();
	$ci->output->set_content_type('application/json');
	$ci->output->set_status_header($statusHeader);
	$ci->output->set_output(json_encode($response));
}
## check Required Parameter
function check_required($array)
{
	$requried = "";
	foreach($array as $key => $value) {
		if ($value==''){
			$requried .= $key.',';
		}
	}
	if($requried!=""){
		return rtrim($requried,',');
	}else{
		return false;
	}
}
## UTC Date
function today(){
	$date = array();
	$date[0] = date('Y-m-d H:i:s', time());
	$date[1] = strtotime(date('Y-m-d H:i:s', time()));
	return $date;
}

##convert UTC or server time to Local
function convterUTCtoLocal($format,$datetime,$timezone){
	$timestamp = strtotime($datetime);
	$daylight_saving = false;
	$f =  gmt_to_local($timestamp, $timezone, $daylight_saving);
	date_default_timezone_set($timezone);
	$date =  date($format,$f);
	date_default_timezone_set('UTC');
	return $date;
}

#hours diff between timezone
function getHoursMin($timezone){
	$local_tz = new DateTimeZone('UTC');
	$local = new DateTime('now', $local_tz);
	
	$user_tz = new DateTimeZone($timezone);
	$user = new DateTime('now', $user_tz);
	
	$usersTime = new DateTime($user->format('Y-m-d H:i:s'));
	$localsTime = new DateTime($local->format('Y-m-d H:i:s'));
	
	$interval = $usersTime->diff($localsTime);
	if($interval->invert==1){
		$sign   =    '+';
	}else{
		$sign   =    '-';
	}
	
	return $sign.$interval->h.':'.$interval->i;
}

##UTC To Local
function convertUtcToLocal($format,$datetime,$timezone){
	$date = new DateTime($datetime, new DateTimeZone('UTC'));
	$date->setTimezone(new DateTimeZone($timezone));
	return $date->format($format);
}

##Local To UTC
function covertLocaltoUTC($format,$datetime,$timezone){
	$date = new DateTime($datetime, new DateTimeZone($timezone));
	$date->setTimezone(new DateTimeZone('UTC'));
	return $date->format($format);
}


## Get random password
function randomCode($length = 9, $add_dashes = false, $available_sets = 'hems'){
	$sets = array();
    if(strpos($available_sets, 'h') !== false)
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    if(strpos($available_sets, 'e') !== false)
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if(strpos($available_sets, 'm') !== false)
        $sets[] = '23456789';
    if(strpos($available_sets, 's') !== false)
        $sets[] = '@#$&*?';

    $all = '';
    $password = '';
    foreach($sets as $set)
    {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }

    $all = str_split($all);
    for($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];

    $password = str_shuffle($password);

    if(!$add_dashes)
        return $password;

    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while(strlen($password) > $dash_len)
    {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}

## Number Suffix
function sufixNumber($number) {
    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
    if ((($number % 100) >= 11) && (($number%100) <= 13)){
        return $number. 'th';
    }else{
        return $number. $ends[$number % 10];
	}
}
function get_location($address){
	$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false";
	$result_string = file_get_contents($url);
	$result = json_decode($result_string, true);
	$result1[]=$result['results'][0];
	$result2[]=$result1[0]['geometry'];
	$result3[]=$result2[0]['location'];
	return $result3[0];
}


############# DB Helper  ###########

## Get any data of any table
function getAnythingData($table,$condition_field,$condition_table_id,$whatever){
	$CI =& get_instance();
	$check = $CI->db->select($whatever)->from($table)->WHERE(array($condition_field=>$condition_table_id))->get()->row();
	if($check){
		return $check->$whatever;
	}else{
		return "";
	}
}
## Sort Array
function array_sort_by_column(&$arr, $col, $dir = SORT_DESC) { ## SORT_ASC
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }
	array_multisort($sort_col, $dir, $arr);
	return $arr;
}
// Get Thread id 
function getThreadId($sender_id,$receiver_id){
	$CI =& get_instance();
	$check = $CI->db->query("SELECT DISTINCT(`thread_id`) FROM `med_message` WHERE ((`sender_id`=$sender_id AND `receiver_id`=$receiver_id) OR ( `sender_id`=$receiver_id AND `receiver_id`=$sender_id ))")->row();
	if($check){
		return $check->thread_id;
		exit;
	}
	$check = $CI->db->select('max(thread_id) as thread_id')->from('med_message')->get()->row();
	if($check){
		## Activity History
		$name = getAnythingData("med_employees","employeeid",$receiver_id,'employee_name');
		$CI->db->insert("med_activities_history",array("employeeid"=>$sender_id,"section"=>4,"activity"=>"You have started chat with ".$name,"main_id"=>$receiver_id,"activity_at"=>today()[0]));
		return ($check->thread_id+1);
	}else{
		return 1;
	}
}

## Get Hous and mintues from two datetimes

function getHourAndMinutsDiff($startdatetime, $enddatetime){
	$diff = strtotime($enddatetime)-strtotime($startdatetime);
	return gmdate("H:i",$diff);
}

## Pushnotification Android
	
function andiPush($token,$message,$badge,$section,$keys,$emergency=0){
	$token			=	$token;
	$message		=	addslashes($message); 
	$badge			=	$badge;
	
	// Replace with the real server API key from Google APIs
	$apiKey = "AAAAAaNFxxw:APA91bHFn9j20yDbBaerTlfcGCVMXlJekMrNrZLAeqZFfwgW7Hsb28w3npoS7Kg-Y5k5S2DcN1V83nQTEAxmSu4wOr6VHWs_7PAFHPoC0VyEH1Vc9sEakagFq1Wzug-IC3TXwSBsV7Sm";
				
	// Set POST variables
	$url = 'https://android.googleapis.com/gcm/send';
				
	$fields = array(
		'registration_ids'  => array($token),
		'data'              => array( "message" => $message,'badge'=>$badge,'section'=>$section,'keys'=>$keys,'emergency'=>$emergency)
	);
	
	$headers = array( 
		'Authorization: key=' . $apiKey,
		'Content-Type: application/json'
	);
				
	// Open connection
	$ch = curl_init();
				
	// Set the url, number of POST vars, POST data
	curl_setopt( $ch, CURLOPT_URL, $url );
	curl_setopt( $ch, CURLOPT_POST, true );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
						
	// Execute post
	$result = curl_exec($ch);
				
	// Close connection
	curl_close($ch);
	$var=json_decode($result,true);
	//print_R($var);
	//return $var['success'];
	
}
## Push notification ios
function applePush($token,$message,$badge=0,$section,$keys,$emergency=0){
	
	 
	$deviceToken = $token;
	// Put your private key's passphrase here:
	$passphrase = '123';
	
	// Put your alert message here:
	$message = $message;
	
	////////////////////////////////////////////////////////////////////////////////
	
	$ctx = stream_context_create();
	//stream_context_set_option($ctx, 'ssl', 'local_cert', './certs/ck_developer.pem');
	stream_context_set_option($ctx, 'ssl', 'local_cert', './certs/ck.pem');
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
	
	
	// for developer ssl://gateway.sandbox.push.apple.com:2195
	
	// for distribution : ssl://gateway.push.apple.com:2195
	
	// Open a connection to the APNS server
	$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,
	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		
	/*if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);
	
	echo 'Connected to APNS' . PHP_EOL;*/
	
	// Create the payload body
	$body['aps'] = array(
	'alert' => $message,
	'sound' => 'default',
	'badge'=>$badge,
	'section'=>$section,
	'keys'	=>	$keys,
	'emergency'=>$emergency,
	'content-available'=>1
	);
	
	// Encode the payload as JSON
	$payload = json_encode($body);
	
	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
	
	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));
	// Close the connection to the server
	fclose($fp);
		
	if (!$result){
		return false;
		//echo 'Message not delivered';exit;
	}else{
		return true;
		//echo 'Message successfully delivered';exit;
	}
	////return false;
	//echo 'Message not delivered' . PHP_EOL;
	//else
	//return true;
	//echo 'Message successfully delivered' . PHP_EOL;
}

## Push notification ios
function applePush1($token,$message,$badge=0,$section,$keys,$emergency=0){
	
	 
	$deviceToken = $token;
	// Put your private key's passphrase here:
	$passphrase = '123';
	
	// Put your alert message here:
	$message = $message;
	
	////////////////////////////////////////////////////////////////////////////////
	
	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', './certs/ck_developer.pem');
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
	
	
	// for developer ssl://gateway.sandbox.push.apple.com:2195
	
	// for distribution : ssl://gateway.push.apple.com:2195
	
	// Open a connection to the APNS server
	$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,
	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		
	/*if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);
	
	echo 'Connected to APNS' . PHP_EOL;*/
	
	// Create the payload body
	if($emergency){
		$body['aps'] = array(
		'alert' => $message,
		'sound' => 'sound.mp3',
		'badge'=>$badge,
		'section'=>$section,
		'keys'	=>	$keys,
		'emergency'=>$emergency,
		'content-available'=>1
		);
		
	}else{
		$body['aps'] = array(
		'alert' => $message,
		'sound' => 'default',
		'badge'=>$badge,
		'section'=>$section,
		'keys'	=>	$keys,
		'emergency'=>$emergency,
		'content-available'=>1
		);
	}
	
	// Encode the payload as JSON
	$payload = json_encode($body);
	
	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
	
	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));
	// Close the connection to the server
	fclose($fp);
		
	if (!$result){
		return false;
		//echo 'Message not delivered';exit;
	}else{
		return true;
		//echo 'Message successfully delivered';exit;
	}
	////return false;
	//echo 'Message not delivered' . PHP_EOL;
	//else
	//return true;
	//echo 'Message successfully delivered' . PHP_EOL;
}


function getBadge($id){
	$CI =& get_instance();
	$badge = $CI->db->query("SELECT IFNULL(count,'') as count FROM badge WHERE employeeid = '".$id."'")->row_array()['count'];
	if($badge==""){
		$CI->db->insert('badge',array('count'=>1,'employeeid'=>$id));
		$badge = 1;
	}else{
		$badge=$badge+1;
		$CI->db->query('UPDATE badge SET count="'.$badge.'" WHERE employeeid="'.$id.'"');
	}
	return $badge;	
}


function activePlanLimit($company_id,$user){
	$CI =& get_instance();
	if($user==1){
		$Assigned_emp		= $CI->db->query("SELECT total_emp FROM med_companies WHERE company_id='".$company_id."'")->row_array()['total_emp'];
		$already_register	= $CI->db->query("SELECT employeeid FROM med_employees WHERE is_active=1 and company_id='".$company_id."'")->num_rows();

		if($Assigned_emp>$already_register){
			return (bool)TRUE;
		}else{
			return (bool)FALSE;
		}
	}elseif($user==2){
		$Assigned_user	= $CI->db->query("SELECT total_client FROM med_companies WHERE company_id='".$company_id."'")->row_array()['total_client'];
		$already_register	= $CI->db->query("SELECT client_id FROM med_clients WHERE company_id='".$company_id."'")->num_rows();

		if($Assigned_user>$already_register){
			return (bool)TRUE;
		}else{
			return (bool)FALSE;
		}
	}else{
		return (bool)FALSE;
	}
}