<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

###################### Admin Section #############################
$route['admin'] = 'admin/Auth/index';
$route['admin/login'] = 'admin/Auth/login';
$route['admin/forgot'] = 'admin/Auth/password_recover';
$route['admin/dashboard'] = 'admin/Dashboard/index';


## Admin > Employee Section
$route['admin/employees'] = 'admin/Employees/index';
$route['admin/new_employee'] = 'admin/Employees/new_employee';
$route['admin/update_employee/(:any)'] = 'admin/Employees/update_employee';

## Admin > Client Section
$route['admin/clients'] = 'admin/Clients/index';
$route['admin/new_client'] = 'admin/Clients/new_client';
$route['admin/update_care_program/(:any)'] = 'admin/Clients/update_care_program/$l';
$route['admin/mar/(:any)'] = 'admin/Clients/marReport/$1';
$route['admin/daily_records/(:any)'] = 'admin/Clients/daily_records/$1';
$route['admin/update_profile/(:any)'] = 'admin/Clients/update_profile/$1';

## Admin > Chat Section
$route['admin/chat_room'] = 'admin/Chat/index';
$route['admin/contact'] = 'admin/Chat/contact';
$route['admin/chat/(:any)/view'] = 'admin/Chat/chat/$1';
$route['admin/chat/(:any)/clear'] = 'admin/Chat/clear/$1';

## Admin > Schedule section
$route['admin/schedules'] = 'admin/Schedules/index';
$route['admin/new_schedule'] = 'admin/Schedules/new_schedule';
$route['admin/update_schedule/(:any)'] = 'admin/Schedules/update_schedule/$1';

## Admin > Data usage
$route['admin/datausage'] = 'admin/Data_usage/index';

## Admin > Time Sheet
$route['admin/timesheet'] = 'admin/Time_sheet/index'    ;

## Admin > Notifications
$route['admin/notifications'] = 'admin/Notifications/index';

## Admin > Payment
$route['admin/payment'] = 'admin/Payment/index';
$route['admin/payment_done'] = 'admin/Payment/success';

## Admin > Profile
$route['admin/profile'] = 'admin/profile/index';


## Admin > Library
$route['admin/library'] = 'admin/library/index';



######################## Super Admin Section ############################
$route['super'] = 'super/Auth/index';
$route['super/login'] = 'super/Auth/login';
$route['super/dashboard'] = 'super/Dashboard/index';
$route['super/companies'] = 'super/Companies/index';
$route['super/change_password'] = 'super/Companies/change_password';
$route['super/settings'] = 'super/Companies/settings';
$route['super/new_company'] = 'super/Companies/new_company';
$route['super/update_company/(:any)'] = 'super/Companies/update_company/$1';
$route['super/super_update_employee/(:any)/(:any)'] = 'super/Companies/super_update_employee/$1/$2';

