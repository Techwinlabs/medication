<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Clients Section

class Notifications extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/Admin_notifications');

	}
	public function index() {
		$this->load->helper('date');
		$data['data'] = array();
		$data['title']	= "Notifications - DCR";
		$this->load_view('notifications', $data);
	}

	
}