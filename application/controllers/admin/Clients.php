<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Clients Section

class Clients extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/Admin_clients');

	}
	public function index() {
		$this->load->helper('date');
		$data['data'] = array();
		$data['title']	= "Clients - DCR";
		$this->load_view('clients', $data);
	}

	public function new_client() {
		$data['error_msg'] = '';
		$data['company_id'] = $this->session->userdata()['userdata']['company_id'];
		if ($this->input->post('submit')){
			$img	=	$_FILES['client_picture']['name'];
			$ext = pathinfo($img, PATHINFO_EXTENSION);
			if($ext=='jpg' || $ext=='png' || $ext=='bmp' || $ext=='gif'){
				$this->form_validation->set_rules('postcode', 'Post code', 'trim|required');
				$this->form_validation->set_rules('firstname', 'First name', 'trim|required');
				$this->form_validation->set_rules('lastname', 'Last name', 'trim|required');
				$this->form_validation->set_rules('dob', 'Date of birth', 'trim|required');
				$this->form_validation->set_rules('address', 'Address', 'trim|required');
				$this->form_validation->set_rules('personal_information', 'Personal information', 'trim|required');
				$this->form_validation->set_rules('emergency_contact', 'Emergency contact number', 'trim|required');
				$input = $this->input->post();
				if ($this->form_validation->run() == TRUE) {
					$result = $this->Admin_clients->new_client($input);
					if($result['status']== 200){
						$this->session->set_flashdata('success_msg', $result['msg']);
						redirect(base_url().'admin/clients');
					}else{
						$data['error_msg'] = $result['msg'];
					}
				}else{
					$data['error_msg'] =  validation_errors();
				}
			}else{
				$data['error_msg']	= "Invalid image file.";
			}
		}	 
		$data['title']	= "Clients - DCR";
		$this->load_view('new_client', $data);
	}
	
	public function update_profile(){
		$get  = $this->uri->segment_array();
		$client_id = end($get);
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$data = $this->Admin_clients->view_profile($client_id,$company_id);
		
		$data['name'] = getAnythingData("med_clients","client_id",$client_id,'firstname') ." " .getAnythingData("med_clients","client_id",$client_id,'lastname');
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('postcode', 'Post code', 'trim|required');
			$this->form_validation->set_rules('firstname', 'First name', 'trim|required');
			$this->form_validation->set_rules('lastname', 'Last name', 'trim|required');
			$this->form_validation->set_rules('dob', 'Date of birth', 'trim|required');
			$this->form_validation->set_rules('address', 'Address', 'trim|required');
			$this->form_validation->set_rules('personal_information', 'Personal information', 'trim|required');
			$this->form_validation->set_rules('emergency_contact', 'Emergency contact number', 'trim|required');
			$input = $this->input->post();
			$error = "";
			if ($this->form_validation->run() == TRUE) {
				$input['company_id'] = $company_id;
				$result = $this->Admin_clients->update_profile($input);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'admin/clients');
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'admin/clients');
		}
		$data['title']	= "Clients - DCR";
		$this->load_view('update_profile', $data);
	}
	public function update_care_program(){
		$this->load->model('admin/Admin_ajax');
		$get  = $this->uri->segment_array();
		$data['status']='';
		$data['client_id'] = end($get);
		$data['name'] = getAnythingData("med_clients","client_id",$data['client_id'],'firstname') ." " .getAnythingData("med_clients","client_id",$data['client_id'],'lastname');
		$data['company_id'] = $this->session->userdata()['userdata']['company_id'];
		$data['data'] = $this->Admin_ajax->care_program(array('id'=>$data['client_id']));
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			$input = $this->input->post();
			$error = "";
			$result = $this->Admin_clients->update_care_program($input);
			if($result['status']== 200){
				$this->session->set_flashdata('success_msg', $result['msg']);
				redirect(base_url().'admin/clients');
			}else{
				$data['error_msg'] = $result['msg'];
			}
			
		}
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'company/users');
		}
		$data['title']	= "Clients - DCR";
		$this->load_view('update_care_program', $data);
	}
	
	public function marReport(){
		$get = $this->uri->segment_array();
		$client_id 	=	$get[3];
		$data['name'] = getAnythingData("med_clients","client_id",$client_id,'firstname')." ".getAnythingData("med_clients","client_id",$client_id,'lastname');
		$data['client_id']= $client_id;
		$data['error_msg'] = '';
		$data['title']	= "Clients - DCR";
		$this->load_view('mar_list', $data);
	}
	
	public function daily_records(){
		$get = $this->uri->segment_array();
		$client_id 	=	$get[3];
		$data['name'] = getAnythingData("med_clients","client_id",$client_id,'firstname')." ".getAnythingData("med_clients","client_id",$client_id,'lastname');
		$data['client_id']= $client_id;
		$data['error_msg'] = '';
		$data['title']	= "Clients - DCR";
		$this->load_view('daily_records', $data);
	}
	
}