<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Chat Section

class Chat extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/Admin_chat');

	}
	public function index() {
		$this->load->helper('date');
		$data = array();
		$data['data'] = $this->Admin_chat->getChatInbox();
		$this->load_view('chat_room', $data);
	}
	public function contact() {
		$this->load->helper('date');
		$data = array();
		//$data['data'] = $this->Admin_chat->select_allClients();
		$this->load_view('chat_contact', $data);
	}
	
	public function chat() {
		$this->load->helper('date');
		$get = $this->uri->segment_array();
		$receiver_id 	=	$get[3];
		$data = array();
		$datacall = $this->Admin_chat->viewChat($receiver_id);
		if($datacall['valid']){
			$data['sender_data'] = $datacall['sender_data'];
			unset($datacall['sender_data']);
			$data['receiver_data'] = $datacall['receiver_data'];
			unset( $datacall['receiver_data']);
			unset($datacall['valid']);
			$data['data'] = $datacall;
			//print"<pre>"; print_r($data); exit;
			$this->load_view('chat', $data);
		}else{
			$this->session->set_flashdata('error_msg', "Unauthorized access.");
			redirect(base_url().'admin/chat_room');
		}
	}
	
	public function clear() {
		$this->load->helper('date');
		$get = $this->uri->segment_array();
		$receiver_id 	=	$get[3];
		$data = array();
		$datacall = $this->Admin_chat->deleteChat($receiver_id);
		if($datacall['valid']){
			redirect(base_url('admin/chat/'.$receiver_id.'/view'));
		}else{
			$this->session->set_flashdata('error_msg', "Unauthorized access.");
			redirect(base_url().'admin/chat_room');
		}
	}
}