<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends ADMIN_Controller {

	public function index()
	{
		$this->load->view('admin/header');	
		$this->load->view('payment/payment');	
	}
	
	## Stripe test account credential 
	## User : hemchand.techwinlabs@gmail.com
	## PWD : Test@1985
	
	public function check()
	{
		//check whether stripe token is not empty
		if(!empty($_POST['stripeToken']))
		{
			//get token, card and user info from the form
			$token  = $_POST['stripeToken'];
			$email = $_POST['stripeEmail'];
			//include Stripe PHP library
			require_once APPPATH."third_party/stripe/init.php";
			
			//set api key
			$stripe = array(
			  "secret_key"      => "sk_test_L4OWjCP7G0ByQU1fzuwUpV0d",
			  "publishable_key" => "pk_test_LXmk1sX3bcBmeNpwlcAukcmx"
			);
			
			\Stripe\Stripe::setApiKey($stripe['secret_key']);
			
			//add customer to stripe
			$customer = \Stripe\Customer::create(array(
				'email' => $email,
				'source'  => $token
			));
			$company_id = $this->session->userdata()['userdata']['company_id'];
			
			//Payment information
			$description = "Payment for activate Medication App administration rights after expire free trial.";
			$amount = 24400;
			$currency = "usd";
			
			
			try {
				//charge a credit or a debit card
				$charge = \Stripe\Charge::create(array(
						'customer' => $customer->id,
						'amount'   => $amount,
						'currency' => $currency,
						'description' => $description,
						'metadata' => array(
							'item_id' => $company_id
						)
					));
				
				$chargeJson = $charge->jsonSerialize();
				$amount = $chargeJson['amount'];
				$balance_transaction = $chargeJson['balance_transaction'];
				$currency = $chargeJson['currency'];
				$status = $chargeJson['status'];
				$date = date("Y-m-d H:i:s");
				
				
				$this->db->insert("med_payments",array('company_id'=>$company_id, 'email'=>$email, 'paid_amount'=>($amount/100), 'paid_amount_currency'=>$currency, 'txn_id'=>$balance_transaction, 'payment_status'=>$status, 'description'=>$description,'created_at'=>$date, 'modified_at'=>$date));
				
				$this->db->query("UPDATE `med_companies` SET `status`='1',billing_plan='1' WHERE `company_id`='".$company_id."'");
				$error = "";
				$this->session->set_userdata(['access'=>1,'billing_plan'=>1]);
				$this->session->set_flashdata('transaction_id', $balance_transaction);
				redirect(base_url()."admin/payment_done");
			} catch(Stripe_CardError $e) {
				$error = $e->getMessage();
			} catch (Stripe_InvalidRequestError $e) {
			  // Invalid parameters were supplied to Stripe's API
				$error = $e->getMessage();
			} catch (Stripe_AuthenticationError $e) {
				// Authentication with Stripe's API failed
				$error = $e->getMessage();
			} catch (Stripe_ApiConnectionError $e) {
				// Network communication with Stripe failed
				$error = $e->getMessage();
			} catch (Stripe_Error $e) {
				// Display a very generic error to the user, and maybe send
				// yourself an email
				$error = $e->getMessage();
			} catch (Exception $e) {
				// Something else happened, completely unrelated to Stripe
				$error = $e->getMessage();
			}
			if($error){
				$this->session->set_flashdata('error_msg', $error);
			}
		}
	}
	
	public function success()
	{
		$this->load->view('admin/header');	
		$this->load->view('payment/success');
	}
}
