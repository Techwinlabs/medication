<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Profile Section

class Profile extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/Admin_employees');

	}
	public function index() {
		$data['data'] 			= array();
		$this->load->model('common/Common');
		$data['error_msg'] 		= '';
		$data['country_code'] 	= $this->Common->get_countryCode();
		$data['company_id'] 	= $this->session->userdata()['userdata']['company_id'];
		$data['designations'] 	= $this->Common->get_designationsList($data['company_id']);
		$data['title']			= "Profile - DCR";
		$data['stats']			= $this->Common->load_stats($data['company_id']);
		$this->load_view('profile', $data);
	}

	public function updateAdminProfile(){
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			
			$this->form_validation->set_rules('employee_name', 'Name', 'trim|required');
			$this->form_validation->set_rules('employee_email', 'Email', 'trim|required');
			$this->form_validation->set_rules('employee_country_code', 'Country code', 'trim|required');
			$this->form_validation->set_rules('employee_mobile', 'Mobile No', 'trim|required');
			$input = $this->input->post();
			$error = "";
			if ($this->form_validation->run() == TRUE) {
				
				$result = $this->Admin_employees->UpdateAdminProfile($input);
				
				if($result['status']== 200){
					
					$updated = $this->Admin_employees->view_userprofile($this->session->userdata()['userdata']['employeeid'])['data'];
					
					$session = [
						'userdata' => $updated,
					];
					$this->session->set_userdata($session);
					
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'admin/profile');
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}
	}
	
	public function changePassword(){
		$data['error_msg'] = '';
		if ($this->input->post( 'p_submit' )){
			$this->form_validation->set_rules('oldpassword', 'Old password ', 'trim|required');
			$this->form_validation->set_rules('newpassword', 'New password', 'trim|required');
			$this->form_validation->set_rules('confirmpassword', 'Confirm password', 'trim|required');
			$input = $this->input->post();
			$error = "";
			if ($this->form_validation->run() == TRUE) {
				
				$result = $this->Admin_employees->ChangePassword($input);
				
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
				}else{
					$this->session->set_flashdata('error_msg', $result['msg']);
				}
			}else{
				$this->session->set_flashdata('error_msg', validation_errors());
			}
		}
		redirect(base_url().'admin/profile');
	}
	
}