<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Admin Ajax Section

class Ajax extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/Admin_ajax');
	}
	
	# get carer list
	
	public function get_CarerList(){
		$input = array('search'=>$_POST['search']['value'],'order'=>$_POST['order'][0]['column'],'start'=>$_POST['start'],'length'=>$_POST['length'],'draw'=>$_POST['draw']);
		echo json_encode($this->Admin_ajax->get_CarerList($input));
	}
	
	# get care program

	public function care_program(){
		$input = $this->input->post();
		$data['data'] = $this->Admin_ajax->care_program($input);
		$this->load->view('admin/ajax/poc',$data);
	}
	
	# get clients

	public function get_clients(){
		$input = array('search'=>$_POST['search']['value'],'order'=>$_POST['order'][0]['column'],'start'=>$_POST['start'],'length'=>$_POST['length'],'draw'=>$_POST['draw']);
		echo json_encode($this->Admin_ajax->get_clients($input));
	}
	
	# get records for client ( library )
	
	public function get_records(){
		$input = array('year'=>$_POST['year'],'month'=>$_POST['month'],'client'=>$_POST['client']);
		echo ($this->Admin_ajax->get_records($input));
	}
	
	# get daily care record ( library )
	
	public function get_daily_care_record(){
		$input = array('year'=>$_GET['year'],'month'=>$_GET['month'],'client'=>$_GET['client']);
		echo ($this->Admin_ajax->get_daily_care_record($input));
	}
	
	# get medication record ( library )
	
	public function get_medication_record(){
		$input = array('year'=>$_GET['year'],'month'=>$_GET['month'],'client'=>$_GET['client']);
		echo ($this->Admin_ajax->get_medication_record($input));
	}
	
	# get MAR reports

	public function getMARReports(){
		$input = array('client_id'=>$_POST['client_id'],'search'=>$_POST['search']['value'],'start'=>$_POST['start'],'length'=>$_POST['length'],'draw'=>$_POST['draw']);
		echo json_encode($this->Admin_ajax->getMARReports($input));
	}
	
	# get searched MAR reports
	
	public function searchMAR(){
		$input = $this->input->post();
		$data['data'] = $this->Admin_ajax->searchMAR($input);
		$data['name'] = getAnythingData("med_clients","client_id",$input['client_id'],'firstname') ." " .getAnythingData("med_clients","client_id",$input['client_id'],'lastname');
		$this->load->view('admin/ajax/mar',$data);		
	}
	
	# get filtered daily records
	
	public function filterDailyRecords(){
		$input = $this->input->post();
		$data['data'] = $this->Admin_ajax->filterDailyRecords($input);
		$data['name'] = getAnythingData("med_clients","client_id",$input['client_id'],'firstname') ." " .getAnythingData("med_clients","client_id",$input['client_id'],'lastname');
		
		$this->load->view('admin/ajax/filterDailyRecords',$data);
	}
	
	# get schedules

	public function get_schedules(){
		$input = array('search'=>$_POST['search']['value'],'order'=>$_POST['order'][0]['column'],'start'=>$_POST['start'],'length'=>$_POST['length'],'draw'=>$_POST['draw']);
		echo json_encode($this->Admin_ajax->get_schedules($input));
	}
	
	# get daiy records

	public function daily_records(){		
		$input = array('client_id'=>$_POST['client_id'],'search'=>$_POST['search']['value'],'start'=>$_POST['start'],'length'=>$_POST['length'],'draw'=>$_POST['draw']);
		echo json_encode($this->Admin_ajax->daily_records($input));
	}
	
	# get schedule by dates
	
	public function getScheduleByDate(){
		$input = $this->input->post();
		$data['data'] = $this->Admin_ajax->getScheduleByDate($input);
		if(count($data['data'])){
			$this->load->view('admin/ajax/schedule',$data);
		}else{
			echo "1";
		}
	}
	
	# view employee profile
	
	public function viewProfile_emp(){
		$input = $this->input->post();
		$data = $this->Admin_ajax->viewProfile_emp($input['employeeid']);
		$data['id'] = $input['tid'];
		if($data['valid']){
			$this->load->view('admin/ajax/profile_emp',$data);
		}else{
			echo "1";
		}
	}
	
	# view client profile	
	
	public function viewProfile_client(){
		$input = $this->input->post();
		$data = $this->Admin_ajax->viewProfile_client($input['client_id']);
		$data['id'] = $input['tid'];
		if($data['valid']){
			$this->load->view('admin/ajax/profile_client',$data);
		}else{
			echo "1";
		}
	}
	
	# send new message
	
	public function sendNewMessage(){
		$input = $this->input->post();
		$send = $this->Admin_ajax->sendNewMessage($input);
		if($send){
			$data=['message_id'=>$send['message_id'],'message'=>$input['message'],'sender_picture'=>$send['sender_picture'],'date'=>date("F j, Y, g:i a",time())];
			$this->load->view('admin/ajax/send_message',$data);
		}else{
			echo "error";
		}
	}
	
	# load new message
	
	public function loadNewMessages(){
		$this->load->model('admin/Admin_chat');
		$input = $this->input->post();
		$datacall = $this->Admin_chat->viewChat($input['receiver_id'],$input['message_id']);
		if($datacall['valid']){
			$data['sender_data'] = $datacall['sender_data'];
			unset($datacall['sender_data']);
			$data['receiver_data'] = $datacall['receiver_data'];
			unset( $datacall['receiver_data']);
			unset($datacall['valid']);
			$data['data'] = $datacall;
			if(count($data['data'])){
				$this->load->view('admin/ajax/refresh_chat',$data);
			}else{
				echo '1';
			}
		}else{
			echo "1";
		}
	}
	
	# load carer
	
	public function loadCarer(){
		$this->load->model('admin/Admin_data_usage');
		return $this->Admin_data_usage->loadCarer();
	}
	
	# load carer in dropdown ( Timesheet )
	
	public function loadCarerForTimesheet(){
		$this->load->model('admin/Admin_timesheet');
		return $this->Admin_data_usage->loadCarer();
	}
	
	# get usage data
	
	public function getDataUsage(){
		$this->load->model('admin/Admin_data_usage');
		$input = $this->input->post();
		$data['data'] = $this->Admin_data_usage->getDataUsage($input);
		$this->load->view('admin/ajax/usage',$data);		
	}
	
	# get time sheet
	
	public function getTimeSheet(){
		$this->load->model('admin/Admin_timesheet');
		$input = $this->input->post();
		$data['data'] = $this->Admin_timesheet->getTimeSheet($input);
		$this->load->view('admin/ajax/timesheet',$data);		
	}
	
	# get filtered notifications

	public function getfilteredNotifications(){
		$this->load->model('admin/Admin_notifications');
		$input = $this->input->post();
		$data['data'] = $this->Admin_notifications->getfilteredNotifications($input);
		$this->load->view('admin/ajax/load_notifications',$data);		
	}
	
	# block free trial

	public function blockFreeTrial(){
		$this->Admin_ajax->blockFreeTrial();
		$this->session->set_userdata(['access'=>0]);
		return true;
	}
	
	# archive / unarchive user
	
	public function archive_unarchive(){
		$input = $this->input->post();
		$archive = $this->Admin_ajax->archive_unarchive($input);
		if($archive){
			echo $archive;
		}else{
			echo "error";
		}
	}
	
	# delete employee
	
	public function delete_employee(){
		$input = $this->input->post();
		$delete = $this->Admin_ajax->delete_employee($input);
		if($delete == 200){
			echo 1;
		}else{
			echo 2;
		}
	}
	
	# delete client
	
	public function delete_client(){
		$input = $this->input->post();
		$delete = $this->Admin_ajax->delete_client($input);
		if($delete == 200){
			echo 1;
		}else{
			echo 2;
		}
	}
	
	# delete schedule
	
	public function delete_schedule(){
		$input = $this->input->post();
		$delete = $this->Admin_ajax->delete_schedule($input);
		if($delete == 200){
			echo 1;
		}else{
			echo 2;
		}
	}
	
	# load schedule info
	
	public function get_schedule_info(){
		$input = $this->input->post();
		$sch = $this->Admin_ajax->get_schedule_info($input);
		if($sch['code'] == 200){
			echo $sch['msg'];
		}else{
			echo $sch['msg'];
		}
	}
	
	# load edit schedule view
	
	public function edit_schedule_view(){
		$input = $this->input->post();
		$data = $this->Admin_ajax->edit_schedule_view($input);
		$this->load->view('admin/ajax/edit_schedule_view',$data);
	}

	#--- Check old password using ajax call in profile page ---#
	public function check_old_password(){
		$input	=	$this->input->post();
		$res 	=	$this->Admin_ajax->check_old_password($input);
		echo $res;
	}

	#----- Resolve concern View------#
	public function reply_concern(){
		$input['concernid']		=	$this->input->post('concernid');
		$input['employee_id']	=	$this->input->post('employee_id');

		$this->load->view('admin/ajax/reply_concern.php',$input);
	}

	#--- Update Concern---#
	public function update_concern(){
		$input	=	$this->input->post();
		$data	=	$this->Admin_ajax->update_concern($input);
		echo $data;
	}
}