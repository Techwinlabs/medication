<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/Admin_auth');
	}
	
	public function index() {
		$session = $this->session->userdata('status');
		if ($session != 'ADMIN') {
			$this->load->view('admin/index');
		} else {
			redirect(base_url() . 'admin/clients');
		}
	}
	
	public function login(){
		$_SESSION['timestamp'] = time();
		$this->form_validation->set_rules('employee_number', 'Employee Number or Email', 'required');
		$this->form_validation->set_rules('employee_password', 'Password', 'required');
		if ($this->form_validation->run() == TRUE) {
			$employee_number = trim($_POST['employee_number']);
			$employee_password = trim($_POST['employee_password']);
			$session = [
				'timezone'=>$_POST['timezone']
			];
			$this->session->set_userdata($session);
			$data = $this->Admin_auth->login($employee_number, $employee_password);
			if ($data == 401) {
				$this->session->set_flashdata('error_msg', 'Admin account not exist. Please try again');
				redirect(base_url().'admin');
			}elseif($data == 400){
				$this->session->set_flashdata('error_msg', 'Wrong password entered.');
				redirect(base_url().'admin');
			} else {
				$_SESSION['change_pass'] = time()+10;
				$session = [
					'userdata' => $data,
					'status' => "ADMIN",
					'trial' =>$data['trial'],
					'access' =>1,
					'billing_plan' =>$data['billing_plan'],
					'employee_type'=>$data['employee_type'],
				];
				$status = getAnythingData('med_companies','company_id',$session['userdata']['company_id'],'status');
				$session['company_status']	=	$status;
				$this->session->set_userdata($session);
				if($status==1){
					if($data['employee_type']==3 ){
						redirect(base_url() . 'admin/clients');
					}else{
						redirect(base_url() . 'admin/clients');
					}
				}else{
					redirect(base_url() . 'admin/library');
				}
			}
		} else {
			$this->session->set_flashdata('error_msg', validation_errors());
			redirect(base_url() .'admin');
		}
	}
	public function password_recover() {
		if ($this->input->post('submit')){
			$this->form_validation->set_rules('email', 'Email', 'required');
			if ($this->form_validation->run() == TRUE) {
				$email = trim($_POST['email']);
				$data = $this->Admin_auth->password_recover($email);
				if($data == 400){
					$this->session->set_flashdata('error_msg', 'Admin account not exist. Please try again');
					redirect(base_url().'admin/forgot');
				}elseif($data == 500){
					$this->session->set_flashdata('error_msg', 'Internal server error. Please try again.');
					redirect(base_url().'admin/forgot');
				} else {
					$this->session->set_flashdata('success_msg', 'A temporary password has sent at your email. If email not received at your inbox please check spam folder.');
					redirect(base_url() . 'admin');
				}	
			} else {
				$this->session->set_flashdata('error_msg', validation_errors());
				redirect(base_url() .'admin/forgot');
			}
		}else{
			$this->load->view('admin/forgot');
		}
	}
	# Super Admin Masquerade as admin Admin
	public function masquerade(){
		$get  = $this->uri->segment_array();
		$admin_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode(end($get)));
		$data = $this->admin_auth->masquerade($admin_id);
		if ($data == false) {
			$this->session->set_flashdata('error_msg', 'Something went wrong! Try again.');
			redirect(base_url().'admin');
		} else {
			$session = [
				'userdata' => $data,
				'status' => "ADMIN"
			];
			$this->session->set_userdata($session);
			redirect(base_url() . 'admin/dashboard');
		}
	}
	
	public function logout() {
		$this->session->unset_userdata('userdata');
        $this->session->unset_userdata('status');
		$this->session->unset_userdata('trial');
		$this->session->unset_userdata('access');
		$this->session->unset_userdata('billing_plan');
		redirect(base_url());
	}
	
	public function profile(){
		$this->load_view('profile', []);
	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
