<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Clients Section

class Data_usage extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/Admin_data_usage');

	}
	public function index() {
		$this->load->helper('date');
		$data['data'] = array();
		$data['title']	= "Data usage - DCR";
		$this->load_view('data_usage', $data);
	}

	
}