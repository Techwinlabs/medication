<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Library Section

class Library extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/Admin_library');

	}
	public function index() {
		$this->load->helper('date');
		$data['data'] 		= 	array();
		$data['clients']	=	$this->Admin_library->get_all_clients();
		$data['title']	= "Library - DCR";
		$this->load_view('library', $data);
	}
}