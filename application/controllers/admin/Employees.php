<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Employees Section

class Employees extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/Admin_employees');
	}
	public function index() {
		$this->load->helper('date');
		$data = array();
		$data = $this->Admin_employees->select_allEmployees();
		$data['data']= $data['carer'];
		$data['manager']= $data['manager'];
		$data['supervisor']= $data['supervisor'];
		$data['family']= [];
		$data['outsideuser']=[];
		if(!empty(@$this->session->userdata('employee_designation'))){
			$data['employee_designation'] = $this->session->userdata('employee_designation');
		}else{
			$data['employee_designation'] = 1;
		}
		$data['title']	= "Staff - DCR";
		$this->load_view('employees', $data);
		$this->session->set_userdata('employee_designation','');
	}
	
	public function new_employee() {
		$this->load->model('common/Common');
		$data['error_msg'] = '';
		$data['country_code'] = $this->Common->get_countryCode();
		$data['company_id'] = $this->session->userdata()['userdata']['company_id'];
		$data['designations'] = $this->Common->get_designationsList($data['company_id']);
		$data['position']	=	$_GET['type'];
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('employee_name', 'Employee Name', 'trim|required');
			$this->form_validation->set_rules('employee_number', 'Employee Number', 'trim|required');
			$this->form_validation->set_rules('employee_email', 'Employee Email', 'trim|required');
			$this->form_validation->set_rules('employee_country_code', 'Country code', 'trim|required');
			$this->form_validation->set_rules('employee_mobile', 'Mobile No', 'trim|required');
			$this->form_validation->set_rules('employee_designation', 'Mobile No', 'trim|required');
			$input = $this->input->post();
			if ($this->form_validation->run() == TRUE) {
				$result = $this->Admin_employees->admin_new_employee($input);
				if($result['status']== 200){
					$this->session->set_userdata('employee_designation',$input['employee_designation']);
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'admin/employees');
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}
		$data['title']	= "Staff - DCR";
		$this->load_view('new_employee', $data);
	}
	
	public function update_employee() {
		$this->load->model('common/Common');
		$data['error_msg'] = '';
		$seg_arr = $this->uri->segment_array();
		$employeeid = end($seg_arr);
		$data['country_code'] = $this->Common->get_countryCode();
		$data['company_id'] = $this->session->userdata()['userdata']['company_id'];
		$data['designations'] = $this->Common->get_designationsList($data['company_id']);
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('employee_name', 'Employee Name', 'trim|required');
			$this->form_validation->set_rules('employee_number', 'Employee Number', 'trim|required');
			$this->form_validation->set_rules('employee_email', 'Employee Email', 'trim|required');
			$this->form_validation->set_rules('employee_country_code', 'Country code', 'trim|required');
			$this->form_validation->set_rules('employee_mobile', 'Mobile No', 'trim|required');
			$this->form_validation->set_rules('employee_designation', 'Mobile No', 'trim|required');
			$input = $this->input->post();
			if ($this->form_validation->run() == TRUE) {
				$input['employeeid']	=	$employeeid;
				if(!isset($input['section_roles'])){
					$input['section_roles']=[];
				}
				$result = $this->Admin_employees->admin_update_employee($input);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'admin/employees');
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}		
		$data['employees']	=	$this->Admin_employees->get_profile($employeeid);
		//echo '<pre>';print_r($data);exit;
		$data['title']	= "Staff - DCR";
		$this->load_view('update_employee', $data);
	}
	
	public function edit_userprofile(){
		$user_id = end($this->uri->segment_array());
		$data = $this->Admin_employees->view_userprofile($user_id);
		$data['company_id'] = $this->session->userdata()['userdata']['company_id'];
		$data['designations'] = $this->Common->get_designationsList($data['company_id']);
		$data['country_code'] = $this->Common->get_countryCode();
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('user_name', 'User Name', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			//$this->form_validation->set_rules('country_code', 'Country code', 'trim|required');
			$this->form_validation->set_rules('mobile_number', 'Mobile No', 'trim|required');
			$this->form_validation->set_rules('designation', 'Designation', 'trim|required');
			$input = $this->input->post();
			$error = "";
			if ($this->form_validation->run() == TRUE) {
				$result = $this->Admin_employees->edit_userprofile($input);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'company/users');
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'company/users');
		}	
		$data['title']	= "Staff - DCR";
		$this->load_view('editprofile', $data);
	}
	
	
}