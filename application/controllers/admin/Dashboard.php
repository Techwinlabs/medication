<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/Admin_dashboard');
	}
	public function index() {
		$data['employee_name'] = $this->session->userdata()['userdata']['employee_name'];
		$this->load_view('dashboard', $data);
	}
}