<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Schedues Section

class Schedules extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/Admin_schedules');
	}
	
	public function index() {
		$this->load->helper('date');
		$data = array();
		$this->load->model('common/Common');
		$data['company_id'] = $this->session->userdata()['userdata']['company_id'];
        $data['clients'] = $this->Common->get_ClientList($data['company_id']);
        $data['carer'] = $this->Common->get_CarerList($data['company_id']);
		
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$employeeid = $this->session->userdata()['userdata']['employeeid'];
		$data['schedules'] = $this->db->query("SELECT med_schedule.*,CONCAT(med_clients.firstname,' ',med_clients.lastname) as client_name,med_clients.client_picture,med_employees.employee_name,med_employees.employee_number,med_employees.employee_picture FROM med_schedule JOIN med_clients ON med_clients.client_id=med_schedule.client_id JOIN med_employees ON med_employees.employeeid=med_schedule.employeeid WHERE med_schedule.company_id='".$company_id."' ORDER BY med_schedule.start_datetime ASC")->result_array();
		$final = [];
	    for($i=0;$i<count($data['schedules']);$i++){
			$data['schedules'][$i]['client_picture'] ? $data['schedules'][$i]['client_picture'] = base_url().$data['schedules'][$i]['client_picture'] : base_url().'asset/img/dummy.svg';
			$data['schedules'][$i]['employee_picture'] ? $data['schedules'][$i]['employee_picture'] = base_url().$data['schedules'][$i]['employee_picture'] : base_url().'asset/img/dummy.svg';
		}
		
		//echo '<pre>';print_r($data);exit;
		$data['title']	= "Schedule - DCR";
		$this->load_view('schedules', $data);
    }
    
	public function new_schedule() {
		$data['error_msg'] = '';
		$start_datetime	 = date('Y-m-d H:i:s',strtotime($this->input->post('start')));
		$end_datetime	 = date('Y-m-d H:i:s',strtotime($this->input->post('end')));
		$employeeid		=	$this->input->post('employeeid');
		$client_id		=	$this->input->post('client_id');
		$input	=	array('start_datetime'=>$start_datetime,'end_datetime'=>$end_datetime,'employeeid'=>$employeeid,'client_id'=>$client_id);
		$input['company_id'] = $this->session->userdata()['userdata']['company_id'];
		$input['secheduled_by'] = $this->session->userdata()['userdata']['employeeid'];
		$result = $this->Admin_schedules->new_schedule($input);
		if($result['status']== 200){
			echo 1;
		}else{
			echo $result['msg'];
		}
    }
	
	
	public function update_schedule() {
		$data['error_msg'] = '';
		$start_datetime	 = date('Y-m-d H:i:s',strtotime($this->input->post('start')));
		$end_datetime	 = date('Y-m-d H:i:s',strtotime($this->input->post('end')));
		$employeeid		=	$this->input->post('employeeid');
		$client_id		=	$this->input->post('client_id');
		$schedule_id	=	$this->input->post('schedule_id');
		$input	=	array('start_datetime'=>$start_datetime,'end_datetime'=>$end_datetime,'employeeid'=>$employeeid,'client_id'=>$client_id,'schedule_id'=>$schedule_id);
		$input['company_id'] = $this->session->userdata()['userdata']['company_id'];
		$input['secheduled_by'] = $this->session->userdata()['userdata']['employeeid'];
		$result = $this->Admin_schedules->update_schedule($input);
		if($result['status']== 200){
			echo 1;
		}else{
			echo $result['msg'];
		}
    }
    
}