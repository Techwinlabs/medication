<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Clients Section

class Time_sheet extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/Admin_timesheet');

	}
	public function index() {
		$this->load->helper('date');
		$data['data'] = array();
		$data['title']	= "Time sheet - DCR";
		$this->load_view('time_sheet', $data);
	}

	
}