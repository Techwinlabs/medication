<?php
/**
 * App Name       -    	Medication
 * author         -     Hem Thakur
 * created        -     10/04/2018
**/		
class Cron extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('cronjob/Cronjob');
	}

	public function runSchedule()
	{
		$this->Cronjob->exeSchedule();
		echo "success";
	}
	
	# notification to manager if daily record not sent
	
	public function runDailyRecord()
	{
		$this->Cronjob->exe_runDailyRecord();
		echo "success";
	}
	
	// For notification task
	
	public function runNotificationToApp (){
		$this->Cronjob->exe_runNotificationToApp();
		echo "Cron is running";
	}
	
	
} ## End

