<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends SUPER_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Super_clients');
	}

	public function index() {
		$data['data'] = $this->Super_clients->select_allclients();
		$this->load_view('super/clients', $data);
	}
	
	public function new_client() {
		
		$error['error_msg'] = '';
		$error['country_code'] = $this->Common->get_countryCode();
		if ($this->input->post( 'submit' )){
			
			$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
			$this->form_validation->set_rules('domain', 'Company Domain', 'trim|required');
			$this->form_validation->set_rules('contact_person', 'Contact Person', 'trim|required');
			$this->form_validation->set_rules('contact_email', 'Contact Email', 'trim|required');
			$this->form_validation->set_rules('contact_phone', 'Contact Number', 'trim|required');
			$this->form_validation->set_rules('company_address', 'Company Address', 'trim|required');
			$this->form_validation->set_rules('city', 'City Name', 'trim|required');
			$this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required');

			$data = $this->input->post();
			if ($this->form_validation->run() == TRUE) {
				$result = $this->Super_clients->new_client($data);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'super/clients');
					
				}else{
					$error['error_msg'] = $result['msg'];
					
				}
			}else{
				$error['error_msg'] =  validation_errors();
			}

		}	
		$this->load_view('new_client', $error);
		
	}
	public function updateinfo(){
		$get  = $this->uri->segment_array();
		$data = $this->Super_clients->view_company(end($get));
		$data['error_msg'] = '';
		$data['country_code'] = $this->Common->get_countryCode();
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
			$this->form_validation->set_rules('domain', 'Company Domain', 'trim|required');
			$this->form_validation->set_rules('contact_person', 'Contact Person', 'trim|required');
			$this->form_validation->set_rules('contact_email', 'Contact Email', 'trim|required');
			$this->form_validation->set_rules('contact_phone', 'Contact Number', 'trim|required');
			$this->form_validation->set_rules('company_address', 'Company Address', 'trim|required');
			$this->form_validation->set_rules('city', 'City Name', 'trim|required');
			$this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required');

			$input = $this->input->post();
			$error = "";
			if ($this->form_validation->run() == TRUE) {
				$result = $this->Super_clients->updateinfo($input);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'super/clients');
				}else{
					$data['error_msg']  = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'super/clients');
		}
		
		$this->load_view('updateinfo', $data);
		
	}
	
	
	
	
	
	
}