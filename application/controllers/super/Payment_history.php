<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_history extends SUPER_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('super/Super_payment_history');
	}

	public function index() {
		$data['data'] = $this->Super_payment_history->get_payments();
		$this->load_view('payment_history', $data);
	}
	
	
	
	
	
}