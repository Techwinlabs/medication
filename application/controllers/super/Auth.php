<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('super/Super_auth');
	}
	
	public function index() {
		$session = $this->session->userdata('superstatus');
		if ($session != 'SUPER') {
			$this->load->view('super/index');
		} else {
			redirect(base_url() . 'super/companies');
		}
	}
	public function login() {
		$this->form_validation->set_rules('email', 'email', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == TRUE) {
			$email	 = trim($_POST['email']);
			$password = trim($_POST['password']);
			$data = $this->Super_auth->login($email, $password);
			if ($data == 400 || $data == 401) {
				$this->session->set_flashdata('error_msg', 'Error: Username / Password not matched.');
				redirect(base_url().'super');
			} else {
				$session = [
					'superdata' => $data,
					'superstatus' => "SUPER"
				];
				$this->session->set_userdata($session);
				redirect(base_url() . 'super/companies');
			}
		} else {
			$this->session->set_flashdata('error_msg', validation_errors());
			redirect(base_url() .'super');
		}
	}

	public function logout($session_exp="no") {
		$this->session->unset_userdata('superdata');
        $this->session->unset_userdata('superstatus');
        //$this->session->sess_destroy();
		if($session_exp=="Yes"){
			$this->session->set_flashdata('error_msg', 'Your Session is expired. Please login again.');
		}
		redirect(base_url() .'super');
	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */