<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Admin Ajax Section

class Ajax extends SUPER_Controller {
	
	public function viewCompany_profile(){
		$this->load->model('super/Super_companies');
		$input = $this->input->post();
		$data = $this->Super_companies->viewCompany_profile($input);
		$this->load->view('super/ajax/view_company_profile',$data);
	}
	
	# delete employee
	public function super_delete_employee(){
		$this->load->model('super/Super_companies');
		$input = $this->input->post();
		$delete = $this->Super_companies->super_delete_employee($input);
		if($delete == 200){
			echo 1;
		}else{
			echo 2;
		}
	}
	
	

}