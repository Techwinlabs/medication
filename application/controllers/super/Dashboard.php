<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends SUPER_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('super/Super');
	}

	public function index() {
		$data['data'] = $this->Super->useranalytics();
		$this->load_view('dashboard', $data);
	}
}
