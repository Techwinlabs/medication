<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Companies extends SUPER_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('super/Super_companies');
	}

	public function index() {
		$data['data'] = $this->Super_companies->get_companies();
		$this->load_view('companies', $data);
	}
	
	public function new_company() {
		$this->load->model('common/Common');
		$data['error_msg'] = '';
		$data['country_code'] = $this->Common->get_countryCode();
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
			$this->form_validation->set_rules('country_code', 'Country code', 'trim|required');
			$this->form_validation->set_rules('mobile_number', 'Contact Number', 'trim|required');
			$this->form_validation->set_rules('address', 'Company Address', 'trim|required');
			$this->form_validation->set_rules('city', 'City Name', 'trim|required');
			$this->form_validation->set_rules('state', 'State Name', 'trim|required');
			$this->form_validation->set_rules('country', 'Country Name', 'trim|required');
			$this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required');
			$this->form_validation->set_rules('total_client', 'Total Clients', 'trim|required');
			$this->form_validation->set_rules('total_emp', 'Total Employees', 'trim|required');
			$input = $this->input->post();
			
			if ($this->form_validation->run() == TRUE) {
				$result = $this->Super_companies->new_company($input);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'super/companies');
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}	
		$this->load_view('new_company', $data);
	}
	public function update_company(){
		$this->load->model('common/Common');
		$get  = $this->uri->segment_array();
		$data = $this->Super_companies->viewCompany_profile(array('company_id'=>end($get)));
		$data['error_msg'] = '';
		//for getting company managers (agents)
		$data['agent_lists']=$this->Super_companies->getCompanyAgents(array('company_id'=>end($get)));
		$data['country_codes'] = $this->Common->get_countryCode();
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
			$this->form_validation->set_rules('contact_person', 'Contact person', 'trim|required');
			$this->form_validation->set_rules('email', 'Email id', 'trim|required');
			$this->form_validation->set_rules('country_code', 'Country code', 'trim|required');
			$this->form_validation->set_rules('mobile_number', 'Contact Number', 'trim|required');
			$this->form_validation->set_rules('address', 'Company Address', 'trim|required');
			$this->form_validation->set_rules('city', 'City Name', 'trim|required');
			$this->form_validation->set_rules('state', 'State Name', 'trim|required');
			$this->form_validation->set_rules('country', 'Country Name', 'trim|required');
			$this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required');
			$this->form_validation->set_rules('total_client', 'Total Clients', 'trim|required');
			$this->form_validation->set_rules('total_emp', 'Total Employees', 'trim|required');
			$input = $this->input->post();
			$error = "";
			if ($this->form_validation->run() == TRUE) {
				$result = $this->Super_companies->update_company($input);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'super/companies');
				}else{
					$data['error_msg']  = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'super/companies');
		}
		
		$this->load_view('update_company', $data);
	}
	
	public function restart_trial(){
		$get  = $this->uri->segment_array();
		$companyid = end($get);
		$data = $this->Super_companies->Restart_trial(array('company_id'=>$companyid));
		if($result['status']== 200){
			$this->session->set_flashdata('success_msg', $result['msg']);
		}else{
			$this->session->set_flashdata('error_msg', $result['msg']);
		}
		redirect(base_url().'super/companies');
		
	}
	
	public function begin_service(){
		$get  = $this->uri->segment_array();
		$companyid = end($get);
		$data = $this->Super_companies->Begin_service(array('company_id'=>$companyid));
		if($result['status']== 200){
			$this->session->set_flashdata('success_msg', $result['msg']);
		}else{
			$this->session->set_flashdata('error_msg', $result['msg']);
		}
		redirect(base_url().'super/companies');
		
	}
	
	public function end_service(){
		$get  = $this->uri->segment_array();
		$companyid = end($get);
		$data = $this->Super_companies->End_service(array('company_id'=>$companyid));
		if($result['status']== 200){
			$this->session->set_flashdata('success_msg', $result['msg']);
		}else{
			$this->session->set_flashdata('error_msg', $result['msg']);
		}
		redirect(base_url().'super/companies');
		
	}

	public function settings(){
		$data['error_msg'] = "";
		$data['succ_msg']  = "";
		//getting user email
		$super_user_email=$this->Super_companies->get_super_user_email();
		$data['user_email'] = $super_user_email['email'];
		$this->load_view('settings.php', $data);
	}
	
	public function change_password(){
		$data 	 = $this->input->post();
		$respose = $this->Super_companies->change_password($data);
		$data['succ_msg']="";
		$data['error_msg']="";
		$super_user_email=$this->Super_companies->get_super_user_email();
		$data['user_email'] = $super_user_email['email'];
		if($respose==500){
			$data['error_msg'] = "Internal server error.";
		}elseif($respose==400){
			$data['error_msg'] = "Old password not correct.";
		}elseif($respose==200){
			$data['succ_msg'] = "Password changed successfuly.";
		}else{
			$data['error_msg'] = "Please try again.";
		}
		$this->load_view('settings.php', $data);
	}
	
	// For Agents
	
	public function super_update_employee() {
		$this->load->model('common/Common');
		$data['error_msg'] = '';
		$seg_arr = $this->uri->segment_array();
		$employeeid = $seg_arr[3];
		$data['country_code'] = $this->Common->get_countryCode();
		$data['company_id'] = $seg_arr[4];
		$data['designations'] = $this->Common->get_designationsList($data['company_id']);
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('employee_name', 'Employee Name', 'trim|required');
			$this->form_validation->set_rules('employee_email', 'Employee Email', 'trim|required');
			$input = $this->input->post();
			if ($this->form_validation->run() == TRUE) {
				$input['employeeid']	=	$employeeid;
				if(!isset($input['section_roles'])){
					$input['section_roles']=[];
				}
				$result = $this->Super_companies->syper_update_employee($input);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'super/super_update_employee/'.$seg_arr[3].'/'.$seg_arr[4]);
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}		
		$data['employees']	=	$this->Super_companies->get_profile($employeeid);

		$this->load_view('super_update_employee', $data);
	}
	
	
}