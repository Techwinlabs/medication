<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
		$session = $this->session->userdata('status');
		if ($session != 'ADMIN') {
			$this->load->view('admin/index');
		} else {
			redirect(base_url() . 'admin/clients');
		}
	}

	public function createusers(){
		$gender = array('1','2');
		$post	= array('1','2','3');
		$role 	= array('1,2,3,4','2,3,4','1','1,2');
		for($i=0;$i<100;$i++){
			if($i<40){
				$data['employee_type']			=	2;
			}elseif($i>30 && $i<70){
				$data['employee_type']			=	3;
				$data['section_roles']			=	$role[array_rand($role)];
			}else{
				$data['employee_type']			=	1;
			}
			$data['company_id']				=	"8";	
			
			$data['employee_number']		=	"12345678".($i+1);
			$data['employee_name']			=	"Demo Name".($i+1);
			$data['employee_mother_maiden']	=	"";
			$data['employee_email']			=	"dailycarerecord".($i+1)."@gmail.com";
			$data['employee_password']		=	crypt('12345',APP_SALT);
			$data['employee_pin']			=	crypt('12345',APP_SALT);
			$data['employee_country_code']	=	"91";
			$data['employee_mobile']		=	"9876543".($i+1);
			$data['employee_picture']		=	"";
			$data['employee_designation']	=	$data['employee_type'];
			$data['gender']					=	$gender[array_rand($gender)];
			$data['access']					=	"0";
			$data['service_on_off']			=	"0";
			$data['timezone']				=	"Asia/Kolkata";
			$data['created_at']				=	date('Y-m-d H:i:s');
			$data['updated_at']				=	date('Y-m-d H:i:s');
			$data['first_login']			=	'0';
			$data['is_active']				=	"1";

			$ins = $this->db->insert('med_employees',$data);
			if($ins){
				echo 'success';
			}
		}
	}
}
