<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * App Name       -    	Medication
 * author         -     Hem Thakur
 * created        -     24/01/2018
**/		
class App extends CI_Controller {
	public function __construct(){
		parent::__construct();
	}
	
	################ - COMMON SECTION - #################
	
	/*
	*
	* Search Company
	*
	*/
	public function search_company()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'search_company'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if($check_auth_client == true){
				$input = $_POST;
				$check 	= check_required(array('keyword'=>@$input['keyword']));
				if ($check){
					$response['status'] = 400;
					$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'search_company');
				}else{
					$response = $this->AppBackend->ht_search_company($input);
				}
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* Employee Login
	*
	*/
	public function employee_login()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'employee_login'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if($check_auth_client == true){
				$input = $_POST; // employee_number = email
				$check 	= check_required(array('employee_number'=>@$input['employee_number'],'employee_password'=>@$input['employee_password'],'device_type'=>@$input['device_type'],'device_token'=>@$input['device_token']));
				if ($check){
					$response['status'] = 400;
					$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'employee_login');
				}else{
					$response = $this->AppBackend->ht_employee_login($input);
				}
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* Forgot password
	*
	*/
	public function forgot_password()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'forgot_password'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if($check_auth_client == true){
				$input 	= $_POST;
				$check 	= check_required(array('email'=>@$input['email']));
				if ($check){
					$response['status'] = 400;
					$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'forgot_password');
				}else{
					$response = $this->AppBackend->ht_forgot_password($input);
				}
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* User Log Out
	*
	*/
	public function logout()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'logout'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
		        $response = $this->AppBackend->ht_logout();
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* View own profile
	*
	*/
	public function view_profile()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'view_profile'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
		        	$response = $this->AppBackend->ht_view_profile(array('employeeid'=>$auth['id']));
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Update Profile
	*
	*/
	public function update_profile()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'update_profile'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array("employee_name"=>@$input['employee_name']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'update_profile');
					}else{
						$input['employeeid'] 			= $auth['id'];
						$input['employee_email'] 		= @$input['employee_email'] ? $input['employee_email'] : "";
						$input['employee_country_code']	= @$input['employee_country_code'] ? $input['employee_country_code'] : "";
						$input['employee_mobile'] 		= @$input['employee_mobile'] ? $input['employee_mobile'] : "";
						$input['gender'] 				= @$input['gender'] ? $input['gender'] : "";
						
						$response = $this->AppBackend->ht_update_profile($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Change password
	*
	*/
	public function change_password()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'change_password'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array("oldpassword"=>@$input['oldpassword'],"newpassword"=>@$input['newpassword']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'change_password');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_change_password($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	########################## - EMPLOYEE SECTION - ##########################
	
	
	/*
	*
	* Search Clients
	*
	*/
	public function search_clients(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'search_clients'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $_POST;
					//$check = check_required(array("search_keyword"=>@$input['search_keyword']));
					$check = 0;
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'search_clients');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_search_clients($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Suggested Clients
	*
	*/
	public function get_suggestedClients(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_suggestedClients'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input['latitude'] = $this->uri->segment(4);
					$input['longitude'] = $this->uri->segment(5);
					$check = check_required(array("latitude"=>@$input['latitude'],"longitude"=>@$input['longitude']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'get_suggestedClients');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_get_suggestedClients($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Client Care Program
	*
	*/
	public function get_ClientCareProgram(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE ){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_ClientCareProgram'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['client_id'] = $this->uri->segment(4);
					$input['employeeid'] = $auth['id'];
					$input['date'] = isset($input['date']) ? $input['date'] : date('Y-m-d');
					$response = $this->AppBackend->ht_get_ClientCareProgram($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Enter New Medication
	*
	*/
	## status : 1 for given, 2 refused, 3 prompt, 4 damaged/lost
	## care_time : 1 for AM, 2 for Noon, 3 for Tea tiime, 4 for night
	public function enter_newMedication(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'enter_newMedication'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array("client_id"=>@$input['client_id'],"program_id"=>@$input['program_id'],"date"=>@$input['date'],"medicine"=>@$input['medicine'],"care_time"=>@$input['care_time'],"status"=>@$input['status']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'enter_newMedication');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_enter_newMedication($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* View Client Medications
	*
	*/
	public function view_ClientsMedications(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE || $this->uri->segment(5) == '' || is_numeric($this->uri->segment(5)) == FALSE ){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'view_ClientsMedications'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['client_id'] = $this->uri->segment(4);
					$input['page'] = $this->uri->segment(5);
					$input['start'] = $this->uri->segment(6);
					$input['end'] = $this->uri->segment(7);
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_view_ClientsMedications($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Daily Records Questions
	*
	*/
	public function get_dailyRecordsQuestions(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_dailyRecordsQuestions'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_dailyRecordsQuestions($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Add Daily Record
	*
	*/
	## question_answer format : [{'question_id'=>'1','answer'=>'answer here'},{'question_id'=>'2','answer'=>'answer here'},{....}]
	public function add_DailyRecords(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'add_DailyRecords'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array("client_id"=>@$input['client_id'],"postcode"=>@$input['postcode'],"question_answer"=>@$input['question_answer']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'add_DailyRecords');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_add_DailyRecords($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* View Daily Records of employee
	*
	*/
	public function view_DailyRecords(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'view_DailyRecords'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4);
					$input['start'] = $this->uri->segment(5);
					$input['end'] = $this->uri->segment(6);
					$input['client_id'] = $this->uri->segment(7) ? $this->uri->segment(7) : 0;
				
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_view_DailyRecords($input);
					
					json_output($response['status'],$response);
				}
			}
		}
	}

	/*
	*
	* View Daily Records of employee
	*
	*/
	public function view_DailyRecordsData(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'view_DailyRecordsData'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4);
					$input['start'] = $this->uri->segment(5);
					$input['end'] = $this->uri->segment(6);
					$input['client_id'] = $this->uri->segment(7) ? $this->uri->segment(7) : 0;
				
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_view_DailyRecordsData($input);
					
					json_output($response['status'],$response);
				}
			}
		}
	}

	/*
	*
	* View Daily Records of employee
	*
	*/
	public function view_DailyRecordsManagerSction(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'view_DailyRecordsManagerSction'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4);
					$input['start'] = $this->uri->segment(5);
					$input['end'] = $this->uri->segment(6);
					$input['client_id'] = $this->uri->segment(7) ? $this->uri->segment(7) : 0;
				
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_view_DailyRecordsManagerSection($input);
					
					json_output($response['status'],$response);
				}
			}
		}
	}
	/* $this->AppBackend->ht_view_DailyRecordsManagerSctionData($input);
	*
	* View Daily Records of employee
	*
	*/
	public function view_DailyRecordsManagerSctionData(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'view_DailyRecordsManagerSctionData'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4);
					$input['start'] = $this->uri->segment(5);
					$input['end'] = $this->uri->segment(6);
					$input['client_id'] = $this->uri->segment(7) ? $this->uri->segment(7) : 0;
				
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_view_DailyRecordsManagerSctionData($input);
					
					json_output($response['status'],$response);
				}
			}
		}
	}

	/*
	*
	* Get Message Inbox
	*
	*/
	public function get_MessageInbox(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_MessageInbox'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_MessageInbox($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* View Messages
	*
	*/
	public function view_Messages(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE || $this->uri->segment(5) == '' || is_numeric($this->uri->segment(5)) == FALSE){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'view_Messages'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['receiver_id'] = $this->uri->segment(4);
					$input['page'] = $this->uri->segment(5);
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_view_Messages($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Refresh Chat
	*
	*/
	public function refreshChat()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE || $this->uri->segment(5) == '' || is_numeric($this->uri->segment(5)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'refreshChat'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['receiver_id'] = $this->uri->segment(4);
					$input['last_message_id'] = $this->uri->segment(5);
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_refreshChat($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Send new message
	*
	*/
	public function send_newMessage()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'send_newMessage'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('receiver_id'=>@$input['receiver_id'],'message'=>@$input['message']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'send_newMessage');
					}else{
						$input['sender_id'] = $auth['id'];
						$response = $this->AppBackend->ht_send_newMessage($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Clear Chat
	*
	*/
	public function clear_Chat()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'clear_Chat'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('thread_id'=>@$input['thread_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'clear_Chat');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_clear_Chat($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Delete Message
	*
	*/
	public function delete_SingleMessage()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'delete_SingleMessage'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('message_id'=>@$input['message_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'delete_SingleMessage');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_delete_SingleMessage($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	/*
	*
	* Get My Day wise count of schedule
	*
	*/
	public function get_MyDayWiseScheduleCount(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_MyDayWiseScheduleCount'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['date'] = $this->uri->segment(4); ## Format YYYY-MM
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_MyDayWiseScheduleCount($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get My Day wise schedule list
	*
	*/
	public function get_MyDayWiseSchedule(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_MyDayWiseSchedule'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['date'] = $this->uri->segment(4); ## Format YYYY-MM-DD
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_MyDayWiseSchedule($input);
					json_output($response['status'],$response);
				}
			}
		}
	}




	###################### - MANAGER SECTION - ######################
	/*
	*
	* Get All Employees
	*
	*/
	public function get_allStaffMembers(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_allStaffMembers'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4);
					$input['search'] = $this->uri->segment(5) ? $this->uri->segment(5) : "";
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_allStaffMembers($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Designations list
	*
	*/
	public function get_designationsList(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_designationsList'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_designationsList($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Country Codes list
	*
	*/
	public function get_CountryCode(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_CountryCode'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_CountryCode($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Check Email is Free
	*
	*/
	public function checkEmailIsFree(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'checkEmailIsFree'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('employee_email'=>@$input['employee_email']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'checkEmailIsFree');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_checkEmailIsFree($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Check Employee Code is Free
	*
	*/
	public function checkEmployeeNumberIsFree(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'checkEmployeeNumberIsFree'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('employee_number'=>@$input['employee_number']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'checkEmployeeNumberIsFree');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_checkEmployeeNumberIsFree($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	/*
	*
	* Add new staff member
	*
	*/
	public function add_newStaffMember()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'add_newStaffMember'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('employee_name'=>@$input['employee_name'],'employee_number'=>@$input['employee_number'],'employee_email'=>@$input['employee_email'],'employee_country_code'=>@$input['employee_country_code'],'employee_mobile'=>@$input['employee_mobile']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'add_newStaffMember');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_add_newStaffMember($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Staff Member info
	*
	*/
	public function get_staffMemberInfo(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_staffMemberInfo'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['staff_id'] = $this->uri->segment(4);
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_staffMemberInfo($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Update Staff member info
	*
	*/
	public function update_staffMemberInfo()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'update_staffMemberInfo'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('staff_id'=>@$input['staff_id'],'employee_name'=>@$input['employee_name'],'employee_number'=>@$input['employee_number'],'employee_email'=>@$input['employee_email'],'employee_country_code'=>@$input['employee_country_code'],'employee_mobile'=>@$input['employee_mobile']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'update_staffMemberInfo');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_update_staffMemberInfo($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	/*
	*
	* Get All Clients
	*
	*/
	public function get_allClients(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_allClients'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4);
					$input['section'] = $this->uri->segment(5) ? $this->uri->segment(5) : 1;  ## section = 1 global call, From Schedule page section = date
					$input['search'] = $this->uri->segment(6) ? $this->uri->segment(6) : "";
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_allClients($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	/*
	*
	* Add new staff member
	*
	*/
	public function add_newClient()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'add_newClient'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('postcode'=>@$input['postcode'],'firstname'=>@$input['firstname'],'lastname'=>@$input['lastname'],'dob'=>@$input['dob'],'address'=>@$input['address'],'emergency_contact'=>@$input['emergency_contact']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'add_newClient');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_add_newClient($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	/*
	*
	* View Client Profile
	*
	*/
	public function view_ClientProfile(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'view_ClientProfile'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['client_id'] = $this->uri->segment(4);
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_view_ClientProfile($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
		/*
	*
	* Add new staff member
	*
	*/
	public function update_ClientProfile()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'update_ClientProfile'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('client_id'=>@$input['client_id'],'postcode'=>@$input['postcode'],'firstname'=>@$input['firstname'],'lastname'=>@$input['lastname'],'dob'=>@$input['dob'],'address'=>@$input['address'],'emergency_contact'=>@$input['emergency_contact']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'update_ClientProfile');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_update_ClientProfile($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Update Client Care program
	*
	*/
	public function update_ClientCareProgram()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'update_ClientCareProgram'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('client_id'=>@$input['client_id'],'program_id'=>@$input['program_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'update_ClientCareProgram');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_update_ClientCareProgram($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Manager Message Inbox
	*
	*/
	public function get_ManagerMessageInbox(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_ManagerMessageInbox'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4) ? $this->uri->segment(4) : 1;
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_ManagerMessageInbox($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Day wise count of schedule
	*
	*/
	public function get_DayWiseScheduleCount(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_DayWiseScheduleCount'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['date'] = $this->uri->segment(4); ## Format YYYY-MM
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_DayWiseScheduleCount($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Schedule of perticuer Day of Carer
	*
	*/
	public function get_DayScheduleOfCarer(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == '' || $this->uri->segment(5) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_DayScheduleOfCarer'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['carerid'] = $this->uri->segment(4); 
					$input['date'] = $this->uri->segment(5); 
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_DayScheduleOfCarer($input);
					json_output($response['status'],$response);
				}
			}
		}
	}

	/*
	*
	* Get Day wise schedule
	*
	*/
	public function get_DayWiseSchedule(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_DayWiseSchedule'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['date'] = $this->uri->segment(4); ## Format YYYY-MM-DD
					$input['page'] = $this->uri->segment(5) ? $this->uri->segment(5) : 1;
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_DayWiseSchedule($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Create a schedule
	*
	*/
	public function create_NewSchedule()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'create_NewSchedule'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('start_datetime'=>@$input['start_datetime'],'end_datetime'=>@$input['end_datetime'],'employeeid'=>@$input['employeeid'],'client_id'=>@$input['client_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'create_NewSchedule');
					}else{
						$input['secheduled_by'] = $auth['id'];
						$response = $this->AppBackend->ht_create_NewSchedule($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Update a schedule
	*
	*/
	public function update_Schedule()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'update_Schedule'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('schedule_id'=>@$input['schedule_id'],'start_datetime'=>@$input['start_datetime'],'end_datetime'=>@$input['end_datetime'],'employeeid'=>@$input['employeeid'],'client_id'=>@$input['client_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'update_Schedule');
					}else{
						$input['secheduled_by'] = $auth['id'];
						$response = $this->AppBackend->ht_update_Schedule($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	

	/*
	*
	* Delete schedule
	*
	*/
	public function delete_Schedule()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'delete_Schedule'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['schedule_id'] = $this->uri->segment(4); 
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_delete_Schedule($input);
					json_output($response['status'],$response);
				}
			}
		}
	}

	/*
	*
	* Get Users By user Type
	*
	*/
	public function get_UserByUserTypes(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_UserByUserTypes'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_type'] = $this->uri->segment(4); # user_type = 1 for Carer, 2 for Manager, 3 Supervisor, 4 Family, 0 for Outside user
					$input['page'] = $this->uri->segment(5) ? $this->uri->segment(5) : 1;
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_UserByUserTypes($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Add New out side user
	*
	*/
	## access_grants  : 0 means All profiles, employee id comma (,) separeted
	public function add_OutSideUser()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'add_OutSideUser'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('user_name'=>@$input['user_name'],'email'=>@$input['email'],'access_grants'=>@$input['access_grants']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'update_Schedule');
					}else{
						$input['added_by'] = $auth['id'];
						$response = $this->AppBackend->ht_add_OutSideUser($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Update outside user
	*
	*/
	public function update_OutSideUser()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'update_OutSideUser'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('outside_user_id'=>@$input['outside_user_id'],'user_name'=>@$input['user_name'],'email'=>@$input['email'],'access_grants'=>@$input['access_grants']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'update_OutSideUser');
					}else{
						$input['added_by'] = $auth['id'];
						$response = $this->AppBackend->ht_update_OutSideUser($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Delete schedule
	*
	*/
	public function delete_outSideUser()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'delete_outSideUser'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['outside_user_id'] = $this->uri->segment(4); 
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_delete_outSideUser($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/**
	 * 
	 * Get Supervisor Role and services
	 * 
	*/
	public function get_SupervisorRoleAndServices()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_SupervisorRoleAndServices'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['supervisorid'] = $this->uri->segment(4); 
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_SupervisorRoleAndServices($input);
					json_output($response['status'],$response);
				}
			}
		}
	}

	/**
	 * 
	 * Get List of carer those are not Supervised by any AND already added by selected Supervisor
	 * 
	*/
	public function get_listOfCarerToSupervise()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_listOfCarerToSupervise'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['supervisorid'] = $this->uri->segment(4);
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_listOfCarerToSupervise($input);
					json_output($response['status'],$response);
				}
			}
		}
	}

	/*
	*
	* Add/Update Role
	*
	*/
	## access : 1 for modification, 2 Scheduling, 3 Both
	public function addUpdate_Role()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'addUpdate_Role'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('employeeid'=>@$input['employeeid'],'staff_to_supervise'=>@$input['staff_to_supervise'],'access'=>@$input['access']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'addUpdate_Role');
					}else{
						$input['added_by'] = $auth['id'];
						$response = $this->AppBackend->ht_addUpdate_Role($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Delete Role
	*
	*/
	public function delete_Role()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'delete_Role'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['employeeid'] = $this->uri->segment(4); 
					$input['added_by'] = $auth['id'];
					$response = $this->AppBackend->ht_delete_Role($input);
	  				json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Start / Stop Role Service Access
	*
	*/
	public function startStopRoleServiceAccess()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'startStopRoleServiceAccess'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('employeeid'=>@$input['employeeid']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'startStopRoleServiceAccess');
					}else{
						$input['added_by'] = $auth['id'];
						$response = $this->AppBackend->ht_startStopRoleServiceAccess($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Already Fixed schedules date from Today Of Carer/employee
	*
	*/
	public function getAlreadyFixedSchedules()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'getAlreadyFixedSchedules'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['carerid'] = $this->uri->segment(4); 
					$input['managerid'] = $auth['id'];
					$response = $this->AppBackend->ht_getAlreadyFixedSchedules($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get List of Carers those schedules not fixed in periculer days
	*
	*/
	public function getListOfCarerAvailableForSchedule()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == '' ||  $this->uri->segment(5) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'getListOfCarerAvailableForSchedule'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['start'] = $this->uri->segment(4); 
					$input['end'] = $this->uri->segment(5); 
					$input['managerid'] = $auth['id'];
					$response = $this->AppBackend->ht_getListOfCarerAvailableForSchedule($input);
					json_output($response['status'],$response);
				}
			}
		}
	}

	/*
	*
	* Update Schedule Status 
	*
	*/
	public function updateScheduleStatus()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'updateScheduleStatus'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('schedule_id'=>@$input['schedule_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'updateScheduleStatus');
					}else{
						$input['employeeid'] = $auth['id'];
						$input['status'] = isset($input['status']) ? $input['status'] : 2; // 2 for just check condition
						$response = $this->AppBackend->ht_updateScheduleStatus($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	

	#################### Time Sheet section Common  ###################
	
	/*
	*
	* Get Timesheet records of employee
	*
	*/
	public function get_TimeSheetRecords()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == '' ||  $this->uri->segment(5) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_TimeSheetRecords'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['carerid'] = $this->uri->segment(4); 
					$input['page'] = $this->uri->segment(5); 
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_TimeSheetRecords($input);
					json_output($response['status'],$response);
				}
			}
		}
	}

	#################### Concern/Issue section Common  ###################
	/*
	*
	* Add new concerns 
	*
	*/
	public function add_NewConcern()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'add_NewConcern'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('subject'=>@$input['subject'],'concern'=>@$input['concern'],'concern_type'=>@$input['concern_type']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'add_NewConcern');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_add_NewConcern($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Update concerns 
	*
	*/
	public function update_Concern()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'update_Concern'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('subject'=>@$input['subject'],'concern'=>@$input['concern'],'concern_id'=>@$input['concern_id'],'concern_type'=>@$input['concern_type']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'update_Concern');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_update_Concern($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Concern reply or status update 
	*
	*/
	public function udate_statusOrReplyConcern()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'udate_statusOrReplyConcern'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('concern_id'=>@$input['concern_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'udate_statusOrReplyConcern');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_udate_statusOrReplyConcern($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}

	/*
	*
	* Get My all Concerns Carer
	*
	*/
	public function get_myAllConcerns()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_myAllConcerns'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4); 
					$input['carerid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_myAllConcerns($input);
					json_output($response['status'],$response);
				}
			}
		}
	}

	/*
	*
	* View Concern Detail with replies
	*
	*/
	public function viewConcernDetailWithReplies()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) == '' || $this->uri->segment(5) == ''){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'viewConcernDetailWithReplies'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4); 
					$input['concern_id'] = $this->uri->segment(5); 
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_viewConcernDetailWithReplies($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	################ Notification Manager / Supervisor ###################
	/*
	*
	* Get Supervisor Notificatons
	*
	*/
	public function get_SupervisorNotifications()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) =="" || $this->uri->segment(5) ==""){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_SupervisorNotifications'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4); 
					$input['filter'] = $this->uri->segment(5); ##  1.open concerns 2.resolved concerns 3. all concerns open and closed, 4.medication not given and 5 care records not done. 
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_SupervisorNotifications($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Manager Notificatons
	*
	*/
	public function get_ManagerNotifications()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) =="" || $this->uri->segment(5) =="" || $this->uri->segment(6) ==""){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_ManagerNotifications'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4); 
					$input['filter'] = $this->uri->segment(5); ##  1.open concerns 2.resolved concerns 3. all concerns open and closed, 4.medication not given and 5 care records not done.
					$input['section'] = $this->uri->segment(6); ## = 1 Own Notifiction, = 2 Supervisor Notification
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_ManagerNotifications($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* View Supervisor Notificatons by Manager
	*
	*/
	public function view_SupervisorNotifications()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) =="" || $this->uri->segment(5) =="" || $this->uri->segment(6) ==""){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'view_SupervisorNotifications'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4); 
					$input['filter'] = $this->uri->segment(5); ##  1.open concerns 2.resolved concerns 3. all concerns open and closed, 4.medication not given and 5 care records not done. 
					$input['supervisorid'] = $this->uri->segment(6);
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_view_SupervisorNotifications($input);
					json_output($response['status'],$response);
				}
			}
		}
	}



	######################### Activity History Carer ##########################
	/**
	*
	* Get Activity History of carer
	*
	*/
	public function get_activityHistoryOfCarer()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) ==""){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_activityHistoryOfCarer'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4); 
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_activityHistoryOfCarer($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	######################## Data Usage ##########################
	/**
	*
	* Get Data usage
	*
	*/
	public function get_dataUsagesByStaffMember()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_dataUsagesByStaffMember'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('page'=>@$input['page'],'staffids'=>@$input['staffids']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'get_dataUsagesByStaffMember');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_get_dataUsagesByStaffMember($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}

	/*
	*
	* Insert Data usage 
	*
	*/
	public function insert_dataUsage()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'insert_dataUsage'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('data'=>@$input['data']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'insert_dataUsage');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_insert_dataUsage($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	/**
	*
	* Get My Data usage
	*
	*/
	public function get_MyDataUsages()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) ==""){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_MyDataUsages'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4); 
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_MyDataUsages($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	/**
	*
	* Get company accesses 
	*
	*/
	public function get_company_accesses()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_MyDataUsages'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_company_accesses($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	
	/**
	*
	* get unread notification count
	*
	*/
	public function unread_notification_count()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'unread_notification_count'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_unread_notification_count($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	
		/**
	*
	* get unread message count
	*
	*/
	public function unread_message_count()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'unread_notification_count'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_unread_message_count($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	
	/*
	*
	* Update section access
	*
	*/
	public function update_section_access()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'update_section_access'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('employess'=>@$input['employess']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'update_OutSideUser');
					}else{
						$response = $this->AppBackend->ht_update_section_access($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	
	/**
	*
	* Get section accesses 
	*
	*/
	public function get_section_accesses()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_section_accesses'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_section_accesses($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	
	
	/***** ARCHIVE SECTION  ******/
	
	
	/*
	*
	* Archive User
	*
	*/
	public function archive_User()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'archive_User'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('userid'=>@$input['userid'],'company_id'=>@$input['company_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'archive_User');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_archive_User($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	
	/*
	*
	* Un Archive User
	*
	*/
	public function unarchive_User()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'unarchive_User'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('userid'=>@$input['userid'],'company_id'=>@$input['company_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'unarchive_User');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_unarchive_User($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	
	/*
	*
	* get supervisor accesses manager
	*
	*/
	public function get_supervisor_accesses()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(4) =="" ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_supervisor_accesses'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['employeeid'] = $auth['id'];
					$input['userid'] = $this->uri->segment(4);
					$response = $this->AppBackend->ht_get_supervisor_accesses($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	
	/*
	*
	* Get Carer Notificatons
	*
	*/
	public function get_CarerNotifications()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET" || $this->uri->segment(4) ==""){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'get_CarerNotifications'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['page'] = $this->uri->segment(4);
					$input['employeeid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_CarerNotifications($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	public function Set_CarerNotificationRead()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'Set_CarerNotificationRead'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $_POST;
					$check = check_required(array('carer_notification_id'=>@$input['carer_notification_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'Set_CarerNotificationRead');
					}else{
						$input['employeeid'] = $auth['id'];
						$response = $this->AppBackend->ht_Set_CarerNotificationRead($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	/*
	*
	* Test apple push
	*
	*/
	public function test_push()
	{
		applePush('524487E218414E6714CB2392EECFBFDA1918DBAA7DE0DCEE9D21F262F0F13402','hello',0,1,array('message'=>'heelloo'));
		echo "success";
	}
	

} ## End

