<!-- start: Content -->
<div id="content">
	<div class="panel box-shadow-none content-header">
		<div class="panel-body">
			<div class="col-md-12">
				<h3 class="animated fadeInLeft"> Update Company Information</h3>
			</div>
		</div>
	</div>
    <div class="form-element">
        <div class="col-md-12 padding-0">
            <div class="col-md-12">
                <div class="panel form-element-padding">
                    <div class="panel-body" style="padding-bottom:30px;">
                        <div class="col-md-10">
							<?php
								echo show_err_msg($error_msg);
							?>
							<form method="POST" enctype="Multipart/Form-data" action="">
								<input type="hidden" name="company_id" value="<?= $company_id; ?>" >
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Company Name</label>
									<div class="col-sm-9">
										<input type="text" name="company_name" class="form-control default"  value="<?= $company_name; ?>" placeholder="Company name" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Contact Person</label>
									<div class="col-sm-9">
										<input type="text" name="contact_person" value="<?= $contact_person; ?>"  class="form-control default" required placeholder="Contact person name">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Contact Number</label>
									<div class="col-sm-9">
										<select class="form-control default form" name="country_code" required>
											<option value="">-Select Country Code-</option>
								<?php	for($i=0;$i<count($country_codes);$i++){ ?>
											<option value="<?= $country_codes[$i]['country_code']; ?>" <?= $country_codes[$i]['country_code']==$country_code ? "selected" : ""; ?> ><?= $country_codes[$i]['countries_name']; ?>(<?= $country_codes[$i]['country_code']; ?>)</option>
								<?php	} ?>
										</select>
										
										<input type="text" name="mobile_number" value="<?= $mobile_number; ?>"  class="form-control default" placeholder="Mobile number" required>
									</div>
								</div>
								<div class="clearfix "></div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Email ID</label>
									<div class="col-sm-9">
										<input type="email" name="email" value="<?= $email; ?>"  class="form-control default" placeholder="Email id" required >
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Company Address</label>
									<div class="col-sm-9">
										<input type="text" name="address" value="<?= $address; ?>"  class="form-control default" placeholder="Address" required>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">City</label>
									<div class="col-sm-9">
										<input type="text" name="city" value="<?= $city; ?>"  class="form-control default" placeholder="City name" required>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">State</label>
									<div class="col-sm-9">
										<input type="text" name="state" value="<?= $state; ?>"  class="form-control default" placeholder="State name" required>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Country</label>
									<div class="col-sm-9">
										<select class="form-control default form" name="country" required>
											<option value="">-Select Country-</option>
								<?php	for($j=0;$j<count($country_codes);$j++){ ?>
											<option value="<?= $country_codes[$j]['countries_name']; ?>"  <?= $country_codes[$j]['countries_name']==$country ? "selected" : ""; ?>><?= $country_codes[$j]['countries_name']; ?></option>
								<?php	} ?>
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Zipcode</label>
									<div class="col-sm-9">
										<input type="text" name="zipcode" value="<?= $zipcode; ?>"  class="form-control default" placeholder="Zipcode" required>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Allocated Employee</label>
									<div class="col-sm-9">
										<input type="number" name="total_emp" value="<?= $total_emp; ?>" class="form-control default" placeholder="Allocate Employee" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Allocated Client</label>
									<div class="col-sm-9">
										<input type="number" name="total_client" value="<?= $total_client; ?>"  class="form-control default" placeholder="Allocate Client" required>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Company Logo</label>
									<div class="col-sm-9">
										<input type="file" name="logo" accept="image/*"  >
									</div>
								</div>
							<?php if($logo!=""){ ?>
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Uploaded Logo</label>
									<div class="col-sm-9">
										<image src="<?= $logo; ?>" height="40px">
									</div>
								</div>
							<?php } ?>
								<br />
								<hr />			
								<div class="col-md-4 col-md-offset-4">
									<input class="submit btn btn-primary" name='submit' type="submit" value="Update Data">&emsp;<button type="button" onclick="goBack();" class="btn btn-sm btn-warning">Cancel </button>
								</div>
							</form>
							
							
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
		<!---  For Agents -->
	<div class="form-element">
		<div class="col-md-12 padding-0">
			<div class="col-md-12">
                <div class="panel form-element-padding">
                    <div class="panel-body" style="padding-bottom:30px;">
                        <div class="col-md-10">
							<?php
								echo show_err_msg($error_msg);
							?>
							<h3> Company Agents</h3>
							<form method="POST" enctype="Multipart/Form-data" action="<?php echo base_url(); ?>">			
							<?php if(count($agent_lists) >0){  ?>
								<div class="responsive-table">
									<table id="datatables-manager" class="table table-striped table-bordered" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th>Employee</th>
												<th>Email</th>
												<th>Mother's Maiden Name</th>
												<th>Role</th>
												<th> Action</th>
											</tr>
										</thead>
										<tbody>

									<?php $i=0; ?>
									<?php foreach ($agent_lists as $agent) { ?>
										<tr id="man<?= $i; ?>">

											<td>

											<?= $agent['employee_picture'] ? '<img src="'.base_url().$agent['employee_picture'].'" height="50px" width="50px" onerror="this.src=\''.base_url().'asset/img/dummy.svg\'" >' : '<img src="'.base_url().'asset/img/dummy.svg" height="50px" width="50px">' ?>&emsp;<b><?= $agent['employee_name']; ?></b> (

											<?= $agent['employee_number']; ?>)</td>

											<td>
												<?= $agent['employee_email']; ?>
											</td>
											<td>
												<?= $agent['employee_mother_maiden']; ?>
											</td>

											<td>

											<?= $agent['addedBy_role_type']== 1 ? "<b>Agent</b>":"<b>Manager</b>"; ?></td>

											<td>

											<p><a href="<?= base_url('super/super_update_employee/'.$agent['employeeid'].'/'.$company_id); ?>" title="Edit"><i class="fa fa-edit"></i></a>&emsp;<a onclick="super_delete_employee(<?= $i; ?>,<?= $agent['employeeid'];?>,2);" class="confirm" title="<b>Are you sure to delete!</b>"><i class="fa fa-trash-o" title="Delete"></i></a>
											</td>

										</tr>

										<?php $i++; ?>
									<?php }   ?>
										</tbody>
									</table>
								</div>
								<!---<div class="appendNewagent"></div>
								<div class="col-md-12">
									<i class="add_field_button fa fa-plus-circle fa-2x" onclick="addMoreAgent();" ></i> Add more
								</div>-->
								<?php } else { ?>
								<!---<div class="col-md-12" style="background-color:#fbfbfb;">
									<p style="color:red;"> No Agents Found. Please add below</p>
									<div class="col-md-12">
										<div class="col-xs-12 form-group">
											<label class="col-sm-3 control-label text-right">Agent Name</label>
											<div class="col-sm-9">
												<input type="text" name="contact_person[]" value=""  class="form-control default" required placeholder="Agent name">
											</div>
										</div>
										<div class="col-xs-12 form-group">
											<label class="col-sm-3 control-label text-right">Agent email</label>
											<div class="col-sm-9">
												<input type="email" name="email[]" value=""  class="form-control default" placeholder="Agent Email id" required>
											</div>
										</div>
										<div class="col-xs-12 form-group">
											<label class="col-sm-3 control-label text-right">Agent Mother's Maiden Name</label>
											<div class="col-sm-9">
												<input type="text" name="mother_maiden[]" value=""  class="form-control default" required placeholder="Agent Mother's maiden name">
									
											</div>
											
										</div>
									</div>
									<div class="appendNewagent"></div>
									<div class="col-md-12">
										<i class="add_field_button fa fa-plus-circle fa-2x" onclick="addMoreAgent();" ></i> Add more
									</div>
									<div class="col-md-4 col-md-offset-4">
										<input class="submit btn btn-primary" name='submit' type="submit" value="Add Agent">&emsp;<button type="button" onclick="goBack();" class="btn btn-sm btn-warning">Cancel </button>
									</div>
								</div>--->
								<? } ?>		
							</form>
						</div>
					</div>
                </div>
            </div>
			<!---  End of Agents -->	
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>

<script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>

<script>
	function addMoreAgent(){
	var x = Math.round(Math.random()*1000) + 1;
    var wrapper         = jQuery(".appendNewagent"); //Fields wrapper
	jQuery(wrapper).append('<div class="col-md-12" style="padding-top: 1px;"><hr /><span style="font-size:25px; color:red" title="Click to remove medicine" class="icons icon-close remove_field"></span> <div class="form-group col-xs-12"><label class="col-sm-3 control-label text-right">Agent Name</label><div class="col-sm-9"><input type="text" name="contact_person[]" value="" class="form-control default" required placeholder="Agent name"></div></div><div class="form-group col-xs-12"><label class="col-sm-3 control-label text-right">Agent email</label><div class="col-sm-9"><input type="email" name="email[]" value="" class="form-control default" placeholder="Agent Email id" required></div></div><div class="form-group col-xs-12"><label class="col-sm-3 control-label text-right">Agent Mother\'s Maiden Name</label><div class="col-sm-9"><input type="text" name="mother_maiden[]" value="" class="form-control default" required placeholder="Agent Mother\'s maiden name"></div></div><br/></div>');
	jQuery(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		jQuery(this).parent('div').remove();
	})
}

$(document).ready(function() {
	$('#datatables-manager').DataTable({
		dom: '<"top"f>rt<"bottom"p><"clear">',
		bSort: false
	});
});
</script>

<!-- end: content -->