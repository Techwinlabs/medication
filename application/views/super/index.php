<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Log In</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="images/favicon.ico">

        <!-- App css -->
        <link href="<?= base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>asset/css/style.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>asset/css/custom.css" rel="stylesheet" type="text/css" /> 
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href="<?= base_url(); ?>asset/css/animate.css" rel="stylesheet">
    </head>

    <body class="bg-accpunt-page" style="background-color:#0f0f0f;">
        <!-- HOME -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 jello animated">
                        <div class="wrapper-page">                            
                            <div class="account-pages">
                                <div class="account-box m-t-50">                                   
                                    <div class="account-logo-box">                                        
                                        <h6 class="text-center font-size ">Super Admin</h6>
                                    </div>
                                    <div class="account-content">
										<?php
											echo show_err_msg($this->session->flashdata('error_msg'));
										?>
                                       <form method="post" action="<?php echo base_url('super/login'); ?>">
                                            <div class="form-group">  
												<input type="email" class="form-control user" id="email" placeholder="Email id" name="email" required>
                                            </div>
                                            <div class="form-group">                                             
                                              <input class="form-control user" id="pwd" placeholder="Password"  type="password" name="password" required>
                                            </div>
                                           <button type="submit" class="btn btn-default login" name="submit" >Login</button>
                                          </form>
                                        <div class="row m-t-50">
                                            <div class="col-sm-12 text-center">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end card-box-->
                        </div>
                        <!-- end wrapper -->
                    </div>
                </div>
            </div>
        </section>
        <!-- END HOME -->

        <!-- jQuery  -->
        <script src="<?= base_url(); ?>asset/js/jquery.min.js"></script>       
        <script src="<?= base_url(); ?>asset/js/bootstrap.min.js"></script>
       
    </body>
</html>