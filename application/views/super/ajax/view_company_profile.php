 <div class="modal fade" id="profile_company<?= $company_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <center>
                        <img src="<?= $logo; ?>" name="aboutme" width="140" height="140" border="0" class="img-circle"></a>
                         <h3 class="media-heading"><?= $company_name; ?><small> (<?= $status == 1 ? "Active" : "Inactive"; ?>)</small></h3>
                        <span class="label label-info"><?= $plan; ?></span>
                    </center>
					<br>
					<div class="col-md-12" >
						<div class="col-md-4"><b>Total Employees :</b><!--?= $used_emp; ?--><?= $total_emp; ?></div>
						<div class="col-md-4"></div>
						<div class="col-md-4"><b>Total Client :</b><!--?= $used_client; ?--><?= $total_client; ?></div>
					</div>
					<br>
					<br>
					<div class="col-md-12">
					 <div class="col-md-4"><b>Total Carer :</b> <?= $total_carer; ?></div>
					 <div class="col-md-4"><b>Total Supervisor :</b> <?= $total_supervisor; ?></div>
					 <div class="col-md-4"><b>Total Manager :</b> <?= $total_manager; ?></div>
					</div>
                    <hr>
                    <center>
							<p class="text-left"><strong>Address: </strong><br>
								<?= $address; ?>, <?= $city; ?>, <?= $state; ?>, <?= $country; ?>-<?= $zipcode; ?></p>
							<p class="text-left"><strong>Contact Person: </strong><br>
							   <?= $contact_person; ?></p>
							<p class="text-left"><strong>Contact Number: </strong><br>
							   +<?= $country_code; ?> <?= $mobile_number; ?></p>
							<p class="text-left"><strong>Email:</strong><br>
							   <?= $email; ?></p>
							<p class="text-left"><strong>Mother's Maiden Name:</strong><br>
							   <?= $employee_mother_maiden; ?></p>
							<hr />
							<p class="text-left"><strong>Trial Period History </strong><br></p>
							<?php if($billing_start_date!=""){
								   $dates = explode(',',$billing_start_date);
								   for($d=0;$d<count($dates);$d++){
										echo "<p class='text-left'>Trial Start Date: ".date("d/m/Y",strtotime($dates[$d]))."</p>";
								   }
							
							}else{
								   echo "<p class='text-left'>-NA-</p>";
							}?>
							<?php if($status==3){
								   echo '<p class="text-left"><strong>Service Start History </strong><br></p>';
								   $datess = explode(',',$created_at);
								   for($s=0;$s<count($datess);$s++){
										echo "<p class='text-left'>Service Start Date: ".date("d/m/Y",strtotime($datess[$s]))."</p>";
								   }
							
							}?>
							<?php if($status==2){
								   echo '<p class="text-left"><strong>Service Stop History </strong><br></p>';
								   $datee = explode(',',$updated_at);
								   for($e=0;$e<count($datee);$e++){
										echo "<p class='text-left'>Service Start Date: ".date("d/m/Y",strtotime($datee[$e]))."</p>";
								   }
							
							}?>
						
						
						
                    </center>
					
                </div>
                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </center>
                </div>
            </div>
        </div>
    </div>