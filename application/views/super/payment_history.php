
<!-- start: Content -->
<div id="content">
	<div class="col-md-12 top-20 padding-0">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="animated fadeInLeft">Payment History</h3>
				</div>
				<div class="panel-body">
					<?php
						echo show_err_msg($this->session->flashdata('error_msg'));
						echo show_succ_msg($this->session->flashdata('success_msg'));
					?>
					<div class="responsive-table">
                        <table id="datatables-history" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Company</th>
                                    <th>Transaction Id</th>
                                    <th>Amount</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=0; ?>
                            <?php	foreach($data as $row){ ?>
                                <tr>
                                    <td>
										<a href="" data-toggle="modal" data-target="#profile<?= $i; ?>" onclick="viewCompany_profile(<?= $row['company_id'];?>)"><?= $row['company_name']; ?></a>
									</td>
                                    <td>
                                        <?= $row['txn_id']; ?>
                                    </td>
									<td>$ <?= $row['paid_amount']; ?></td>
									<td><?= $row['description']; ?></td>
									<td><?= $row['payment_status']; ?></td>
                                 </tr>
                            <?php $i++; ?>
                            <?php	}	?>
                            </tbody>
                        </table>
                        <a href="new_company" class="float">
                            <i class="fa fa-plus my-float"></i>
                        </a>
                    </div>
               </div>
			</div>
		</div>
	</div>
	<div id="load_profile"></div>
</div>
<!-- end: content -->
<!-- plugins -->
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>

<!-- custom -->
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#datatables-history').DataTable();
});
</script>
</body>

</html>