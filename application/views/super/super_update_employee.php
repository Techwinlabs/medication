<link rel="stylesheet" type="text/css" href="<?= base_url('asset/css/bootstrap-select.css'); ?>">
<script src="<?= base_url('asset/js/bootstrap-select.js'); ?>"></script>
<style>
	.dropdown-display {
		white-space: nowrap;
		padding: 11px 20px 6px 12px !important;
	}
	.dropdown-chose-list{
		display: none;
	}
	.bootstrap-select{
		width : 97% ! important;
	}
	.bootstrap-select.form-animate-error{
		border: 1px solid red;
		height: 34px;
	}
</style>
	<!-- start: Content -->
	<div id="content">
		<div class="panel box-shadow-none content-header">
			<div class="panel-body">
				<div class="col-md-12">
					<h3 > Update Employee</h3>
				</div>
			</div>
		</div>
		<div class="form-element">

			<div class="col-md-10">
				<div class="col-md-12 panel">
					<div class="col-md-12 panel-body" style="padding-bottom:30px;">
						<div class="col-md-12">
							<?php
								echo show_err_msg($error_msg);
								echo show_succ_msg($this->session->flashdata('success_msg'));
								
							?>
							<form class="cmxform" id="new-employee" method="POST" action="" enctype="Multipart/Form-data">
								<input type="hidden" name="company_id" value="<?= $company_id; ?>">
								<div class="col-md-6">
								
									<div class="form-group form-animate-text" style="margin-top:40px !important;">
									<input type="text" class="form-text" id="employee_name" name="employee_name" value="<?php echo $employees['employee_name'] ?>" required>
									<span class="bar"></span>
									<label>Employee Name <span style="color: #D8000C;">*</span></label>
									</div>
									
									<div class="form-group form-animate-text" style="margin-top:40px !important;">
										<input type="text" class="form-text" id="employee_mother" name="employee_mother_maiden" value="<?php echo $employees['employee_mother_maiden'] ?>" required>
										<span class="bar"></span>
										<label>Mother's Maiden Name <span style="color: #D8000C;">*</span></label>
									</div>
									
									<div class="form-group form-animate-text" style="margin-top:40px !important;">
										<input type="text" class="form-text" id="employee_email" name="employee_email" value="<?php echo $employees['employee_email'] ?>" required>
										<span class="bar"></span>
										<label>Email <span style="color: #D8000C;">*</span></label>
									</div>
									
									
									<div class="form-group form-animate-text designation" style="margin-top:40px !important;">
										<input type="text" class="form-text" value="Manager" readonly>
										<input type="hidden" class="form-text" name="employee_designation" value="2">
									</div>
									
			
									
									<div class="form-group form-animate-text" style="margin-top:40px !important;">
										<label >Gender</label><br /><br />
										<input type="radio" required name="gender" value="1" <?php if($employees['gender']==1){ echo 'checked'; } ?>> Male &emsp;
										<input type="radio" required name="gender" value="2" <?php if($employees['gender']==2){ echo 'checked'; } ?>> Female
									</div>
									
									<div class="form-group form-animate-text" style="margin-top:40px !important;">
										<label >Is Agent</label><br /><br />
										<input type="radio" required name="addedBy_role_type" value="1" <?php if($employees['addedBy_role_type']==1){ echo 'checked'; } ?>> Yes &emsp;
										<input type="radio" required name="addedBy_role_type" value="2" <?php if($employees['addedBy_role_type']==2){ echo 'checked'; } ?>> No
									</div>
									
								</div>
								<div class="col-md-12">								
								<input class="submit btn btn-primary" name='submit' type="submit" value="Submit">&emsp;<button type="button" class="btn btn-sm btn-danger" onclick="window.history.back();">Cancel</button>
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
	<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
	<script src="<?= base_url(); ?>asset/js/plugins/jquery.validate.min.js"></script>
	<script src="<?= base_url(); ?>asset/js/main.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		$(".selectpicker1").selectpicker({
			noneSelectedText : '--- Access Rights ---'
		});

		$("#new-employee").validate({
			errorElement: "em",
			errorPlacement: function(error, element) {
			$(element.parent("div").addClass("form-animate-error"));
				error.appendTo(element.parent("div"));
			},
			success: function(label) {
				$(label.parent("div").removeClass("form-animate-error"));
			},
			rules: {
				employee_name: "required",
				employee_number: "required",
				employee_mobile: "required",
				employee_country_code: "required",
				validate_username: {
					required: true,
				},
				employee_email: {
					required: true,
					email: true
				}
			},
			messages: {
				employee_name: "Please enter employee name",
				employee_number: "Please enter employee number",
				employee_mobile: "Please enter a mobile number",
				employee_country_code: "Please select country code",
				validate_username: {
					required: "Please enter a username",
				},
				employee_email: "Please enter a valid email address",
				
			}
		});
	});
	
	function get_roles(args) {
        if (args == 3) {
            $('.section_roles').css('display','block');
        }else{
			$('.section_roles').css('display','none');
		}
    }
	
	function clear_country_border(val) {
		if (val != '') {
            $('div.country div.bootstrap-select.form-animate-error').css('border','none');
        }        
    }
	
	function clear_designation_border(val) {
		if (val != '') {
			$('div.designation div.bootstrap-select.form-animate-error').css('border','none');
		}
    }
	
	function clear_access_border(val) {
		if (val != '') {
			$('div.access div.bootstrap-select.form-animate-error').css('border','none');
		}
    }
	</script>
	<!-- end: content -->