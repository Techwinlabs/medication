
<!-- start: Content -->
<div id="content">
	<div class="col-md-12 top-20 padding-0">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="animated fadeInLeft">Manage Companies</h3>
				</div>
				<div class="panel-body">
					<?php
						echo show_err_msg($this->session->flashdata('error_msg'));
						echo show_succ_msg($this->session->flashdata('success_msg'));
					?>
					<div class="responsive-table">
                        <table id="datatables-clients" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th width="20">Company</th>
									<th width="5">Logged In</th>
                                    <th width="10">Trial Period</th>
                                    <th width="10">Employee / Allocated</th>
                                    <th width="10">Client / Allocated</th>
                                    <th width="10">Restart</th>
                                  	<th width="10">Begin Service</th>
									<th width="10">End Service</th>
									<th width="5">Status</th>
                                    <th width="10"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=0; ?>
                            <?php	foreach($data as $row){ ?>
                                <tr>
                                    <td>
										<?= $row['logo'] ? '<img src="'.$row['logo'].'" height="50px" width="50px" onerror="this.src=\''.base_url().'media/company/logo/index.png\'" >' : '<img src="'.base_url().'media/company/logo/index.png" height="50px" width="50px">' ?><br><b><?= $row['company_name']; ?></b>
									</td>
									<td>
										<?= $row['loggedin'].' users'; ?>
									</td>
                                    <td><?= $row['plan']; ?></td>
                                    <td><?= $row['used_emp']; ?>/<?= $row['total_emp']; ?></td>
									<td><?= $row['used_client']; ?>/<?= $row['total_client']; ?></td>
									<td><a  <?php if($row['status']==1){ ?> disabled class="btn btn-info btn-sm" <?php } else{ ?> href="Companies/restart_trial/<?= $row['company_id'] ?>"  title="<b>Sure to strart free trail!</b>" class="confirm btn btn-info btn-sm"  <?php } ?>>Restart</a></td>
									<td><a <?php if($row['status']==3){ ?> disabled class="btn btn-info btn-sm" <?php } else{ ?>  href="Companies/begin_service/<?= $row['company_id'] ?>" title="<b>Sure to begin service!</b>" class="confirm btn btn-info btn-sm" <?php } ?>>Begin Service</a></td>
									<td><a <?php if($row['status']==2){ ?> disabled class="btn btn-info btn-sm" <?php } else{ ?> href="Companies/end_service/<?= $row['company_id'] ?>" title="<b>Sure to stop service!</b>" class="confirm btn btn-info btn-sm"<?php } ?> >End Service</a></td>
									<td><?= $row['status-msg']; ?></td>
                                    <td>
                                    <p><a href="<?= base_url(); ?>super/update_company/<?= $row['company_id'] ?>"><i class="fa fa-edit"></i></a>&emsp;<a href="" data-toggle="modal" data-target="#profile<?= $i; ?>" onclick="viewCompany_profile(<?= $row['company_id'];?>)"><i class="fa fa-eye" style="font-size:15px"></i></a>
                                    </td>
                                </tr>
                            <?php $i++; ?>
                            <?php	}	?>
                            </tbody>
                        </table>
                        <a href="new_company" class="float">
                            <i class="fa fa-plus my-float"></i>
                        </a>
                    </div>
               </div>
			</div>
		</div>
	</div>
	<div id="load_profile"></div>
</div>
<!-- end: content -->
<!-- plugins -->
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>

<!-- custom -->
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#datatables-clients').DataTable();
});
</script>
</body>

</html>