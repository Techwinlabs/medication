<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<meta name="description" content="Medication App Super Admin">
	<meta name="author" content="Hem Thakur">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="refresh" content = "180; url=https://dailycarerecords.com/super/Auth/logout/Yes">
	<title>Medication App Super Admin</title>

	<!-- start: Css -->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/bootstrap.min.css">

	<!-- plugins -->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/datatables.bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/simple-line-icons.css"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/animate.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/fullcalendar.min.css"/>
	<link href="<?= base_url(); ?>asset/css/style.css" rel="stylesheet">
	<link href="<?= base_url(); ?>asset/css/hems.css" rel="stylesheet">
	<link href="<?= base_url(); ?>asset/css/animate.css" rel="stylesheet">
  <link href="<?= base_url(); ?>asset/css/bootstrap-select.min.css" rel="stylesheet">
	<!-- end: Css -->
	
	<!-- Main JS !>
	<!-- start: Javascript -->
	<script src="<?= base_url(); ?>asset/js/jquery.min.js"></script>
	<script src="<?= base_url(); ?>asset/js/jquery.ui.min.js"></script>
	<script src="<?= base_url(); ?>asset/js/bootstrap.min.js"></script>
  <script src="<?= base_url(); ?>asset/js/bootstrap-select.min.js"></script>
  <script src="<?= base_url(); ?>asset/js/jquery.popconfirm.js"></script>
		
	<link rel="shortcut icon" href="<?= base_url(); ?>asset/img/logomi.png">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

 <body id="mimin" class="dashboard">
      <!-- start: Header -->
        <nav class="navbar navbar-default header navbar-fixed-top">
          <div class="col-md-12 nav-wrapper">
            <div class="navbar-header" style="width:100%;">
              <div class="opener-left-menu is-open">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
              </div>
                <a href="index.html" class="navbar-brand"> 
                 <b>Medication App Super Admin</b>
                </a>

              <ul class="nav navbar-nav navbar-right user-nav">
                <li class="user-name"><span></span></li>
                  <li class="dropdown avatar-dropdown">
                   <img src="<?= base_url(); ?>asset/img/dummy.svg" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                   <ul class="dropdown-menu user-dropdown">
                     <li><a href="#"><span class="fa fa-user"></span> My Profile</a></li>
                     <li class="more">
                      <ul>
                        
                        <li><a href="Auth/logout"><span class="fa fa-power-off "></span> Log out</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li ><a class="opener-right-menu1"></a></li>
              </ul>
            </div>
          </div>
        </nav>
		<!-- end: Header -->

		<div class="container-fluid mimin-wrapper">
		<!-- start:Left Menu -->
            <div id="left-menu">
              <div class="sub-left-menu scroll">
                <ul class="nav nav-list">
                    <li class="time">
                      <h1 class="animated fadeInLeft">21:00</h1>
                      <p class="animated fadeInRight">Sat,October 1st 2029</p>
                    </li>
                   
					<!--li class="ripple">
                      <a class="nav-header" href="<--?= base_url(); ?>super">
                        <span class="icons fa-home fa"></span> Dashboard
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                    </li-->
                    <li class="ripple">
                      <a class="nav-header" href="<?= base_url(); ?>super/companies">
                        <span class="icons icon-user"></span> Manage Companies
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                    </li>
					 <!--li class="ripple">
                      <a class="nav-header" href="<--?= base_url(); ?>super/payment_history">
                        <span class="icons icon-user"></span> Payment History
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                    </li-->
                    <li class="ripple">
                      <a class="nav-header" href="<?= base_url(); ?>super/settings">
                        <span class="icons icon-settings"></span> Settings
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                    </li>
                  </ul>
                </div>
            </div>
          <!-- end: Left Menu -->
      <div id="loading_div" style="display:none;">
        <img id="loading-image" src="<?= base_url(); ?>asset/media/loading.gif" alt="Loading..." />
      </div>
