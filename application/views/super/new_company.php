<style>
	span.icons.icon-close.remove_field {
    position: absolute;
    margin-top: 15px;
}
</style>
<!-- start: Content -->
<div id="content">
	<div class="panel box-shadow-none content-header">
		<div class="panel-body">
			<div class="col-md-12">
				<h3 class="animated fadeInLeft"> New Company</h3>
			</div>
		</div>
	</div>
    <div class="form-element">
        <div class="col-md-12 padding-0">
            <div class="col-md-12">
                <div class="panel form-element-padding">
                    <div class="panel-body" style="padding-bottom:30px;">
                        <div class="col-md-10">
							<?php
								echo show_err_msg($error_msg);
							?>
							<form method="POST" enctype="Multipart/Form-data" >
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Company Name</label>
									<div class="col-sm-9">
										<input type="text" name="company_name" class="form-control default"  value="<?php echo set_value('company_name'); ?>" placeholder="Company name" required>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Company Address</label>
									<div class="col-sm-9">
										<input type="text" name="address" value="<?php echo set_value('address'); ?>"  class="form-control default" placeholder="Address" required>
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Contact Number</label>
									<div class="col-sm-9">
										<select class="form-control default form" name="country_code" required>
											<option value="">-Select Country Code-</option>
								<?php	for($i=0;$i<count($country_code);$i++){ ?>
											<option value="<?= $country_code[$i]['country_code']; ?>"><?= $country_code[$i]['countries_name']; ?>(<?= $country_code[$i]['country_code']; ?>)</option>
								<?php	} ?>
										</select>
										
										<input type="text" name="mobile_number" value="<?php echo set_value('mobile_number'); ?>"  class="form-control default" placeholder="Mobile number" required>
									</div>
								</div>
								<div class="clearfix "></div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">City</label>
									<div class="col-sm-9">
										<input type="text" name="city" value="<?php echo set_value('city'); ?>"  class="form-control default" placeholder="City name" required>
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">State</label>
									<div class="col-sm-9">
										<input type="text" name="state" value="<?php echo set_value('state'); ?>"  class="form-control default" placeholder="State name" required>
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Country</label>
									<div class="col-sm-9">
										<select class="form-control default form" name="country" required>
											<option value="">-Select Country-</option>
								<?php	for($j=0;$j<count($country_code);$j++){ ?>
											<option value="<?= $country_code[$j]['countries_name']; ?>"><?= $country_code[$j]['countries_name']; ?></option>
								<?php	} ?>
										</select>
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Zipcode</label>
									<div class="col-sm-9">
										<input type="text" name="zipcode" value="<?php echo set_value('zipcode'); ?>"  class="form-control default" placeholder="Zipcode" required>
									</div>
								</div>

								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Allocate Employee</label>
									<div class="col-sm-9">
										<input type="number" name="total_emp" value="4" class="form-control default" placeholder="Allocate Employee" required>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Allocate Client</label>
									<div class="col-sm-9">
										<input type="number" name="total_client" value="4"  class="form-control default" placeholder="Allocate Client" required>
									</div>
								</div>

								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Company Logo</label>
									<div class="col-sm-9">
										<input type="file" name="logo" accept="image/*" required >
									</div>
								</div>
								<div class="col-md-12" style="background-color:#fbfbfb;">
									
									<div class="col-md-12">
										<div class="col-xs-12 form-group">
											<label class="col-sm-3 control-label text-right">Agent Name</label>
											<div class="col-sm-9">
												<input type="text" name="contact_person[]" value=""  class="form-control default" required placeholder="Agent name">
											</div>
										</div>
										<div class="col-xs-12 form-group">
											<label class="col-sm-3 control-label text-right">Agent email</label>
											<div class="col-sm-9">
												<input type="email" name="email[]" value=""  class="form-control default" placeholder="Agent Email id" required>
											</div>
										</div>
										<div class="col-xs-12 form-group">
											<label class="col-sm-3 control-label text-right">Agent Mother's Maiden Name</label>
											<div class="col-sm-9">
												<input type="text" name="mother_maiden[]" value=""  class="form-control default" required placeholder="Agent Mother's maiden name">
											</div>
										</div>
									</div>
									<div class="appendNewagent"></div>
									<div class="col-md-12">
										<i class="add_field_button fa fa-plus-circle fa-2x" onclick="addMoreAgent();" ></i> Add more
									</div>
								</div>
								
								<div class="col-md-6 col-md-offset-6">
									<input class="submit btn btn-primary" name='submit' type="submit" value="Submit Data">&emsp;<button type="reset" class="btn btn-sm btn-warning">Reset Data</button>&emsp;<button type="button" onclick="goBack();" class="btn btn-sm btn-warning">Cancel </button>
								</div>
							</form>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>
<script>
	function addMoreAgent(){
	var x = Math.round(Math.random()*1000) + 1;
    var wrapper         = jQuery(".appendNewagent"); //Fields wrapper
	jQuery(wrapper).append('<div class="col-md-12" style="padding-top: 2px;"><hr /><span style="font-size:25px; color:red" title="Click to remove medicine" class="icons icon-close remove_field"></span> <div class="form-group col-xs-12"><label class="col-sm-3 control-label text-right">Agent Name</label><div class="col-sm-9"><input type="text" name="contact_person[]" value="" class="form-control default" required placeholder="Agent name"></div></div><div class="form-group col-xs-12"><label class="col-sm-3 control-label text-right">Agent email</label><div class="col-sm-9"><input type="email" name="email[]" value="" class="form-control default" placeholder="Agent Email id" required></div></div><div class="form-group col-xs-12"><label class="col-sm-3 control-label text-right">Agent Mother\'s Maiden Name</label><div class="col-sm-9"><input type="text" name="mother_maiden[]" value="" class="form-control default" required placeholder="Agent Mother\'s maiden name"></div></div><br/></div>');
	jQuery(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		jQuery(this).parent('div').remove();
	})
}
</script>

<!-- end: content -->