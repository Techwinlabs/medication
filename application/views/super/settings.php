<style>
	span.icons.icon-close.remove_field {
    position: absolute;
    margin-top: 15px;
}
</style>
<!-- start: Content -->
<div id="content">
	<div class="panel box-shadow-none content-header">
		<div class="panel-body">
			<div class="col-md-12">
				<h3 class="animated fadeInLeft">Update Password</h3>
			</div>
		</div>
	</div>
    <div class="form-element">
        <div class="col-md-12 padding-0">
            <div class="col-md-12">
                <div class="panel form-element-padding">
                    <div class="panel-body" style="padding-bottom:30px;">
                        <div class="col-md-10">
							<?php
								echo show_err_msg($error_msg);
								echo show_succ_msg($succ_msg);
							?>
							<form class="form-horizontal" method="post"  action="change_password">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="textinput">Old Password</label>
                                        <div class="col-md-8">
                                            <input id="password" name="old" type="password" onkeyup="ckeckOldPass();" placeholder="Old Password" autocomplete="false" class="form-control input-md" required="">
                                            <span id="epass" style="color:red;display:none;">Old password is incorret</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="textinput">New Password</label>
                                        <div class="col-md-8">
                                            <input id="newpassword" name="new" type="password" placeholder="New Password" autocomplete='false' class="form-control input-md" required="">
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label class="col-md-3 control-label" for="textinput">Confirm Password</label>
                                        <div class="col-md-8">
                                            <input id="confirmpassword" name="confirm" type="password" placeholder="Confirm Password" oninput="check(this);" autocomplete="false" class="form-control input-md" required="">
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label class="col-md-3 control-label" for="textinput">Email</label>
                                        <div class="col-md-8">
											<?php
												if(isset($_POST['email'])){
													$email=$_POST['email'];
												}else{
													$email=$user_email;
												}
											?>
                                            <input id="email" name="email" value="<?php echo $email ?>" type="email" placeholder="Email" autocomplete='false' class="form-control input-md" required="">
                                        </div>
                                    </div>
									
									<script language='javascript' type='text/javascript'>
                                        function check(input) {
                                            if (input.value != document.getElementById('newpassword').value) {
                                                input.setCustomValidity('Confirm password not matching.');
                                            } else {
                                            	input.setCustomValidity('');
                                            }
                                        }
                                    </script>
                                    <div class="form-group"><br />
                                        <label class="col-md-4 control-label" for="btnsave">Change Password</label>
                                        <div class="col-md-8">
                                            <button id="btnsave1" name="chnage" class="submit btn btn-primary yes" type="submit" value="submit">Change Password</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>
<script>
	function addMoreAgent(){
	var x = Math.round(Math.random()*1000) + 1;
    var wrapper         = jQuery(".appendNewagent"); //Fields wrapper
	jQuery(wrapper).append('<div class="col-md-12" style="padding-top: 2px;"><hr /><span style="font-size:25px; color:red" title="Click to remove medicine" class="icons icon-close remove_field"></span> <div class="form-group col-xs-12"><label class="col-sm-3 control-label text-right">Agent Name</label><div class="col-sm-9"><input type="text" name="contact_person[]" value="" class="form-control default" required placeholder="Agent name"></div></div><div class="form-group col-xs-12"><label class="col-sm-3 control-label text-right">Agent email</label><div class="col-sm-9"><input type="email" name="email[]" value="" class="form-control default" placeholder="Agent Email id" required></div></div><div class="form-group col-xs-12"><label class="col-sm-3 control-label text-right">Agent Mother\'s Maiden Name</label><div class="col-sm-9"><input type="text" name="mother_maiden[]" value="" class="form-control default" required placeholder="Agent Mother\'s maiden name"></div></div><br/></div>');
	jQuery(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		jQuery(this).parent('div').remove();
	})
}
</script>

<!-- end: content -->