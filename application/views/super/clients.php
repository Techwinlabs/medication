
<!-- start: Content -->
<div id="content">
	<div class="col-md-12 top-20 padding-0">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="animated fadeInLeft">Manage Employees</h3>
				</div>
				<div class="panel-body">
					<?php
						echo show_err_msg($this->session->flashdata('error_msg'));
						echo show_succ_msg($this->session->flashdata('success_msg'));
					?>
					<div class="responsive-table">
                        <table id="datatables-clients" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Role</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=0; ?>
                            <?php	foreach($data as $row){ ?>
                                <tr>
                                    <td>
                                    <?= $row['employee_picture'] ? '<img src="'.base_url().$row['employee_picture'].'" height="50px" width="50px" onerror="this.src=\''.base_url().'asset/img/dummy.svg\'" >' : '<img src="'.base_url().'asset/img/dummy.svg" height="50px" width="50px">' ?>&emsp;<b><?= $row['employee_name']; ?></b> (
                                    <?= $row['employee_number']; ?>)</td>
                                    <td>
                                        <?= $row['employee_email']; ?>
                                    </td>
                                    <td>+
                                    <?= $row['employee_country_code']; ?>
                                    <?= $row['employee_mobile']; ?>
                                    </td>
                                    <td>
                                    <?= $row['employee_type']== 1 ? "<b>Employee</b>":"<b>Manager</b>"; ?></td>
                                    <td>
                                    <p><a href=""><i class="fa fa-edit"></i></a>&emsp;<a href="" data-toggle="modal" data-target="#profile<?= $i; ?>" onclick="viewProfile_emp('carer<?= $i; ?>',<?= $row['employeeid'];?>,1)"><i class="fa fa-eye" style="font-size:15px"></i></a>&emsp;<a href="" class="confirm" title="<b>Are you sure to delete!</b>"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php $i++; ?>
                            <?php	}	?>
                            </tbody>
                        </table>
                        <a href="new_employee" class="float">
                            <i class="fa fa-plus my-float"></i>
                        </a>
                    </div>
               </div>
			</div>
		</div>
	</div>
	<div id="load_profile"></div>
</div>
<!-- end: content -->
<!-- plugins -->
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>

<!-- custom -->
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#datatables-clients').DataTable();
});
</script>
</body>

</html>