<style>
.status .panel-title {
    font-family: 'Oswald', sans-serif;
    font-size: 40px;
    font-weight: bold;
    color: #fff;
    line-height: 50px;
    padding-top: 8px;
    letter-spacing: -0.8px;
  
}
.panel.status {
	border: 1px solid #d8d0d0;
	border-radius: 5px;
}
</style>

<div id="content">
	<div class="col-md-12 top-20 padding-0">
		<div class="col-md-12">
			<div class="panel">
				<!-- start: Content -->
				<div class="panel-heading">
					<h3 class="animated fadeInLeft">Dashboard</h3>
				</div>
				<div class="panel-body">
					<div class="col-md-12">
						<div class="col-xs-6 col-md-3 company">
							<div class="panel status panel-success">
								<div class="panel-heading">
									<h1 class="panel-title text-center">25</h1>
								</div>
								<div class="panel-body text-center">                        
									<strong>Total Companies</strong>
								</div>
							</div>
						</div>          
						<div class="col-xs-6 col-md-3 user">
							<div class="panel status panel-info">
								<div class="panel-heading">
									<h1 class="panel-title text-center">2000</h1>
								</div>
								<div class="panel-body text-center">                        
									<strong>Total Users</strong>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-md-3 client">
							<div class="panel status panel-success">
								<div class="panel-heading">
									<h1 class="panel-title text-center">1500</h1>
								</div>
								<div class="panel-body text-center">                        
									<strong>Total Carer</strong>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-md-3 schedules">
							<div class="panel status panel-info">
								<div class="panel-heading">
									<h1 class="panel-title text-center">1000</h1>
								</div>
								<div class="panel-body text-center">                        
									<strong>Total Patients</strong>
								</div>
							</div>
						</div>
			
						<div class="row-fluid">
							<div class="widget-box">
								<div class="widget-title"><span class="icon"><i class="icon-tasks"></i></span>
								</div>
								<div class="widget-content">
									<div class="row-fluid">
										<div class="span12">
											<div id="new_comp_reg" ></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					<div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://highcharts.github.io/export-csv/export-csv.js"></script>

<script>
	Highcharts.chart('new_comp_reg', {
		chart: {
			type: 'spline'
		},
		title: {
			text: 'Monthly Users Registration'
		},
		 credits: {
		  enabled: false
		},
		xAxis: {
			categories: [
		<?php	if(count($data)>0){ 
					for($m=0;$m<count($data);$m++){
						echo "'".date("M-Y",strtotime($data[$m]['date']))."',";
					}
				}
		?>	
			
			]
		},
		yAxis: {
			title: {
				text: 'Monthly Users Registrations'
			}
		},
		plotOptions: {
			spline: {
				dataLabels: {
					enabled: false
				},
				enableMouseTracking: true
			}
		},
		series: [{
			name: 'Monthly registration',
			data: [ <?php	if(count($data)>0){ for($d=0;$d<count($data);$d++){ echo (int)$data[$d]['total'].","; } }else{ echo "" ; } ?>	]
		}]
	});

	</script>
