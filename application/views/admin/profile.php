<style>

    .container.details.del {

        margin-top: 10px;

    }

    

    .pro-img img {

        height: 200px;

        border-radius: 112px;

        width: 200px;

        object-fit: cover;

    }

    

    .jumbotron {

        background: none;

        padding-bottom: 0px;

    }

    

    .form-control.up-pr {

        background-color: #1ec8c8;

        color: #fff;

        border: none;

        margin-bottom: 10px;

    }

    

    .form-control.chng-pas {

        background-color: #1ec8c8;

        color: #fff;

        border: none;

    }

    

    .btn.btn-success.yes {

        padding: 8px 40px 8px 40px;

    }

    

    .btn.btn-danger.no {

        padding: 8px 40px 8px 40px;

    }

    

    .modal-dialog.md-1 {

        margin-top: 12%;

    }

    

    .uplod-img img {

        height: 65px;

    }

    

    @media(max-width:375px) {

        .pro-img img {

            height: 130px;

            border-radius: 112px;

            width: 130px;

            object-fit: cover;

        }

    }

    

    @media(max-width:320px) {

        .jumbotron p {

            margin-bottom: 15px;

            font-size: 15px;

            font-weight: 200;

        }

    }

</style>



<div id="content">

    <div class="col-md-12 top-20 padding-0">

        <div class="col-md-12">

            <div class="panel">

                <div class="panel-heading">

                    <h3 class="">Profile</h3>

                    <a href="new_client" class="pull-right new"></a>

                </div>

                <div class="panel-body">

                    <div class="">

                        <div class="jumbotron">

                            <div class="row">

									<?php

										  echo show_err_msg($this->session->flashdata('error_msg'));

										  echo show_succ_msg($this->session->flashdata('success_msg'));

									?>

                                    <div class="col-md-3 col-xs-12 col-sm-6 col-lg-3">

                                        <div class="pro-img">

                                            <?=  $this->session->userdata()['userdata']['employee_picture'] ? '<img src="'. $this->session->userdata()['userdata']['employee_picture'].'"  class="img" >' : '<img src="'.base_url().'asset/img/dummy.svg" class="img">' ?>

                                        </div>

                                    </div>

                                    <div class="col-md-8 col-xs-12 col-sm-6 col-lg-8">

                                        <div class="container" style="border-bottom:1px solid #d2d2d2">

                                            <h3><?= $this->session->userdata()['userdata']['employee_name']; ?></h3>



                                        </div>



                                        <ul class="container details del">

                                            <li>

                                                <p><span class="glyphicon glyphicon-envelope one" style="width:50px;"></span>

                                                    <?= $this->session->userdata()['userdata']['employee_email']; ?>

                                                </p>

                                            </li>

                                            <li>

                                                <p>

													  <span class="glyphicon glyphicon-earphone one" style="width:50px;"></span>

													  <?= '+'.$this->session->userdata()['userdata']['employee_country_code']; ?>

													  <?= $this->session->userdata()['userdata']['employee_mobile']; ?>

                                                </p>

                                            </li>

                                            <!--li><p><span class="glyphicon glyphicon-map-marker one" style="width:50px;"></span>London, UK</p></li-->



                                        </ul>

                                        <div class="btn-row-cl">

                                            <div class="col-md-4">

                                                <a href="#">

                                                    <button class="form-control up-pr" data-toggle="modal" data-target="#myModal">Update Profile</button>

                                                </a>

                                            </div>

                                            <div class="col-md-4">

                                                <a href="#">

                                                    <button class="form-control chng-pas" data-toggle="modal" data-target="#myModal2">Change Password</button>

                                                </a>

                                            </div>

                                            <!-- Modal -->

                                            <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false" >

                                                <div class="modal-dialog md-1">



                                                    <!-- Modal content-->

                                                    <div class="modal-content">

                                                        <div class="modal-header">

                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                                                            <h4 class="modal-title">Update Profile</h4>

                                                        </div>

                                                        <div class="modal-body">

                                                            <form class="form-horizontal" method="post"  enctype="Multipart/Form-data" action="Profile/updateAdminProfile">

                                                                <input type="hidden" id="eid" name="employeeid" value="<?= $this->session->userdata()['userdata']['employeeid']; ?>">

                                                                <fieldset>

                                                                    <!-- Text input-->

                                                                    <div class="form-group">

                                                                        <label class="col-md-3 control-label" for="textinput">User Name</label>

                                                                        <div class="col-md-8">

                                                                            <input id="ename" name="employee_name" type="text" placeholder="User name" class="form-control input-md" value="<?= $this->session->userdata()['userdata']['employee_name']; ?>" required="">

																		</div>

                                                                    </div>

                                                                    <!-- Text input-->

                                                                    <div class="form-group">

                                                                        <label class="col-md-3 control-label" for="textinput">Email</label>

                                                                        <div class="col-md-8">

                                                                            <input id="eemail" name="employee_email" type="text" placeholder="Email address" class="form-control input-md" value="<?= $this->session->userdata()['userdata']['employee_email']; ?>">

                                                                        </div>

                                                                    </div>

                                                                    <!-- Text input-->

                                                                    <div class="form-group">

                                                                        <label class="col-md-3 control-label" for="textinput">Country Code</label>

                                                                        <div class="col-md-8">

                                                                            <select class="form-control" id="employee_country_code" name="employee_country_code" required>

                                                                                <option value="">Select Country Code</option>

                                                                                <?php	for($i=0;$i<count($country_code);$i++){ ?>

                                                                                    <option value="<?= $country_code[$i]['country_code']; ?>" <?=$this->session->userdata()['userdata']['employee_country_code'] == $country_code[$i]['country_code'] ? "selected" : ""; ?> >

                                                                                        <?= $country_code[$i]['countries_name']." (+".$country_code[$i]['country_code'].")"; ?>

                                                                                    </option>

                                                                                    <?php	}	?>

                                                                            </select>

                                                                        </div>

                                                                    </div>

                                                                    <!-- Text input-->

                                                                    <div class="form-group">

                                                                        <label class="col-md-3 control-label" for="textinput">Phone Number</label>

                                                                        <div class="col-md-8">

                                                                            <input id="ephone" name="employee_mobile" type="tel" pattern="^\d{10}$" title="Enter a valid 10 digit mobile numer. eg. 9874561230" placeholder="Phone Number" value="<?= $this->session->userdata()['userdata']['employee_mobile']; ?>" class="form-control input-md" required>

                                                                        </div>

                                                                    </div>

																	<div class="form-group">

                                                                        <label class="col-md-3 control-label" for="textinput">Employee Number</label>

                                                                        <div class="col-md-8">

                                                                            <input id="emp_no" name="employee_number" type="text" title="Enter your Employee Number" placeholder="Employee Number" value="<?= $this->session->userdata()['userdata']['employee_number']; ?>" class="form-control input-md" required>

                                                                        </div>

                                                                    </div>

                                                                    <!-- Text input-->

                                                                    <!--div class="form-group">

																		<label class="col-md-3 control-label" for="textinput">Address</label>  

																		<div class="col-md-8">

																		<input id="textinput" name="textinput" type="text" placeholder="Address" class="form-control input-md">

																		</div>

																	  </div-->

                                                                    <!-- Text input-->

                                                                    <div class="form-group">

                                                                        <label class="col-md-3 control-label" for="textinput">Image Upload</label>

                                                                        <div class="col-md-8">

                                                                            <div class="row">

                                                                                <input type='file' accept="image/*" name="employee_picture" onchange="readURL(this);" />

                                                                            </div>

                                                                            <div class="uplod-img">

                                                                                <img id="blah" src="" />

                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                    <!-- Button (Double) -->

                                                                    <div class="form-group">

                                                                        <label class="col-md-4 control-label" for="btnsave">Save Changes</label>

                                                                        <div class="col-md-8">

                                                                            <button id="btnsave" class="submit btn btn-primary yes" type="submit" name="submit" value="submit">Yes</button>&nbsp;&nbsp;

                                                                            <button id="btncancel" name="btncancel" class="btn  btn-warning no" data-dismiss="modal" type="button">No</button>

                                                                        </div>

                                                                    </div>

                                                                </fieldset>

                                                            </form>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                            <!-- end-Modal -->



                                            <!-- Modal2 -->

                                            <div class="modal fade" id="myModal2" role="dialog" data-backdrop="static" data-keyboard="false">

                                                <div class="modal-dialog md-1">

                                                    <!-- Modal content-->

                                                    <div class="modal-content">

                                                        <div class="modal-header">

                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                                                            <h4 class="modal-title">Change Password</h4>

                                                        </div>

                                                        <div class="modal-body">

                                                            <form class="form-horizontal" method="post"  action="Profile/changePassword">

                                                                <input type="hidden" name="employeeid" value="<?= $this->session->userdata()['userdata']['employeeid']; ?>">

                                                                <fieldset>

                                                                    <!-- Text input-->

                                                                    <div class="form-group">

                                                                        <label class="col-md-3 control-label" for="textinput">Old Password</label>

                                                                        <div class="col-md-8">

                                                                            <input id="password" name="oldpassword" type="password" onkeyup="ckeckOldPass();" placeholder="Old Password" autocomplete="false" class="form-control input-md" required="">

                                                                            <span id="epass" style="color:red;display:none;">Old password is incorret</span>

                                                                        </div> 

                                                                    </div>

                                                                    <!-- Text input-->

                                                                    

                                                                                                                            

                                                                    

                                                                   

                                                                    <!-- Text input-->

                                                                    <div class="form-group">

                                                                        <label class="col-md-3 control-label" for="textinput">New Password</label>

                                                                        <div class="col-md-8">

                                                                            <input id="newpassword" name="newpassword" type="password" placeholder="New Password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter and at least 8 or more characters" autocomplete='false' class="form-control input-md" required="">

                                                                        </div>

                                                                    </div>

																	 <div class="form-group">

                                                                        <label class="col-md-3 control-label" for="textinput">Confirm Password</label>

                                                                        <div class="col-md-8">

                                                                            <input id="confirmpassword" name="confirmpassword" type="password" placeholder="Confirm Password" oninput="check(this);" autocomplete="false" class="form-control input-md" required="">

                                                                        </div>

                                                                    </div>

																	 <span style="font-size:10px;color:red;">(Note: Password must contain at least one number and one uppercase and lowercase letter and at least 8 or more characters)</span>

																	 

                                                                     <script language='javascript' type='text/javascript'>

                                                                        function check(input) {

                                                                            if (input.value != document.getElementById('newpassword').value) {

                                                                                input.setCustomValidity('Confirm password not matching.');

                                                                            } else {

                                                                                // input is valid -- reset the error message

                                                                                input.setCustomValidity('');

                                                                            }

                                                                        }

                                                                        

                                                                    </script>

                                                                               

                                                                    <!-- Button (Double) -->

                                                                    <div class="form-group">

																		<br />

                                                                        <label class="col-md-4 control-label" for="btnsave">Change Password</label>

                                                                        <div class="col-md-8">

                                                                            <button id="btnsave1" name="p_submit" class="submit btn btn-primary yes" type="submit" value="submit">Yes</button>&nbsp;&nbsp;

                                                                            <button id="btncancel1" name="btncancel" class="btn btn-warning no" data-dismiss="modal" type="button">No</button>

                                                                        </div>

                                                                    </div>

                                                                </fieldset>

                                                            </form>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                            <!-- end-Modal -->



                                        </div>

                                    </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
<div class="table-responsive" style="margin-bottom: 30px;">
    <table class="table" style="width:96%;">
        <tr>
            <th></th>
            <th>Allocated</th>
            <th>Registed</th>
            <th>Remain</th>
        </tr>
        <tr>
            <td>Employee</td>
            <td><?= $stats['total']['total_emp']; ?></td>
            <td><?= $stats['used']['employee']; ?></td>
            <td><?php if($stats['total']['total_emp']-$stats['used']['employee']<0){ echo 0; }else{ echo $stats['total']['total_emp']-$stats['used']['employee'];} ?></td>
        </tr>
        <tr>
            <td>Clients</td>
            <td><?= $stats['total']['total_client']; ?></td>
            <td><?= $stats['used']['client']; ?></td>
            <td><?php if($stats['total']['total_client']-$stats['used']['client']<0){ echo 0; }else{ echo $stats['total']['total_client']-$stats['used']['client']; } ?></td>
        </tr>
    </table>
</div>
        </div>

    </div>

    <!-- end: content -->

    <!-- plugins -->

    <script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>

    <script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>

    <script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>

    <script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>



    <!-- custom -->

    <script src="<?= base_url(); ?>asset/js/main.js"></script>

    <script src="<?= base_url(); ?>asset/js/append.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {

            var dataTable = $('#table-grid').DataTable({

                serverSide: true,

                ajax: {

                    url: "../admin/ajax/get_clients", // json datasource

                    type: "post", // method  , by default get

                    error: function(data) { // error handling

                        $(".table-grid-error").html("");

                        $("#table-grid").append('<tbody class="table-grid-error"><tr><th colspan="6">No data found!</th></tr></tbody>');

                        $("#table-grid_processing").css("display", "none");

                    },

                    complete: function() {

                        $(".confirm").popConfirm();

                    }

                },

            });

        });

    </script>

    <script>

        function readURL(input) {

            if (input.files && input.files[0]) {

                var reader = new FileReader();



                reader.onload = function(e) {

                    $('#blah')

                        .attr('src', e.target.result);

                };



                reader.readAsDataURL(input.files[0]);

            }

        }

    </script>

    <script>

        function ckeckOldPass(){

            var pass = $('#password').val();

            var eid  = $('#eid').val();

            

            $.ajax({

                url:    '../admin/ajax/check_old_password', 

                type:   'POST',

                data:   'oldPass='+pass+'&eid='+eid,

                

                success: function(data){

                    if(data==0){

                        $("#password").focus();

                        $("#epass").show(100);

                    }else{

                        $("#epass").hide(100);

                    }

                }

            });

        }

    </script>



    </body>



    </html>