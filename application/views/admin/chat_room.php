<link href="<?= base_url(); ?>asset/css/chat_style.css" rel="stylesheet">
<div id="content">
    <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top:20px;">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel-heading">
                <h3 class="">Chat Room </h3></div>
                <?php
                    echo show_err_msg($this->session->flashdata('error_msg'));
                    echo show_succ_msg($this->session->flashdata('success_msg'));
                ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-box">
                       <div class="contacts d-none d-md-block">
                            <div class="mousescroll ps ps--active-y" id="chatlist1">
                                <ul class="list-group border-top-0 border-left-0 border-right-0 list-group-linked">
                    <?php   if(count($data)){ ?>
                        <?php   foreach($data as $row){ ?>  
                                    <li class="list-group-item ban ">
                                        <a href="chat/<?= $row['receiver_id']; ?>/view" class="media <?= $row['new_messages'] ? 'newmessage' : ""; ?>">
                                <?php   if($row['employee_picture']){ ?>
                                            <img src="<?= base_url($row['employee_picture']); ?>" onerror="this.src='<?= base_url() ?>asset/img/dummy.svg'" class="img-thumb mr-2">
                                <?php   }else{ ?>
                                            <img src="<?= base_url() ?>asset/img/dummy.svg" class="img-thumb mr-2" >
                                <?php   } ?>
                                            <div class="media-body ml-3 ri-4">
                                <?php   if($row['sent_date']) { ?><span class="tme"><?= $row['sent_date']; ?></span><?php } ?>
                                <h6 class="mt-1 mb-1 name"><?= $row['employee_name']; ?> <?php if($row['new_messages']){ ?> <small><span class="newmeesagecount"><?= $row['new_messages'] ?></span></small> <?php } ?></h6>
                                                <p class="text-secondary text-truncate mb-0" style="width:auto">
                                                    <?= $row['message'] ? $row['message']: "conversion not initiated yet" ; ?>
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                    <?php   }   ?>
                <?php   }else{    ?>
                                    <li class="list-group-item ban">
                                        <a class="media" style="color: inherit; width:100%">
                                            <img  class="img-thumb mr-2">
                                            <div class="media-body ml-3" style="overflow: hidden">
                                                <span class="tme"></span>
                                                <h6 class="mt-1 mb-1 name"></h6>
                                                <p class="text-secondary text-truncate mb-0" style="width:auto">
                                                    --- No contact found ---
                                                </p>
                                               
                                            </div>
                                        </a>
                                    </li>
                <?php   } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
<!-- custom -->
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>
<!-- custom -->

<script>
    (function($) {
        jQuery.fn.extend({
            slimScroll: function(o) {
                var ops = o;
                //do it for every element that matches selector
                this.each(function() {

                    var isOverPanel, isOverBar, isDragg, queueHide, barHeight,
                        divS = '<div></div>',
                        minBarHeight = 30,
                        wheelStep = 30,
                        o = ops || {},
                        cwidth = o.width || 'auto',
                        cheight = o.height || '250px',
                        size = o.size || '7px',
                        color = o.color || '#000',
                        position = o.position || 'right',
                        opacity = o.opacity || .4,
                        alwaysVisible = o.alwaysVisible === true;

                    //used in event handlers and for better minification
                    var me = $(this);

                    //wrap content
                    var wrapper = $(divS).css({
                        position: 'relative',
                        overflow: 'hidden',
                        width: cwidth,
                        height: cheight
                    }).attr({
                        'class': 'slimScrollDiv'
                    });

                    //update style for the div
                    me.css({
                        overflow: 'hidden',
                        width: cwidth,
                        height: cheight
                    });

                    //create scrollbar rail
                    var rail = $(divS).css({
                        width: '15px',
                        height: '100%',
                        position: 'absolute',
                        top: 0
                    });

                    //create scrollbar
                    var bar = $(divS).attr({
                        'class': 'slimScrollBar ',
                        style: 'border-radius: ' + size
                    }).css({
                        background: color,
                        width: size,
                        position: 'absolute',
                        top: 0,
                        opacity: opacity,
                        display: alwaysVisible ? 'block' : 'none',
                        BorderRadius: size,
                        MozBorderRadius: size,
                        WebkitBorderRadius: size,
                        zIndex: 99
                    });

                    //set position
                    var posCss = (position == 'right') ? {
                        right: '1px'
                    } : {
                        left: '1px'
                    };
                    rail.css(posCss);
                    bar.css(posCss);

                    //wrap it
                    me.wrap(wrapper);

                    //append to parent div
                    me.parent().append(bar);
                    me.parent().append(rail);

                    //make it draggable
                    bar.draggable({
                        axis: 'y',
                        containment: 'parent',
                        start: function() {
                            isDragg = true;
                        },
                        stop: function() {
                            isDragg = false;
                            hideBar();
                        },
                        drag: function(e) {
                            //scroll content
                            scrollContent(0, $(this).position().top, false);
                        }
                    });

                    //on rail over
                    rail.hover(function() {
                        showBar();
                    }, function() {
                        hideBar();
                    });

                    //on bar over
                    bar.hover(function() {
                        isOverBar = true;
                    }, function() {
                        isOverBar = false;
                    });

                    //show on parent mouseover
                    me.hover(function() {
                        isOverPanel = true;
                        showBar();
                        hideBar();
                    }, function() {
                        isOverPanel = false;
                        hideBar();
                    });

                    var _onWheel = function(e) {
                        //use mouse wheel only when mouse is over
                        if (!isOverPanel) {
                            return;
                        }

                        var e = e || window.event;
                        var delta = 0;
                        if (e.wheelDelta) {
                            delta = -e.wheelDelta / 120;
                        }
                        if (e.detail) {
                            delta = e.detail / 3;
                        }

                        //scroll content
                        scrollContent(0, delta, true);

                        //stop window scroll
                        if (e.preventDefault) {
                            e.preventDefault();
                        }
                        e.returnValue = false;
                    }

                    var scrollContent = function(x, y, isWheel) {
                        var delta = y;

                        if (isWheel) {
                            //move bar with mouse wheel
                            delta = bar.position().top + y * wheelStep;

                            //move bar, make sure it doesn't go out
                            delta = Math.max(delta, 0);
                            var maxTop = me.outerHeight() - bar.outerHeight();
                            delta = Math.min(delta, maxTop);

                            //scroll the scrollbar
                            bar.css({
                                top: delta + 'px'
                            });
                        }

                        //calculate actual scroll amount
                        percentScroll = parseInt(bar.position().top) / (me.outerHeight() - bar.outerHeight());
                        delta = percentScroll * (me[0].scrollHeight - me.outerHeight());

                        //scroll content
                        me.scrollTop(delta);

                        //ensure bar is visible
                        showBar();
                    }

                    var attachWheel = function() {
                        if (window.addEventListener) {
                            this.addEventListener('DOMMouseScroll', _onWheel, false);
                            this.addEventListener('mousewheel', _onWheel, false);
                        } else {
                            document.attachEvent("onmousewheel", _onWheel)
                        }
                    }

                    //attach scroll events
                    attachWheel();

                    var getBarHeight = function() {
                        //calculate scrollbar height and make sure it is not too small
                        barHeight = Math.max((me.outerHeight() / me[0].scrollHeight) * me.outerHeight(), minBarHeight);
                        bar.css({
                            height: barHeight + 'px'
                        });
                    }

                    //set up initial height
                    getBarHeight();

                    var showBar = function() {
                        //recalculate bar height
                        getBarHeight();
                        clearTimeout(queueHide);

                        //show only when required
                        if (barHeight >= me.outerHeight()) {
                            return;
                        }
                        bar.fadeIn('fast');
                    }

                    var hideBar = function() {
                        //only hide when options allow it
                        if (!alwaysVisible) {
                            queueHide = setTimeout(function() {
                                if (!isOverBar && !isDragg) {
                                    bar.fadeOut('slow');
                                }
                            }, 1000);
                        }
                    }

                });

                //maintain chainability
                return this;
            }
        });

        jQuery.fn.extend({
            slimscroll: jQuery.fn.slimScroll
        });

    })(jQuery);

    //invalid name call
    $('#chatlist').slimscroll({
        color: '#1ec8c8',
        size: '10px',
        width: '700px',
        height: '370px'
    });
    //invalid name call
    $('#chatlist1').slimscroll({
        color: '#1ec8c8',
        size: '10px',
        height: '430px'
    });

    var interval = 30000; 
    function refreshChat() {
        location.reload(true); 
    }
    setInterval(refreshChat,interval);

</script>
</body>

</html>