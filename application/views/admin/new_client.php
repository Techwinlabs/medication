<style>
<!-- Nav CSSS !-->
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #ffffff;
	border-image: none;
    border-style: solid;
    border-width: 1px;
    color: #555555;
    cursor: default;
}

.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #ffffff;
    border-image: none;
    border-style: solid;
    border-width: 1px;
    color: #418bbb;
    cursor: default;
    width: 100px;
    height: 50px;
    text-align: center;
    padding-top: 15px;margin-left: -1px;border-radius: 0px;
}
.nav-tabs > li > a{
	color:#333;
   
    width: 100px;
    height: 50px;
	text-align: center;padding-top: 15px;
}

.nav-tabs > li > a:hover {
   
    width: 100px;
    height: 50px;padding-top: 15px;
}
.tab-content{
	margin-top:0px;
}

div#home { margin-top: 0px;}
#menu1 {
    margin-top: 17px;
}

#menu2 {
    margin-top: 17px;
}
.bg-all {
    background-color: #fff;
	border: 1px solid rgba(54, 64, 74, 0.08);
    border-radius: 1px;
    box-shadow: 0 4px 4px 0 #ccc;
}

.widget-simple-chart.card-box-one.top-no-border {
    border-top: none;
    border-radius: 0 5px 5px 5px;
    padding: 10px 20px 10px 25px;
}
.append {
    margin-bottom: 0px !important;
}

.col-md-12.mar-box {
    background-color: #e4e4e4;
    padding-bottom: 10px;
    margin-top: 6px;
    margin-bottom: 3px;
    padding-top: 30px;
}
.nav-tabs > li > a {
    margin-right: 2px;
    line-height: 1.42857143;
    border: 0px solid transparent;
    border-radius: 4px 4px 0 0;
}
.form-control.default {
    padding-bottom: 2px !important;
    padding-top: 2px !important;
}
em {
	color: #f94944;
}
</style>
<!-- start: Content -->
<div id="content">
	<div class="panel box-shadow-none content-header">
		<div class="panel-body">
			<div class="col-md-12">
				<h3 class=""> New Client</h3>
			</div>
		</div>
	</div>
    <div class="form-element">
        <div class="col-md-12 padding-0">
            <div class="col-md-12">
                <div class="panel form-element-padding">
                    <div class="panel-body" style="padding-bottom:30px;">
                        <div class="col-md-10">
							<?php
								echo show_err_msg($error_msg);
							?>
	<?php if(activePlanLimit($company_id,2)){	?>	
							<form method="POST" id="new-client" enctype="Multipart/Form-data" action="">
								<input type="hidden" name="company_id" value="<?= $company_id; ?>" >
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Post Code <span style="color: #D8000C;">*</span></label>
									<div class="col-sm-9">
										<input type="text" name="postcode" pattern="[0-9]{6}" id="postcode" class="form-control default"  value="<?php echo set_value('postcode'); ?>" required>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">First Name <span style="color: #D8000C;">*</span></label>
									<div class="col-sm-9">
										<input type="text" name="firstname" id="firstname" value="<?php echo set_value('firstname'); ?>"  class="form-control default" required>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Last Name <span style="color: #D8000C;">*</span></label>
									<div class="col-sm-9">
										<input type="text" name="lastname" id="lastname" value="<?php echo set_value('lastname'); ?>"  class="form-control default" required>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Email<span style="color: #D8000C;">*</span></label>
									<div class="col-sm-9">
										<input type="email" name="email" id="email" value="<?php echo set_value('email'); ?>"  class="form-control default" required>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Phone Number<span style="color: #D8000C;">*</span></label>
									<div class="col-sm-9">
										<input type="number" pattern="[0-9]{10}" name="phone_number" id="phone_number" value="<?php echo set_value('phone_number'); ?>"  class="form-control default" required>
                                        <span class="validity"></span>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Date of Birth <span style="color: #D8000C;">*</span></label>
									<div class="col-sm-9">
										<input type="date" name="dob" id="dob" value="<?php echo set_value('dob'); ?>" max="<?= date('Y-m-d'); ?>" class="form-control default" placeholder="mm/dd/yyyy" required>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Gender</label>
									<div class="col-sm-9">
										<div class="col-sm-2 padding-0">
										  <input type="radio" name="gender" value="1"> Male
										</div>
										<div class="col-sm-2 padding-0">
										  <input type="radio" name="gender" value="2"> Female
										</div>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Address <span style="color: #D8000C;">*</span></label>
									<div class="col-sm-9">
										<input type="text" name="address" id="address" value="<?php echo set_value('address'); ?>"  class="form-control default" required>
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Personal Information <span style="color: #D8000C;">*</span></label>
									<div class="col-sm-9">
										<input type="text" name="personal_information" id="personal_information" value="<?php echo set_value('personal_information'); ?>"  class="form-control default" required>
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Emergency Contact Person <span style="color: #D8000C;">*</span></label>
									<div class="col-sm-9">
										<input type="text" pattern="[0-9]{10}" name="emergency_contact_name" id="emergency_contact_name" value="<?php echo set_value('emergency_contact_name'); ?>"  class="form-control default" required>
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Emergency Contact Relation <span style="color: #D8000C;">*</span></label>
									<div class="col-sm-9">
										<input type="text" pattern="[0-9]{10}" name="emergency_contact_relation" id="emergency_contact_relation" value="<?php echo set_value('emergency_contact_relation'); ?>"  class="form-control default" required>
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Emergency Contact Number <span style="color: #D8000C;">*</span></label>
									<div class="col-sm-9">
										<input type="tel" pattern="[0-9]{10}" name="emergency_contact" id="emergency_contact" minlength="10" maxlength="10" title="Only 10 digits" pattern="\d{3}[\-]\d{3}[\-]\d{4}" value="<?php echo set_value('emergency_contact'); ?>"  class="form-control default" required>
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Picture</label>
									<div class="col-sm-9">
										<input type="file" name="client_picture" id="client_picture" accept="image/*" >
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">General Practitioner/Doctor Name</label>
									<div class="col-sm-9">
										<input type="text" name="doctor_name" value="<?php echo set_value('doctor_name'); ?>"  class="form-control default" >
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">General Practitioner/Doctor Address</label>
									<div class="col-sm-9">
										<input type="text" name="doctor_address" value="<?php echo set_value('doctor_address'); ?>"  class="form-control default" >
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">General Practitioner/Doctor Contact</label>
									<div class="col-sm-9">
										<input type="tel" pattern="[0-9]{10}"  name="doctor_contact_number" value="<?php echo set_value('doctor_contact_number'); ?>"  class="form-control default" >
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Allergies</label>
									<div class="col-sm-9">
										<input type="text" name="allergies" value="<?php echo set_value('allergies'); ?>"  class="form-control default" >
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Instructions</label>
									<div class="col-sm-9">
										<textarea type="text" name="comments" class="form-control default" rows="8" placeholder="Instruction about care."><?php echo set_value('comments'); ?></textarea>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Additional Information</label>
									<div class="col-sm-9">
										<textarea type="text" name="additional_information" class="form-control default" rows="8" placeholder="Additional instruction."><?php echo set_value('additional_information'); ?></textarea>
									</div>
								</div>
								<div class="clearfix"></div>
<?php			$section_roles = explode(',',ltrim(rtrim($this->userdata['section_roles'],','),','));
					if(!in_array(1,$section_roles) && $this->userdata['employee_type']==3){}else{				?>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Program of Care</label>
									<div class="col-sm-9">
										 <ul class="nav nav-tabs " style="margin-left:1px">
											<li class="active"><a data-toggle="tab" href="#am">AM</a></li>
											<li class="tab-wh"><a data-toggle="tab" href="#noon">Noon</a></li>
											<li class="tab-wh"><a data-toggle="tab" href="#evening">Evening</a></li>
											<li class="tab-wh"><a data-toggle="tab" href="#night">Night</a></li>
										</ul>
										<div class="tab-content">
											<div id="am" class="tab-pane fade in active"> 
												<div class="row">
													<div class="col-md-12 cover-div">
														<div class="col-md-12 mar-box">
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Medicine Name</label>
																<div class="col-md-8">
																	<input type="text" name="am_time_care_program[]" class="form-control default">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Medicine Type</label>
																<div class="col-md-8">
																	<input type="text" name="am_time_care_program_type[]" class="form-control default" placeholder="eg. Pill, Cream, Injection etc">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Dose</label>
																<div class="col-md-8">
																	<input type="text" name="am_time_care_program_dose[]" class="form-control default" placeholder="">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Medicine Time</label>
																<div class="col-md-8">
																	<input type="time" name="am_time_care_program_time[]" class="form-control default">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Other Detail</label>
																<div class="col-md-8">
																	<input type="text" name="am_time_care_program_detail[]" class="form-control default">
																</div>
															</div>
														</div>
														<div class="apendAm"></div>
														<div class="col-md-3 col-md-offset-8">
															<a style="font-size:12px" class="add_field_button btn btn-primary" onclick="addMoreAMMedicine();" title="Add New Medicine">+Add New Medicine</a>
														</div>
													</div>
												</div>
											</div>
											<div  id="noon" class="tab-pane fade"> 
												<div class="row">
													<div class="col-md-12 cover-div">
														<div class="col-md-12 mar-box">
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Medicine Name</label>
																<div class="col-md-8">
																	<input type="text" name="noon_time_care_program[]" class="form-control default">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Medicine Type</label>
																<div class="col-md-8">
																	<input type="text" name="noon_time_care_program_type[]" class="form-control default" placeholder="eg. Pill, Cream, Injection etc">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Dose</label>
																<div class="col-md-8">
																	<input type="text" name="noon_time_care_program_dose[]" class="form-control default" placeholder="">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Medicine Time</label>
																<div class="col-md-8">
																	<input type="time" name="noon_time_care_program_time[]" class="form-control default">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Other Detail</label>
																<div class="col-md-8">
																	<input type="text" name="noon_time_care_program_detail[]" class="form-control default">
																</div>
															</div>
														</div>
														<div class="apendNoon"></div>
														<div class="col-md-3 col-md-offset-8">
															<a style="font-size:12px" class="add_field_button btn btn-primary" onclick="addMoreNoonMedicine();" title="Add New Medicine">+Add New Medicine</a>
															
														</div>
													</div>
												</div>
											</div>
											<div  id="evening" class="tab-pane fade"> 
												<div class="row">
													<div class="col-md-12 cover-div">
														<div class="col-md-12 mar-box">
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Medicine Name</label>
																<div class="col-md-8">
																	<input type="text" name="tea_time_care_program[]" class="form-control default">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Medicine Type</label>
																<div class="col-md-8">
																	<input type="text" name="tea_time_care_program_type[]" class="form-control default" placeholder="eg. Pill, Cream, Injection etc">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Dose</label>
																<div class="col-md-8">
																	<input type="text" name="tea_time_care_program_dose[]" class="form-control default" placeholder="">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Medicine Time</label>
																<div class="col-md-8">
																	<input type="time" name="tea_time_care_program_time[]" class="form-control default">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Other Detail</label>
																<div class="col-md-8">
																	<input type="text" name="tea_time_care_program_detail[]" class="form-control default">
																</div>
															</div>
														</div>
														
														<div class="apendEvening"></div>
														<div class="col-md-3 col-md-offset-8">
															<a style="font-size:12px" class="add_field_button btn btn-primary" onclick="addMoreEveingMedicine();" title="Add New Medicine">+Add New Medicine</a>
														</div>
													</div>
												</div>
											</div>
											<div  id="night" class="tab-pane fade"> 
												<div class="row">
													<div class="col-md-12 cover-div">
														<div class="col-md-12 mar-box">
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Medicine Name</label>
																<div class="col-md-8">
																	<input type="text" name="night_time_care_program[]" class="form-control default">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Medicine Type</label>
																<div class="col-md-8">
																	<input type="text" name="night_time_care_program_type[]" class="form-control default" placeholder="eg. Pill, Cream, Injection etc">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Dose</label>
																<div class="col-md-8">
																	<input type="text" name="night_time_care_program_dose[]" class="form-control default" placeholder="">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Medicine Time</label>
																<div class="col-md-8">
																	<input type="time" name="night_time_care_program_time[]" class="form-control default">
																</div>
															</div>
															<div class="col-xs-12 form-group">
																<label class="col-sm-3 control-label text-right">Other Detail</label>
																<div class="col-md-8">
																	<input type="text" name="night_time_care_program_detail[]" class="form-control default">
																</div>
															</div>
														</div>
														
														<div class="apendNight"></div>
														<div class="col-md-3 col-md-offset-8">
															<a style="font-size:12px" class="add_field_button btn btn-primary" onclick="addMoreNightMedicine();" title="Add New Medicine">+Add New Medicine</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
<?php 					}		?>

								<div class="clearfix "></div>
								<hr />
								<div class="col-md-9 col-md-offset-3">
									<input class="submit btn btn-primary" name='submit' type="submit" value="Submit Data">&emsp;<button type="reset" class="btn btn-sm btn-warning">Reset Data</button>&emsp;<button type="reset" class="btn btn-sm btn-danger" onclick="goBack();">Cancel</button>
								</div>
							</form>
		<?php 	}else{	 ?>
			<p style="font-weight: 550; color: #af5959; text-align: center;">Maximum users allocated! Please call support to increase allocation.</p>
		<?php 	}	?>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>

<script type="text/javascript">
	$('#client_picture').change(function(e){
		var imgname = e.target.files[0].name;
		imgname = imgname.substr((imgname.lastIndexOf('.') +1));
		if(imgname=='jpg' || imgname=='jpeg' || imgname=='png' || imgname=='bmp' || imgname=='gif'){
		}else{
			alert('Invalid image. Please select a valid image.');
			$('#client_picture').val('');
		}
	});
</script>


<!-- end: content -->