	<link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>

	<link rel="stylesheet" src="<?= base_url('asset/css/fullcalendar.css'); ?>">
	<!-- start: Content -->
		<div id="content">		  
		  <div class="col-md-12 top-20 padding-0">
			<div class="col-md-12">
				 <div id='calendar'></div>		
				<br> 
				<br> 
				<br> 
		  </div>  
		  </div>
		</div>
		<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-dialog-centered">
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Add Schedule</h4>
			</div>
			<form method="post">
			  <div class="modal-body">
				  <div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-sm-3 control-label text-right">Start Date Time</label>
							<div class="col-sm-9">
								<input type="text" class="form-control default" value="" id="sechedule_date_start" name="sechedule_date_start"  required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label text-right">End Date Time</label>
							<div class="col-sm-9">
								<input type="text" class="form-control default" value="" id="sechedule_date_end" name="sechedule_date_end"  required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label text-right">Select Carer</label>
							<div class="col-sm-9">							
								<select class="form-control default livesearch" data-live-search="true"  id="employeeid" name="employeeid" required >
									<option value="" ></option>
						<?php	for($x=0;$x<count($carer);$x++){ ?>
									<option data-tokens="<?= $carer[$x]['employee_name']; ?> (<?= $carer[$x]['employee_number']; ?>)" value="<?= $carer[$x]['employeeid']; ?>" ><?= $carer[$x]['employee_name']; ?> (<?= $carer[$x]['employee_number']; ?>)</option>
						<?php	}	?>
								</select>
							</div>
						</div>  
						<div class="form-group">
							<label class="col-sm-3 control-label text-right">Select Client</label>
							<div class="col-sm-9">
								<select class="form-control default livesearch" data-live-search="true"  id="client_id" name="client_id" required >
									<option ></option>
									  <?php	for($i=0;$i<count($clients);$i++){ ?>
												  <option data-tokens="<?= $clients[$i]['client_name']; ?> (<?= $clients[$i]['postcode']; ?>)" value="<?= $clients[$i]['client_id']; ?>" ><?= $clients[$i]['client_name']; ?> (<?= $clients[$i]['postcode']; ?>)</option>
									  <?php	}	?>
								</select>
							</div>
						</div>
					</div>
				  </div>
			  </div>
			  <div class="modal-footer">
				<button type="reset" class="btn btn-default">Reset</button><button type="button" name="submit" class="btn btn-primary" onclick="add_schedule()">Add Schedule</button>
			  </div>
			</form>
		  </div>
		</div>
	  </div>
		
		
	  <div id="view_schedule"></div>
	  <div id="edit_schedule_view"></div>
	  <!-- end: content -->
		<!-- plugins -->
		<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
		<script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>
		<script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>
		<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>

		<script src="<?= base_url(); ?>asset/js/fullcalendar.js"></script>


		<!-- custom -->
		<script src="<?= base_url(); ?>asset/js/main.js"></script>
		<script src="<?= base_url(); ?>asset/js/append.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {

			  // full calendar
			  
				$('#calendar').fullCalendar({
					slotDuration: '00:15:00',
					defaultView: 'agendaDay',
				  header: {
					right: 'prev,next today',
					center: 'title',
					//right: 'agendaDay,agendaWeek,month'
				  },
				  customButtons: {
			           myCustomButton: {
			               text: ' ',
			               click: function () {
			                   //it scrolls to the position of the datepicker
			                   $('body,html').animate({
			                       scrollTop: $(document).height()
			                   }, 1000);
			                   $('#datepicker').DatePickerShow();
			               }
			           },
			             //...other buttons
			        },
				  defaultDate: "<?= date('Y-m-d') ?>",
				  navLinks: true, // can click day/week names to navigate views
				  selectable: true,
				  selectHelper: true,
				  slotEventOverlap: false,
				  editable: true,
				  select: function(start, end) {
						var startDate 	= 	moment(start).format('YYYY-MM-DD HH:mm')
						var endDate 	= 	moment(end).format('YYYY-MM-DD HH:mm')
					if (startDate >= "<?= date('Y-m-d H:i') ?>") {
					  $('#sechedule_date_start').val(startDate);
					  $('#sechedule_date_end').val(endDate);
					  $('#myModal').modal('show');
					}else{
					  alert("Error ! you cannot select past date");
					}
				  },
				  events: [
					<?php for ($i=0; $i <count($schedules); $i++) { ?>
					  {
						  id			: 	"<?php echo $schedules[$i]['schedule_id']; ?>",
						  title			: 	"<?php echo $schedules[$i]['employee_name']." (".$schedules[$i]['client_name'].")"; ?>",
						  start			: 	"<?php echo $schedules[$i]['start_datetime']; ?>",
						  end			: 	"<?php echo $schedules[$i]['end_datetime']; ?>",
						  textColor  	: 	'#333',
						  BorderColor	:	"#fff",
						  backgroundColor: "#E6E6FA",
					  },
				  <?php } ?>
				  ],
				  eventClick: function(calEvent, jsEvent, view) {
					  if(calEvent.id!=null || calEvent.id!='' || calEvent.id!=undefined){
						  get_schedule_info(calEvent.id);
					  }
				  },
				  eventDrop: function(event, delta, revertFunc) {
					  var update_start =  moment(event.start).format('YYYY-MM-DD HH:mm');
					  var update_end =  moment(event.end).format('YYYY-MM-DD HH:mm');
					  
					  if (update_start > "<?= date('Y-m-d H:i') ?>" && update_end > "<?= date('Y-m-d H:i') ?>") {
						  if (!confirm("Are you sure about this change?")) {
							  revertFunc();
						  }else{
							  edit_schedule_view(event.id,'<?= $company_id; ?>',update_start,update_end);
						  }
						}else{
						  alert("Error ! you cannot select past date");
						  revertFunc();
					  }
				  },
				  eventResize: function(event, delta, revertFunc) {
					var update_start =  moment(event.start).format('YYYY-MM-DD HH:mm');
					  var update_end =  moment(event.end).format('YYYY-MM-DD HH:mm');
					  
					  if (update_start > "<?= date('Y-m-d H:i') ?>" && update_end > "<?= date('Y-m-d H:i') ?>") {
						  if (!confirm("Are you sure about this change?")) {
							  revertFunc();
						  }else{
							  edit_schedule_view(event.id,'<?= $company_id; ?>',update_start,update_end);
						  }
                      }else{
						  alert("Error ! you cannot select past date");
						  revertFunc();
					  }
				  },
					eventAfterRender: function(event, element, view) {  
					 // Set the new width
					 $(element).css('width', '100px');
				  }
					
					
					
				});
				$('.fc-left').html('<p class=""><input type="hidden" id="datepicker"></p>');
				
				$('#datepicker').datepicker({
			        showOn: "both",
			        buttonImage: "<?= base_url(); ?>images/calender_50x50.png",
			        buttonImageOnly: true,
			        buttonText: "Select date ",
			        dateFormat:"yy-mm-dd",
			        onSelect: function (dateText, inst) {
			            $('#calendar').fullCalendar('gotoDate', dateText);
			        },

			    });
				
				var columnWidth = jQuery('#single-day-container td.last').width();
				jQuery('#single-day-container .single-day .view-item-events_calendar .dayview').each(function(){

				  var width = jQuery(this).width();
				  width = (width / columnWidth * 100) + '%';

				  var marginLeft = jQuery(this).css('margin-left');
				  var marginArray = marginLeft.split("p");
				  marginLeft = (marginArray[0] / columnWidth * 100) + '%';

				  var marginRight = jQuery(this).css('margin-right');
				  marginArray = marginRight.split("p");
				  marginRight = (marginArray[0] / columnWidth * 100) + '%';

				  var parentItem = jQuery(this).parent();
				  parentItem.css('width', width).css('margin-left', marginLeft).css('margin-right', marginRight);
				  jQuery(this).css('margin-left', '0').css('margin-right', '0').attr('style', function(i,s) { return s + 'width: 100% !important;' });

				});
				

			    $(".fc-other-month .fc-day-number").hide();
			} );

			
		</script>
		<style type="text/css">
			.ui-datepicker{
			    width: auto !important;
			}
			.fc-view-container { 
			  overflow-x: scroll; 
			}
			.fc-view.fc-agendaDay-view.fc-agenda-view{
			  width: 100%;
			}
			/* **For 2 day view** */
			.fc-view.fc-agendaTwoDay-view.fc-agenda-view{
			  width: 100%;
			} 
			
			/* Background color */ 
			td.fc-day.fc-widget-content.fc-mon.fc-today, td.fc-widget-content{
				background-color: #fff;
			}
		</style>
	</body>
</html>