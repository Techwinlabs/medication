<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Medication Admin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= base_url(); ?>images/favicon.ico">

        <!-- App css -->
        <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    </head>
    <body id="mimin" class="form-signin-wrapper">

      <div class="container">

        <form class="form-signin">
          <div class="panel periodic-login">
              <span class="atomic-number">28</span>
              <div class="panel-body text-center">
                  <h1 class="atomic-symbol">Mi</h1>
                  <p class="atomic-mass">14.072110</p>
                  <p class="element-name">Miminium</p>

                  <i class="icons icon-arrow-down"></i>
                  <div class="form-group form-animate-text" style="margin-top:40px !important;">
                    <input type="text" class="form-text" required>
                    <span class="bar"></span>
                    <label>Email</label>
                    <p>Input your email to reset your password</p>
                  </div>
                  <input id="timezone" type="hidden">
                  <input type="submit" class="btn col-md-12" value="Reset"/>
              </div>
                <div class="text-center" style="padding:5px;">
                    <a href="login.html">SignIn</a> | 
          </div>
        </form>

      </div>

      <!-- end: Content -->
      <!-- start: Javascript -->
	  		
		<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>       
		<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
		<script src="<?= base_url(); ?>assets/js/modernizr.min.js"></script>
		<!-- custom -->
		<script src="<?= base_url(); ?>asset/js/main.js"></script>
		<script src="<?= base_url(); ?>asset/js/jstz.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var tz = jstz.determine(); // Determines the time zone of the browser client
            var timezone = tz.name(); //'Asia/Kolhata' for Indian Time.
            $("#timezone").val(timezone);
        });
    </script>
     <!-- end: Javascript -->
   </body>
   </html>