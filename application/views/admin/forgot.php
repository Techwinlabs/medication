<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Log In</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="images/favicon.ico">

        <!-- App css -->
        <link href="<?= base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>asset/css/style.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>asset/css/custom.css" rel="stylesheet" type="text/css" /> 
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href="<?= base_url(); ?>asset/css/animate.css" rel="stylesheet">
    </head>

    <body class="bg-color">
        <!-- HOME -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 ">
                        <div class="wrapper-page">                            
                            <div class="account-pages">
                                <!--<div class="login-text m-t-50">                                           
                                    <h2 class="text-center login-1">Manager Login</h2>
								</div>-->
                                <div class="clearfix"></div>
                                <div class="account-box m-t-50">                                   
                                    <div class="account-logo-box">                                        
                                        <h6 class="text-center font-size "><img src="<?= base_url(); ?>asset/img/logo2.png"></h6>
                                    </div>
                                    <div class="account-content">
										<?php
											echo show_err_msg($this->session->flashdata('error_msg'));
										?>
                                        <form method="post" action="<?php echo base_url('admin/forgot'); ?>">
                                            <input id="timezone" name="timezone" type="hidden">
											<div class="row m-t-50">
												<div class="col-sm-12 text-center">
													<p class="text-muted">Please enter your registered email id :</p>
												</div>
											</div>
                                            <div class="form-group">  
												<input type="email" class="form-control user" id="username" placeholder="Email Id here." name="email" >
                                            </div>
                                           <button type="submit" class="btn btn-default login" name="submit" value="submit" >Recover your password</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- end card-box-->
                        </div>
                        <!-- end wrapper -->
                    </div>
                </div>
            </div>
        </section>
		<!------------------footer------------------>
<div class="footer">
            <div class="row footer">
                <div class="col-md-12">
                <div class="col-md-3">
                  <div class="web-title"><p>Zonati Developments Limited</p></div>
                  
                    <!--div class="content-left-row">
                        <p style="font-size: 11px;line-height: 0px;">Zonati Developments Limited</p>
                    </div-->
				</div>
				<div class="col-md-6">
                     <div class="men-rw">
						 <a href="http://dailycarerecords.com/admin" class="about-rw">Log In</a>
					  <a href="http://dailycarerecords.com/home.html" class="about-rw">Daily Care Records</a>
					  
						  <a href="http://dailycarerecords.com/platform-features.html" class="about-rw">Platform Features</a>
						  <a href="http://dailycarerecords.com/blog.html" class="about-rw">Blog</a>
					 </div>
                </div>
               
                <div class="col-md-3">
                    <div class="content-right-row">
                        <p>Copyright © 2018 . All rights reserved</p>
                    </div>
                </div>
                </div>
            </div>
        </div>
<!------------------end-footer------------------>
        <!-- END HOME -->

        <!-- jQuery  -->
        <script src="<?= base_url(); ?>asset/js/jquery.min.js"></script>       
        <script src="<?= base_url(); ?>asset/js/bootstrap.min.js"></script>
        <script src="<?= base_url(); ?>asset/js/jstz.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                var tz = jstz.determine(); // Determines the time zone of the browser client
                var timezone = tz.name(); //'Asia/Kolkata' for Indian Time.
                $("#timezone").val(timezone);
            });
        </script>
       
    </body>
</html>