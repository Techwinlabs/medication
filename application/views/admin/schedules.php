<link  href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<link  href="<?= base_url('asset/js/fc/lib/fullcalendar.min.css'); ?>" rel='stylesheet' />
<link  href="<?= base_url('asset/js/fc/lib/fullcalendar.print.min.css'); ?>" rel='stylesheet' media='print' />
<link  href="<?= base_url('asset/js/fc/scheduler.min.css'); ?>" rel='stylesheet' />
<script src="<?= base_url('asset/js/fc/lib/moment.min.js'); ?>"></script>
<script src="<?= base_url('asset/js/fc/lib/fullcalendar.min.js'); ?>"></script>
<script src="<?= base_url('asset/js/fc/scheduler.min.js'); ?>"></script>
<script src="<?= base_url('asset/js/plugins/moment.min.js'); ?>"></script>
<script src="<?= base_url('asset/js/plugins/jquery.datatables.min.js'); ?>"></script>
<script src="<?= base_url('asset/js/plugins/datatables.bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('asset/js/plugins/jquery.nicescroll.js'); ?>"></script>
<script src="<?= base_url('asset/js/main.js'); ?>"></script>
<script src="<?= base_url('asset/js/append.js'); ?>"></script>

<div id="content">
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div id='calendar' style="margin-bottom: 30px;"></div>
            <br>
        </div>
    </div>
</div>
<br>
<br>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Schedule</h4>
            </div>
            <form method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Start Date Time</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control default" value="" id="sechedule_date_start" name="sechedule_date_start" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">End Date Time</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control default" value="" id="sechedule_date_end" name="sechedule_date_end" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Select Carer</label>
                                <div class="col-sm-9">
                                    <select class="form-control default livesearch_1" data-live-search="true" id="employeeid" name="employeeid" required>
                                        <option value=""></option>
                                        <?php	for($x=0;$x<count($carer);$x++){ ?>
                                            <option data-tokens="<?= $carer[$x]['employee_name']; ?> (<?= $carer[$x]['employee_number']; ?>)" value="<?= $carer[$x]['employeeid']; ?>">
                                                <?= $carer[$x]['employee_name']; ?> (
                                                    <?= $carer[$x]['employee_number']; ?>)</option>
                                            <?php	}	?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Select Client</label>
                                <div class="col-sm-9">
                                    <select class="form-control default livesearch" data-live-search="true" id="client_id" name="client_id" required>
                                        <option></option>
                                        <?php	for($i=0;$i<count($clients);$i++){ ?>
                                            <option data-tokens="<?= $clients[$i]['client_name']; ?> (<?= $clients[$i]['postcode']; ?>)" value="<?= $clients[$i]['client_id']; ?>">
                                                <?= $clients[$i]['client_name']; ?> (
                                                    <?= $clients[$i]['postcode']; ?>)</option>
                                            <?php	}	?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default">Reset</button>
                    <button type="button" name="submit" class="btn btn-primary" onclick="add_schedule()">Add Schedule</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="view_schedule"></div>
<div id="edit_schedule_view"></div>
<script> $('#calendar').fullCalendar( {
    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source', 
		resourceRender: function(resourceObj, labelTds, bodyTds) {
			labelTds.on('click', function(){ $(".fc-widget-content").css({'background-color':'#f0f2f7','color':"#222"}); $(this).css({'background-color':'#3782a2','color':"#fff"}); $('#sechedule').modal('show');});
		labelTds.prepend(
			'<div class="col-md-3" style="float:left;">' +
			'<img src="'+resourceObj.image+'" width="58" height="48" class="img-circle">' +
			'</div>'
		);
    },
		now: "<?= date("Y-m-d") ?>", 
		editable: true, 
    droppable: false,
		scrollTime: '00:00',
    defaultView: "timelineDay", 
		overlap: false, 
		selectable: true,  
		header: {
        left: 'today prev,next',
        center: 'title',
        right: 'timelineDay,agendaWeek,month'
      }, 
			views: {
        timelineThreeDays: {
          type: 'timeline',
          duration: { days: 3 }
        }
      },
		resourceLabelText: 'Carer', resources: [ 
			<?php for ($i=0; $i <count($carer); $i++) { if($carer[$i]['employee_picture']==''){ $pic = '../asset/img/dummy.svg'; }else{ $pic = base_url($carer[$i]['employee_picture']); } ?>
							{
								id: "<?php echo $schedules[$i]['employeeid']; ?>", 
								title: "<?php echo $schedules[$i]['employee_name']; ?>",
								image: '<?= $pic; ?>'
							},
			<?php } ?>
		
    ], 
		events: [
					<?php for ($i=0; $i <count($schedules); $i++) { ?>
					  {
						  id					: 	"<?php echo $schedules[$i]['schedule_id']; ?>",
						  title				: 	"<?php echo /*$schedules[$i]['employee_name']." (".*/$schedules[$i]['client_name']/*.")"*/; ?>",
						  start				: 	"<?php echo $schedules[$i]['start_datetime']; ?>",
						  end					: 	"<?php echo $schedules[$i]['end_datetime']; ?>",
							resourceId	: 	"<?php echo $schedules[$i]['employeeid']; ?>",
						  textColor  	: 	'#333',
						  BorderColor	:		"#fff",
						  backgroundColor:"#E6E6FA",
					  },
				  <?php } ?>
		], 
		eventClick: function(calEvent, jsEvent, view) {
        if(calEvent.id!=null || calEvent.id!='' || calEvent.id!=undefined) {
						get_schedule_info(calEvent.id);
        }
		}, 
		select: function(start, end, event, view, resource) {
				var resourceidselect = resource.id;
        var startDate=moment(start).format('YYYY-MM-DD HH:mm');
				var endDate=moment(end).format('YYYY-MM-DD HH:mm');
				if (startDate >= "<?= date('Y-m-d H:i') ?>") {
					  $('#sechedule_date_start').val(startDate);
						$('#sechedule_date_end').val(endDate);
						$('#employeeid').val(resourceidselect);
						$('#employeeid').attr('disabled','1');
						
					  $('#myModal').modal('show');
				}else{
					  alert("Error ! you cannot select past date");
				}
		},
		// drop: function(date, event, ui, resourceId, start) {
		// 	alert('err');
    // }, 
		// eventReceive: function(event) {
    //     alert('errrr');
    // }, 
		eventDrop:function(event, delta, revertFunc) {
			var update_start 	=  moment(event.start).format('YYYY-MM-DD HH:mm');
			var update_end 		=  moment(event.end).format('YYYY-MM-DD HH:mm');
			
			if (update_start > "<?= date('Y-m-d H:i') ?>" && update_end > "<?= date('Y-m-d H:i') ?>") {
				if (!confirm("Are you sure about this change?")) {
					revertFunc();
				}else{
					edit_schedule_view(event.id,'<?= $company_id; ?>',update_start,update_end,event.resourceId);
				}
								}else{
				alert("Error ! you cannot select past date");
				revertFunc();
			}
    }, 
		eventResize: function(event, delta, revertFunc) {
			var update_start 	=  moment(event.start).format('YYYY-MM-DD HH:mm');
			var update_end 		=  moment(event.end).format('YYYY-MM-DD HH:mm');
			
			if (update_start > "<?= date('Y-m-d H:i') ?>" && update_end > "<?= date('Y-m-d H:i') ?>") {
				if (!confirm("Are you sure about this change?")) {
					revertFunc();
				}else{
					edit_schedule_view(event.id,'<?= $company_id; ?>',update_start,update_end);
				}
			}else{
				alert("Error ! you cannot select past date");
				revertFunc();
			}
    }
});
</script>
</body>
</html>