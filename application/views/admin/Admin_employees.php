<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_employees extends CI_Model {
	
	public function select_allEmployees(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$employeeid = $this->session->userdata()['userdata']['employeeid'];
		$data = $this->db->select('employeeid, company_id, employee_type, employee_number, employee_name, employee_email, employee_country_code, employee_mobile, employee_picture, employee_designation, created_at,is_active')->from('med_employees')->where(array('company_id'=>$company_id,'employeeid!='=>$employeeid))->order_by('employeeid','DESC')->get()->result_array();
		return $data;
	}
	
	public function admin_new_employee($input){
		
		$check = $this->db->select('employee_email')->from('med_employees')->where(array('employee_email'=>$input['employee_email'],'company_id'=>$input['company_id']))->get()->row();
		if($check==''){
			$check = $this->db->select('employee_email')->from('med_employees')->where(array('employee_number'=>$input['employee_number'],'company_id'=>$input['company_id']))->get()->num_rows();
			if ($check){
				return array('status' => 400,'msg' =>$input['employee_number'].' employee number already exist.');
			}else{
				
				unset($input['submit']);
				$input['employee_type']	= 1;
				$input['created_at']	= today()[0];
				$input['updated_at']	= today()[0];
				$input['employee_picture']	= '';
				$config['allowed_types'] 	= '*';
				$config['max_size'] 		= '1024';
				$date 						= date("Y-m");
				
				if (!empty($_FILES['employee_picture']['name'])){
					$path	=  './media/profile/'.$date."/";
					if (!is_dir($path)){
						mkdir($path , 0777);
						$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
						$fp = fopen($path."index.html","wb"); 
						fwrite($fp,$content); 
						fclose($fp); 
					}
					$config['file_name']	= date("Ymd")."_PP-".rand(1000,9999).'-'.time();
					$config['upload_path']	= $path;
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('employee_picture')){
						return array('status' =>400,'msg' =>"Profile picture uploading error: ".strip_tags($this->upload->display_errors()));
						exit;
					}else{
						$input['employee_picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
					}
				}
				$rand = rand(11111111,99999999);
				$input['employee_password']		= crypt($rand,APP_SALT); 
				$this->db->trans_start();
				$this->db->insert('med_employees',$input);
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'msg' => 'Internal server error. Please try again!');
				}else{
					$this->db->trans_commit();
					$this->load->library('email');
					$this->email->from('noreply@dailycarerecords.com', 'Medication Support Team');
					$this->email->to($input['employee_email']);
					$this->email->subject('Medication App login credential');
					$this->email->set_mailtype("html");
					$email_message = "";
					$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
					$email_message .= "<div>Hi, ".$input['employee_name']."</div>" ;
					$email_message .= "<div>Please find your Medication app login credential listed below:</div>" ;
					$email_message .= "<div>Employee Code : ".$input['employee_number']."</div>" ;
					$email_message .= "<div>Password : ".$rand."</div>" ;
					$email_message .= "<div>Please use these credential for Medication app account. After successful login we recommend you to change your password.</div>" ;
					$email_message .= "<div>Thanks</div>" ;
					$this->email->message($email_message);
					$this->email->send();
					return array('status' => 200,'msg' =>'Employee account is created. Login credential are sent to employee email successfully.');
				}
			}					
		}else{
			return array('status' => 400,'msg' =>'Account already registered with '.$input['employee_email']);
		}
		
	}
	
	public function view_userprofile($id){
		$data = $this->db->select('*')->from('med_employees')->where(array('user_id'=>$id))->get()->row_array();
		if (count($data)){
			return array('data'=>$data,'status' => 200,'msg' =>'');
			
		}else{
			return array('status' => 400,'msg' =>'User account not exist!');
		}
	}
	
	public function edit_userprofile($input){
		
		$check = $this->db->select('email')->from('med_employees')->where(array('email'=>$input['email'],'user_id!='=>$input['user_id']))->get()->row();
		if($check==''){
			$check = $this->db->select('email')->from('med_employees')->where(array('email'=>$input['email'],'user_id!='=>$input['user_id']))->get()->row();
			if($check){
				$check = $this->db->select('user_id')->from('med_employees')->where(array('mobile_number'=>$input['mobile_number'],'country_code'=>$input['country_code']))->where(array('user_id!='=>$input['user_id']))->get()->num_rows();
				if ($check){
					return array('status' => 400,'msg' =>'Account already registered with '.$input['mobile_number'])." mobile number";
				}else{
					unset($input['submit']);
					$input['profile_picture']	= getusersdata($input['user_id'],'profile_picture');
					$config['allowed_types'] 	= '*';
					$config['max_size'] 		= '500';
					$date 						= date("Y-m");
					
					if (!empty($_FILES['profile_picture']['name'])){
						$path	=  './media/profile/'.$date."/";
						if (!is_dir($path)){
							mkdir($path , 0777);
							$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
							$fp = fopen($path."index.html","wb"); 
							fwrite($fp,$content); 
							fclose($fp); 
						}
						$config['file_name']	= date("Ymd")."_PP-".rand(1000,9999).'-'.time();
						$config['upload_path']	= $path;
						$this->upload->initialize($config);
						if (!$this->upload->do_upload('profile_picture')){
							return array('status' =>400,'msg' =>"Profile picture uploading error: ".strip_tags($this->upload->display_errors()));
							exit;
						}else{
							$input['profile_picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
						}
					}
					$this->db->trans_start();
					$this->db->where(array('user_id'=>$input['user_id']))->update('med_employees',array('user_name'=>$input['user_name'],'profile_picture'=>$input['profile_picture'],'designation'=>$input['designation'],'email'=>$input['email'],'country_code'=>$input['country_code'],'mobile_number'=>$input['mobile_number'],'updated_at'=>today()[0]));
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						return array('status' => 500,'msg' => 'Internal server error. Please try again!');
					}else{
						$this->db->trans_commit();
						return array('status' => 200,'msg' =>'User account is updated.');
					}
				}
			}else{
				
			}
		}else{
			return array('status' => 400,'msg' =>'Account already registered with '.$input['employee_email']);
		}
	}
	
}