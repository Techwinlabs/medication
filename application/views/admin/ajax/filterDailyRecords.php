<table id="table-grid" class="table table-striped" width="100%" cellspacing="0"  data-export-title="Daily care recored of <?= $name; ?>">
    <thead>
        <tr>
            <th width="10%">Date</th>
            <th width="10%">Time</th>
            <th width="10%">Staff Id</th>
            <th width="70%">Daily Care Records</th>
        </tr>
    </thead>
    <tbody>
<?php   for($i=0;$i<count($data);$i++) { ?>
        <tr>
            <td><?= $data[$i]['date']; ?></td>
            <td><?= $data[$i]['time']; ?></td>
            <td><a href="" data-toggle="modal" data-target="#profile<?= $i; ?>" onclick="viewProfile_emp(<?= $i; ?>,<?= $data[$i]['employeeid']; ?>)"><?= $data[$i]['employee_number']; ?></a></td>
            <td><?= $data[$i]['answers']; ?></td>
        </tr>

<?php } ?>
    
    </tbody>
</table>
<script type="text/javascript">
        $(document).ready(function() {
            var buttonCommon = {
                init: function (dt, node, config) {
                    var table = dt.table().context[0].nTable;
                    if (table) config.title = $(table).data('export-title')
                },
                title: 'default title'
            };
            $.extend( $.fn.dataTable.defaults, {
                "buttons": [
                    $.extend( true, {}, buttonCommon, {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    } ),
                    $.extend( true, {}, buttonCommon, {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        exportOptions: {
                            columns: ':visible'
                        }
                    } ),
                    $.extend( true, {}, buttonCommon, {
                        extend: 'print',
                        exportOptions: {
                            columns: ':visible'
                        },
                        orientation: 'landscape'
                    } )/*,
                    $.extend( true, {}, buttonCommon, {
                        extend: 'csv',
                        exportOptions: {
                            columns: ':visible'
                        },
                        orientation: 'landscape'
                    } )*/
                ]
            } );
        } );
    </script>