<div class="col-md-11"></div><button type="button" id="print-btn" class="btn btn-primary col-md-1" target="_blank" onclick="printDiv('data_usage');">Print</button>
<br>
<br>
<div class="clearfix"></div>
<?php for($i=0;$i<count($data);$i++){ ?>
    <p class="accordion"><span><img src="<?= $data[$i]['employee_picture'] ?>" height="40px" width="40px" onerror="this.src='<?= base_url() ?>asset/img/dummy.svg'" >&emsp;<b><?= $data[$i]['employee_name']; ?></b></span><span class="alignFroomRight"><?= $data[$i]['data']; ?></span></p>
    <div class="panel">
        <?php $usage =  $data[$i]['data_usages'];
      if(count($usage)){ ?>
         <table class="table table-striped display">
            <tr><th>Date</th><th>Data Used</th></tr>
        <?php for($u=0;$u<count($usage);$u++){ ?>
            <tr><td><?= $usage[$u]['date']; ?></td><td><?= $usage[$u]['data']; ?></td></tr>  

        <?php   } ?>
        </table>
        <?php } else { ?>
            <p>No data usage history for selected date range.</p>
        <?php } ?>
    </div>

    
<?php } ?>

<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
	function printDiv(divName) {
        $('#print-btn').hide();
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
		$('#print-btn').show();
	}
</script>