<div class="modal fade" id="<?= $id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <center>
                    <img src="<?= $employee_picture; ?>" name="aboutme" width="140" height="140" border="0" class="img-circle"></a>
                        <h3 class="media-heading"><?= $employee_name; ?> <small> (<?= $employee_number; ?>)</small></h3>
                    <span class="label label-info"><?= $designation; ?></span>
                </center>
                <hr>
                <center>
                <p class="text-center"><strong>Email: </strong><br>
                    <?= $employee_email; ?></p>
                    <p class="text-center"><strong>Contact Number: </strong><br>
                    +<?= $employee_country_code; ?> <?= $employee_mobile; ?></p>
                <br>
                </center>
            </div>
            <div class="modal-footer">
                <center>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </center>
            </div>
        </div>
    </div>
</div>
