<table id="mar-table" class="table table-striped" width="100%" cellspacing="0" data-export-title="Medication recored of <?= $name; ?>">
	<thead>
		<tr>
			<th width="10%">Date</th>
			<th width="10%">Time</th>
			<th width="15%">Medicine</th>
			<th width="10%">Status</th>
			<th width="10%">Staff ID</th>
			<th width="55">Detail</th>
		</tr>
	</thead>
	<tbody>
<?php	$i=0; ?>
<?php	foreach($data as $row){ ?>
			<tr>
				<td><?= $row['date']; ?></td>
				<td><?= $row['time']; ?></td>
				<td><?= $row['medicine']; ?></td>
				<td><?= $row['status']; ?></td>
				<td><a href="" data-toggle="modal" data-target="#profile<?= $i; ?>" onclick="viewProfile_emp(<?= $i; ?>,<?= $row['employeeid'];  ?>)"><?= $row['employee_number']; ?></a></td>
				<td><?= $row['detail']; ?></td>
		
			</tr>
<?php	$i++;	?>
<?php	}	?>
	</tbody>
</table>
<script type="text/javascript">
	$(document).ready(function() {
		var buttonCommon = {
			init: function (dt, node, config) {
				var table = dt.table().context[0].nTable;
				if (table) config.title = $(table).data('export-title')
			},
			title: 'default title'
		};
		$.extend( $.fn.dataTable.defaults, {
			"buttons": [
				$.extend( true, {}, buttonCommon, {
					extend: 'excelHtml5',
					exportOptions: {
						columns: ':visible'
					}
				} ),
				$.extend( true, {}, buttonCommon, {
					extend: 'pdfHtml5',
					orientation: 'landscape',
					exportOptions: {
						columns: ':visible'
					}
				} ),
				$.extend( true, {}, buttonCommon, {
					extend: 'print',
					exportOptions: {
						columns: ':visible'
					},
					orientation: 'landscape'
				} ),
				$.extend( true, {}, buttonCommon, {
					extend: 'csv',
					exportOptions: {
						columns: ':visible'
					},
					orientation: 'landscape'
				} )
			]
		} );
	} );
</script>
   