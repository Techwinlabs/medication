<div class="modal fade" id="reply_concern" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reply Concern</h4>
                </div>
                <div class="modal-body">
                   <form method="post">
                        <input type="hidden" id="concern_id" value="<?php echo $concernid; ?>">
                        <input type="hidden" id="employee_id" value="<?php echo $employee_id; ?>">
                        <textarea id="concern_message" class="form-control"></textarea>
                        <br>
                        <button type="button" class="btn btn-primary" style="margin-left:auto;margin-right:auto;" onclick="updateConcern();">Submit</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                   </form>
                </div>
        </div>
    </div>
</div>