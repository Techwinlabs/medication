<?php if(count($data)){ ?>
<hr />
<h4 class="col-md-11">&emsp;Timesheet of <?= $data[0]['employee_name'] ?></h4><button type="button" id="print-btn" class="btn btn-primary col-md-1" target="_blank" onclick="printDiv('time_sheets');">Print</button>
<hr />
<div class="col-md-12">
<?php for($i=0;$i<count($data);$i++){ ?>
    <p class="accordion"><span><a data-toggle="modal" data-target="#profile<?= $i; ?>" onclick="viewProfile_emp('carer<?= $i; ?>',<?= $data[$i]['carerid']; ?>,1)"><b><?= $data[$i]['employee_number']; ?></b></a> &emsp; - &emsp;<?= $data[$i]['date']; ?></span><span class="alignFromRightTime"><?= $data[$i]['workedhours']; ?> hrs / <?= $data[$i]['totalhours']; ?> hrs</span></p>
    <div class="panel">
        <?php $schedule =  $data[$i]['schedule'];
      if(count($schedule)){ ?>
         <table class="table table-striped display">
            <tr><th>Client</th><th>Start</th><th>End</th><th>Status</th><th>Total</th></tr>
        <?php for($u=0;$u<count($schedule);$u++){ ?>
            <tr><td><a data-toggle="modal" data-target="#profile<?= $i.$u; ?>" onclick="viewProfile_client(<?= $i.$u; ?>,<?= $schedule[$u]['client_id']; ?>)"><?= $schedule[$u]['client_name']; ?></a></td><td><?= date("H:i A",(strtotime($schedule[$u]['start_datetime']))); ?></td><td><?= date("H:i A",(strtotime($schedule[$u]['end_datetime']))); ?></td><td><?= $schedule[$u]['status'] ? "Done" : "To Do"; ?></td><td><?= $schedule[$u]['totalhrs'];  ?> hrs</td></tr> 
        <?php   } ?>
        </table>
        <?php } else { ?>
            <p>No timesheet data to show.</p>
        <?php } ?>
    </div>
<?php } ?>
</div>
<?php }else{ ?>
<div class="col-md-12">
    <p>No timesheet data to show.</p>
</div>

<?php } ?>
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
    function printDiv(divName) {
        $('#print-btn').hide();
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
        $('#print-btn').show();
    }
</script>