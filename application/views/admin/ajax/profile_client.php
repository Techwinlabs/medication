<div class="modal fade" id="profile_client<?= $id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="print_client_details">
			 <style>
 span.left_conetent {
    float: left;
    width: 40%;
    font-weight: 700;
}

span.right_conetent {
    float: right;
    width: 60%;
}

.client_persnal_info {
    clear: both;
    padding: 3px 0px 5px 3px;
    width: 100%;
    text-align: left;
    display: inline-block;
}
.client_persnal_info_outer {
    clear: both;
    /*display: inline-block;*/
}
 </style>
                <div class="modal-body">
                    <center>
                        <img src="<?= $client_picture; ?>" name="aboutme" width="140" height="140" border="0" class="img-circle"></a>
                         <h3 class="media-heading"><?= $firstname; ?> <?= $lastname; ?>  <small> (<?= $gender == 2 ? "Femele" : "Male"; ?>)</small></h3>
                        <span class="label label-info"><?= $status == 1 ? "Under Care" : "Recovered"; ?></span>
                    </center>
                    <hr>
					<div class="client_persnal_info_outer">
						<div class="client_persnal_info">
							<span class="left_conetent ">Date of Birth:</span>
							<span class="right_conetent "><?= $dob; ?></span>
						</div>
						
						<div class="client_persnal_info">
							<span class="left_conetent ">Address:</span>
							<span class="right_conetent right"><?= $address; ?></span>
						</div>
						
						<div class="client_persnal_info">
							<span class="left_conetent ">Post Code:</span>
							<span class="right_conetent right"><?= $postcode; ?></span>
						</div>
						
						<div class="client_persnal_info">
							<span class="left_conetent left">Emergency Contact Person:</span>
							<span class="right_conetent right"> <?= $emergency_contact_name ? $emergency_contact_name : "-"; ?></span>
						</div>
						
						<div class="client_persnal_info">
							<span class="left_conetent left">Emergency Contact Relation:</span>
							<span class="right_conetent right">  <?= $emergency_contact_relation ? $emergency_contact_relation : "-"; ?></span>
						</div>
						
						<div class="client_persnal_info">
							<span class="left_conetent left">Emergency Contact Number:</span>
							<span class="right_conetent right">  <?= $emergency_contact ? $emergency_contact : "-"; ?></span>
						</div>
						
						<div class="client_persnal_info">
							<span class="left_conetent left">Persnol Information:</span>
							<span class="right_conetent right">  <?= $personal_information; ?></span>
						</div>
						
						<div class="client_persnal_info">
							<span class="left_conetent left">General Practitioner/Doctor Name:</span>
							<span class="right_conetent right"> <?= $doctor_name ? $doctor_name: "-"; ?></span>
						</div>
						
						<div class="client_persnal_info">
							<span class="left_conetent left">General Practitioner/Doctor Address:</span>
							<span class="right_conetent right">  <?= $doctor_address ? $doctor_address: "-";  ?></span>
						</div>
						
						<div class="client_persnal_info">
							<span class="left_conetent left">General Practitioner/Doctor Contact:</span>
							<span class="right_conetent right">   <?= $doctor_contact_number ? $doctor_contact_number: "-";  ?></span>
						</div>
						
						<div class="client_persnal_info">
							<span class="left_conetent left">Allergies:</span>
							<span class="right_conetent right">    <?= $allergies ? $allergies: "-";  ?></span>
						</div>
						
						<div class="client_persnal_info">
							<span class="left_conetent left">Instructions:</span>
							<span class="right_conetent right">   <?= nl2br($comments); ?></p></span>
						</div>
						
						<div class="client_persnal_info">
							<span class="left_conetent left">Additional Information:</span>
							<span class="right_conetent right"> <?= $additional_information ? $additional_information: "-";  ?></p></span>
						</div>
						
					</div>
                </div>
                <div class="modal-footer">
                   <div class="profile-modal-btn" style="text-align: center;">
						<button type="button" class="btn btn-default hide_controles_onPrint" data-dismiss="modal">Close</button>
						<button type="button" id="print-btn" class="btn btn-default hide_controles_onPrint" target="_blank" onclick="printDiv('print_client_details');">Print</button>
                    </div>
                </div>
            </div>
        </div>
    </div>