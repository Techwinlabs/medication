<table id="schedule_table" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Carer</th>
            <th>Client</th>
            <th>Schedule</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
<?php	foreach($data as $row){ ?>
        <tr>
            <td><?= $row['employee_picture'] ? '<img src="'.base_url().$row['employee_picture'].'" height="50px" width="50px" onerror="this.src=\''.base_url().'asset/img/dummy.svg\'" >' : '<img src="'.base_url().'asset/img/dummy.svg" height="50px" width="50px">' ?>&emsp;<b><?= $row['employee_name']; ?></b> (<?= $row['employee_number']; ?>)</td>
            
            <td><?= $row['client_picture'] ? '<img src="'.base_url().$row['client_picture'].'" height="50px" width="50px" onerror="this.src=\''.base_url().'asset/img/dummy.svg\'" >' : '<img src="'.base_url().'asset/img/dummy.svg" height="50px" width="50px">' ?>&emsp;<?= $row['client_name']; ?></td>
            <td><?= $row['start_datetime']; ?> - <?= $row['end_datetime']; ?></td>
            <td><p><a href=""><i class="fa fa-edit"></i></a>&emsp;<a href=""  class="confirm"  title="<b>Are you sure to delete!</b>" ><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
<?php	}	?>
    </tbody>
</table>