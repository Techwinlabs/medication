<div id="edit_schedule<?= $schedule['schedule_id']; ?>" class="modal fade" role="dialog">
			<div class="modal-dialog modal-dialog-centered">
			  <!-- Modal content-->
			  <div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Edit Schedule</h4>
				</div>
				<form method="post">
				  <div class="modal-body">
					  <div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="col-sm-3 control-label text-right">Start Date Time</label>
								<div class="col-sm-9">
									<input type="text" class="form-control default" value="<?= date('Y-m-d H:i',strtotime($schedule['start_datetime'])); ?>" id="edit_sechedule_date_start" name="sechedule_date_start"  required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label text-right">End Date Time</label>
								<div class="col-sm-9">
									<input type="text" class="form-control default" value="<?= date('Y-m-d H:i',strtotime($schedule['end_datetime'])); ?>" id="edit_sechedule_date_end" name="sechedule_date_end"  required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label text-right">Select Carer</label>
								<div class="col-sm-9">							
									<select class="form-control default livesearch" data-live-search="true"  id="edit_employeeid" name="employeeid" required >
										<option value="" ></option>
							<?php	for($x=0;$x<count($carer);$x++){ ?>
										<option data-tokens="<?= $carer[$x]['employee_name']; ?> (<?= $carer[$x]['employee_number']; ?>)" value="<?= $carer[$x]['employeeid']; ?>" <?php if($carerid==$carer[$x]['employeeid']){ echo 'selected'; } ?> ><?= $carer[$x]['employee_name']; ?> (<?= $carer[$x]['employee_number']; ?>)</option>
							<?php	}	?>
									</select>
								</div>
							</div>  
							<div class="form-group">
								<label class="col-sm-3 control-label text-right">Select Client</label>
								<div class="col-sm-9">
									<select class="form-control default livesearch" data-live-search="true"  id="edit_client_id" name="client_id" required >
										<option ></option>
										  <?php	for($i=0;$i<count($clients);$i++){ ?>
													  <option data-tokens="<?= $clients[$i]['client_name']; ?> (<?= $clients[$i]['postcode']; ?>)" value="<?= $clients[$i]['client_id']; ?>" <?php if($schedule['client_id']==$clients[$i]['client_id']){ echo 'selected'; } ?>><?= $clients[$i]['client_name']; ?> (<?= $clients[$i]['postcode']; ?>)</option>
										  <?php	}	?>
									</select>
								</div>
							</div>
						</div>
					  </div>
				  </div>
				  <div class="modal-footer">
					<button type="reset" class="btn btn-default">Reset</button><button type="button" name="submit" class="btn btn-primary" onclick="update_schedule(<?= $schedule['schedule_id']; ?>)">Update Schedule</button>
				  </div>
				</form>
			  </div>
			</div>
		  </div>