<?php	$i=0; ?>
<table id="notifications-tbl" class="table table-bordered" width="100%" cellspacing="0"  >
	<thead style="display:none;">
		<tr><th></th></tr>
	<thead>
	<tbody>
		
<?php	foreach($data as $row){ ?>
			<tr>
				<td >
				<?php if(@$row['notification']['status']==0){	?>
				<button Type="button" class="btn btn-primary" style="float:right" onclick="resolveCon(<?= @$row['notification']['concern_id']?>,<?= $row['notification']['employeeid']; ?>)">Resolve</button>
				<?php	}	?>
					<p><b>Opened on :&emsp;</b><?= @$row['notification']['open_at'] ? convertUtcToLocal("d/m/Y h:i A",@$row['notification']['open_at'],@$row['notification']['timezone']) : "-NA-"; ?></p>
					<p><b>Notification Type : &emsp;</b><?= $row['notification']['for']; ?></p>
					<p><b>Employee Name :&emsp;</b><?= @$row['notification']['employee_name']; ?></p>
					<p><b>Employee ID : &emsp;</b><a href="" data-toggle="modal" data-target="#profile<?= $i; ?>" onclick="viewProfile_emp('carer<?= $i; ?>',<?= @$row['carer_id']; ?>,1)"><?= @$row['notification']['employee_number']; ?></a></p>
					<p><b>Subject :&emsp; </b><?= @$row['notification']['subject']; ?></p>
					<p><b>Carer Message :&emsp;</b><?= $row['notification']['concern']; ?></p>
					<p><b>Resolving Supervisor Name :&emsp; </b><?= @$row['notification']['closed_by_name'] ? @$row['notification']['closed_by_name'] : "-NA-";  ?></p>
					<p><b>Resolving Supervisor ID : &emsp;</b><?= @$row['notification']['closed_by'] ? '<a href="" data-toggle="modal" data-target="#profile'.$i.'A" onclick="viewProfile_emp(\'carer'.$i.'A\','.$row['notification']['closed_by'].',1)">'. @$row['notification']['closed_by_employeeNumber'].'</a>' : "-NA-";  ?></p>
					<p><b>Resolution Message :&emsp; </b><?= $row['notification']['reply']; ?></p>
					<p><b>Closed on : &emsp;</b><?= @$row['notification']['closed_at']  ? convertUtcToLocal("d/m/Y h:i A",@$row['notification']['closed_at'],@$row['notification']['timezone']) : "-NA-"; ?></p>
				</td>
        </tr>

	
<?php	$i++;	?>

<?php	}	?>
	</tbody>
</table>
<div id="reply_concern_modal"></div>
<script type="text/javascript">
$(document).ready(function() {
	$('#notifications-tbl').DataTable({
		dom: '<"top">rt<"bottom"p><"clear">'
	});
});
</script>