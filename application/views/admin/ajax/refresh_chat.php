<?php   foreach($data as $row){ ?>
<?php       if($receiver_data['receiver_id']==$row['sender_id']) { ?>
            <li class="in"><img src="<?= $receiver_data['receiver_picture']; ?>" class="avatar" alt="">
                <div class="message">
                    <span class="arrow"></span>
                    <span class="body"> <?= $row['message']; ?></span>
                </div>
                <span class="datetime"> <?= date("F j, Y, g:i a",strtotime($row['sent_date'])); ?> ( UTC +0:00 )</span>
            </li>
<?php       }else{ ?>
    
            <li class="out"><img src="<?= $sender_data['sender_picture']; ?>" class="avatar" alt="">
                <div class="message">
                    <span class="arrow"></span>
                    <span class="body"> <?= $row['message']; ?> </span>
                </div>
                <span class="datetime"> <?= date("F j, Y, g:i a",strtotime($row['sent_date'])); ?> ( UTC +0:00 )</span>
            </li>
<?php       } ?>
<?php   } ?>
