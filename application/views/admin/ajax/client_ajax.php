<?php	$i=0; ?>
<?php	foreach($data as $row){ ?>
			<tr>
				<td>
					<div class="col-md-4"><?= $row['client_picture'] ? '<img src="'.base_url().$row['client_picture'].'" height="50px" width="50px" onerror="this.src=\''.base_url().'asset/img/dummy.svg\'" >' : '<img src="'.base_url().'asset/img/dummy.svg" height="50px" width="50px">' ?>
					</div>
					<div class="col-md-8">
					<b><?= $row['firstname']; ?> <?= $row['lastname']; ?></b> (<?= $row['dob']; ?>)
					</div>
				</td>
				<td>
					<?= $row['address']; ?>
					<br/>
					<?= $row['postcode']; ?>
				</td>
				<td>
					<?= $row['emergency_contact']; ?>
				</td>
				<td>
					<p><a href="<?= base_url(); ?>admin/update_profile/<?= $row['client_id']; ?>"><i class="fa fa-edit" style="font-size:15px"></i></a>&emsp;<a href="" href="" data-toggle="modal" data-target="#profile<?= $i; ?>" onclick="viewProfile_client(<?= $i; ?>,<?= $row['client_id'];  ?>)"><i class="fa fa-eye" style="font-size:15px"></i></a>&emsp;<a href="" class="confirm"  title="<b>Are you sure to delete!</b>"><i class="fa fa-trash-o"style="font-size:15px"></i></a></p>
				</td>
				<td>
					<a ><i class="fa fa-calendar-plus-o" style="font-size:24px" onclick="care_program(<?= $row['client_id']; ?>)"></i></a>
				</td>
				<td>
					<a href="<?= base_url(); ?>admin/mar/<?= $row['client_id']; ?>"><i class="fa fa-eye" style="font-size:20px"></i></a>
				</td>
				<td>
					<a href="<?= base_url(); ?>admin/daily_records/<?= $row['client_id']; ?>" ><i class="fa fa-icon-notebook" style="font-size:24px" ></i></a>
				</td>
			</tr>
	<?php	$i++; ?>
	<?php	}	?>