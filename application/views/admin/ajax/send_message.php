<li class="out"><img src="<?= $sender_picture; ?>" class="avatar" alt="">
    <div class="message">
        <span class="arrow"></span>
        <span class="body"> <?= $message; ?> </span>
    </div>
    <span class="datetime"> <?= $date; ?> ( UTC +0:00 )</span>
</li>