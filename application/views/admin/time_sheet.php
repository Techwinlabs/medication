    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/bootstrap-material-datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/accordian.css"/>
    <style>
        .dropdown-toggle{ width:200px!important; }
        .btn-group.bootstrap-select.form-control {box-shadow: none;}
    </style>
    <!-- start: Content -->
    <div id="content">
        <div class="col-md-12 top-20 padding-0">
            <div class="col-md-12">
                <div class="panel-outer-border panel-main">
                    <div class="panel-heading">
                        <h3 class="">Time Sheets</h3>
                    </div>
                    <div class="panel-body">
                        <?php
                            echo show_err_msg($this->session->flashdata('error_msg'));
                            echo show_succ_msg($this->session->flashdata('success_msg'));
                        ?>
                        <div class="row input">
                            <div class="col-md-12 search-by-date srchby-dtar">
                                <div class="col-md-10">
                                    <div class="col-sm-3">
                                        <div class="col-sm-12">
                                        <input type="text" id="startdate" class="input-m date" name="start" value="" placeholder="From">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="col-sm-12">
                                        <input type="text" id="enddate" class="input-md date" name="end" value="" placeholder="To">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group" id="carer_triger" onmouseover="loadCarer();">
                                            <select class="form-control selectpicker" data-live-search="true" id="load_carer" >
                                                <option value="0">-Select carer-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="col-sm-12">
                                        <button onclick="getTimeSheet()" class="btn btn-primary">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-md-12">
                                <div class="col-md-4 col-md-offset-4">
                                    <a onclick="getTimeSheet()" class="btn btn-primary">Submit</a>
                                </div>
                            </div> -->
                        </div>
                      <div class="col-md-12">
                            <div id="time_sheets"></div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div id="load_profile"></div>
    <!-- end: content -->
    <!-- plugins -->
    <script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/bootstrap-material-datetimepicker.js"></script>
    <!-- custom -->
    <script src="<?= base_url(); ?>asset/js/main.js"></script>
    <script src="<?= base_url(); ?>asset/js/append.js"></script>
    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                    panel.style.display = "none";
                } else {
                    panel.style.display = "block";
                }
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
           $('.date').bootstrapMaterialDatePicker({ weekStart : 0, time: false});
        });
    </script>
    </body>
    </html>