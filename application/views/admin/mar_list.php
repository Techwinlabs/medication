<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/bootstrap-material-datetimepicker.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap.min.css"/>
<!-- start: Content -->
<div id="content">
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="">Medication Records of <?= $name; ?></h3> <span class="pull-right new btn" onclick="goBack();">Back</span></div>
                <div class="panel-body">
                    <?php
						echo show_err_msg($this->session->flashdata('error_msg'));
						echo show_succ_msg($this->session->flashdata('success_msg'));
					?>
					<div class="col-md-12 search-by-date">
						<div class="col-md-10">
							<div class="col-sm-2"><b>Search by date</b> </div>
							<div class="col-sm-3">
								<div class="col-sm-12">
								<input type="text" id="startdate" class="input-md date" name="start" value="" placeholder="From">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="col-sm-12">
								<input type="text" id="enddate" class="input-md date" name="end" value="" placeholder="To">
								</div>
								
							</div>
							<div class="col-sm-4">
								<div class="col-sm-12">
    								<button onclick="searchMAR(<?= $client_id; ?>)" class="btn btn-primary">Search</button>	
    							</div>
						  </div>
					   </div>
                    </div>
					<div class="responsive-table" id="MAR">
					
						<table id="mar-table" class="table table-striped" width="100%" cellspacing="0" data-export-title="Medication Records of <?= $name; ?>">
							<thead>
								<tr>
									<th width="10%">Date</th>
									<th width="10%">Time</th>
									<th width="15%">Medicine</th>
									<th width="10%">Dose</th>
									<th width="10%">Status</th>
									<th width="10%">Staff ID</th>
									<th width="55%">Detail</th>
									
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="load_profile"></div>
<!-- end: content -->
<!-- plugins -->
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/bootstrap-material-datetimepicker.js"></script>
<script src="//cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<!-- custom -->
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
            var buttonCommon = {
                init: function (dt, node, config) {
                    var table = dt.table().context[0].nTable;
                    if (table) config.title = $(table).data('export-title')
                },
                title: 'default title'
            };
            $.extend( $.fn.dataTable.defaults, {
                "buttons": [
                    $.extend( true, {}, buttonCommon, {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    } ),
                    $.extend( true, {}, buttonCommon, {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        exportOptions: {
                            columns: ':visible'
                        }
                    } ),
                    $.extend( true, {}, buttonCommon, {
                        extend: 'print',
                        exportOptions: {
                            columns: ':visible'
                        },
                        orientation: 'landscape'
                    } ),
                    /*$.extend( true, {}, buttonCommon, {
                        extend: 'csv',
                        exportOptions: {
                            columns: ':visible'
                        },
                        orientation: 'landscape'
                    } )*/
                ]
            } );
            var dataTable =  $('#mar-table').DataTable( {
			serverSide: true,
			dom: '<"top"B>rt<"bottom"p><"clear">',
			bSort: false,
            ajax:{
                    url :"../../admin/ajax/getMARReports", // json datasource
                    type: "post",  // method  , by default get
                    data:{ "client_id":<?= $client_id; ?>  },
                    error: function(response){  // error handling
                        console.log(response);
                        $(".mar-table-error").html("");
                        $("#mar-table").append('<tbody class="mar-table-error"><tr><th colspan="6">No data found!</th></tr></tbody>');
                        $("#mar-table_processing").css("display","none");
                    },complete : function(){
                        $(".confirm").popConfirm();
                    }
                },
            } );
        } );
    </script>

<script type="text/javascript">
    $(document).ready(function() {
     	$('.date').bootstrapMaterialDatePicker({ weekStart : 0, time: false});
    });
</script>
</body>

</html>