    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/bootstrap-material-datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/accordian.css"/>
    <!-- start: Content -->
    <div id="content">
        <div class="col-md-12 top-20 padding-0">
            <div class="col-md-12">
                <div class="panel-outer-border panel-main">
                    <div class="panel-heading">
                        <h3 class="">Data usages</h3>
                    </div>
                    <div class="panel-body">
                        <?php
                            echo show_err_msg($this->session->flashdata('error_msg'));
                            echo show_succ_msg($this->session->flashdata('success_msg'));
                        ?>
                        <div class="row input">
                            <div class="col-md-12">
                               <!-- <div class="col-sm-2" style="padding-left: 0;"><b>Select Date Range</b> </div>
                                <div class="col-sm-1" style="padding-left: 0;">
                                    <span class="">Start:</stpan>
                                </div>
                                <div class="col-sm-2" style="padding-left: 0;">
                                    <div class="col-sm-12" style="padding-left: 0;">
                                        <input type="text" id="startdate" class="input-md date" name="start" value="" placeholder="From Date YYYY-MM-DD" max="<?= date('Y-m-d'); ?>">
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <span class="pull-right">End:</span>
                                </div>
                                <div class="col-sm-2">
                                    <div class="col-sm-12">
                                    <input type="text" id="enddate" class="input-md date" name="end" value="" placeholder="End Date YYYY-MM-DD" max="<?= date('Y-m-d'); ?>">
                                    </div>
                                </div>-->

                                <div class="col-sm-2" style="padding-left: 0;"><b>Select Date Range</b> </div>
                                <div class="col-sm-3" style="padding-left: 0;">
                                    <div class="col-sm-12" style="padding: 0;">
                                        <input type="text" id="startdate" class="input-md date" name="start" value="" placeholder="From" max="<?= date('Y-m-d'); ?>">
                                    </div>
                                </div>
                                <div class="col-sm-3" style="padding: 0;">
                                    <div class="col-sm-12" style="padding: 0;">
                                    <input type="text" id="enddate" class="input-md date" name="end" value="" placeholder="To" max="<?= date('Y-m-d'); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">&nbsp;</div>
                            <div class="col-md-12">
                                <div class="col-sm-2" style="padding-left: 0;"><b>Select Carer</b> </div>
                                <div class="col-sm-5" style="padding-left: 0;">
                                <div class="form-group" id="carer_triger" onmouseover="loadCarer();">
                                    <select class="form-control selectpicker" data-live-search="true" id="load_carer" >
                                        <option value="0">-Select carer-</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="col-sm-5 dtsub-btn">
                                    <a onclick="getDataUsage()" class="btn btn-primary">Submit</a>
                                </div>
                            </div>
                        </div>
                      <div class="col-md-12">
                            <div id="data_usage"></div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    <div id="load_profile"></div>
    <!-- end: content -->
    <!-- plugins -->
    <script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/bootstrap-material-datetimepicker.js"></script>
    <!-- custom -->
    <script src="<?= base_url(); ?>asset/js/main.js"></script>
    <script src="<?= base_url(); ?>asset/js/append.js"></script>
    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                    panel.style.display = "none";
                } else {
                    panel.style.display = "block";
                }
            });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.date').bootstrapMaterialDatePicker({ weekStart : 0, time: false});
        });
    </script>

    </body>
    </html>