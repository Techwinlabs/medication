    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/bootstrap-material-datetimepicker.css"/>
    <!-- start: Content -->
    <div id="content">
        <div class="col-md-12 top-20 padding-0">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="">Notifications</h3>
                    </div>
                    <div class="panel-body">
                        <?php
                            echo show_err_msg($this->session->flashdata('error_msg'));
                            echo show_succ_msg($this->session->flashdata('success_msg'));
                        ?>
                        <div class="row input">
                            <div class="col-md-12 notification-filter">
                                <div class="col-sm-2 notif-hdg"><b>Notification Filter</b> </div>
                                <div class="col-sm-6">
                                <div class="form-group" id="carer_triger" >
                                    <select class="form-control"  id="notifications" onchange="getfilteredNotifications(this.value)" >
                                        <option value="1">All open concerns</option>
                                        <option value="2">All closed concerns</option>
                                        <option value="0">All open & closed Concerns</option>
                                        
                                    </select>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" id="view_notifications">
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div id="load_profile"></div>
    <!-- end: content -->
    <!-- plugins -->
    <script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/bootstrap-material-datetimepicker.js"></script>
    <!-- custom -->
    <script src="<?= base_url(); ?>asset/js/main.js"></script>
    <script src="<?= base_url(); ?>asset/js/append.js"></script>

    <script>
        $(function(){
            getfilteredNotifications(1);
        });
    </script>
 
    </body>

</html>