<?php

    require_once(APPPATH.'libraries/fpdf.php');
    
    class PDF extends FPDF
    {
        
        function DailyRecord($header, $data)
        {
            // Column widths
            $w = array(30, 25, 30, 110);
            // Header
            for($i=0;$i<count($header);$i++)
                $this->Cell($w[$i],7,$header[$i],1,0,'C');
            $this->Ln();
            // Data
            foreach($data as $row)
            {
                $this->Cell($w[0],6,$row[0],'LR');
                $this->Cell($w[1],6,$row[1],'LR');
                $this->Cell($w[2],6,($row[2]),'LR');
                $this->Cell($w[3],6,($row[3]),'LR');
                $this->Ln();
            }
            // Closing line
            $this->Cell(array_sum($w),0,'','T');
        }
        
        function Medication_Record($header, $data)
        {
            // Column widths
            $w = array(30, 25, 25, 25 ,25 ,60);
            // Header
            for($i=0;$i<count($header);$i++)
                $this->Cell($w[$i],7,$header[$i],1,0,'C');
            $this->Ln();
            // Data
            foreach($data as $row)
            {
                $this->Cell($w[0],6,$row[0],'LR');
                $this->Cell($w[1],6,$row[1],'LR');
                $this->Cell($w[2],6,($row[2]),'LR');
                $this->Cell($w[3],6,($row[3]),'LR');
                $this->Cell($w[4],6,($row[4]),'LR');
                $this->Cell($w[5],6,($row[5]),'LR');
                $this->Ln();
            }
            // Closing line
            $this->Cell(array_sum($w),0,'','T');
        }
        
        function Title($title)
        {
            // Arial 12
            $this->SetFont('Arial','',12);
            // Background color
            $this->SetFillColor(200,220,255);
            // Title
            $this->Cell(0,6,"$title",0,1,'L',true);
            // Line break
            $this->Ln(4);
        }
                
    }
?>
