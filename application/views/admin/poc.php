<?php if($data['POC']){ ?>
<style>
<!-- Nav CSSS !-->
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #ffffff;
	border-image: none;
    border-style: solid;
    border-width: 1px;
    color: #555555;
    cursor: default;
}

.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #ffffff;
    border-image: none;
    border-style: solid;
    border-width: 1px;
    color: #418bbb;
    cursor: default;
    width: 100px;
    height: 50px;
    text-align: center;
    padding-top: 15px;margin-left: -1px;border-radius: 0px;
}
.nav-tabs > li > a{
	color:#333;
   
    width: 100px;
    height: 50px;
	text-align: center;padding-top: 15px;
}

.nav-tabs > li > a:hover {
   
    width: 100px;
    height: 50px;padding-top: 15px;
}
.tab-content{
	margin-top:0px;
}

div#home { margin-top: 0px;}
#menu1 {
    margin-top: 17px;
}

#menu2 {
    margin-top: 17px;
}
.bg-all {
    background-color: #fff;
	border: 1px solid rgba(54, 64, 74, 0.08);
    border-radius: 1px;
    box-shadow: 0 4px 4px 0 #ccc;
}

.widget-simple-chart.card-box-one.top-no-border {
    border-top: none;
    border-radius: 0 5px 5px 5px;
    padding: 10px 20px 10px 25px;
}

</style>
<div class="modal-body">
	<div class="row">
		<div class="col-sm-12">
			<div class="form-group">				
				<label class="control-label text-right"><h5>Instructions</h5></label>
				<textarea readonly type="text" name="comments" style="resize:none;" class="form-control default" rows="4" placeholder="Instruction about care."><?= $data['comments']; ?></textarea>
			</div>
		</div>
		<div class="col-sm-12">
			 <ul class="nav nav-tabs " style="margin-left:1px">
				<li class="active"><a data-toggle="tab" href="#am">AM</a></li>
				<li class="tab-wh"><a data-toggle="tab" href="#noon">Noon</a></li>
				<li class="tab-wh"><a data-toggle="tab" href="#evening">Evening</a></li>
				<li class="tab-wh"><a data-toggle="tab" href="#night">Night</a></li>
			</ul>
			<div class="tab-content">
				<div id="am" class="tab-pane fade in active">
					<div class="col-md-12  table-responsive">
						<table class="table table-striped" >
							<tbody>
					<?php	if(count($data['am_time_care_program'])){ ?>
					<?php	for($a=0;$a<count($data['am_time_care_program']);$a++){ 
								if ($a%2 == 0) 
									$ac = 'style="background-color:#f6f6f6;"';
								else 
									$ac = 'style="background-color:#f6f6f6;"';
					?>
								<tr <?= $ac; ?>>
									<td><b>Medicine Name : &nbsp;</b><?= $data['am_time_care_program'][$a]['medicine']; ?><?= !empty($data['am_time_care_program'][$a]['type']) ? '('.$data['am_time_care_program'][$a]['type'].')':""; ?><br /><b>Time : &nbsp;</b><?= @$data['am_time_care_program'][$a]['time']?></br><b>Dose : &nbsp;</b><?= @$data['am_time_care_program'][$a]['dose']?></br><b>Detail : &nbsp;</b><?= @$data['am_time_care_program'][$a]['detail']?></td>
								</tr>
					<?php	} ?>
					<?php	}else{ ?>
								<tr >
									<td rowspan="2">No Medicine</td>
								</tr>
					<?php	} ?>
							</body>
						</table>		
					</div>
				</div>
				<div  id="noon" class="tab-pane fade"> 
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped">
								<tbody>
						<?php	if(count($data['noon_time_care_program'])){ ?>
						<?php	for($n=0;$n<count($data['noon_time_care_program']);$n++){ 
									if ($n%2 == 0) 
										$nc = 'style="background-color:#f6f6f6;"';
									else 
										$nc = 'style="background-color:#f6f6f6;"';
						?>
									<tr <?= $nc; ?>>
										<td><b>Medicine Name : &nbsp;</b><?= $data['noon_time_care_program'][$n]['medicine']?><?= !empty($data['noon_time_care_program'][$n]['type']) ? '('.$data['noon_time_care_program'][$n]['type'].')' :""; ?><br /><b>Time : &nbsp;</b><?= @$data['noon_time_care_program'][$n]['time']?><br /><b>Dose : &nbsp;</b><?= @$data['noon_time_care_program'][$n]['dose']?><br /><b>Detail : &nbsp;</b><?= @$data['noon_time_care_program'][$n]['detail']?></td>
									</tr>
						<?php	} ?>
						<?php	}else{ ?>
									<tr >
										<td rowspan="4">No Medicine</td>
									</tr>
						<?php	} ?>
								</tbody>
							</table>	
						</div>
					</div>
				</div>
				<div  id="evening" class="tab-pane fade"> 
					<div class="row">
						<div class="col-md-12  table-responsive">
							<table class="table table-striped" >
								<tbody>
						<?php	if(count($data['tea_time_care_program'])){ ?>
						<?php	for($e=0;$e<count($data['tea_time_care_program']);$e++){ 
									if ($e%2 == 0) 
										$ec = 'style="background-color:#f6f6f6;"';
									else 
										$ec = 'style="background-color:#f6f6f6;"';
						?>
									<tr <?= $ec; ?>>
										<td><b>Medicine Name : &nbsp;</b><?= $data['tea_time_care_program'][$e]['medicine']?><?= !empty($data['tea_time_care_program'][$e]['type']) ? '('.$data['tea_time_care_program'][$e]['type'].')' : ''; ?><br /><b>Time : &nbsp;</b><?= @$data['tea_time_care_program'][$e]['time']?><br /><b>Dose : &nbsp;</b><?= @$data['tea_time_care_program'][$e]['dose']?><br /><b>Detail : &nbsp;</b><?= @$data['tea_time_care_program'][$e]['detail']?></td>
									</tr>
						<?php	} ?>
						<?php	}else{ ?>
									<tr >
										<td rowspan="2">No Medicine</td>
									</tr>
						<?php	} ?>
								</tbody>
							</table>	
						</div>
					</div>
				</div>
				<div  id="night" class="tab-pane fade"> 
					<div class="row">
						<div class="col-md-12  table-responsive">
							<table class="table table-striped" >
								<tbody>
						<?php	if(count($data['night_time_care_program'])){ ?>
						<?php	for($nt=0;$nt<count($data['night_time_care_program']);$nt++){ 
									if ($nt%2 == 0) 
										$ntc = 'style="background-color:#f6f6f6;"';
									else 
										$ntc = 'style="background-color:#f6f6f6;"';
						?>
									<tr <?= $ntc; ?>>
										<td><b>Medicine Name : &nbsp;</b><?= $data['night_time_care_program'][$nt]['medicine']; ?><?= !empty($data['night_time_care_program'][$nt]['type']) ? '('.$data['night_time_care_program'][$nt]['type'].')' :''; ?><br /><b>Time : &nbsp;</b><?= @$data['night_time_care_program'][$nt]['time']?><br /><b>Dose : &nbsp;</b><?= @$data['night_time_care_program'][$nt]['dose']?><br /><b>Detail : &nbsp;</b><?= @$data['night_time_care_program'][$nt]['detail']?></td>
									</tr>
						<?php	} ?>
						<?php	}else{ ?>
									<tr >
										<td rowspan="2">No Medicine</td>
									</tr>
						<?php	} ?>
								</thead>
							</table>	
						</div>
					</div>
				</div>
				<hr />
				<?php
				$section_roles = explode(',',ltrim(rtrim($this->userdata['section_roles'],','),','));
				if(in_array(1,$section_roles) && $this->userdata['employee_type']==2){ ?>
					<a href="<?= base_url()?>admin/update_care_program/<?= $data['client_id']; ?>" class="submit btn btn-primary" >Update Medication<a>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php } else { ?>
<div class="modal-body">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<p>Program of care not updated Yet.</p>
			<p><a href="" class="submit btn btn-primary" >Update Program<a></p>
		</div>
	</div>
</div>



<?php } ?>							