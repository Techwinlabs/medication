<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Medication Admin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= base_url(); ?>images/favicon.ico">

        <!-- App css -->
        <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    </head>
	<body class="bg-accpunt-pages">
		<!-- HOME -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">
                            
                            <div class="account-pages">
                                 <div class="login-text m-t-50">                                           
                                       <h2 class="text-center login-1">Login</h2>                                           
                                        </div>
                                 <div class="clearfix"></div>
                                <div class="account-box m-t-50">                                   
                                    <div class="account-logo-box">                                        
                                        <h6 class="text-center font-size ">Medication Admin</h6>
                                    </div>
                                    <div class="account-content">
                                       <form action="#">
                                            <div class="form-group">                                                                     
                                              <input type="email" class="form-control user" id="username" placeholder="Username" name="username">
                                            </div>
                                            <div class="form-group">                                             
                                              <input type="password" class="form-control user" id="pwd" placeholder="Password" name="pwd">
                                            </div>
                                            
                                            <button type="submit" class="btn btn-default login">Login</button>
                                          </form>
                                        <div class="row m-t-50">
                                            <div class="col-sm-12 text-center">
                                                <p class="text-muted"><a href="#" class="forgot">Forgot  Your Password</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end card-box-->
						</div>
                        <!-- end wrapper -->
					</div>
                </div>
            </div>
        </section>
        <!-- END HOME -->
		<!-- jQuery  -->
		
        <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>       
        <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
		<script src="<?= base_url(); ?>assets/js/modernizr.min.js"></script>
    </body>
</html>