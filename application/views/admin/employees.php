<!-- start: Content -->

<div id="content">

	<div class="col-md-12 top-20 padding-0">

		<div class="col-md-12">

			<div class="panel">
				<?php	$section_roles = explode(',',ltrim(rtrim($this->userdata['section_roles'],','),','));	?>	
				<div class="panel-heading">
					<h3 class="">Manage Employees</h3>
					<?php if(in_array(3,$section_roles) || $this->userdata['employee_type']==2 ){ ?>
						<a href="new_employee?type=1" class="pull-right new">+ New Employee</a>
					<?php } ?>
				</div>

				<div class="panel-body">

					<?php

						echo show_err_msg($this->session->flashdata('error_msg'));

						echo show_succ_msg($this->session->flashdata('success_msg'));

					?>

					<ul class="nav nav-tabs " style="margin-left:1px">

						<li data-value='1' class="<?php if($employee_designation==1){ echo 'active'; }else{ echo 'tab-wh'; } ?> pre-select"><a data-toggle="tab" href="#carer">Carer</a></li>

						

						<!---if superviser login --->

							

				<?php	if($this->userdata['employee_type']==2){		?>

						<li data-value='3' class="<?php if($employee_designation==3){ echo 'active'; }else{ echo 'tab-wh'; } ?> pre-select"><a data-toggle="tab" href="#supervisor">Supervisor</a></li>

						<li data-value='2'class="<?php if($employee_designation==2){ echo 'active'; }else{ echo 'tab-wh'; } ?> pre-select"><a data-toggle="tab" href="#manager">Manager</a></li>

				<?php	}	?>		

						

						<!--<li class="tab-wh"><a data-toggle="tab" href="#family">Family</a></li>

						<li class="tab-wh"><a data-toggle="tab" href="#outsideuser">Outside User</a></li>-->

					</ul>

					<br />

					<div class="tab-content">

						<div class="tab-pane fade <?php if($employee_designation==1){ echo 'in active'; } ?>" id="carer">

							<div class="responsive-table">

								<table id="datatables-carer" class="table table-striped table-bordered" width="100%" cellspacing="0">

									<thead>

										<tr>

											<th>Employee</th>

											<th>Email</th>

											<th>Phone</th>

											<th>Role</th>

											<th width="5%"></th>

										</tr>

									</thead>

									<tbody>

									<?php $i=0; ?>

									<?php	foreach($data as $row){ ?>

										<tr id="emp<?= $i; ?>">

											<td>

											<?= $row['employee_picture'] ? '<img src="'.base_url().$row['employee_picture'].'" height="50px" width="50px" onerror="this.src=\''.base_url().'asset/img/dummy.svg\'" >' : '<img src="'.base_url().'asset/img/dummy.svg" height="50px" width="50px">' ?>&emsp;<b><?= $row['employee_name']; ?></b> (

											<?= $row['employee_number']; ?>)</td>

											<td>

												<?= $row['employee_email']; ?>

											</td>

											<td>+

											<?= $row['employee_country_code']; ?>

											<?= $row['employee_mobile']; ?>

											</td>

											<td>

											<?= $row['employee_type']== 1 ? "<b>Employee</b>":"<b>Manager</b>"; ?></td>

											<td>
											<?php if(in_array(3,$section_roles) || $this->userdata['employee_type']==2 ){ ?>
												<p><a href="<?= base_url('admin/update_employee/'.$row['employeeid'].''); ?>"><i class="fa fa-edit" title="Edit"></i></a><!--&emsp;<a href="" title="Preview" data-toggle="modal" data-target="#profile<!?= $i; ?>" onclick="viewProfile_emp('carer<!?= $i; ?>',<?= $row['employeeid'];?>,1)"><i class="fa fa-eye" style="font-size:15px"></i></a--><br><a onclick="delete_employee(<?= $i; ?>,<?= $row['employeeid'];?>,1);" class="confirm" title="<b>Are you sure to delete!</b>"><i class="fa fa-trash-o" title="Delete"></i></a><br><?= ($row['is_active']== 1) ? '<span id="archive'.$row["employeeid"].'"><a href="" onclick="archive_unarchive('.$row["employeeid"].',1)" class="confirm" title="<b>Are you sure to archive!</b>"><i class="fa fa-archive" title="Archive"></i></a></span>' : '<span id="archive'.$row["employeeid"].'"><a href="" onclick="archive_unarchive('.$row["employeeid"].',2)" class="confirm" title="<b>Are you sure to unarchive!</b>"><i class="fa fa-undo"></i></a></span>'; ?>
											</p>
											<?php } ?>

											</td>

										</tr>

									<?php $i++; ?>

									<?php	}	?>

									</tbody>

								</table>

							</div>

						</div>

						<div class="tab-pane fade <?php if($employee_designation==2){ echo 'in active'; } ?>" id="manager" >

							<div class="responsive-table">

								<table id="datatables-manager" class="table table-striped table-bordered" width="100%" cellspacing="0">

									<thead>

									<tr>

										<th>Employee</th>

										<th>Email</th>

										<th>Phone</th>

										<th>Role</th>

										<th width="5%"></th>

									</tr>

									</thead>

									<tbody>

									<?php $i=0; ?>

									<?php	foreach($manager as $row){ ?>

										<tr id="man<?= $i; ?>">

											<td>

											<?= $row['employee_picture'] ? '<img src="'.base_url().$row['employee_picture'].'" height="50px" width="50px" onerror="this.src=\''.base_url().'asset/img/dummy.svg\'" >' : '<img src="'.base_url().'asset/img/dummy.svg" height="50px" width="50px">' ?>&emsp;<b><?= $row['employee_name']; ?></b> (

											<?= $row['employee_number']; ?>)</td>

											<td>

											<?= $row['employee_email']; ?>

											</td>

											<td>+

											<?= $row['employee_country_code']; ?>

											<?= $row['employee_mobile']; ?>

											</td>

											<td>

											<?= $row['employee_type']== 1 ? "<b>Employee</b>":"<b>Manager</b>"; ?></td>

											<td>

											
											<?php if($row['addedBy_role_type'] != 1){ ?>
												<p><a href="<?= base_url('admin/update_employee/'.$row['employeeid'].''); ?>" title="Edit"><i class="fa fa-edit"></i></a><br>
												<!--&emsp;<a href="" data-toggle="modal" data-target="#profile<!?= $i; ?>" onclick="viewProfile_emp('manager<!?= $i; ?>',<!?= $row['employeeid'];?>,1)" title="Preview"><i class="fa fa-eye" style="font-size:15px"></i></a-->
											
												<a onclick="delete_employee(<?= $i; ?>,<?= $row['employeeid'];?>,2);" class="confirm" title="<b>Are you sure to delete!</b>"><i class="fa fa-trash-o" title="Delete"></i>
												</a><br><?= ($row['is_active']== 1) ? '<span id="archive'.$row["employeeid"].'"><a href="" onclick="archive_unarchive('.$row["employeeid"].',1)" class="confirm" title="<b>Are you sure to archive!</b>"><i class="fa fa-archive" title="Archive"></i></a></span>' : '<span id="archive'.$row["employeeid"].'"><a href="" onclick="archive_unarchive('.$row["employeeid"].',2)" class="confirm" title="<b>Are you sure to unarchive!</b>"><i class="fa fa-undo"></i></a></span>'; ?>
											<?php } else { echo ""; }?>
											</p>
											</td>

										</tr>

									<?php $i++; ?>

									<?php	}	?>

									</tbody>

								</table>

							</div>

						</div>

						<div class="tab-pane fade <?php if($employee_designation==3){ echo 'in active'; } ?>" id="supervisor">

							<div class="responsive-table">

								<table id="datatables-supervisor" class="table table-striped table-bordered" width="100%" cellspacing="0">

									<thead>

										<tr>

											<th>Employee</th>

											<th>Email</th>

											<th>Phone</th>

											<th>Role</th>

											<th width="5%"></th>

										</tr>

									</thead>

									<tbody>

									<?php $i=0; ?>

									<?php	foreach($supervisor as $row){ ?>

										<tr id="sup<?= $i; ?>">

											<td>

											<?= $row['employee_picture'] ? '<img src="'.base_url().$row['employee_picture'].'" height="50px" width="50px" onerror="this.src=\''.base_url().'asset/img/dummy.svg\'" >' : '<img src="'.base_url().'asset/img/dummy.svg" height="50px" width="50px">' ?>&emsp;<b><?= $row['employee_name']; ?></b> (

											<?= $row['employee_number']; ?>)</td>

											<td>

												<?= $row['employee_email']; ?>

											</td>

											<td>+

												<?= $row['employee_country_code']; ?>

												<?= $row['employee_mobile']; ?>

											</td>

											<td>

												<?= "<b>Supervisor</b>" ?>

											</td>

											<td>

												<p><a href="<?= base_url('admin/update_employee/'.$row['employeeid'].''); ?>" title="Edit"><i class="fa fa-edit"></i></a><!--&emsp;<a href="" data-toggle="modal" data-target="#profile<!?= $i; ?>" onclick="viewProfile_emp('supervisor<!?= $i; ?>',<!?= $row['employeeid'];?>,1)" title="Preview"><i class="fa fa-eye" style="font-size:15px"></i></a--><br><a onclick="delete_employee(<?= $i; ?>,<?= $row['employeeid'];?>,3);" class="confirm" title="<b>Are you sure to delete!</b>"><i class="fa fa-trash-o" title="Delete"></i></a><br><?= ($row['is_active']== 1) ? '<span id="archive'.$row["employeeid"].'"><a href="" onclick="archive_unarchive('.$row["employeeid"].',1)" class="confirm" title="<b>Are you sure to archive!</b>"><i class="fa fa-archive" title="Archive"></i></a></span>' : '<span id="archive'.$row["employeeid"].'"><a href="" onclick="archive_unarchive('.$row["employeeid"].',2)" class="confirm" title="<b>Are you sure to unarchive!</b>"><i class="fa fa-undo"></i></a></span>'; ?></p>

											</td>

										</tr>

									<?php $i++; ?>

									<?php	}	?>

									</tbody>

								</table>

							</div>

						</div>

						<!--<div class="tab-pane fade" id="family">

							<div class="responsive-table">

								<table id="datatables-family" class="table table-striped table-bordered" width="100%" cellspacing="0">

									<thead>

										<tr>

											<th>Employee</th>

											<th>Email</th>

											<th>Phone</th>

											<th>Role</th>

											<th width="10%"></th>

										</tr>

									</thead>

								<tbody>

								<?php $i=0; ?>

									<?php	foreach($family as $row){ ?>

										<tr>

											<td>

											<?= $row['employee_picture'] ? '<img src="'.base_url().$row['employee_picture'].'" height="50px" width="50px" onerror="this.src=\''.base_url().'asset/img/dummy.svg\'" >' : '<img src="'.base_url().'asset/img/dummy.svg" height="50px" width="50px">' ?>&emsp;<b><?= $row['employee_name']; ?></b> (

											<?= $row['employee_number']; ?>)</td>

											<td>

												<?= $row['employee_email']; ?>

											</td>

											<td>+

												<?= $row['employee_country_code']; ?>

												<?= $row['employee_mobile']; ?>

											</td>

											<td>

												<?= $row['employee_type']== 1 ? "<b>Employee</b>":"<b>Manager</b>"; ?>

											</td>

											<td>

												<p><a href=""><i class="fa fa-edit"></i></a>&emsp;<a href="" data-toggle="modal" data-target="#profile<?= $i; ?>" onclick="viewProfile_emp('family<?= $i; ?>',<?= $row['employeeid'];?>,1)"><i class="fa fa-eye" style="font-size:15px"></i></a>&emsp;<a href="" class="confirm" title="<b>Are you sure to delete!</b>"><i class="fa fa-trash-o"></i></a>

											</td>

										</tr>

									<?php $i++; ?>

									<?php } ?>

								</tbody>

								</table>

								<a href="new_employee" class="float">

									<i class="fa fa-plus my-float"></i>

								</a>

							</div>

						</div>

						<div class="tab-pane fade" id="outsidesuer">

							<div class="responsive-table">

								<table id="datatables-outsideuser" class="table table-striped table-bordered" width="100%" cellspacing="0">

									<thead>

										<tr>

											<th>Employee</th>

											<th>Email</th>

											<th>Phone</th>

											<th>Role</th>

											<th width="10%"></th>

										</tr>

									</thead>

									<tbody>

									<?php $i=0; ?>

									<?php	foreach($outsideuser as $row){ ?>

										<tr>

											<td>

											<?= $row['employee_picture'] ? '<img src="'.base_url().$row['employee_picture'].'" height="50px" width="50px" onerror="this.src=\''.base_url().'asset/img/dummy.svg\'" >' : '<img src="'.base_url().'asset/img/dummy.svg" height="50px" width="50px">' ?>&emsp;<b><?= $row['employee_name']; ?></b> (

											<?= $row['employee_number']; ?>)</td>

											<td>

												<?= $row['employee_email']; ?>

											</td>

											<td>+

												<?= $row['employee_country_code']; ?>

												<?= $row['employee_mobile']; ?>

											</td>

											<td>

												<?= $row['employee_type']== 1 ? "<b>Employee</b>":"<b>Manager</b>"; ?>

											</td>

											<td>

												<p><a href=""><i class="fa fa-edit"></i></a>&emsp;<a href="" data-toggle="modal" data-target="#profile<?= $i; ?>" onclick="viewProfile_emp('outside<?= $i; ?>',<?= $row['employeeid'];?>,1)"><i class="fa fa-eye" style="font-size:15px"></i></a>&emsp;<a href="" class="confirm" title="<b>Are you sure to delete!</b>"><i class="fa fa-trash-o"></i></a>

											</td>

										</tr>

									<?php $i++; ?>

									<?php } ?>

									</tbody>

								</table>

								<a href="new_employee" class="float">

									<i class="fa fa-plus my-float"></i>

								</a>

							</div>

						</div>-->

					</div>

				</div>

			</div>

		</div>

	</div>

	<div id="load_profile"></div>

</div>

<!-- end: content -->

<!-- plugins -->

<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>

<script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>

<script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>

<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>



<!-- custom -->

<script src="<?= base_url(); ?>asset/js/main.js"></script>

<script src="<?= base_url(); ?>asset/js/append.js"></script>

<script type="text/javascript">

$(document).ready(function() {

	$('#datatables-carer').DataTable({

		dom: '<"top"f>rt<"bottom"p><"clear">',

		bSort: false

	});

	$('#datatables-manager').DataTable({

		dom: '<"top"f>rt<"bottom"p><"clear">',

		bSort: false

	});

	$('#datatables-supervisor').DataTable({

		dom: '<"top"f>rt<"bottom"p><"clear">',

		bSort: false

	});

	//$('#datatables-family').DataTable({

	//	dom: '<"top">rt<"bottom"p><"clear">'

	//});

	//$('#datatables-outsideuser').DataTable({

	//	dom: '<"top"f>rt<"bottom"p><"clear">'

	//});

});

</script>

<script>

$('.pre-select').click(function(){

	var select =$(this).attr('data-value');

	$('.new').attr('href','new_employee?type='+select);

});

</script>

</body>

</html>