<link rel="stylesheet" type="text/css" href="<?= base_url('asset/css/bootstrap-select.css'); ?>">
<script src="<?= base_url('asset/js/bootstrap-select.js'); ?>"></script>
<style>
	.dropdown-display {
		white-space: nowrap;
		padding: 11px 20px 6px 12px !important;
	}
	.dropdown-chose-list{
		display: none;
	}
	.bootstrap-select{
		width : 97% ! important;
	}
	.bootstrap-select.form-animate-error{
		border: 1px solid red;
		height: 34px;
	}
</style>
	<div id="content">
		<div class="panel box-shadow-none content-header">
			<div class="panel-body">
				<div class="col-md-12">
					<h3>New Employee</h3>
				</div>
			</div>
		</div>
		<div class="form-element">
			<div class="col-md-10">
				<div class="col-md-12 panel">
					<div class="col-md-12 panel-body" style="padding-bottom:30px;">
						<div class="col-md-12">
							<?php
								echo show_err_msg($error_msg);
							?>
	<?php if(activePlanLimit($company_id,1)){	?>						
							<form class="cmxform" id="new-employee" method="POST" action="" enctype="Multipart/Form-data">
								<input type="hidden" name="company_id" value="<?= $company_id; ?>">
								<div class="col-md-6">
									<div class="form-group form-animate-text" style="margin-top:40px !important;">
									<input type="text" class="form-text" id="employee_name" name="employee_name" value="<?php echo set_value('user_name'); ?>" required>
									<span class="bar"></span>
									<label>Employee Name <span style="color: #D8000C;">*</span></label>
									</div>
									<div class="form-group form-animate-text" style="margin-top:40px !important;">
									<input type="text" class="form-text" id="employee_number" name="employee_number" value="<?php echo set_value('employee_number'); ?>" required>
									<span class="bar"></span>
									<label>Employee Number <span style="color: #D8000C;">*</span></label>
									</div>
									<div class="form-group form-animate-text country" style="margin-top:40px !important;">
										<select class="selectpicker" required id="employee_country_code" name="employee_country_code" required data-live-search="true" onchange="clear_country_border(this.value);">
											<option value="">--- Select Country Code ---</option>
											<option value="44">United Kingdom (+44)</option>
											<?php	for($i=0;$i<count($country_code);$i++){ if($country_code[$i]['country_code']=='44'){}else{ ?>
												<option value="<?= $country_code[$i]['country_code']; ?>" ><?= $country_code[$i]['countries_name']." (+".$country_code[$i]['country_code'].")"; ?></option>
											<?php } } ?>
										</select>
										<span style="color: #D8000C;">*</span>
									</div>
									<div class="form-group form-animate-text" style="margin-top:40px !important;">
										<input type="number" class="form-text" minlength="10" maxlength="10" title="Only 10 digits"  id="employee_mobile" name="employee_mobile" required >
										<span class="bar"></span>
										<label>Mobile Number <span style="color: #D8000C;">*</span></label>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group form-animate-text" style="margin-top:40px !important;">
										<input type="text" class="form-text" id="employee_email" name="employee_email" value="<?php echo set_value('employee_email'); ?>" required>
										<span class="bar"></span>
										<label>Email <span style="color: #D8000C;">*</span></label>
									</div>
									<div class="form-group form-animate-text designation" style="margin-top:40px !important;">
										<select class="selectpicker" required id="employee_designation" name="employee_designation" required  data-live-search="true" onchange="clear_designation_border(this.value);get_roles(this.value);">
											
											<!---if superviser login --->
								<?php	$section_roles = explode(',',ltrim(rtrim($this->userdata['section_roles'],','),','));	?>				
								<?php	if($this->userdata['employee_type']==2){		?>
									<option value="">--- Select Designation  ---</option>
									<?php for($x=0;$x<count($designations);$x++){ ?>
												<option value="<?= $designations[$x]['designation_id']; ?>" <?php if($designations[$x]['designation_id']==$position){ echo "SELECTED"; } ?> ><?= $designations[$x]['designation']; ?></option>
									<?php } ?>
								<?php	}else{	?>
												<option value="1" >Carer</option>
								<?php 	} 	?>				
											
										</select>
										<span style="color: #D8000C;">*</span>
										<span class="bar"></span>
									</div>
									<div class="form-group form-animate-text section_roles access" style="margin-top:40px !important; display:none;">
										<select class="selectpicker1" required required name="section_roles[]" multiple onchange="clear_access_border(this.value);">
											<option value="1">Medication Creation Or Modification</option>
											<option value="2">Schedule Creation Or Modification</option>
											<option value="3">User Creation Or Modification</option>
											<option value="4">Profile Creation Or Modification</option>
										</select>
										<span style="color: #D8000C;">*</span>
									</div>
									<div class="form-group form-animate-text" style="margin-top:40px !important;">
										<input type="file" class="form-text" accept="image/*" id="employee_picture" name="employee_picture" >
										<span class="bar">Upload Profile picture (Max size allowed 1MB)</span>
										<label></label>
									</div>
									
									<div class="form-group form-animate-text" style="margin-top:40px !important;">
										<label >Gender</label><br /><br />
										<input type="radio" required name="gender" value="1"> Male &emsp;
										<input type="radio" required name="gender" value="2"> Female
										
										
									</div>
									
								</div>
								<div class="col-md-12">
								<input class="submit btn btn-primary" name='submit' type="submit" value="Submit">&emsp;<button type="button" class="btn btn-sm btn-danger" onclick="window.history.back();">Cancel</button>
								</div>
							</form>
		<?php 	}else{	 ?>
			<p style="font-weight: 550; color: #af5959; text-align: center;">Maximum users allocated! Please call support to increase allocation.</p>
		<?php 	}	?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
	<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
	<script src="<?= base_url(); ?>asset/js/plugins/jquery.validate.min.js"></script>
	<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		
		$(".selectpicker1").selectpicker({
			noneSelectedText : '--- Access Rights ---'
		});

		$("#new-employee").validate({
			errorElement: "em",
			errorPlacement: function(error, element) {
			$(element.parent("div").addClass("form-animate-error"));
				error.appendTo(element.parent("div"));
			},
			success: function(label) {
				$(label.parent("div").removeClass("form-animate-error"));
			},
			rules: {
				employee_name: "required",
				employee_number: "required",
				employee_mobile: "required",
				employee_country_code: "required",
				employee_designation:"required",
				validate_username: {
					required: true,
				},
				employee_email: {
					required: true,
					email: true
				}
			},
			messages: {
				employee_name: "Please enter employee name",
				employee_number: "Please enter employee number",
				validate_username: {
					required: "Please enter a username",
				},
				employee_email: "Please enter a valid email address",
				employee_mobile: "Please enter valid mobile number",
				employee_country_code: "Please select country code",
				employee_designation : "Please select any of the designation"
			}
		});
	});
	
	function get_roles(args) {
        if (args == 3) {
            $('.section_roles').css('display','block');
        }else{
			$('.section_roles').css('display','none');
		}
    }
	
	function clear_country_border(val) {
		if (val != '') {
            $('div.country div.bootstrap-select.form-animate-error').css('border','none');
        }        
    }
	
	function clear_designation_border(val) {
		if (val != '') {
			$('div.designation div.bootstrap-select.form-animate-error').css('border','none');
		}
    }
	
	function clear_access_border(val) {
		if (val != '') {
			$('div.access div.bootstrap-select.form-animate-error').css('border','none');
		}
    }
	
	$( document ).ready(function() {
		var a = $('#employee_designation').val();
		if(a==3){
			$('.section_roles').css('display','block');
		}	
	});
	</script>