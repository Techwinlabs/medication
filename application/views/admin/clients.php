<div id="content">
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="">Manage Clients</h3>
					<?php
						$section_roles = explode(',',ltrim(rtrim($this->userdata['section_roles'],','),','));
						if(in_array(4,$section_roles) || $this->userdata['employee_type']==2){
					?>
							<a href="new_client" class="pull-right new">+ New Client</a>
						<?php } ?>
				</div>
                <div class="panel-body">
                    <?php
						echo show_err_msg($this->session->flashdata('error_msg'));
						echo show_succ_msg($this->session->flashdata('success_msg'));
					?>
					<div class="responsive-table">
						<table id="table-grid" class="table table-striped display" width="100%" cellspacing="0">
							<thead style="display:none;">
								<tr>
									<th width="45%">Client</th>
									<th width="10%">Profile</th>
									<th width="10%">DailyRecords</th>
									<th width="10%">MAR</th>
									<th width="10%">Program</th>
									<th width="10%">Edit</th>
									<th width="5%">Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
                </div>
            </div>
        </div>
    </div>
	<div id="load_profile"></div>
	<div id="poc_mode" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Program of Care</h4>
				</div>
				<div id="POC"></div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="changepassword" role="dialog">
		<div class="modal-dialog md-1">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Change Password</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" method="post"  action="<?= base_url(); ?>admin/Profile/changePassword">
						<input type="hidden" name="employeeid" value="<?= $this->session->userdata()['userdata']['employeeid']; ?>">
						<fieldset>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Old Password</label>
								<div class="col-md-8">
									<input id="textinput" name="oldpassword" type="password" placeholder="Old Password" autocomplete="false" class="form-control input-md" required="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">New Password</label>
								<div class="col-md-8">
									<input id="textinput" name="newpassword" type="password" placeholder="New Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter and at least 8 or more characters" autocomplete='false' class="form-control input-md">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Confirm Password</label>
								<div class="col-md-8">
									<input id="textinput" name="confirmpassword" type="password" placeholder="Confirm Password" autocomplete="false" class="form-control input-md">
								</div>
							</div>
							 <span style="font-size:10px;color:red;">(Note: Password must contain at least one number and one uppercase and lowercase letter and at least 8 or more characters)</span>
							<div class="form-group">
								<br />
								<label class="col-md-4 control-label" for="btnsave">Change Password</label>
								<div class="col-md-8">
									<button id="btnsave" name="p_submit" class="submit btn btn-primary yes" type="submit" value="submit">Yes</button>&nbsp;&nbsp;
									<button id="btncancel" name="btncancel" class="btn btn-warning no" data-dismiss="modal" type="button">No</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if($_SESSION['change_pass']>time() && $this->session->userdata()['userdata']['first_login']==1){ ?>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#changepassword').modal('show');
    });
</script>
<?php } ?>
<!-- end-Modal -->
<!-- end: content -->
<!-- plugins -->
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>

<!-- custom -->
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>
<script type="text/javascript">
   	$(document).ready(function() {
		var dataTable =  $('#table-grid').DataTable({
		dom: '<"top"f>rt<"bottom"p><"clear">',
		serverSide: true,
    	bLengthChange: false,
    	bFilter: true,
		ajax:{
				url :"../admin/ajax/get_clients", // json datasource
				type: "post",  // method  , by default get
				error: function(data){  // error handling
					$(".table-grid-error").html("");
					$("#table-grid").append('<tbody class="table-grid-error"><tr><th colspan="6">No data found!</th></tr></tbody>');
					$("#table-grid_processing").css("display","none");
				},complete : function(){
					$(".confirm").popConfirm();
				}
			},
		} );
	} );
	function printDiv(divName) {
        $('.hide_controles_onPrint').hide();
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
		$('.hide_controles_onPrint').show();
	}
</script>
</body>
</html>