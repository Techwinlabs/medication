<style>
<!-- Nav CSSS !-->
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #ffffff;
	border-image: none;
    border-style: solid;
    border-width: 1px;
    color: #555555;
    cursor: default;
}

.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #ffffff;
    border-image: none;
    border-style: solid;
    border-width: 1px;
    color: #418bbb;
    cursor: default;
    width: 100px;
    height: 50px;
    text-align: center;
    padding-top: 15px;margin-left: -1px;border-radius: 0px;
}
.nav-tabs > li > a{
	color:#333;
   
    width: 100px;
    height: 50px;
	text-align: center;padding-top: 15px;
}

.nav-tabs > li > a:hover {
   
    width: 100px;
    height: 50px;padding-top: 15px;
}
.tab-content{
	margin-top:0px;
}

div#home { margin-top: 0px;}
#menu1 {
    margin-top: 17px;
}

#menu2 {
    margin-top: 17px;
}
.bg-all {
    background-color: #fff;
	border: 1px solid rgba(54, 64, 74, 0.08);
    border-radius: 1px;
    box-shadow: 0 4px 4px 0 #ccc;
}

.widget-simple-chart.card-box-one.top-no-border {
    border-top: none;
    border-radius: 0 5px 5px 5px;
    padding: 10px 20px 10px 25px;
}
.append {
    margin-bottom: 0px !important;
}

.col-md-12.mar-box {
    background-color: #f9f9f9;
    padding-bottom: 10px;
    margin-top: 5px;
    margin-bottom: 3px;
    padding-top: 15px;
}
.cancl-btn{
    padding: 6px 20px;
    font-size: 14px;
}
</style>
<!-- start: Content -->
<div id="content">
	<div class="col-md-12 top-20 padding-0">
		<div class="col-md-12">
			<div class="panel" style="margin-bottom: -2px;">
				<div class="panel-heading">
					<h3 class=""> Update Program Of Care for <?= $name; ?></h3>
				</div>
			</div>
		</div>
	</div>
    <div class="form-element upcr-progrm">
        <div class="col-md-12 padding-0">
            <div class="col-md-12">
                <div class="panel form-element-padding">
                    <div class="panel-body" style="padding-bottom:30px;">
                        <div class="col-md-12">
							<?php
								echo show_err_msg($error_msg);
							?>
							<form method="POST" action="">
								<input type="hidden" name="client_id" value="<?= @$client_id; ?>" >
								<input type="hidden" name="program_id" value="<?= @$data['program_id']; ?>" >
								<!----<div class="form-group">
									<div class="col-sm-12">										
										<!-- <label class="col-sm-3 control-label"><h5>Instructions</h5></label> 
										<textarea type="text" style="resize:none; margin: 0 0 20px;" name="comments" class="form-control default ip-ar" rows="7" placeholder="Instructions to carer"><?= $data['comments']; ?></textarea>
									</div>
								</div>-->
								<div class="clearfix"></div>
								<div class="form-group">									
									<div class="col-sm-12">
										 <ul class="nav nav-tabs prgrm-tbar" style="margin-left:1px">
											<li class="active"><a data-toggle="tab" href="#am">AM</a></li>
											<li class="tab-wh"><a data-toggle="tab" href="#noon">Noon</a></li>
											<li class="tab-wh"><a data-toggle="tab" href="#evening">Evening</a></li>
											<li class="tab-wh"><a data-toggle="tab" href="#night">Night</a></li>
										</ul>
										<div class="tab-content">
											<div id="am" class="tab-pane fade in active"> 
												<div class="row">
													<div class="col-md-12 cover-div">
												<?php if(isset($data['am_time_care_program']) && count($data['am_time_care_program'])){ ?>
															
												<?php	for($a=0;$a<count($data['am_time_care_program']);$a++){ ?>
														<div class="col-md-12 mar-box">
															<span style="font-size:25px; color:red; position: absolute;" title="Click to remove medicine" class="icons icon-close remove_field"></span>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Name</label>
																<div class="col-md-8">
																	<input type="text" name="am_time_care_program[]" value="<?= $data['am_time_care_program'][$a]['medicine']; ?>" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Type</label>
																<div class="col-md-8">
																	<input type="text" name="am_time_care_program_type[]" value="<?= @$data['am_time_care_program'][$a]['type']; ?>" class="form-control default ip-ar" placeholder="eg. Pill, Cream, Injection etc">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Dose</label>
																<div class="col-md-8">
																	<input type="text" name="am_time_care_program_dose[]" value="<?= @$data['am_time_care_program'][$a]['dose']; ?>"  class="form-control default ip-ar" placeholder="">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Time</label>
																<div class="col-md-8">
																	<input type="time" name="am_time_care_program_time[]" value="<?= @$data['am_time_care_program'][$a]['time']; ?>" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Other Detail</label>
																<div class="col-md-8">
																	<input type="text" name="am_time_care_program_detail[]" value="<?= @$data['am_time_care_program'][$a]['detail']; ?>" class="form-control default ip-ar">
																</div>
															</div>
														</div>
													<?php } ?>
														
												<?php }else{ ?>
														<div class="col-md-12 mar-box">
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Name</label>
																<div class="col-md-8">
																	<input type="text" name="am_time_care_program[]" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Type</label>
																<div class="col-md-8">
																	<input type="text" name="am_time_care_program_type[]" class="form-control default ip-ar" placeholder="eg. Pill, Cream, Injection etc">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Dose</label>
																<div class="col-md-8">
																	<input type="text" name="am_time_care_program_dose[]" class="form-control default ip-ar" placeholder="">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Time</label>
																<div class="col-md-8">
																	<input type="time" name="am_time_care_program_time[]" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Other Detail</label>
																<div class="col-md-8">
																	<input type="text" name="am_time_care_program_detail[]" class="form-control default ip-ar">
																</div>
															</div>
														</div>
												<?php } ?>
														<div class="apendAm"></div>
														<div class="col-md-2 col-md-offset-10">
															<span style="font-size:30px" class="icons icon-plus add_field_button" onclick="addMoreAMMedicine();"></span>
														</div>
													</div>
												</div>
											</div>
											<div  id="noon" class="tab-pane fade"> 
												<div class="row">
													<div class="col-md-12 cover-div">
												<?php if(isset($data['noon_time_care_program']) && count($data['noon_time_care_program'])){ ?>
												<?php	for($n=0;$n<count($data['noon_time_care_program']);$n++){ ?>
														<div class="col-md-12 mar-box">
															<span style="font-size:25px; color:red; position: absolute;" title="Click to remove medicine" class="icons icon-close remove_field"></span>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Name</label>
																<div class="col-md-8">
																	<input type="text" name="noon_time_care_program[]" value="<?= $data['noon_time_care_program'][$n]['medicine']; ?>" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Type</label>
																<div class="col-md-8">
																	<input type="text" name="noon_time_care_program_type[]" value="<?= @$data['noon_time_care_program'][$n]['type']; ?>" class="form-control default ip-ar" placeholder="eg. Pill, Cream, Injection etc">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Dose</label>
																<div class="col-md-8">
																	<input type="text" name="noon_time_care_program_dose[]" value="<?= @$data['noon_time_care_program'][$n]['dose']; ?>"  class="form-control default ip-ar" placeholder="">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Time</label>
																<div class="col-md-8">
																	<input type="time" name="noon_time_care_program_time[]" value="<?= @$data['noon_time_care_program'][$n]['time']; ?>" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Other Detail</label>
																<div class="col-md-8">
																	<input type="text" name="noon_time_care_program_detail[]" value="<?= @$data['noon_time_care_program'][$n]['detail']; ?>" class="form-control default ip-ar">
																</div>
															</div>
														</div>
													<?php } ?>		
														
												<?php }else{ ?>
														<div class="col-md-12 mar-box">
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Name</label>
																<div class="col-md-8">
																	<input type="text" name="noon_time_care_program[]" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Type</label>
																<div class="col-md-8">
																	<input type="text" name="noon_time_care_program_type[]"  class="form-control default ip-ar" placeholder="eg. Pill, Cream, Injection etc">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Dose</label>
																<div class="col-md-8">
																	<input type="text" name="noon_time_care_program_dose[]" class="form-control default ip-ar" placeholder="">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Time</label>
																<div class="col-md-8">
																	<input type="time" name="noon_time_care_program_time[]" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Other Detail</label>
																<div class="col-md-8">
																	<input type="text" name="noon_time_care_program_detail[]" class="form-control default ip-ar">
																</div>
															</div>
														</div>
												<?php } ?>

														<div class="apendNoon"></div>
														<div class="col-md-2 col-md-offset-10">
															<span style="font-size:30px" class="icons icon-plus add_field_button" onclick="addMoreNoonMedicine();"></span>
														</div>
														<script>
															 var wrapper = jQuery(".apendNoon"); //Fields wrapper
														</script>
													</div>
												</div>
											</div>
											<div  id="evening" class="tab-pane fade"> 
												<div class="row">
													<div class="col-md-12 cover-div">
												<?php if(isset($data['tea_time_care_program']) && count($data['tea_time_care_program'])){ ?>
												<?php	for($t=0;$t<count($data['tea_time_care_program']);$t++){ ?>
														<div class="col-md-12 mar-box">
															<span style="font-size:25px; color:red; position: absolute;" title="Click to remove medicine" class="icons icon-close remove_field"></span>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Name</label>
																<div class="col-md-8">
																	<input type="text" name="tea_time_care_program[]" value="<?= $data['tea_time_care_program'][$t]['medicine']; ?>" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Type</label>
																<div class="col-md-8">
																	<input type="text" name="tea_time_care_program_type[]" value="<?= @$data['tea_time_care_program'][$t]['type']; ?>" class="form-control default ip-ar" placeholder="eg. Pill, Cream, Injection etc">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Dose</label>
																<div class="col-md-8">
																	<input type="text" name="tea_time_care_program_dose[]" value="<?= @$data['tea_time_care_program'][$t]['dose']; ?>"  class="form-control default ip-ar" placeholder="">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Time</label>
																<div class="col-md-8">
																	<input type="time" name="tea_time_care_program_time[]" value="<?= @$data['tea_time_care_program'][$t]['time']; ?>" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Other Detail</label>
																<div class="col-md-8">
																	<input type="text" name="tea_time_care_program_detail[]" value="<?= @$data['tea_time_care_program'][$t]['detail']; ?>" class="form-control default ip-ar">
																</div>
															</div>
														</div>
													<?php } ?>	
												<?php }else{ ?>
														<div class="col-md-12 mar-box">
														
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Name</label>
																<div class="col-md-8">
																	<input type="text" name="tea_time_care_program[]" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Type</label>
																<div class="col-md-8">
																	<input type="text" name="tea_time_care_program_type[]"  class="form-control default ip-ar" placeholder="eg. Pill, Cream, Injection etc">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Dose</label>
																<div class="col-md-8">
																	<input type="text" name="tea_time_care_program_dose[]" class="form-control default ip-ar" placeholder="">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Time</label>
																<div class="col-md-8">
																	<input type="time" name="tea_time_care_program_time[]" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Other Detail</label>
																<div class="col-md-8">
																	<input type="text" name="tea_time_care_program_detail[]" class="form-control default ip-ar">
																</div>
															</div>
														</div>
												<?php } ?>
														<div class="apendEvening"></div>
														<div class="col-md-2 col-md-offset-10">
															<span style="font-size:30px" class="icons icon-plus add_field_button" onclick="addMoreEveingMedicine();"></span>
														</div>
														<script>
															 var wrapper = jQuery(".apendEvening"); //Fields wrapper
														</script>
													</div>
												</div>
											</div>
											<div  id="night" class="tab-pane fade"> 
												<div class="row">
													<div class="col-md-12 cover-div">
												<?php if(isset($data['night_time_care_program']) && count($data['night_time_care_program'])){ ?>
												<?php	for($nt=0;$nt<count($data['night_time_care_program']);$nt++){ ?>
														<div class="col-md-12 mar-box">
															<span style="font-size:25px; color:red; position: absolute;" title="Click to remove medicine" class="icons icon-close remove_field"></span>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Name</label>
																<div class="col-md-8">
																	<input type="text" name="night_time_care_program[]" value="<?= $data['night_time_care_program'][$nt]['medicine']; ?>" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Type</label>
																<div class="col-md-8">
																	<input type="text" name="night_time_care_program_type[]" value="<?= @$data['night_time_care_program'][$nt]['type']; ?>" class="form-control default ip-ar" placeholder="eg. Pill, Cream, Injection etc">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Dose</label>
																<div class="col-md-8">
																	<input type="text" name="night_time_care_program_dose[]" value="<?= @$data['night_time_care_program'][$nt]['dose']; ?>"  class="form-control default ip-ar" placeholder="">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Time</label>
																<div class="col-md-8">
																	<input type="time" name="night_time_care_program_time[]" value="<?= @$data['night_time_care_program'][$nt]['time']; ?>" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Other Detail</label>
																<div class="col-md-8">
																	<input type="text" name="night_time_care_program_detail[]" value="<?= @$data['night_time_care_program'][$nt]['detail']; ?>" class="form-control default ip-ar">
																</div>
															</div>
														</div>
													<?php } ?>	
												<?php }else{ ?>
														<div class="col-md-12 mar-box">
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Name</label>
																<div class="col-md-8">
																	<input type="text" name="night_time_care_program[]" class="form-control default">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Type</label>
																<div class="col-md-8">
																	<input type="text" name="night_time_care_program_type[]"  class="form-control default ip-ar" placeholder="eg. Pill, Cream, Injection etc">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Dose</label>
																<div class="col-md-8">
																	<input type="text" name="night_time_care_program_dose[]" class="form-control default ip-ar" placeholder="">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Medicine Time</label>
																<div class="col-md-8">
																	<input type="time" name="night_time_care_program_time[]" class="form-control default ip-ar">
																</div>
															</div>
															<div class="form-group">
																<label class="col-sm-3 control-label text-right">Other Detail</label>
																<div class="col-md-8">
																	<input type="text" name="night_time_care_program_detail[]" class="form-control default ip-ar">
																</div>
															</div>
														</div>
												<?php } ?>		
														<div class="apendNight"></div>
														<div class="col-md-2 col-md-offset-10">
															<span style="font-size:30px" class="icons icon-plus add_field_button" onclick="addMoreNightMedicine();"></span>
														</div>
														<script>
															 var wrapper = jQuery(".apendNight"); //Fields wrapper
														</script>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="clearfix "></div>
								<hr />
								<div class="col-md-4 col-md-offset-4">
									<input class="submit btn btn-primary" name='submit' type="submit" value="Update Program">&emsp;<button type="button" onclick="goBack()" class="btn btn-sm btn-warning cancl-btn">Cancel</button>
								</div>
							</form>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
		 $(".remove_field").click( function(){ //user click on remove text
			jQuery(this).parent('div').remove();
		});
    });
	
</script>
<!-- end: content -->