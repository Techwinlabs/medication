	<!-- start: Content -->
	<div id="content">
		<div class="panel box-shadow-none content-header">
			<div class="panel-body">
				<div class="col-md-12">
					<h3 class=""> Update Schedule</h3>
				</div>
			</div>
		</div>
		<div class="form-element">
			<div class="col-md-12">
				<div class="panel form-element-padding">
					<div class=" panel-body" style="padding-bottom:30px;">
						<div class="col-md-12">
							<?php
								echo show_err_msg($error_msg);
							?>
							<form  id="new_schedule" method="POST" action="">
								<input type="hidden" name="company_id" value="<?= $company_id; ?>">
                                
								<div class="col-md-8">
									<div class="form-group">
										<label class="col-sm-3 control-label text-right">Start Date</label>
										<div class="col-sm-9">
											<input type="date" class="form-control default" value="<?php echo $schedule['start_date']; ?>" id="sechedule_date" name="sechedule_date"  required>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-3 control-label text-right">Start Time</label>
										<div class="col-sm-9">
											<input type="time" value="<?php echo $schedule['start_time']; ?>" class="form-control default" id="sechedule_time" name="sechedule_time" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label text-right">End Date</label>
										<div class="col-sm-9">
											<input type="date" class="form-control default" value="<?php echo $schedule['end_date']; ?>" id="sechedule_date_end" name="sechedule_date_end"  required>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-3 control-label text-right">End Time</label>
										<div class="col-sm-9">
											<input type="time" value="<?php echo $schedule['end_time']; ?>" class="form-control default" id="sechedule_time_end" name="sechedule_time_end" required>
										</div>
									</div>


									<div class="form-group">
										<label class="col-sm-3 control-label text-right">Select Carer</label>
										<div class="col-sm-9">
										
											<select class="form-control default livesearch" data-live-search="true"  id="employeeid" name="employeeid" required >
												<option value="" ></option>
									<?php	for($x=0;$x<count($carer);$x++){ ?>
												<option data-tokens="<?= $carer[$x]['employee_name']; ?> (<?= $carer[$x]['employee_number']; ?>)" value="<?= $carer[$x]['employeeid']; ?>" <?php if($schedule['employeeid']==$carer[$x]['employeeid']){ echo 'selected'; } ?> ><?= $carer[$x]['employee_name']; ?> (<?= $carer[$x]['employee_number']; ?>)</option>
									<?php	}	?>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-3 control-label text-right">Select Client</label>
										<div class="col-sm-9">
											<select class="form-control default livesearch" data-live-search="true"  id="client_id" name="client_id" required >
												<option ></option>
									<?php	for($i=0;$i<count($clients);$i++){ ?>
												<option data-tokens="<?= $clients[$i]['client_name']; ?> (<?= $clients[$i]['postcode']; ?>)" value="<?= $clients[$i]['client_id']; ?>" <?php if($schedule['client_id']==$clients[$i]['client_id']){ echo 'selected'; } ?>><?= $clients[$i]['client_name']; ?> (<?= $clients[$i]['postcode']; ?>)</option>
									<?php	}	?>
											</select>
										</div>
									</div>

									
								</div>     
								<div class="clearfix "></div>
								<hr />              
								<div class="col-md-4 col-md-offset-4">
								
								<input class="submit btn btn-danger" name='submit' type="submit" value="Submit">
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
	<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
	<script src="<?= base_url(); ?>asset/js/main.js"></script>
<!-- end: content -->