<style>
<!-- Nav CSSS !-->
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #ffffff;
	border-image: none;
    border-style: solid;
    border-width: 1px;
    color: #555555;
    cursor: default;
}

.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #ffffff;
    border-image: none;
    border-style: solid;
    border-width: 1px;
    color: #418bbb;
    cursor: default;
    width: 100px;
    height: 50px;
    text-align: center;
    padding-top: 15px;margin-left: -1px;border-radius: 0px;
}
.nav-tabs > li > a{
	color:#333;
   
    width: 100px;
    height: 50px;
	text-align: center;padding-top: 15px;
}

.nav-tabs > li > a:hover {
   
    width: 100px;
    height: 50px;padding-top: 15px;
}
.tab-content{
	margin-top:0px;
}

div#home { margin-top: 0px;}
#menu1 {
    margin-top: 17px;
}

#menu2 {
    margin-top: 17px;
}
.bg-all {
    background-color: #fff;
	border: 1px solid rgba(54, 64, 74, 0.08);
    border-radius: 1px;
    box-shadow: 0 4px 4px 0 #ccc;
}

.widget-simple-chart.card-box-one.top-no-border {
    border-top: none;
    border-radius: 0 5px 5px 5px;
    padding: 10px 20px 10px 25px;
}
.append {
    margin-bottom: 0px !important;
}
</style>
<!-- start: Content -->
<div id="content">
	<div class="panel box-shadow-none content-header">
		<div class="panel-body">
			<div class="col-md-12">
				<h3 class=""> New Client</h3>
			</div>
		</div>
	</div>
    <div class="form-element">
        <div class="col-md-12 padding-0">
            <div class="col-md-12">
                <div class="panel form-element-padding">
                    <div class="panel-body" style="padding-bottom:30px;">
                        <div class="col-md-10">
							<?php
								echo show_err_msg($error_msg);
							?>
							<form method="POST" enctype="Multipart/Form-data" action="">
								<input type="hidden" name="company_id" value="<?= $company_id; ?>" >
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Post Code</label>
									<div class="col-sm-9">
										<input type="text" name="postcode" class="form-control default" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">First Name</label>
									<div class="col-sm-9">
										<input type="text" name="firstname" class="form-control default" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Last Name</label>
									<div class="col-sm-9">
										<input type="text" name="lastname" class="form-control default" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Date of Birth</label>
									<div class="col-sm-9">
										<input type="date" name="dob" class="form-control default" placeholder="mm/dd/yyyy" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Address</label>
									<div class="col-sm-9">
										<input type="text" name="address" class="form-control default" required>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Personal Information</label>
									<div class="col-sm-9">
										<input type="text" name="personal_information" class="form-control default" required>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Emergency Contact Number</label>
									<div class="col-sm-9">
										<input type="text" name="emergency_contact" class="form-control default" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Comment</label>
									<div class="col-sm-9">
										<input type="text" name="comments" class="form-control default">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Picture</label>
									<div class="col-sm-9">
										<input type="file" name="client_picture" accept="image/*" >
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-right">Program of Care</label>
									<div class="col-sm-9">
										 <ul class="nav nav-tabs " style="margin-left:1px">
											<li class="active"><a data-toggle="tab" href="#am">AM</a></li>
											<li class="tab-wh"><a data-toggle="tab" href="#noon">Noon</a></li>
											<li class="tab-wh"><a data-toggle="tab" href="#evening">Evening</a></li>
											<li class="tab-wh"><a data-toggle="tab" href="#night">Night</a></li>
										</ul>
										<div class="tab-content">
											<div id="am" class="tab-pane fade in active"> 
												<div class="row">
													<div class="col-md-12 cover-div">
														<div class="form-group">
															<label class="col-sm-2 control-label text-right">Medicine</label>
															<div class="col-sm-9">
																<input type="text" name="am_time_care_program[]" class="form-control default">
															</div>
														</div>
														<div class="apendAm"></div>
														<div class="col-md-2 col-md-offset-10">
															<span style="font-size:30px" class="icons icon-plus add_field_button" onclick="addMoreAMMedicine();"></span>
														</div>
													</div>
												</div>
											</div>
											<div  id="noon" class="tab-pane fade"> 
												<div class="row">
													<div class="col-md-12 cover-div">
														<div class="form-group">
															<label class="col-sm-2 control-label text-right">Medicine</label>
															<div class="col-sm-9">
																<input type="text" name="noon_time_care_program[]" class="form-control default">
															</div>
														</div>
														<div class="apendNoon"></div>
														<div class="col-md-2 col-md-offset-10">
															<span style="font-size:30px" class="icons icon-plus add_field_button" onclick="addMoreNoonMedicine();"></span>
														</div>
													</div>
												</div>
											</div>
											<div  id="evening" class="tab-pane fade"> 
												<div class="row">
													<div class="col-md-12 cover-div">
														<div class="form-group">
															<label class="col-sm-2 control-label text-right">Medicine</label>
															<div class="col-sm-9">
																<input type="text" name="tea_time_care_program[]" class="form-control default">
															</div>
														</div>
														<div class="apendEvening"></div>
														<div class="col-md-2 col-md-offset-10">
															<span style="font-size:30px" class="icons icon-plus add_field_button" onclick="addMoreEveingMedicine();"></span>
														</div>
													</div>
												</div>
											</div>
											<div  id="night" class="tab-pane fade"> 
												<div class="row">
													<div class="col-md-12 cover-div">
														<div class="form-group">
															<label class="col-sm-2 control-label text-right">Medicine</label>
															<div class="col-sm-9">
																<input type="text" name="night_time_care_program[]" class="form-control default">
															</div>
														</div>
														<div class="apendNight"></div>
														<div class="col-md-2 col-md-offset-10">
															<span style="font-size:30px" class="icons icon-plus add_field_button" onclick="addMoreNightMedicine();"></span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="clearfix "></div>
								<hr />
								<div class="col-md-4 col-md-offset-4">
									<input class="submit btn btn-primary" name='submit' type="submit" value="Submit Data">&emsp;<button type="reset" class="btn btn-sm btn-warning">Reset Data</button>
								</div>
							</form>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
		$("#new-employee").validate({
            errorElement: "em",
            errorPlacement: function(error, element) {
                $(element.parent("div").addClass("form-animate-error"));
                error.appendTo(element.parent("div"));
            },
            success: function(label) {
                $(label.parent("div").removeClass("form-animate-error"));
            },
            rules: {
                employee_name: "required",
                employee_number: "required",
                employee_mobile: "required",
                employee_country_code: "required",
                validate_username: {
                    required: true,
                },
                employee_email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                employee_name: "Please enter employee name",
                employee_number: "Please enter employee number",
                validate_username: {
                    required: "Please enter a username",
                },
                employee_email: "Please enter a valid email address",
                employee_mobile: "Please enter a mobile number",
                employee_country_code: "Please select country code",
            }
        });
    });
	
</script>
<!-- end: content -->