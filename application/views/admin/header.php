<?php $action=basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
$section_roles = explode(',',ltrim(rtrim($this->userdata['section_roles'],','),','));
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="description" content="Medication Admin">
	<meta name="author" content="Hem Thakur">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $title; ?></title>

	<!-- start: Css -->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/bootstrap.min.css">

	<!-- plugins -->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/datatables.bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/simple-line-icons.css"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/animate.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/fullcalendar.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/scheduler.min.css"/>
	<link href="<?= base_url(); ?>asset/css/style.css" rel="stylesheet">
	<link href="<?= base_url(); ?>asset/css/hems.css" rel="stylesheet">
	<link href="<?= base_url(); ?>asset/css/animate.css" rel="stylesheet">
  <link href="<?= base_url(); ?>asset/css/bootstrap-select.min.css" rel="stylesheet">
  <!-- end: Css -->
	
	<!-- Main JS !>
	<!-- start: Javascript -->
	<script src="<?= base_url(); ?>asset/js/jquery.min.js"></script>
	<script src="<?= base_url(); ?>asset/js/jquery.ui.min.js"></script>
	<script src="<?= base_url(); ?>asset/js/bootstrap.min.js"></script>
	<script src="<?= base_url(); ?>asset/js/bootstrap-select.min.js"></script>
	<script src="<?= base_url(); ?>asset/js/jquery.popconfirm.js"></script>
	<script src="<?= base_url(); ?>asset/js/bootstrap-session-timeout.js"></script>
	<link rel="shortcut icon" href="<?= base_url(); ?>asset/img/logo-new.png">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<!--------------- LOGOUT AFTER INACTIVITY -------------->

		<script>
	  
	  // if app is minimised
	  
	//  document.addEventListener("visibilitychange", function() {
	//	if(document.visibilityState == 'hidden'){
	//	  if (confirm('Are you sure you want to leave ?')) {
	//		window.location.href = "<?= base_url('admin/Auth/logout'); ?>";
	//	  }
	//	}
	//  }, false);
	  
	  // session-timeout
	  
	  var timer = 0; var popup = 0;
	  function set_interval() {
		timer = setInterval("show_expire_modal()",300000);
		$('#mimin').on("mousemove", reset_interval);
		$('#mimin').on("click", reset_interval);
		$('#mimin').on("keypress", reset_interval);
		$('#mimin').on("scroll", reset_interval);
		$('#mimin').on("load", reset_interval);
	  }
	  
	  function reset_interval() {
		if (timer != 0) {
		  clearInterval(timer);
		  timer = 0;
		  timer = setInterval("show_expire_modal()", 300000);
		}
	  }
	  var status = 0;
	  function stay_connected() {
	  	  reset_interval();
	  	  clearInterval(popup);
		  $('#session-timeout').modal('toggle');
		  $('#mimin').on("mousemove", reset_interval);
		  $('#mimin').on("click", reset_interval);
		  $('#mimin').on("keypress", reset_interval);
		  $('#mimin').on("scroll", reset_interval);
		  $('#mimin').on("load", reset_interval);
		  if (timer != 0) {
			clearInterval(timer);
			timer = 0;
			timer = setInterval("show_expire_modal()", 300000);
		  }
	  }
	  
	  function show_expire_modal() {
		  $('#session-timeout').modal({ backdrop: 'static', keyboard: true, show: true });
		  $('#mimin').off("mousemove", reset_interval);
		  $('#mimin').off("click", reset_interval);
		  $('#mimin').off("keypress", reset_interval);
		  $('#mimin').off("scroll", reset_interval);
		  $('#mimin').off("load", reset_interval);
		  popup = setInterval("session_logout()", 10000);
		  
      }
	  
	  function session_logout() {
		window.location.href = "<?= base_url('admin/Auth/logout'); ?>";
	  }
	
	</script>
  </head>
 <body id="mimin" class="dashboard" onload="set_interval()">
      <!-- start: Header -->
        <nav class="navbar navbar-default header navbar-fixed-top">
          <div class="col-md-12 nav-wrapper">
            <div class="navbar-header" style="width:100%;">
				<div class="logo-row">
                	<img src="<?= base_url(); ?>asset/img/logo-new.png" height="55px">
              	</div>
                <a href="<?= base_url()?>admin" class="navbar-brand"> 
                 <b id="web_title">Daily Care Records</b>
                 <b id="countdown"></b>
                </a>
              <ul class="nav navbar-nav navbar-right user-nav">
                <li class="user-name">
					<span><?= @$this->userdata['employee_name']; ?></span><br>
					<span><?= @$this->userdata['company_name']; ?></span>
				</li>
				<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
				<!---<li class="user-company-name"><span><?= @$this->userdata['company_name']; ?></span></li>---->
                  <li class="dropdown avatar-dropdown">
					<?=  $this->session->userdata()['userdata']['employee_picture'] ? '<img src="'. $this->session->userdata()['userdata']['employee_picture'].'"  onerror="this.src=\''.base_url().'asset/img/dummy.svg\'" class="img-circle avatar" alt="Picture" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >' : '<img src="'.base_url().'asset/img/dummy.svg" class="img-circle avatar" alt="Picture" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">' ?>
                   <ul class="dropdown-menu user-dropdown">
                     <li><a href="<?= base_url(); ?>admin/profile"><span class="fa fa-user"></span> My Profile</a></li>
                     <li class="more">
                      <ul>
                        <li><a href="<?= base_url();?>admin/Auth/logout"><span class="fa fa-power-off "></span> Log out</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li ><a class="opener-right-menu1"></a></li>
              </ul>
            </div>
          </div>
        </nav>
		<!-- end: Header -->

		<div class="container-fluid mimin-wrapper">
		<!-- start:Left Menu -->
            <div id="left-menu">
              <div class="sub-left-menu scroll">
                <ul class="nav nav-list">
                    <li class="time">
                      <h1 class="animated fadeInLeft">&nbsp;&nbsp;</h1>
                      <p class="animated fadeInRight">&nbsp;&nbsp;</p>
                    </li>
                    <!--li class="active ripple">
                      <a class="nav-header" href="<!?= base_url(); ?>admin/dashboard"><span class="fa-home fa"></span> Dashboard 
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                    </li-->
		<!--?php	if($this->session->userdata('access')){ ?-->
		<?php	if(true){ ?>
			<?php $status = getAnythingData('med_companies','company_id',$this->session->userdata('userdata')['company_id'],'status'); if($status==1){  ?>
					  <?php //if($this->userdata['employee_type']==2){ ?>
					  
					  
						  <?php if($action == 'clients') { ?>
						  <li class="ripple active">
							<?php } else{ ?>
							<li class="ripple">
							<?php } ?>
							<a class="nav-header" href="<?= base_url(); ?>admin/clients">
							  <span class="icons icon-people"></span> Manage Clients
							  <span class="fa-angle-right fa right-arrow text-right"></span>
							</a>
						  </li>
					  <?php //}else{ if(in_array(4,$section_roles)){ ?>
						<!---  <?php if($action == 'clients') { ?>
						  <li class="ripple active">
							<?php } else{ ?>
							<li class="ripple">
							<?php } ?>
						   
							<a class="nav-header" href="<?= base_url(); ?>admin/clients">
							  <span class="icons icon-people"></span> Manage Clients
							  <span class="fa-angle-right fa right-arrow text-right"></span>
							</a>
						  </li>---->
					  <?php //} } ?>
					  
					  <?php //if($this->userdata['employee_type']==2){ ?>
						 <!---- <?php if($action == 'employees') { ?>
							<li class="ripple active">
							<?php } else{ ?>
							<li class="ripple">
							<?php } ?>
							<a class="nav-header " href="<?= base_url(); ?>admin/employees">
							  <span class="icons icon-user"></span> Manage Staff
							  <span class="fa-angle-right fa right-arrow text-right"></span>
							</a>
						  </li>------>
					  <?php //}else{ if(in_array(3,$section_roles)){ ?>
						  <?php if($action == 'employees') { ?>
							<li class="ripple active">
							<?php } else{ ?>
							<li class="ripple">
							<?php } ?>
							<a class="nav-header " href="<?= base_url(); ?>admin/employees">
							  <span class="icons icon-user"></span> Manage Staff
							  <span class="fa-angle-right fa right-arrow text-right"></span>
							</a>
						  </li>
					  <?php //} } ?>
					  
					  <?php if($this->userdata['employee_type']==2){ ?>
						  <?php if($action == 'schedules') { ?>
							<li class="ripple active">
							<?php } else{ ?>
							<li class="ripple">
							<?php } ?>
							<a class="nav-header" href="<?= base_url(); ?>admin/schedules">
							  <span class="icons icon-calendar"></span> Schedule
							  <span class="fa-angle-right fa right-arrow text-right"></span>
							</a>
						  </li>
					  <?php }else{ if(in_array(2,$section_roles)){ ?>
						  <?php if($action == 'schedules') { ?>
							<li class="ripple active">
							<?php } else{ ?>
							<li class="ripple">
							<?php } ?>
							<a class="nav-header" href="<?= base_url(); ?>admin/schedules">
							  <span class="icons icon-calendar"></span> Schedule
							  <span class="fa-angle-right fa right-arrow text-right"></span>
							</a>
						  </li>
					  <?php } } ?>


						<!--?php if($action == 'chat_room') { ?>
					   <li class="ripple active">
					   <!?php } else{ ?>
							<li class="ripple">
							<!?php } ?>
								<a class="nav-header" href="<!?= base_url(); ?>admin/chat_room">
								<span class="icons icon-envelope-letter"></span> Message
								<span class="fa-angle-right fa right-arrow text-right"></span>
					   </a>
					 </li-->


					
					<?php //if($this->userdata['employee_type']==2){ ?>
					
					  <!-------<?php if($action == 'timesheet') { ?>
					   <li class="ripple active">
					   <?php } else{ ?>
					   <li class="ripple">
					   <?php } ?>
					   <a class="nav-header" href="<?= base_url(); ?>admin/timesheet">
						 <span class="fa fa-clock-o" style="font-size:20px"></span> Time Sheet
						 <span class="fa-angle-right fa right-arrow text-right"></span>
					   </a>
					 </li>----->
					 
					  <?php if($action == 'datausage') { ?>
					   <li class="ripple active">
					   <?php } else{ ?>
					   <li class="ripple">
					   <?php } ?>
					   <a class="nav-header" href="<?= base_url(); ?>admin/datausage">
						 <span class="fa fa-bar-chart" style="font-size:20px"></span> Data Usage
						 <span class="fa-angle-right fa right-arrow text-right"></span>
					   </a>
					 </li>
					  <!--?php if($action == 'notifications') { ?>
					   <li class="ripple active">
					   <!?php } else{ ?>
					   <li class="ripple">
					   <!?php } ?>
					   <a class="nav-header" href="<!?= base_url(); ?>admin/notifications">
						 <span class="icons icon-bell style="font-size:20px"></span> Notifications
						 <span class="fa-angle-right fa right-arrow text-right"></span>
					   </a>
					   </li-->
 
					  
					    <?php if($action == 'library') { ?>
					  <li class="ripple active">
					  <?php } else{ ?>
					  <li class="ripple">
					  <?php } ?>
                      <a class="nav-header" href="<?= base_url(); ?>admin/library">
                        <span class="icons icon-notebook"></span> Library
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                    </li>
					   <?php //}elseif($this->userdata['employee_type']==3){
						?>
						<!----
						<?php if($action == 'library') { ?>
							  <li class="ripple active">
							  <?php } else{ ?>
							  <li class="ripple">
							  <?php } ?>
							  <a class="nav-header" href="<?= base_url(); ?>admin/library">
								<span class="icons icon-notebook"></span> Library
								<span class="fa-angle-right fa right-arrow text-right"></span>
							  </a>
							</li>
							
							
							
							
								  
								  
						<?php if(in_array(2,$section_roles)){ ?>		  
										  
							<!------<?php if($action == 'timesheet') { ?>
							   <li class="ripple active">
							   <?php } else{ ?>
							   <li class="ripple">
							   <?php } ?>
							   <a class="nav-header" href="<?= base_url(); ?>admin/timesheet">
								 <span class="fa fa-clock-o" style="font-size:20px"></span> Time Sheet
								 <span class="fa-angle-right fa right-arrow text-right"></span>
							   </a>
							 </li>
							  <?php if($action == 'datausage') { ?>
							   <li class="ripple active">
							   <?php } else{ ?>
							   <li class="ripple">
							   <?php } ?>
							   <a class="nav-header" href="<?= base_url(); ?>admin/datausage">
								 <span class="fa fa-bar-chart" style="font-size:20px"></span> Data Usage
								 <span class="fa-angle-right fa right-arrow text-right"></span>
							   </a>
							 </li>
						
						<?php } ?>
					<?php// } ?>---->
					 <?php if($action == 'profile') { ?>
					  <li class="ripple active">
					  <?php } else{ ?>
					  <li class="ripple">
					  <?php } ?>
                      <a class="nav-header" href="<?= base_url(); ?>admin/profile">
                        <span class="icons icon-note"></span>My Profile
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                    </li>
					  
			<?php }else{ ?>
				  <?php if($action == 'library') { ?>
					  <li class="ripple active">
					  <?php } else{ ?>
					  <li class="ripple">
					  <?php } ?>
                      <a class="nav-header" href="<?= base_url(); ?>admin/library">
                        <span class="icons icon-notebook"></span> Library
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                    </li>
			<?php } ?>
        <?php	}else{ ?>
					<li class="ripple">
                      <a class="nav-header" href="<?= base_url(); ?>admin/payment">
                        <span class="icons icon-bell style="font-size:20px"></span> Payment
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                    </li>
		<?php	}	?>
                  </ul>
                </div>
            </div>
          <!-- end: Left Menu -->
      <div id="loading_div" style="display:none;">
        <img id="loading-image" src="<?= base_url(); ?>asset/media/loading.gif" alt="Loading..." />
      </div>

<!------   SESSION TIMEOUT MODAL  --------->

<div id="session-timeout" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Alert !</h4>
      </div>
      <div class="modal-body">
        <p>Your Session is About to Expire !</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick = "session_logout();">Logout</button><button type="button" class="btn btn-primary" onclick="stay_connected();">Stay Connected</button>
      </div>
    </div>
  </div>
</div>

<script>
// Add active class to the current button (highlight it)
var header = document.getElementById("nav nav-list");
var btns = header.getElementsByClassName("ripple");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}
</script>
<script>
<?php if($this->session->userdata('billing_plan')==10000){ ?>// 0 default 10000 for always true
	var countDownDate = new Date("<?= $this->session->userdata('trial'); ?>").getTime();
	var x = setInterval(function() {
		var now = new Date().getTime();
		var distance = countDownDate - now;
		var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((distance % (1000 * 60)) / 1000);
			
		document.getElementById("countdown").innerHTML = "Free trial expire after: "+days + "d " + hours + "h "
		+ minutes + "m " + seconds + "s ";
		if (distance <= 0) {
			clearInterval(x);
			document.getElementById("countdown").innerHTML = "<span style='color:red'>FREE TRIAL EXPIRED</span>";
			// Set permission for to access
<?php 	if ($this->session->userdata('access')==1){ echo 1;exit; ?>
			$.ajax({
				url: '../admin/ajax/blockFreeTrial',
				method: 'post',
				success: function(data) {
					window.location.href = "<?= base_url()?>admin/payment";
				}
			});
<?php	}	?>
		}
	}, 1000);
<?php } ?>
</script>
