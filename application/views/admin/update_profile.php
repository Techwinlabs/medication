<style>
.form-control.default {
    padding-bottom: 2px !important;
    padding-top: 2px !important;
}
</style>
<!-- start: Content -->
<div id="content">
<?php
$section_roles = explode(',',ltrim(rtrim($this->userdata['section_roles'],','),','));
	if(in_array(4,$section_roles) || $this->userdata['employee_type']==2){
?>
	<div class="col-md-12 top-20 padding-0">
		<div class="col-md-12">
			<div class="panel" style="margin-bottom: -2px;">
				<div class="panel-heading">
					<h3 class=""> Update profile of <?= $name; ?></h3>
					<button type="button" class="btn btn-sm btn-warning pull-right new" onclick="goBack();">Back</button>
					
				</div>
			</div>
		</div>
	</div>
    <div class="form-element">
        <div class="col-md-12 padding-0">
            <div class="col-md-12">
                <div class="panel form-element-padding">
                    <div class="panel-body" style="padding-bottom:30px;">
                        <div class="col-md-10">
							<?php
								echo show_err_msg($error_msg);
							?>
							<form method="POST" enctype="Multipart/Form-data" action="">
								<input type="hidden" name="client_id" value="<?= $data['client_id']; ?>" >
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Post Code</label>
									<div class="col-sm-9">
										<input type="text" name="postcode"  value="<?= $data['postcode']; ?>" class="form-control default ip-ar" required>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">First Name</label>
									<div class="col-sm-9">
										<input type="text" name="firstname"  value="<?= $data['firstname']; ?>" class="form-control default ip-ar" required>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Last Name</label>
									<div class="col-sm-9">
										<input type="text" name="lastname"  value="<?= $data['lastname']; ?>" class="form-control default ip-ar" required>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Email </label>
									<div class="col-sm-9">
										<input type="email" name="email" id="email" value="<?= $data['email']; ?>"  class="form-control default ip-ar" required>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Phone Number</label>
									<div class="col-sm-9">
										<input type="text" maxlength="10" name="phone_number" id="phone_number" value="<?= $data['phone_number']; ?>"  class="form-control default ip-ar" required>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Date of Birth</label>
									<div class="col-sm-9">
										<input type="date" name="dob"  value="<?= $data['dob']; ?>" class="form-control default ip-ar" placeholder="mm/dd/yyyy" required>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Gender</label>
									<div class="col-sm-9">
										<div class="col-sm-2 padding-0">
										  <input type="radio" name="gender" <?= $data['gender']==1 ? 'Checked' : ""; ?> value="1"> Male
										</div>
										<div class="col-sm-2 padding-0">
										  <input type="radio" name="gender" <?= $data['gender']==2 ? 'Checked' : ""; ?> value="2"> Female
										</div>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Address</label>
									<div class="col-sm-9">
										<input type="text" name="address"  value="<?= $data['address']; ?>" class="form-control default ip-ar" required>
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Personal Information</label>
									<div class="col-sm-9">
										<input type="text" name="personal_information"  value="<?= $data['personal_information']; ?>" class="form-control default ip-ar" required>
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Emergency Contact Person</label>
									<div class="col-sm-9">
										<input type="text" name="emergency_contact_name" value="<?= $data['emergency_contact_name']; ?>"  class="form-control default ip-ar" required>
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Emergency Contact Relation</label>
									<div class="col-sm-9">
										<input type="text" name="emergency_contact_relation" value="<?= $data['emergency_contact_relation']; ?>"  class="form-control default ip-ar" required>
									</div>
								</div>
								
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Emergency Contact Number</label>
									<div class="col-sm-9">
										<input type="text" title="Only 10 digits" pattern="[1-9]{1}[0-9]{9}" name="emergency_contact"  value="<?= $data['emergency_contact']; ?>" class="form-control default ip-ar" required>
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Picture</label>
									<div class="col-sm-9">
										<input class="ip-ar" type="file" name="client_picture" accept="image/*" >
									</div>
								</div>
						<?php if($data['client_picture']) { ?>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Uploaded Picture</label>
									<div class="col-sm-9">
										<img src="<?= base_url().$data['client_picture']; ?>" height="100px;">
									</div>
								</div>
						<?php	} ?>
								<div class="clearfix "></div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">General Practitioner/Doctor Name</label>
									<div class="col-sm-9">
										<input type="text" name="doctor_name" value="<?= $data['doctor_name']; ?>"  class="form-control default ip-ar" >
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">General Practitioner/Doctor Address</label>
									<div class="col-sm-9">
										<input type="text" name="doctor_address" value="<?= $data['doctor_address']; ?>"  class="form-control default ip-ar" >
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">General Practitioner/Doctor Contact</label>
									<div class="col-sm-9">
										<input type="text" name="doctor_contact_number" value="<?= $data['doctor_contact_number']; ?>"  class="form-control default ip-ar" >
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Allergies</label>
									<div class="col-sm-9">
										<input type="text" name="allergies" value="<?= $data['allergies']; ?>"  class="form-control default ip-ar" >
									</div>
								</div>
								
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Instructions</label>
									<div class="col-sm-9">
										<textarea type="text" name="comments" class="form-control default ip-ar" rows="10" placeholder="Instructions to carer"><?= $data['comments']; ?></textarea>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-12 form-group">
									<label class="col-sm-3 control-label text-right">Additional Information</label>
									<div class="col-sm-9">
										<textarea type="text" name="additional_information"  class="form-control default ip-ar" rows="8" placeholder="Additional instruction."><?= $data['additional_information']; ?></textarea>
									</div>
								</div>
								<div class="clearfix"></div>
								
								<hr />
								<div class="col-md-4 col-md-offset-4">
									<input class="submit btn btn-primary" name='submit' type="submit" value="Update Profile">&emsp;<button type="button" class="btn btn-sm btn-warning" onclick="goBack();">Cancel</button>
								</div>
							</form>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
	<?php }else {?>
	<div class="col-md-12 padding-0">
        <div class="col-md-12" style="padding-top: 50px;font-weight: 700;font-size: 25px;">
			<p style="font-weight: 550; color: #af5959; text-align: center;">Access Denied. Please contact owner.</p>
		</div>
	</div>
	<?php } ?>
</div>
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
		
    });
	
</script>
<!-- end: content -->