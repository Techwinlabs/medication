<link href="<?= base_url(); ?>asset/css/chat_style.css" rel="stylesheet">

<div id="content">
    <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top:20px;">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel-heading">
                <h3 class="">Chat with <?= $receiver_data['receiver_name']?></h3><a href="<?= base_url('admin/chat/'.$receiver_data['receiver_id'].'/clear'); ?>" title="Clear Chat" class="btn btn-danger pull-right new confirm">Clear Chat</a>&emsp;<a href="<?= base_url()?>admin/chat_room" class="btn btn-primary pull-right new">Back to Chat Room</a>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-box">
                        <div class="card-body no-padding height-9 mousescroll" id="chatlist">
                            <div class="row">
                                <div id="chat_div" style="position: relative; overflow: hidden; width: auto;">
                                    <ul class="chat nice-chat chat-page small-slimscroll-style" style="overflow: hidden; width: auto;">
                            <?php   foreach($data as $row){ ?>
                            <?php       if($receiver_data['receiver_id']==$row['sender_id']) { ?>
                                        <li class="in"><img src="<?= $receiver_data['receiver_picture']; ?>" class="avatar" alt="">
                                            <div class="message">
                                                <span class="arrow"></span>
                                                <span class="body"> <?= $row['message']; ?></span>
                                            </div>
                                            <span class="datetime"> <?= date("F j, Y, g:i a",strtotime($row['sent_date'])); ?> ( UTC +0:00 )</span>
                                        </li>
                            <?php       }else{ ?>
                               
                                        <li class="out"><img src="<?= $sender_data['sender_picture']; ?>" class="avatar" alt="">
                                            <div class="message">
                                                <span class="arrow"></span>
                                                <span class="body"> <?= $row['message']; ?> </span>
                                            </div>
                                            <span class="datetime"> <?= date("F j, Y, g:i a",strtotime($row['sent_date'])); ?> ( UTC +0:00 )</span>
                                        </li>
                            <?php       } ?>
                            <?php   } ?>
                                        <span id="append_message"></span>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer chat-box-submit">
                            <div class="input-group sn">
                            <input type="hidden" id="last_message" value="<?= count($data) ? end($data)['message_id']: 0; ?>">
                              <input name="message" id="newmessage" autocomplete="off" placeholder="Type your message..." class="form-control text-here" type="text">
                                <span class="paper-icon cir" style="cursor:pointer;" onclick="sendNewMessage(<?= $receiver_data['receiver_id']; ?>);"><i class="fa fa-paper-plane-o"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end: content -->
    <!-- plugins -->
    <script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
    <!-- custom -->
    <script src="<?= base_url(); ?>asset/js/main.js"></script>
    <script src="<?= base_url(); ?>asset/js/append.js"></script>
    <script>
        (function($) {
            jQuery.fn.extend({
                slimScroll: function(o) {
                    var ops = o;
                    //do it for every element that matches selector
                    this.each(function() {
                        var isOverPanel, isOverBar, isDragg, queueHide, barHeight,
                            divS = '<div></div>',
                            minBarHeight = 30,
                            wheelStep = 30,
                            o = ops || {},
                            cwidth = o.width || 'auto',
                            cheight = o.height || '250px',
                            size = o.size || '7px',
                            color = o.color || '#000',
                            position = o.position || 'right',
                            opacity = o.opacity || .4,
                            alwaysVisible = o.alwaysVisible === true;

                        //used in event handlers and for better minification
                        var me = $(this);

                        //wrap content
                        var wrapper = $(divS).css({
                            position: 'relative',
                            overflow: 'hidden',
                            width: cwidth,
                            height: cheight
                        }).attr({
                            'class': 'slimScrollDiv'
                        });

                        //update style for the div
                        me.css({
                            overflow: 'hidden',
                            width: cwidth,
                            height: cheight
                        });

                        //create scrollbar rail
                        var rail = $(divS).css({
                            width: '15px',
                            height: '100%',
                            position: 'absolute',
                            top: 0
                        });

                        //create scrollbar
                        var bar = $(divS).attr({
                            'class': 'slimScrollBar ',
                            style: 'border-radius: ' + size
                        }).css({
                            background: color,
                            width: size,
                            position: 'absolute',
                            top: 0,
                            opacity: opacity,
                            display: alwaysVisible ? 'block' : 'none',
                            BorderRadius: size,
                            MozBorderRadius: size,
                            WebkitBorderRadius: size,
                            zIndex: 99
                        });

                        //set position
                        var posCss = (position == 'right') ? {
                            right: '1px'
                        } : {
                            left: '1px'
                        };
                        rail.css(posCss);
                        bar.css(posCss);

                        //wrap it
                        me.wrap(wrapper);

                        //append to parent div
                        me.parent().append(bar);
                        me.parent().append(rail);

                        //make it draggable
                        bar.draggable({
                            axis: 'y',
                            containment: 'parent',
                            start: function() {
                                isDragg = true;
                            },
                            stop: function() {
                                isDragg = false;
                                hideBar();
                            },
                            drag: function(e) {
                                //scroll content
                                scrollContent(0, $(this).position().top, false);
                            }
                        });

                        //on rail over
                        rail.hover(function() {
                            showBar();
                        }, function() {
                            hideBar();
                        });

                        //on bar over
                        bar.hover(function() {
                            isOverBar = true;
                        }, function() {
                            isOverBar = false;
                        });

                        //show on parent mouseover
                        me.hover(function() {
                            isOverPanel = true;
                            showBar();
                            hideBar();
                        }, function() {
                            isOverPanel = false;
                            hideBar();
                        });

                        var _onWheel = function(e) {
                            //use mouse wheel only when mouse is over
                            if (!isOverPanel) {
                                return;
                            }
                            var e = e || window.event;
                            var delta = 0;
                            if (e.wheelDelta) {
                                delta = -e.wheelDelta / 120;
                            }
                            if (e.detail) {
                                delta = e.detail / 3;
                            }
                            //scroll content
                            scrollContent(0, delta, true);
                            //stop window scroll
                            if (e.preventDefault) {
                                e.preventDefault();
                            }
                            e.returnValue = false;
                        }

                        var scrollContent = function(x, y, isWheel) {
                            var delta = y;

                            if (isWheel) {
                                //move bar with mouse wheel
                                delta = bar.position().top + y * wheelStep;

                                //move bar, make sure it doesn't go out
                                delta = Math.max(delta, 0);
                                var maxTop = me.outerHeight() - bar.outerHeight();
                                delta = Math.min(delta, maxTop);

                                //scroll the scrollbar
                                bar.css({
                                    top: delta + 'px'
                                });
                            }

                            //calculate actual scroll amount
                            percentScroll = parseInt(bar.position().top) / (me.outerHeight() - bar.outerHeight());
                            delta = percentScroll * (me[0].scrollHeight - me.outerHeight());

                            //scroll content
                            me.scrollTop(delta);

                            //ensure bar is visible
                            showBar();
                        }

                        var attachWheel = function() {
                            if (window.addEventListener) {
                                this.addEventListener('DOMMouseScroll', _onWheel, false);
                                this.addEventListener('mousewheel', _onWheel, false);
                            } else {
                                document.attachEvent("onmousewheel", _onWheel)
                            }
                        }

                        //attach scroll events
                        attachWheel();

                        var getBarHeight = function() {
                            //calculate scrollbar height and make sure it is not too small
                            barHeight = Math.max((me.outerHeight() / me[0].scrollHeight) * me.outerHeight(), minBarHeight);
                            bar.css({
                                height: barHeight + 'px'
                            });
                        }

                        //set up initial height
                        getBarHeight();

                        var showBar = function() {
                            //recalculate bar height
                            getBarHeight();
                            clearTimeout(queueHide);

                            //show only when required
                            if (barHeight >= me.outerHeight()) {
                                return;
                            }
                            bar.fadeIn('fast');
                        }

                        var hideBar = function() {
                            //only hide when options allow it
                            if (!alwaysVisible) {
                                queueHide = setTimeout(function() {
                                    if (!isOverBar && !isDragg) {
                                        bar.fadeOut('slow');
                                    }
                                }, 1000);
                            }
                        }

                    });

                    //maintain chainability
                    return this;
                }
            });

            jQuery.fn.extend({
                slimscroll: jQuery.fn.slimScroll
            });

        })(jQuery);
        //invalid name call
        $('#chatlist').slimscroll({
            color: '#1ec8c8',
            size: '10px',
            width: '100%',
            height: '370px'
        });
        //invalid name call
        $('#chatlist1').slimscroll({
            color: '#1ec8c8',
            size: '10px',
            width: '240px',
            height: '420px'
        });

        var interval = 5000; 
        function refreshChat() {
            loadNewMessages(<?= $receiver_data['receiver_id']; ?>);
        }
        setInterval(refreshChat,interval);
        setTimeout(function(){ 
            $('#chatlist').scrollTop($('#chatlist')[0].scrollHeight);
         },
         500);

        $('#newmessage').keydown(function (e){
            if(e.keyCode == 13){
                sendNewMessage(<?= $receiver_data['receiver_id']; ?>);
            }
        })
        
    </script>
</body>
</html>