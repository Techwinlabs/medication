<link rel="stylesheet" type="text/css" href="<?= base_url('asset/css/bootstrap-select.css'); ?>">

<script src="<?= base_url('asset/js/bootstrap-select.js'); ?>"></script>

<style>

	.dropdown-chose-list{

		display: none;

	}

</style>

<div id="content">

	<div class="col-md-12 top-20 padding-0">
            <div class="col-md-12">
                <div class="panel-outer-border panel-main">

					<div class="panel-heading">
		                <h3>Manage Library</h3>
		            </div>


					<div class="panel-body">
						<div class="form-element frmelmnt-dtlar">

							<form class="cmxform" id="library">							

								<div class="col-md-12" style="padding: 0;">

									<div class="col-md-3">

										<select class="selectpicker" id="year" required data-live-search="true">

											<option value="">Select Year</option>

											<?php

											$currently_selected = date('Y');

											$earliest_year = 2000;

											$latest_year = date('Y');

											// Loops over each int[year] from current year, back to the $earliest_year [1950]

											foreach ( range( $latest_year, $earliest_year ) as $i ) {

											  // Prints the option with the next year in range.

											  print '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';

											}

											?>

										</select>

									</div>

									<div class="col-md-3">

										<select class="selectpicker" id="month" required data-live-search="true">

											<option value="">Select Month</option>

											<option value="01">January</option>

											<option value="02">February</option>

											<option value="03">March</option>

											<option value="04">April</option>

											<option value="05">May</option>

											<option value="06">June</option>

											<option value="07">July</option>

											<option value="08">August</option>

											<option value="09">September</option>

											<option value="10">October</option>

											<option value="11">November</option>

											<option value="12">December</option>

										</select>

									</div>

									<div class="col-md-3">

										<select class="selectpicker" id="client" required data-live-search="true">

											<option value="">Select Client</option>

											<?php if(count($clients)){ for($i=0;$i<count($clients);$i++){ ?>

												<option value="<?= $clients[$i]['client_id']; ?>"><?= strtoupper($clients[$i]['firstname'].' '.$clients[$i]['lastname']); ?></option>

											<?php } } ?>

										</select>										

									</div>

									<div class="col-md-3">

										<input class="btn btn-info" name='submit' type="button" value="Get Records" onclick="get_records()">

									</div>

								</div>

							</form>




						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

<div id="ajax_content">

</div>

<!-- end: content -->

<!-- plugins -->

<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>

<script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>

<script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>

<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>



<!-- custom -->

<script src="<?= base_url(); ?>asset/js/main.js"></script>

<script src="<?= base_url(); ?>asset/js/append.js"></script>

<script type="text/javascript">

   	function get_records() {

		var year 	= 	$('#year').val();

		var month 	= 	$('#month').val();

		var fullDate = new Date();

		var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) :(fullDate.getMonth()+1);

		var currentDate = twoDigitMonth + "/" + fullDate.getDate() + "/" + fullDate.getFullYear();

		var selectdate = month+'/1/'+year;

		if(Date.parse(selectdate)>Date.parse(currentDate)){

			alert("You can't select upcoming month.");

		}else{

			var client 	=	$('#client').val();

			if(client=="" || month=="" || year==""){

				alert("Please fill all requied fields");

			}else{

				$.ajax({

					type:'POST',

					url:'../admin/ajax/get_records',

					data:{'year':year,'month':month,'client':client},

					success: function(data){

						if (data) {

							$('#ajax_content').html(data);

						}

					}

				});

			}

		}

    }

	

</script>

</body>

</html>