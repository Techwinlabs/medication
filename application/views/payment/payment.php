	<div id="content">
		<div class="col-md-12 top-20 padding-0">
			<div class="col-md-12">
				<div class="panel">
					<div class="panel-heading">
						<h3 class="animated fadeInLeft">Payment required</h3></div>
					<div class="panel-body">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
						?>
						<div class="col-md-12">
							<p class="card-text">Your free 30 days trial has been expired. Please make the payment to resume your services.</p>
							<br />
						   <form method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/Payment/check">
							  <script
								src="https://checkout.stripe.com/checkout.js" class="stripe-button"
								data-key="pk_test_LXmk1sX3bcBmeNpwlcAukcmx"
								data-amount="24400"
								data-name="Medication App"
								data-description="Monthly service charges"
								data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
								data-locale="auto"
								data-zip-code="true">
							  </script>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
<!-- custom -->
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>
	</body>
</html>