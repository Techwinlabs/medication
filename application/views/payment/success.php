	<div id="content">
		<div class="col-md-12 top-20 padding-0">
			<div class="col-md-12">
				<div class="panel">
					<div class="panel-heading">
						<h3 class="animated fadeInLeft">Payment Successful</h3></div>
					<div class="panel-body">
						<div class="col-md-12">
						    <h4 class="card-title">Transaction id :  # <?= $this->session->flashdata('transaction_id'); ?></h4>
							<p class="card-text">We received your payment to resume your Medication app Administration rights, please check your email for more information.</p>
							<a href="<?php echo base_url('admin'); ?>" class="btn btn-info btn-sm float-right">Go Home</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- plugins -->
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
<!-- custom -->
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="<?= base_url(); ?>asset/js/append.js"></script>
	
	</body>
</html>