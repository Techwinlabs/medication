<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_companies extends CI_Model {
	
	public function get_companies(){
		$data = $this->db->query("SELECT `company_id`,(SELECT count(client_id) FROM med_clients WHERE med_clients.company_id=a.company_id) as used_client,(SELECT count(employeeid) FROM med_employees WHERE med_employees.company_id=a.company_id AND med_employees.is_active='1') as used_emp,`total_client`,`total_emp`,`company_name`, `contact_person`, `email`, `country_code`, `mobile_number`, IF (`logo`='', '',CONCAT('".base_url()."','',logo)) AS logo, `address`, `city`, `state`, `zipcode`, `country`, `status`, `created_at`, `updated_at`, `billing_plan`, SUBSTRING_INDEX(`billing_start_date`, ',', -1) as `billing_start_date`,(select count(distinct(employeeid)) from med_employees_authentication where employeeid in (select employeeid from med_employees where company_id = a.company_id)) as loggedin FROM `med_companies` as a ORDER by `company_id` DESC")->result_array();
		for($i=0;$i<count($data);$i++){
			
			if($data[$i]['billing_start_date']=="" && $data[$i]['billing_plan']==0 && $data[$i]['status']!=2){
				$data[$i]['status'] = 1;
				$data[$i]['plan'] = "<font color='#009688'>14 days (Not Started Yet)</font>";
				$data[$i]['status-msg'] = "<font color='blue'>Trial</font>";
			}elseif($data[$i]['billing_start_date']!="" && $data[$i]['billing_plan']==0 && $data[$i]['status']!=2){
				$max = strtotime(date("Y-m-d H:i:s",strtotime("+ 14 days",strtotime($data[$i]['billing_start_date']))));
				$now = strtotime(date("Y-m-d H:i:s"));
				$days =  round(($max-$now)/(60 * 60 * 24));
				if($days>0){
					$data[$i]['status'] = 1;
					$data[$i]['plan'] = "<font color='blue'>".$days." days</font>";
					$data[$i]['status-msg'] = "<font color='blue'>Trial</font>";
				}else{
					$data[$i]['status'] = 2;
					$data[$i]['plan'] = "Free trail ( <font color='red'>Expired</font> )";
					$data[$i]['status-msg'] = "<font color='Red'>Inactive</font>";
				}
			}elseif($data[$i]['status']==2){
				$data[$i]['plan'] = "-NA-";
				$data[$i]['status-msg'] = "<font color='red'>Inactive</font>";
			}else{
				$data[$i]['status'] = 3;
				$data[$i]['plan'] = "-NA-";
				$data[$i]['status-msg'] = "<font color='green'>Active</font>";
			}
		}
		return $data;
	}
	
	
	public function viewCompany_profile($input){
		$data = $this->db->query("SELECT `company_id`,`total_client`,`total_emp`,(SELECT count(client_id) FROM med_clients WHERE med_clients.company_id=a.company_id) as used_client,(SELECT count(employeeid) FROM med_employees WHERE med_employees.company_id=a.company_id AND med_employees.is_active='1') as used_emp, `company_name`, `contact_person`, `email`, `country_code`, `mobile_number`, IF (`logo`='', '',CONCAT('".base_url()."','',logo)) AS logo, `address`, `city`, `state`, `zipcode`, `country`, `status`, `created_at`, `updated_at`, `billing_plan`, `billing_start_date`,(select employee_mother_maiden from med_employees where company_id = a.company_id and employee_type = 2 limit 1) as employee_mother_maiden,(select count(employeeid) from med_employees where company_id = a.company_id and employee_type = 1) as total_carer,(select count(employeeid) from med_employees where company_id = a.company_id and employee_type = 2) as total_manager,(select count(employeeid) from med_employees where company_id = a.company_id and employee_type = 3) as total_supervisor FROM `med_companies` as a WHERE `company_id`='".$input['company_id']."'")->row_array();
		if($data['billing_start_date']=="" && $data['billing_plan']==0 && $data['status']!=2){
				$data['plan'] = "<font color='#009688'>30 days (Not Started Yet)</font>";
		}elseif($data['billing_start_date']!="" && $data['billing_plan']==0 && $data['status']!=2){
				$max = strtotime(date("Y-m-d H:i:s",strtotime("+ 30 days",strtotime($data['billing_start_date']))));
				$now = strtotime(date("Y-m-d H:i:s"));
				$days =  round(($max-$now)/(60 * 60 * 24));
				if($days>0){
					$data['plan'] = "<font color='blue'>".$days." days</font>";
				}else{
					$data['plan'] = "Free trail ( <font color='red'>Expired</font> )";
				}
			}elseif($data['status']==2){
				$data['plan'] = "<font color='red'>Inactive</font>";
			}else{
				$data['status'] = 3;
				$data['plan'] = "<font color='green'>Active</font>";
			}
		
		return $data;
	}
	public function new_company($input){
		
		$input['created_at']		= today()[0];
		$input['updated_at']		= today()[0];
		$agent 						= $input['contact_person'];
		$agent_email 				= $input['email'];
		$mother_maiden 				= $input['mother_maiden'];
		$total_emp					= $input['total_emp'];
		$total_client				= $input['total_client'];
		unset($input['submit'],$input['contact_person'],$input['email'],$input['mother_maiden']);
		$input['contact_person'] 	= $agent[0];
		$input['email'] 			= $agent_email[0];
		$input['status']			= 1;
		$config['allowed_types'] 	= '*';
		$date 						= date("Y-m");
		$input['logo'] 				= "";
		if (!empty($_FILES['logo']['name'])){
			$path	=  './media/company/logo/'.$date."/";
			if (!is_dir($path)){
				mkdir($path , 0777);
				$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
				$fp = fopen($path."index.html","wb"); 
				fwrite($fp,$content); 
				fclose($fp); 
			}
			$config['file_name']	= date("Ymd")."_CL-".rand(1000,9999).'-'.time();
			$config['upload_path']	= $path;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('logo')){
				return array('status' =>400,'msg' =>"Logo uploading error: ".strip_tags($this->upload->display_errors()));
				exit;
			}else{
				$input['logo'] = ltrim($path,"./").$this->upload->data()['file_name'];
			}
		}
		$check =  $this->db->query("SELECT employeeid FROM `med_employees` WHERE employee_email='".$agent_email[0]."'")->row_array();
		if(count($check)){
			return array('status' => 400,'msg' => $agent_email[0].' email address is already associated with an account!');
		}else{
			$this->db->trans_start();
			$this->db->insert('med_companies',$input);
			$company_id = $this->db->insert_id();
			
			$already ="";
			for($i=0;$i<count($agent);$i++){
				
				if($i>0){
					$check =  $this->db->query("SELECT employeeid FROM `med_employees` WHERE employee_email='".$agent_email[$i]."'")->row_array();
					if(count($check)){
						$already .=$agent_email[$i]." ";
						continue;
					}
				}
				$random		= randomCode();
				$random1 	= rand(14567,98789);
				$password 	= crypt($random,APP_SALT);
				$password_pin 	= crypt($random1,APP_SALT);
				$employe_number = substr($input['company_name'],0,4).$company_id.'00'.($i+1);
				$this->db->insert('med_employees',array('company_id'=>$company_id,'employee_number'=>$employe_number,'employee_name'=>$agent[$i],'employee_email'=>$agent_email[$i],'employee_password'=>$password,'employee_pin'=>$password_pin,'employee_mother_maiden'=>$mother_maiden[$i],'employee_designation'=>2,'employee_type'=>2));
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'msg' => 'Internal server error. Please try again!');
					break;
				}else{
					$email_message = "";
					$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
					$email_message .= "<div>Hi, ".$agent[$i]."</div>" ;
					if($i==0){
						$email_message .= "<div>".$input['company_name']." account has setup successfully. Below you will find the credential to access your company admin panel.</div>" ;
						$email_message .= "<div>Admin Panel Link : ".base_url()."admin</div>" ;
						$email_message .= "<div>Username : ".$agent_email[$i]."</div>" ;
						$email_message .= "<div>Password : ".$random."</div>" ;
						
					//	$email_message .= "<div><b>Mobile App Credential:</div>" ;
						//$email_message .= "<div>Username : ".$agent_email[$i]."</div>" ;
					//	$email_message .= "<div>Password : ".$random1."</div>" ;
					//	$email_message .= "<div>Please use these credential to login your company admin panel and Medication app account. After successful login we recommend you to change your password.</div>" ;
						$email_message .= "<div>Please use these credential to login your company DCR admin panel account. After successful login we recommend you to change your password.</div>" ;
					}else{
						$email_message .= "<div>".$input['company_name']." account has setup successfully. Below you will find the credential to access your medication account.</div>" ;
						$email_message .= "<div><div><b>Admin Panel Credential:</div>" ;
						$email_message .= "<div>Username : ".$agent_email[$i]."</div>" ;
						$email_message .= "<div>Password : ".$random."</div>" ;
						//$email_message .= "<div><div><b>Mobile App Credential:</div>" ;
						//$email_message .= "<div>Username : ".$agent_email[$i]."</div>" ;
						//$email_message .= "<div>Password : ".$random1."</div>" ;
						$email_message .= "<div>Please use these credential to login DCR account. After successful login we recommend you to change your password.</div>" ;
					}
					$email_message .= "<div>Thanks</div>" ;
					/*if($i==0){
						$subject = 'Medication app - '.$input['company_name'].' admin panel login credential';
					}else{
						$subject = 'Medication app login credential';
					}*/
					$subject 	= 'Medication app - '.$input['company_name'].' admin panel login credential';
					$from 		= 'noreply@medication.com';
					$to	  		= $agent_email[$i];

					$this->mail_send($to,$from,$subject,$email_message);
				}

			}
			$this->db->trans_commit();
			if($already!=""){
				$already = "<br />Note: Account already exisit with ".$already." email id(s).";
			}
			return array('status' => 200,'msg' =>'Company created. Admin panel link and credential has sent to contact person\'s mail box successfully.'.$already);
		}
	}
	
	public function update_company($input){
		$check = $this->db->query("SELECT `logo` FROM `med_companies` WHERE `company_id`='".$input['company_id']."' AND `email`='".$input['email']."'")->row_array();
		if (count($check)){
			$logo = $check['logo'];
			if (!empty($_FILES['logo']['name'])){
				$config['allowed_types'] 	= '*';
				$date 						= date("Y-m");
				$path	=  './media/company/logo/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_CL-".rand(1000,9999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('logo')){
					$logo = $check['logo'];
				}else{
					$logo = ltrim($path,"./").$this->upload->data()['file_name'];
				}
			}
			$this->db->trans_start();
			$this->db->query("UPDATE `med_companies` SET `company_name`='".$input['company_name']."',`contact_person`='".$input['contact_person']."',`country_code`='".$input['country_code']."',`mobile_number`='".$input['mobile_number']."',`logo`='".$logo."',`address`='".$input['address']."',`city`='".$input['city']."',`state`='".$input['state']."',`zipcode`='".$input['zipcode']."',`country`='".$input['country']."',`total_emp`='".$input['total_emp']."',`total_client`='".$input['total_client']."' WHERE company_id='".$input['company_id']."'");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Company information updated');
			}
		}else{
			return array('status' => 400,'msg' => 'Unauthorized access.');
		}
		
	}
	
	
	public function Restart_trial($input){
		$check = $this->db->query("SELECT billing_start_date FROM `med_companies` WHERE `company_id`='".$input['company_id']."'")->row_array();
		if (count($check)){
			$billing_start_date = $check['billing_start_date'];
			if($billing_start_date==""){
				$update_trial = "'".today()[0]."'";
			}else{
				$update_trial = "CONCAT(billing_start_date,',".today()[0]."')"; 
			}
			$this->db->query("UPDATE med_companies SET billing_start_date=".$update_trial.",billing_plan=0 WHERE company_id='".$input['company_id']."'");
			return array('status' => 200,'msg' =>'Trial restarted successfully');
		}else{
			return array('status' => 400,'msg' =>'No Company found! Try again.');
		}
	}
	
	public function Begin_service($input){
		$check = $this->db->query("SELECT billing_start_date FROM `med_companies` WHERE `company_id`='".$input['company_id']."'")->row_array();
		if (count($check)){
			$this->db->query("UPDATE med_companies SET billing_plan=1,status=1,created_at='".today()[0]."' WHERE company_id='".$input['company_id']."'");
			return array('status' => 200,'msg' =>'Service started successfully');
		}else{
			return array('status' => 400,'msg' =>'No Company found! Try again.');
		}
	}
	public function End_service($input){
		$check = $this->db->query("SELECT billing_start_date FROM `med_companies` WHERE `company_id`='".$input['company_id']."'")->row_array();
		if (count($check)){
			$billing_start_date = $check['billing_start_date'];
			if($billing_start_date==""){
				$update_trial = today()[0];
			}else{
				$update_trial = "CONCAT(billing_start_date,',".today()[0]."')"; 
			}
			$this->db->query("UPDATE med_companies SET status=2,billing_plan=0,updated_at='".today()[0]."' WHERE company_id='".$input['company_id']."'");
			return array('status' => 200,'msg' =>'Service stoped successfully');
		}else{
			return array('status' => 400,'msg' =>'No Company found! Try again.');
		}
	}

	public function change_password($data){
		$this->load->helper('common_helper');
		$superid = $this->session->userdata()['superdata']['super_id'];
		$check 	 = $this->db->query('SELECT * FROM med_super WHERE super_id="'.$superid.'"')->row_array();	
		if (count($check)>0) {
			$hashed_password = $check['password'];
			
			if (hash_equals($hashed_password, crypt($data['old'],APP_SALT))) {
				$this->db->trans_start();
			   	$this->db->query("UPDATE med_super SET email='".$data['email']."', password='".crypt($data['new'],APP_SALT)."', modifiedat='".today()[0]."' WHERE super_id='".$superid."'");
			   	$this->db->trans_complete();
			   	if ($this->db->trans_status() === FALSE) {
			   		$this->db->trans_rollback();
			   		return 500;
			   	}else{
			   		$this->db->trans_commit();
			   		return 200;
			   	}
			}else{
				return 400;
			}
		} else {
			return 401;
		}
	}
	
	public function get_super_user_email(){
		$this->load->helper('common_helper');
		$superid = $this->session->userdata()['superdata']['super_id'];
		$check 	 = $this->db->query('SELECT * FROM med_super WHERE super_id="'.$superid.'"')->row_array();
		return $check;
	}
	
	
	// Function for agents 
	public function getCompanyAgents($input){
		$data = $this->db->query("select * from med_employees where employee_type=2 and company_id=".$input['company_id'])->result_array();
		return $data;
	}
	
	public function get_profile($id){
		$data = $this->db->select('*')->from('med_employees')->where(array('employeeid'=>$id))->get()->row_array();
		return $data;
	}
	public function syper_update_employee($input){
		
		$check = $this->db->select('employee_email')->from('med_employees')->where(array('employeeid!='=>$input['employeeid'],'employee_email'=>$input['employee_email'],'company_id'=>$input['company_id']))->get()->row_array();
		if(count($check)==0){
			unset($input['submit']);
			
			$input['employee_type']	= 2;
			//$input['created_at']	= today()[0];
			$input['updated_at']	= today()[0];
			
			/*$input['employee_picture']	= '';
			$config['allowed_types'] 	= '*';
			$config['max_size'] 		= '1024';
			$date 						= date("Y-m");
			if (!empty($_FILES['employee_picture']['name'])){
				$path	=  './media/profile/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content);
					fclose($fp);
				}
				$config['file_name']	= date("Ymd")."_PP-".rand(1000,9999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('employee_picture')){
					return array('status' =>400,'msg' =>"Profile picture uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					$input['employee_picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
				}
			}else{
				$input['employee_picture']	=	$input['employee_picture_hidden'];
			}
			*/
			$employeeid = $input['employeeid'];
			unset($input['employeeid']);
			unset($input['employee_picture_hidden']);
			$input['section_roles']		= implode(',',$input['section_roles']);
			$this->db->trans_start();
			$this->db->where('employeeid', $employeeid);
			$this->db->update('med_employees',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Account updated successfully.');
			}
		}else{
			return array('status' => 400,'msg' =>'Account already exists with '.$input['employee_email']);
		}		
	}
	
	public function super_delete_employee($input)
	{
		$emp_id	=	$input['emp_id'];
		# get employee
		$emp_type =	2;
		if($emp_type == 2){	# manager
			$this->db->query("delete from med_activities_history where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_carer_notifications where authority_id = '".$emp_id."'");
			$this->db->query("delete from med_concerns where supervisor_id = '".$emp_id."'");
			$this->db->query("delete from med_concern_replies where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_daily_records where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_data_usage where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_employees_authentication where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_medication_chart where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_message where ( sender_id = '".$emp_id."' or receiver_id = '".$emp_id."' )");
			$this->db->query("delete from med_notifications where authority_id = '".$emp_id."'");
			$this->db->query("delete from med_employees where employeeid = '".$emp_id."'");
		}
		return 200;
	}

	public function mail_send($to,$from,$subject,$email_message){
		$this->load->library('email');
		$config['protocol']    = 'smtp';
		$config['smtp_host']    = 'smtp.1and1.com';
		$config['smtp_port']    = '587';
		$config['smtp_timeout'] = '7';
		$config['smtp_user']    = 'noreply@dailycarerecords.com';
		$config['smtp_pass']    = 'Dailycarerecords!@#123';
		$config['charset']    = 'utf-8';
		$config['newline']    = "\r\n";
		$this->email->initialize($config);
		$this->email->from('noreply@dailycarerecords.com', 'DailyCareRecords Support Team');
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->set_mailtype("html");
		$this->email->message($email_message);
		$this->email->send();
		//dump($this->email->print_debugger());
	}
}