<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_auth extends CI_Model {
	
	public function login($email, $pass) {
		
		$data =  $this->db->query("SELECT `super_id`, `name`, `email`, `password` FROM `med_super` WHERE `email`='".$email."' AND `is_active`='1'")->row_array();
		if (count($data)) {
			$hashed_password = $data['password'];
			unset($data['password']);
			if (!hash_equals($hashed_password, crypt($pass,APP_SALT))) {
			   return 400;
			}else{
				return $data;
			}
		} else {
			return 401;
		}
	}
	public function masquerade($company_id) {
		$this->db->select('company_id');
		$this->db->from('companies');
		$this->db->where('company_id', $company_id);
		$data = $this->db->get();

		if ($data->num_rows() == 1) {
			$data = array('user_id'=>'0','company_id'=>$company_id,'user_name'=>'Super Admin','email'=>'super@ceeca.com','profile_picture'=>'');
			return $data;
		} else {
			return false;
		}
	}
}
