<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super extends CI_Model{
	
	public function useranalytics(){
		$user_graph = $this->db->select("count(employeeid) as total, DATE_FORMAT(created_at, '%Y-%m-01') as date,DATE_FORMAT(created_at, '%Y') as year,DATE_FORMAT(created_at, '%m') as month")->from('med_employees')->group_by("DATE_FORMAT(created_at, '%Y-%m-01')")->order_by("year","ASC")->order_by("month","asc")->get()->result_array();
		return $user_graph;
		
	}
	
}