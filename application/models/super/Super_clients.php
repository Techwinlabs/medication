<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_clients extends CI_Model {
	
	public function select_allclients(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$data = $this->db->query("SELECT `company_id`, `company_name`, `contact_person`, `email`, `country_code`, `mobile_number`, `logo`, `address`, `city`, `state`, `zipcode`, `country`, `status`, `created_at`, `updated_at` FROM `med_companies` WHERE `status`=1 ORDER BY `created_at` DESC")->result_array();
		return $data;
	}
	
	
	
}