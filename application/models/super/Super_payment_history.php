<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_payment_history extends CI_Model {
	
	public function get_payments(){
		$data = $this->db->query("SELECT med_payments.*,med_companies.company_name FROM `med_payments` JOIN med_companies ON med_companies.company_id=med_payments.company_id ORDER BY med_payments.payment_id DESC")->result_array();
		return $data;
		
	}
	
	
}