<?php
/**
 * App Name       -    	Medication
 * author         -     Hem Thakur
 * created        -     10/04/2018
**/		
class Cronjob extends CI_Model {
	
	public function exeSchedule()
	{
		$servertz = (date_default_timezone_get());
		$now = date("Y-m-d H:i:s");
		$data = $this->db->query("SELECT employeeid,company_id,client_id,schedule_id,start_datetime,end_datetime,secheduled_by,(select employee_name from med_employees where employeeid = a.employeeid ) as employeename,(select employee_name from med_employees where employeeid = a.client_id ) as clientname from med_schedule as a where status = 0 and end_datetime < '".$now."'")->result_array();
		$done = [];
		for($i=0;$i<count($data);$i++){
						
			## get all managers
			$all_managers = $this->db->query("select employeeid,employee_type,section_roles from med_employees where company_id = '".$data[$i]['company_id']."' and employee_type = 3 ")->result_array();
			
			## Notification feed
			$this->db->insert('med_notifications',array("notification_type"=>2, "authority_id"=>0, "carer_id"=>$data[$i]['employeeid'],"table_id"=>$data[$i]['schedule_id'],"filter"=>4,"created_at"=>today()[0]));
			
			if(count($all_managers)){
				for($j=0;$j<count($all_managers);$j++){
					$authority_id	=	$all_managers[$j]['employeeid'];
					$authority_type	=	$all_managers[$j]['employee_type'];
					
					$check_read = $this->db->query("select badge from med_authority_notification_read where authority_id = '".$authority_id."'")->row_array();
					$badge = 1;
					if(count($check_read)){
						$this->db->query("update med_authority_notification_read set badge = badge + 1 where authority_id = '".$authority_id."'");
						$badge = $check_read['badge']+1;
					}else{
						$this->db->query("insert into med_authority_notification_read (authority_id,badge) values ('".$authority_id."',1)");
					}
					
					if($authority_type == 2){
						#Send Push Notification
						$keys = array("schedule_id"=>$data[$i]['schedule_id']);
						$message = $data[$i]['employeename']." has not given medication to ".$data[$i]['clientname']." today";
						$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$authority_id."'")->result_array();
						foreach($tokens as $token){
							if(strlen($token['device_token'])>30){
								if($token['device_type']==1){
									applePush($token['device_token'],$message,$badge,1,$keys);
								}else{
									andiPush($token['device_token'],$message,$badge,1,$keys);
								}
							}
						}
					}else{
						$authority_role	=	$all_managers[$j]['section_roles'];
						$role_arr		=	explode(',',$authority_role);
						if(in_array(4,$role_arr)){
							## Notification feed
							$this->db->insert('med_notifications',array("notification_type"=>2, "authority_id"=>$authority_id, "carer_id"=>$data[$i]['employeeid'],"table_id"=>$data[$i]['schedule_id'],"filter"=>4,"created_at"=>today()[0]));
					
							#Send Push Notification
							$keys = array("schedule_id"=>$data[$i]['schedule_id']);
							$message = $data[$i]['employeename']." has not given medication to ".$data[$i]['clientname']." today";
							$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$authority_id."'")->result_array();
							foreach($tokens as $token){
								if(strlen($token['device_token'])>30){
									if($token['device_type']==1){
										applePush($token['device_token'],$message,$badge,1,$keys);
									}else{
										andiPush($token['device_token'],$message,$badge,1,$keys);
									}
								}
							}
						}
					}
				}
			}
			
			// update row
			$this->db->query("UPDATE med_schedule SET status='2' WHERE schedule_id='".$data[$i]['schedule_id']."'");
		}
	}
		
	# daily record not entered
	
	public function exe_runDailyRecord()
	{
		$servertz = (date_default_timezone_get());
		$now = date("Y-m-d H:i");
		
		$getschedule = $this->db->query("SELECT * FROM med_schedule order by schedule_id desc")->result_array();
		if(count($getschedule)>0){
			for($i=0;$i<count($getschedule);$i++){
				$schedule_id	=	$getschedule[$i]['schedule_id'];
				$company_id		=	$getschedule[$i]['company_id'];
				$employeeid		=	$getschedule[$i]['employeeid'];
				$emp_name		=	getAnythingData("med_employees","employeeid",$employeeid,'employee_name');
				$client_id		=	$getschedule[$i]['client_id'];
				$start_datetime	=	$getschedule[$i]['start_datetime'];
				$end_datetime	=	$getschedule[$i]['end_datetime'];
				$secheduled_by	=	$getschedule[$i]['secheduled_by'];
				
				$timezone	 = getAnythingData("med_employees","employeeid",$secheduled_by,'timezone');
				if($timezone == ''){
					$timezone = 'UTC';
				}
				date_default_timezone_set($timezone);
				
				$local_now_st 	= 	date("Y-m-d H:i",strtotime("-2 minutes"));
				$local_now_ed 	= 	date("Y-m-d H:i",strtotime("+2 minutes"));
				$match_time		=	date('Y-m-d H:i',strtotime('+30 minutes',strtotime($end_datetime)));
				if($local_now_st < $match_time && $match_time < $local_now_ed){
					echo "test"; echo "<br>";
					#check daily record
					$checkdaily		=	$this->db->query("select record_id from med_daily_records where date between '".$start_datetime."' and '".$end_datetime."' and employeeid = '".$employeeid."' and client_id = ".$client_id."")->result_array();
					if(count($checkdaily)==0){
						
						## Notification feed
						$this->db->insert('med_notifications',array("notification_type"=>1, "authority_id"=>$authority_id, "carer_id"=>$employeeid,"table_id"=>$schedule_id,"filter"=>5,"created_at"=>today()[0]));
						
						## get all managers
						$all_managers = $this->db->query("select employeeid,employee_type,section_roles from med_employees where company_id = '".$company_id."' and employee_type = 3")->result_array();
						
						if(count($all_managers)){
							for($j=0;$j<count($all_managers);$j++){
								$authority_id	=	$all_managers[$j]['employeeid'];
								$authority_type	=	$all_managers[$j]['employee_type'];
								
								$check_read = $this->db->query("select badge from med_authority_notification_read where authority_id = '".$authority_id."'")->row_array();
								$badge = 1;
								if(count($check_read)){
									$this->db->query("update med_authority_notification_read set badge = badge + 1 where authority_id = '".$authority_id."'");
									$badge = $check_read['badge']+1;
								}else{
									$this->db->query("insert into med_authority_notification_read (authority_id,badge) values ('".$authority_id."',1)");
								}
								
								if($authority_type == 2){
									#Send Push Notification
									$keys = array("schedule_id"=>$schedule_id);
									$message = $emp_name." has not added daily record";
									$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$authority_id."'")->result_array();
									foreach($tokens as $token){
										if(strlen($token['device_token'])>30){
											if($token['device_type']==1){
												applePush($token['device_token'],$message,$badge,3,$keys);
											}else{
												andiPush($token['device_token'],$message,$badge,3,$keys);
											}
										}
									}
								}else{
									$authority_role	=	$all_managers[$j]['section_roles'];
									$role_arr		=	explode(',',$authority_role);
									if(in_array(4,$role_arr)){
										## Notification feed
										$this->db->insert('med_notifications',array("notification_type"=>1, "authority_id"=>$authority_id, "carer_id"=>$employeeid,"table_id"=>$schedule_id,"filter"=>5,"created_at"=>today()[0]));
							
										#Send Push Notification
										$keys = array("schedule_id"=>$schedule_id);
										$message = $emp_name." has not added daily record";
										$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$authority_id."'")->result_array();
										foreach($tokens as $token){
											if(strlen($token['device_token'])>30){
												if($token['device_type']==1){
													applePush($token['device_token'],$message,$badge,3,$keys);
												}else{
													andiPush($token['device_token'],$message,$badge,3,$keys);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public function exe_runNotificationToApp(){
			//For profile updation
			$getUpdatedClient  = $this->db->query("select * from med_clients where updated_at > date_sub(now(), interval 3 minute)")->result_array();
			if($getUpdatedClient)
			{
				$this->load->helper('common_helper');
				foreach($getUpdatedClient as $clientInfo){
					if($clientInfo['noti_send']==1){
						$carer  = $this->db->query("SELECT employeeid FROM med_employees WHERE employee_type=1 AND company_id='".$clientInfo['company_id']."'")->result_array();
						foreach($carer as $user){
							## Notification feed
							$this->db->insert('med_carer_notifications',array("notification_type"=>3, "authority_id"=>$user['employeeid'], "carer_id"=>$user['employeeid'],"table_id"=>$clientInfo['client_id'],"main_id"=>$clientInfo['client_id'],"created_at"=>today()[0]));
							
							#Send Push Notification
							$keys = array("client_id"=>$clientInfo['client_id']);
							$message = $clientInfo['firstname'].' '.$clientInfo['lastname']." updated his profile";
							$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$user['employeeid']."'")->result_array();
								foreach($tokens as $token){
									if(strlen($token['device_token'])>30){
									if($token['device_type']==1){
										applePush($token['device_token'],$message,1,4,$keys);
									}else{
										andiPush($token['device_token'],$message,1,4,$keys);
									}
								}
							}
						}
						//update the status to 0 of send noti field 
						$this->db->where(array('client_id' => $clientInfo['client_id']))->update('med_clients',array('noti_send'=>0));
					}
				}
			}else{
				echo "No profile updation Found!";
			}
			
			//For medication update
			
			$getUpdatedMedication  = $this->db->query("select * from med_care_program where noti_send=1 and  program_updated > date_sub(now(), interval 3 minute)")->result_array();
			if($getUpdatedMedication)
			{
				$this->load->helper('common_helper');
				foreach($getUpdatedMedication as $clientInfo){
					$companyID  = $this->db->query("SELECT company_id FROM med_clients WHERE client_id='".$clientInfo['client_id']."'")->result_array();
					$company_id 		 = $companyID[0]['company_id'];
					$carer  = $this->db->query("SELECT employeeid FROM med_employees WHERE employee_type=1 AND company_id='".$company_id."'")->result_array();
					foreach($carer as $user){
						## Notification feed
						$this->db->insert('med_carer_notifications',array("notification_type"=>4, "authority_id"=>$clientInfo['employeeid'], "carer_id"=>$user['employeeid'],"table_id"=>$clientInfo['client_id'],"main_id"=>$clientInfo['program_id'],"created_at"=>today()[0]));
						#Send Push Notification
						$keys = array("client_id"=>$clientInfo['client_id']);
						$message = "Program of care updated";
						$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$user['employeeid']."'")->result_array();
						foreach($tokens as $token){
							if(strlen($token['device_token'])>30){
								if($token['device_type']==1){
									applePush($token['device_token'],$message,1,6,$keys);  #section 6 client program update
								}else{
									andiPush($token['device_token'],$message,1,6,$keys);
								}
							}
						}
					}
				}
			}else{
				echo "No medication found!";
			}
	}
}