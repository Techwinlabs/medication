<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Model {
	
	
	/*
	* Get Country Code
	*/
	public function get_countryCode()
	{	
		$data = $this->db->select('countries_name, country_code, countries_iso_code as flag')->from('med_countries')->order_by('countries_name','ASC')->get()->result_array();
		for($i=0;$i<count($data);$i++){
			$data[$i]['flag'] = base_url().'media/flags/'.strtolower($data[$i]['flag']).'.png';
		}
		return $data;
	}
	
	/*
	* Get Designations List
	*/
	public function get_designationsList($company_id)
	{
		$data = $this->db->select('designation_id,designation')->from('med_designations')->order_by('designation','ASC')->get()->result_array();
		return $data;
	}
	
	
	/*
	* Get Client List
	*/
	public function get_ClientList($company_id)
	{
		$data = $this->db->query("SELECT `client_id`, CONCAT(`firstname`,' ',`lastname`) as client_name, postcode FROM `med_clients` WHERE `company_id`='".$company_id."' AND `status`='1'")->result_array();
		return $data;
	}
	
	/*
	* Get Carer List
	*/
	public function get_CarerList($company_id)
	{
		$data = $this->db->query("SELECT `employeeid`,`employee_name`,`employee_number`,`employee_picture` FROM `med_employees` WHERE `company_id`='".$company_id."' AND `is_active`='1' AND `employee_type`='1'")->result_array();
		return $data;
	}
	
	public function load_stats($company_id){
		$data['total'] 				= $this->db->query("SELECT total_client,total_emp FROM med_companies WHERE company_id='".$company_id."'")->row_array();
		$data['used']['employee']	= $this->db->query("SELECT employeeid FROM med_employees WHERE company_id='".$company_id."' AND is_active='1'")->num_rows();
		$data['used']['client']		= $this->db->query("SELECT client_id FROM med_clients WHERE company_id='".$company_id."'")->num_rows();

		return $data;
	}
	
}
