<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_auth extends CI_Model {
	
	public function login($employee_number, $pass) {
		
		$data =  $this->db->query("SELECT * FROM `med_employees` WHERE ( employee_type=2 or employee_type = 3 ) and `is_active`=1 AND employee_email='".$employee_number."'")->row_array();
		if (count($data)) {
			$hashed_password = $data['employee_password'];
			unset($data['employee_password']);
			if (!hash_equals($hashed_password, crypt($pass,APP_SALT))) {
			   return 400;
			}else{
				$bilingstart = getAnythingData("med_companies","company_id",$data['company_id'],'billing_start_date');
				if($bilingstart==""){
					$this->db->query("UPDATE med_employees SET first_login = 0 WHERE employeeid='".$data['employee_id']."'");
					$this->db->query("UPDATE med_companies SET billing_start_date='".today()[0]."' WHERE company_id='".$data['company_id']."'");
				}
				$bilingstart = getAnythingData("med_companies","company_id",$data['company_id'],'billing_start_date');
				$billing_plan = getAnythingData("med_companies","company_id",$data['company_id'],'billing_plan');
				$data['employee_picture'] != '' ? $data['employee_picture'] = base_url().$data['employee_picture'] : '';
				$data['trial'] = date("M d,, Y H:i:s",strtotime("+ 30 days",strtotime($bilingstart)));
				$data['billing_plan'] = $billing_plan;
				return $data;
			}
		} else {
			return 401;
		}
	}
	
	public function password_recover($email) {
		
		$data  = $this->db->select('*')->from('med_employees')->where(array('employee_email'=>$email,'employee_type'=>'2'))->get()->row();
		if($data == ""){
			return 400;
        } else { 
			$random		=rand(12345,99999);
			$password 	= crypt($random,APP_SALT);
			$this->db->trans_start();
			$this->db->where(array('employee_email'=>$email))->update('med_employees',array('employee_password'=>$password,'updated_at'=>today()[0]));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return 500;
			} else {
				$this->db->trans_commit();
				$this->load->library('email');
				$config['protocol']    = 'smtp';
				$config['smtp_host']    = 'smtp.1and1.com';
				$config['smtp_port']    = '587';
				$config['smtp_timeout'] = '7';
				$config['smtp_user']    = 'noreply@dailycarerecords.com';
				$config['smtp_pass']    = 'Dailycarerecords!@#123';
				$config['charset']    = 'utf-8';
				$config['newline']    = "\r\n";
				$this->email->initialize($config);
				$this->email->from('noreply@dailycarerecords.com', 'DailyCareRecords Support Team');
				$this->email->to($email);
				$this->email->subject('Daily care records temporary passowrd');
				$this->email->set_mailtype("html");
				$email_message = "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
				$email_message .= "<div>Temporary login password: ".$random."</div>" ;
				$email_message .= "<div>Please use this passowrd for login your account. After successful login we recommend you to change your password.</div>" ;
				$email_message .= "<div>Thanks</div>" ;
				$this->email->message($email_message);
				$this->email->send();
				return 200;
			}
		}
	}
	
	
	public function masquerade($company_id) {
		$this->db->select('company_id');
		$this->db->from('companies');
		$this->db->where('company_id', $company_id);
		$data = $this->db->get();

		if ($data->num_rows() == 1) {
			$data = array('user_id'=>'0','company_id'=>$company_id,'user_name'=>'Super Admin','email'=>'super@ceeca.com','profile_picture'=>'');
			return $data;
		} else {
			return false;
		}
	}
	
}
