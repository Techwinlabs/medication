<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_schedules extends CI_Model {	
	
	public function new_schedule($input){
		$checkcompanyid = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		if ($checkcompanyid == $input['company_id']){
			## check already exists
			$check = $this->db->query("select schedule_id from med_schedule where ((start_datetime BETWEEN '".$input['start_datetime']."' AND '".$input['end_datetime']."') OR (end_datetime BETWEEN '".$input['start_datetime']."' AND '".$input['end_datetime']."')) and employeeid = '".$input['employeeid']."'")->row_array();
			if(count($check)==0){			
				$input['created_at']	= today()[0];
				$input['updated_at']	= today()[0];
				$this->db->trans_start();
				$this->db->insert('med_schedule',$input);
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'msg' => 'Internal server error. Please try again!');
				}else{
					$client_id = $this->db->insert_id();
					$this->db->trans_commit();
					return array('status' => 200,'msg' =>'Scheduled successfully.');
				}
			}else{
				return array('status' => 409,'msg' =>'Time slot not available.');
			}
		}else{
			return array('status' => 400,'msg' =>'Something went worng. Please try again.');
		}
	}
	
	public function update_schedule($input){
		$checkcompanyid = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		if ($checkcompanyid == $input['company_id']){
			
			$check = $this->db->query("select client_id,schedule_id from med_schedule where ((start_datetime BETWEEN '".$input['start_datetime']."' AND '".$input['end_datetime']."') OR (end_datetime BETWEEN '".$input['start_datetime']."' AND '".$input['end_datetime']."')) and employeeid = '".$input['employeeid']."' AND client_id!='".$input['client_id']."'")->row_array();
			
			if(count($check)==0){		
				$input['created_at']	= today()[0];
				$input['updated_at']	= today()[0];
				$this->db->trans_start();
				$this->db->where('schedule_id',$input['schedule_id']);
				$this->db->update('med_schedule',$input);
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'msg' => 'Internal server error. Please try again!');
				}else{
					$client_id = $this->db->insert_id();
					$this->db->trans_commit();
					return array('status' => 200,'msg' =>'Schedule updated successfully.');
				}
			}else{
				return array('status' => 409,'msg' =>'Time slot not available.');
			}
		}else{
			return array('status' => 400,'msg' =>'Something went worng. Please try again.');
		}
	}
	
	public function get_schedule($id){
		$data = $this->db->select('*,date(start_datetime) as start_date,DATE_FORMAT(start_datetime,"%H:%i:%s") as start_time,date(end_datetime) as end_date,DATE_FORMAT(end_datetime,"%H:%i:%s") as end_time')->from('med_schedule')->where(array('schedule_id'=>$id))->get()->row_array();
		return $data;
	}
}