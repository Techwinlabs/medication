<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_data_usage extends CI_Model {



	public function loadCarer(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
        $data = $this->db->query("SELECT `employeeid`, `employee_name`,`employee_number` FROM `med_employees` WHERE `company_id`='".$company_id."' AND `is_active`=1 AND `employee_type`=1")->result_array();
        
        if(count($data)){
            $carer = ""; 
            $carer .='<select class="form-control selectpicker" data-live-search="true" id="load_carer" >';
            $carer .='<option value="">-Select carer-</option>';
            for($i=0;$i<count($data);$i++){
               $carer .="<option data-tokens='".$data[$i]['employee_name']."' value='".$data[$i]['employeeid']."'>".$data[$i]['employee_name']."(".$data[$i]['employee_number'].")</option>";
            }
            $carer .= "</select>"; 
           echo $carer;
        }else{
            return "<option value=''>-No carer found-</option>";
        }
	}
    
    
    public function getDataUsage($input){
        $timezone = $this->session->userdata('timezone');
        $company_id	= $this->session->userdata()['userdata']['company_id'];
		if($input['carer_id']=='all'){
			$condition = " AND `employee_type`='1'";
		}else{
			$condition =   " AND `employeeid` IN(".$input['carer_id'].")";
		}
		
		$data = $this->db->query("SELECT `employeeid`,employee_number,employee_name,IF(employee_picture='','',CONCAT('".base_url()."','',employee_picture)) as employee_picture,IFNULL((SELECT SUM(`data`) FROM `med_data_usage` WHERE `employeeid`=a.employeeid AND date(`date`) BETWEEN '".$input['start']."' AND '".$input['end']."' ),0) as data FROM `med_employees` a WHERE `company_id`='".$company_id."' $condition ORDER BY data DESC ")->result_array();
		for($i=0;$i<count($data);$i++){
			$usages = $this->db->query("SELECT DISTINCT(DATE(`date`)) AS `date`  FROM `med_data_usage` WHERE `employeeid`='".$data[$i]['employeeid']."' AND date(`date`) BETWEEN '".$input['start']."' AND '".$input['end']."' ")->result_array();
			for($x=0;$x<count($usages);$x++){
                $usages[$x]['data'] = $this->db->query("SELECT SUM(`data`) AS `data` FROM `med_data_usage` WHERE `employeeid`='".$data[$i]['employeeid']."' AND DATE(`date`)='".$usages[$x]['date']."'")->row_array()['data'];
                $usages[$x]['date'] = convertUtcToLocal("d/m/Y",$usages[$x]['date'],$timezone);
               
                if($usages[$x]['data']>1024){
                    $usages[$x]['data']     =   round($usages[$x]['data']/1024,2)." MB";
                }else{
                    $usages[$x]['data'] =   $usages[$x]['data']." KB";
                }
                
			}
			$data[$i]['data_usages'] = $usages;
		}

        $data[0]['totalUsage'] = $this->db->query("SELECT IFNULL(SUM(`data`),0) as `data` FROM `med_data_usage` WHERE `company_id`='".$company_id."' AND date(`date`) BETWEEN '".$input['start']."' AND '".$input['end']."' ")->row_array()['data'];
        if($data[0]['data']>1024){
            $data[0]['data']     =   round($data[0]['data']/1024,2)." MB";
        }else{
            $data[0]['data'] =  $data[0]['data']." KB";
        }
       

		return $data;
	}

}