<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_notifications extends CI_Model {
	
    public function getfilteredNotifications($input){
        $timezone = $this->session->userdata('timezone');
        $company_id	= $this->session->userdata()['userdata']['company_id'];
        $employeeid	= $this->db->query("SELECT GROUP_CONCAT(`employeeid`) AS `employeeid` FROM `med_employees` WHERE `company_id`='".$company_id."' and (employee_type='2'  OR employee_type='3')")->row_array()['employeeid'];

	    $filter = ["(filter=1 or filter=2 )","filter=1","filter=2","filter=3","filter=4","filter=5"];
        $data = $this->db->query("SELECT `notifcation_id`, `notification_type`, `authority_id`, `carer_id`, `table_id`, `status`, `forworded_by`, `created_at` FROM `med_notifications` where authority_id IN(".$employeeid.") AND ".$filter[$input['filter']]." ORDER BY notifcation_id desc")->result_array();
        //print "<pre>"; print_r(count($data));exit;
        for($i=0;$i<count($data);$i++){
            if ($data[$i]['notification_type']==1){
                $data[$i]['notification']       =[];
                $data[$i]['notification']['for']="Record not enter";
            }elseif($data[$i]['notification_type']==2){
                $data[$i]['notification']=[];
                $data[$i]['notification']['for']="Medication not given";
            }elseif($data[$i]['notification_type']==3){
                $concern_id = $data[$i]['table_id'];
                $data[$i]['notification'] = $this->db->query("SELECT med_concerns.`concern_id`, med_concerns.`employeeid`,med_concerns.`subject`, med_concerns.`concern`,med_concerns.`closed_by`,med_concerns.`open_at`,med_concerns.`closed_at`, med_concerns.`status`,med_employees.employee_name,med_employees.employee_number,IFNULL((med_concern_replies.reply),'-NA-') as reply FROM  `med_concerns` JOIN med_employees ON med_employees.employeeid=med_concerns.employeeid LEFT JOIN med_concern_replies ON med_concern_replies.concern_id=med_concerns.concern_id  WHERE med_concerns.`concern_id`='".@$concern_id."'")->row_array();
                $data[$i]['notification']['closed_by_name'] = getAnythingData("med_employees","employeeid",$data[$i]['notification']['closed_by'],'employee_name');
				$data[$i]['notification']['closed_by_employeeNumber'] = getAnythingData("med_employees","employeeid",$data[$i]['notification']['closed_by'],'employee_number');
				
                $data[$i]['notification']['timezone'] = $timezone;
                $data[$i]['notification']['for']="New Concern";                 
            }
        }
        return $data;
	}

}