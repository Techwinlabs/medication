<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_ajax extends CI_Model {

	public function select_allEmployees(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$employeeid = $this->session->userdata()['userdata']['employeeid'];

		$search = "";
		if ($input['search']){
			$search = " AND (employee_number LIKE '%".$input['search']."%' OR employee_number LIKE '%".$input['search']."%' OR employee_email LIKE '%".$input['search']."%')";
		}
		$totalrows 	= $this->db->query("SELECT client_id FROM med_clients WHERE company_id=".$company_id."  $search ")->num_rows();		

		$data['carer'] = $this->db->select('employeeid, company_id, employee_type, employee_number, employee_name, employee_email, employee_country_code, employee_mobile, employee_picture, employee_designation, created_at,is_active')->from('med_employees')->where(array('company_id'=>$company_id,'employeeid!='=>$employeeid,'employee_type'=>1))->order_by('employeeid','DESC')->get()->result_array();

		
		$json_data = array(
			"draw"=> intval( $input['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval(count($final) ),  // total number of records
			"recordsFiltered" => intval($totalrows), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $final   // total data array
		);
		return ($json_data);	
	}	
	
	public function get_clients($input){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$search = "";
		if (!empty(trim($input['search']))){
			$search = " AND (firstname LIKE '%".$input['search']."%' OR lastname LIKE '%".$input['search']."%' OR postcode LIKE '%".$input['search']."%')";
		}
		$totalrows 	= $this->db->query("SELECT client_id FROM med_clients WHERE company_id=".$company_id."  $search ")->num_rows();
		
		$data  = $this->db->query("SELECT * FROM med_clients WHERE company_id=".$company_id." $search ORDER BY client_id DESC limit ".$input['start'].",".$input['length']."")->result_array();
		$final = [];
		$i=0;
		foreach($data as $row){
			if(!empty($row['client_picture'])){
				$profilepic = '<img src="'.base_url().$row['client_picture'].'" height="50px" width="50px" onerror="this.src=\''.base_url().'asset/img/dummy.svg\'" >';
			}else{
				$profilepic = '<img src="'.base_url().'asset/img/dummy.svg" height="50px" width="50px">';
			}

			$section_roles = explode(',',ltrim(rtrim($this->userdata['section_roles'],','),','));
			//if(!in_array(1,$section_roles) && $this->userdata['employee_type']==3){
			//	$programofcare = '<a title="Program of Care" ><img src="'.base_url().'asset/img/calendar-icon.png" style="max-width:35px;" onclick="" /></a>';
			//}else{
				$programofcare = '<a title="Program of Care"><img src="'.base_url().'asset/img/calendar-icon.png" style="max-width:35px;" onclick="care_program('.$row['client_id'].')" /></a>';
			//}
			
			

			if(in_array(4,$section_roles) || $this->userdata['employee_type']==2){
				$editProfileTitle="Edit";
			}else{
				$editProfileTitle="Access Denied!";
			}


			$final[] = array(
							"DT_RowId" => "client_".$i,
							'<div class="col-md-2">'.$profilepic.'</div>
							<div class="col-md-10 client-name">
								<b>'.$row['firstname'].' '.$row['lastname']. '</b> ('.$row['dob'].')
							</div>',
							
							'<a href="" data-toggle="modal" data-target="#profile'.$i.'" onclick="viewProfile_client('.$i.','.$row['client_id'].')" title="Profile"><img src="'.base_url().'asset/img/dummy.svg" style="max-width:35px;" /></a>',

							'<a href="'.base_url().'admin/daily_records/'.$row['client_id'].'" title="Daily Records"><i class="icons icon-notebook" style="font-size:35px"></i></a>',

							'<a href="'.base_url().'admin/mar/'.$row['client_id'].'" title="Medication Record"><img src="'.base_url().'asset/img/medicine-icon.png" style="max-width:35px;" /></a>',

							$programofcare,

							'<a href="'.base_url().'admin/update_profile/'.$row['client_id'].'" title="'.$editProfileTitle.'"><i class="fa fa-pencil" style="font-size:35px"></i></a>',
							
							'<p style="margin: 10px 0;"><a href="javascript:void(0);" onclick="" class="confirm" title="Archive" data-original-title="<b>Are you sure to archive!</b>"><i class="fa fa-archive"  title="Archive"></i></a><br/><a href="" onclick="delete_client('.$i.','.$row['client_id'].')" class="confirm" title="<b>Are you sure to delete!</b>"><i class="fa fa-trash-o"style="font-size:15px"  title="Delete"></i></a></p>'
					);
			
			$i++;
		}

		$json_data = array(
						"draw"=> intval( $input['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
						"recordsTotal"    => intval(count($final) ),  // total number of records
						"recordsFiltered" => intval($totalrows), // total number of records after searching, if there is no searching then totalFiltered = totalData
						"data"            => $final   // total data array
					);
		return ($json_data);
	}


	public function get_schedules($input){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$employeeid = $this->session->userdata()['userdata']['employeeid'];
		$totalrows 	= $this->db->query("SELECT employeeid  FROM med_schedule WHERE med_schedule.company_id='".$company_id."'")->num_rows();
		$data = $this->db->query("SELECT med_schedule.*,CONCAT(med_clients.firstname,' ',med_clients.lastname) as client_name,med_clients.client_picture,med_employees.employee_name,med_employees.employee_number,med_employees.employee_picture FROM med_schedule JOIN med_clients ON med_clients.client_id=med_schedule.client_id JOIN med_employees ON med_employees.employeeid=med_schedule.employeeid WHERE med_schedule.company_id='".$company_id."' AND med_schedule.status=0 ORDER BY med_schedule.start_datetime ASC limit ".$input['start'].",".$input['length']."")->result_array();
		$final = [];
	    for($i=0;$i<count($data);$i++){
			$data[$i]['client_picture'] ? $data[$i]['client_picture'] = base_url().$data[$i]['client_picture'] : base_url().'asset/img/dummy.svg';
			$data[$i]['employee_picture'] ? $data[$i]['employee_picture'] = base_url().$data[$i]['employee_picture'] : base_url().'asset/img/dummy.svg';

			$final[] = array(
							'DT_RowId' => 'row_'.$i,
							'<img src="'.$data[$i]['employee_picture'].'" height="50px" width="50px" onerror="this.src=\''. base_url().'asset/img/dummy.svg\'" >&emsp;<b>'.$data[$i]['employee_name'].'</b> ('. $data[$i]['employee_number'].')',
							'<img src="'.$data[$i]['client_picture'].'" height="50px" width="50px" onerror="this.src=\''.base_url().'asset/img/dummy.svg\'" >&emsp;'.$data[$i]['client_name'],
							date("d/m/Y h:i A",strtotime($data[$i]['start_datetime'])).' - '.date("d/m/Y h:i A",strtotime($data[$i]['end_datetime'])),
							'<p><a href="'.base_url('admin/update_schedule/'.$data[$i]['schedule_id']).'"><i class="fa fa-edit"></i></a>&emsp;<a onclick="delete_schedule('.$i.','.$data[$i]['schedule_id'].')" class="confirm" title="<b>Are you sure to delete!</b>"><i class="fa fa-trash-o"></i></a>'
						);
		}
		
		$json_data = array(
			"draw"=> intval( $input['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval(count($final) ),  // total number of records
			"recordsFiltered" => intval($totalrows), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $final   // total data array
		);
		//print"<pre>";	print_r($data); exit;
		return $json_data;
	}

	public function daily_records($input)
	{
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$timezone = $this->session->userdata('timezone');
		$daterange = "";
		$totalrows 	= $this->db->query("SELECT DISTINCT(DATE(med_daily_records.`date`)) AS `date` from med_daily_records WHERE client_id=".$input['client_id']." $daterange ")->num_rows();
		$data  = $this->db->query("SELECT DISTINCT(DATE(med_daily_records.`date`)) AS `date`,TIME(med_daily_records.`date`) AS `time`,med_daily_records.client_id,med_daily_records.employeeid,med_daily_records.employee_number,med_daily_records.employee_name, med_daily_records.date as `fulldate` from  med_daily_records where med_daily_records.client_id = ".$input['client_id']." $daterange order by med_daily_records.date DESC limit ".$input['start'].",".$input['length']."")->result_array();
		
		$final = [];
		
	    for($i=0;$i<count($data);$i++){
			$answers = $this->db->query("SELECT med_daily_records.record_id,med_daily_records.answer,med_daily_records_questions.question from  med_daily_records JOIN med_daily_records_questions ON med_daily_records_questions.question_id=med_daily_records.question_id  where med_daily_records.client_id = ".$input['client_id']." AND `date`='".$data[$i]['fulldate']."'")->result_array();
			$ansString = ""; 
			//$ansString .= "<ul>"; 
			for($a=0;$a<count($answers);$a++){
				$ansString .= rtrim(ucfirst($answers[$a]['answer']),'.').". ";
			}
			$final[] = array(
							convertUtcToLocal("d/m/Y",$data[$i]['date']." ".$data[$i]['time'],$timezone),
							convertUtcToLocal("h:i A",$data[$i]['date']." ".$data[$i]['time'],$timezone),
							$data[$i]['employee_name'],
							'<a href="" data-toggle="modal" data-target="#profile'.$i.'" onclick="viewProfile_emp('.$i.','.$data[$i]['employeeid'].')">'.$data[$i]['employee_number'].'</a>',
							$ansString 
						);
		}
		$json_data = array(
			"draw"=> intval( $input['draw'] ),
			"recordsTotal"    => intval(count($final) ), 
			"recordsFiltered" => intval($totalrows),
			"data"            => $final
		);
		//print"<pre>";	print_r($data); exit;
		return $json_data;
	}

	public function care_program($input)
	{		
		$data  = $this->db->select("program_id, client_id,(select comments from med_clients where client_id = a.client_id ) as comments, am_time_care_program, noon_time_care_program, tea_time_care_program, night_time_care_program, program_startdate")->from('med_care_program as a')->where(array("client_id"=>$input['id']))->get()->row_array();
		if(count($data)){
			$data['am_time_care_program'] = unserialize($data['am_time_care_program']);
			$data['noon_time_care_program']=	unserialize($data['noon_time_care_program']);
			$data['tea_time_care_program'] = unserialize($data['tea_time_care_program']);
			$data['night_time_care_program'] = unserialize($data['night_time_care_program']);
			
			$key = array("am_time_care_program","noon_time_care_program","tea_time_care_program","night_time_care_program");
			for($i=0;$i<4;$i++){
				$program = $data[$key[$i]];
				//print_r($program); exit;
				if(count($program)){
					for($x=0;$x<count($program);$x++){
						$medicine = $program[$x]['medicine'];
						$check  = $this->db->select('medication_chart_id')->from('med_medication_chart')->where(array('client_id'=>$input['id'],'care_time'=>($i+1),'medicine'=>$medicine))->group_start()->like('medication_datetime',date("Y-m-d"))->group_end()->get()->row();
						if($check){
							$data[$key[$i]][$x]['status'] = '1';
						}else{
							$data[$key[$i]][$x]['status'] = '0';
						}
					}
				}
			}
			$data['POC']= true;
		}else{
			$data['client_id']= $input['id'];
			$data['POC']= false;
		}		
		return $data;
	}
	
	/*
	*  View Daily Records Manager Section
	*/
	
	public function filterDailyRecords($input)
	{
		$timezone = $this->session->userdata('timezone');
		if($input['start']!='' && $input['end']!=''){
			$daterange 	=" AND DATE(date)>='".$input['start']."' AND DATE(date)<= '".$input['end']."'";
		}else{
			$daterange = "";
		}
		$data  = $this->db->query("SELECT DISTINCT(DATE(med_daily_records.`date`)) AS `date`,TIME(med_daily_records.`date`) AS `time`,med_daily_records.client_id,med_daily_records.postcode,med_daily_records.employeeid,med_daily_records.employee_number,med_daily_records.date as `fulldate` from  med_daily_records where med_daily_records.client_id = ".$input['client_id']." $daterange order by med_daily_records.date DESC")->result_array();
		
		for($i=0;$i<count($data);$i++){
			$answers = $this->db->query("SELECT med_daily_records.record_id,med_daily_records.answer,med_daily_records_questions.question from  med_daily_records JOIN med_daily_records_questions ON med_daily_records_questions.question_id=med_daily_records.question_id  where med_daily_records.client_id = ".$input['client_id']." AND `date`='".$data[$i]['fulldate']."'")->result_array();
			$ansString = ""; 
			for($a=0;$a<count($answers);$a++){
				$ansString .= rtrim(ucfirst($answers[$a]['answer']),'.').". ";
			}
			$date = convertUtcToLocal("d/m/Y",$data[$i]['date']." ".$data[$i]['time'],$timezone);
			$time = convertUtcToLocal("h:i A",$data[$i]['date']." ".$data[$i]['time'],$timezone);
			$data[$i]['date'] = $date;
			$data[$i]['time'] = $time;
			$data[$i]['answers'] = $ansString;
		}
		return $data;
	}

	public function getMARReports($input)
	{
		$timezone = $this->session->userdata('timezone');

		if($input['search']!=""){
			$search = " AND medication_datetime LIKE '%".$input['search']."%' OR medicine LIKE '%".$input['search']."%' OR status LIKE '%".$input['search']."%' OR message LIKE '%".$input['search']."%'";
		}else{
			$search = "";
		}

		$totalrows 	= $this->db->query("SELECT medication_chart_id from  med_medication_chart WHERE med_medication_chart.client_id='".$input['client_id']."'".$search)->num_rows();

		$data  = $this->db->query("SELECT med_medication_chart.* from med_medication_chart where med_medication_chart.client_id='".$input['client_id']."' ".$search." order by med_medication_chart.medication_datetime DESC limit ".$input['start'].",".$input['length']."")->result_array();
		$final = [];
		$status = ['-Na-','Administer','Prepared','Prompt','Other'];
	    for($i=0;$i<count($data);$i++){
			$final[] = array(
							convertUtcToLocal("d/m/Y",$data[$i]['medication_datetime'],$timezone),
							convertUtcToLocal("h:i A",$data[$i]['medication_datetime'],$timezone),
							$data[$i]['medicine'],
							$data[$i]['dose'],
							$status[$data[$i]['status']],
							'<a href="" data-toggle="modal" data-target="#profile'.$i.'" onclick="viewProfile_emp('.$i.','.$data[$i]['employeeid'].')">'.$data[$i]['employee_number'].'</a>',
							$data[$i]['message']
						);
		}
		
		$json_data = array(
			"draw"=> intval( $input['draw'] ),
			"recordsTotal"    => intval(count($final)), 
			"recordsFiltered" => intval($totalrows),
			"data"            => $final
		);
		//print"<pre>";	print_r($data); exit;
		return $json_data;
		
	}

	public function searchMAR($input)
	{
		$timezone = $this->session->userdata('timezone');
		if($input['start']=='' && $input['end']==''){
			$daterange = "";
		}else{
			$daterange = " AND med_medication_chart.medication_datetime>= '".$input['start']." 00:00:00' AND med_medication_chart.medication_datetime<= '".$input['end']." 23:59:59'";
		}
		$data  = $this->db->query("SELECT med_medication_chart.* from med_medication_chart where med_medication_chart.client_id='".$input['client_id']."' $daterange order by med_medication_chart.medication_datetime DESC")->result_array();
		$status = ['-Na-','Administer','Prepared','Prompt','Damage/Lost'];
		for($i=0;$i<count($data);$i++){
			$data[$i]['date'] = convertUtcToLocal("d/m/Y",$data[$i]['medication_datetime'],$timezone);
			$data[$i]['time'] = convertUtcToLocal("h:i A",$data[$i]['medication_datetime'],$timezone);
			$data[$i]['status'] = $status[$data[$i]['status']];
		}
		//print $this->db->last_query(); EXIT;
		return $data;
	}
	
	public function getScheduleByDate($input)
	{
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$data = $this->db->query("SELECT med_schedule.*,CONCAT(med_clients.firstname,' ',med_clients.lastname) as client_name,med_clients.client_picture,med_employees.employee_name,med_employees.employee_number,med_employees.employee_picture FROM med_schedule JOIN med_clients ON med_clients.client_id=med_schedule.client_id JOIN med_employees ON med_employees.employeeid=med_schedule.employeeid AND DATE(med_schedule.start_datetime) >='".$input['start']."'  WHERE med_schedule.company_id='".$company_id."' ORDER BY schedule_id DESC")->result_array();
        for($i=0;$i<count($data);$i++){
			$data[$i]['client_picture'] ? $data[$i]['client_picture'] = base_url().$data[$i]['client_picture'] : '';
			$data[$i]['employee_picture'] ? $data[$i]['employee_picture'] = base_url().$data[$i]['employee_picture'] : '';
		}
		//print $this->db->last_query(); EXIT;
		return $data;
	}
	
	## View Carer Profile
	
	public function viewProfile_emp($employeeid)
	{
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$data = $this->db->select('employeeid, company_id, employee_type, employee_number, employee_name, employee_email, employee_country_code, employee_mobile, employee_picture, employee_designation')->from('med_employees')->where(array('employeeid'=>$employeeid,'company_id'=>$company_id))->get()->row_array();
		$data['valid'] = "0";
		if(count($data)){
			$data['employee_picture'] != '' ? $data['employee_picture'] = base_url().$data['employee_picture'] :  $data['employee_picture'] = base_url()."asset/img/dummy.svg";
			$data['designation'] = getAnythingData("med_designations","designation_id",$data['employee_designation'],'designation');
			$data['valid'] = "1";
		}
		return $data;
	}

	## View Profile of Client
	
	public function viewProfile_client($client_id)
	{
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$data = $this->db->query("SELECT * FROM `med_clients` WHERE `client_id`='".$client_id."' AND `company_id`='".$company_id."'")->row_array();
		$data['valid'] = "0";
		if(count($data)){
			$data['dob'] = date("d/m/Y",strtotime($data['dob']));
			$data['client_picture'] != '' ? $data['client_picture'] = base_url().$data['client_picture'] :  $data['client_picture'] = base_url()."asset/img/dummy.svg";			
			$data['valid'] = "1";
		}
		return $data;
	}

	## Send New Message
	
	public function sendNewMessage($input)
	{
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$input['sender_id'] = $this->session->userdata()['userdata']['employeeid'];
		$receiver_company_id	 = getAnythingData("med_employees","employeeid",$input['receiver_id'],'company_id');
		if($company_id = $receiver_company_id && $input['sender_id']!=$input['receiver_id']){
			$input['thread_id'] = getThreadId($input['sender_id'],$input['receiver_id']);
			$input['sent_date'] = today()[0];
			$input['company_id'] = $company_id;
			$this->db->trans_start();
			$this->db->insert('med_message',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return 0;
			} else {
				$sender_picture	 = getAnythingData("med_employees","employeeid",$input['sender_id'],'employee_picture');
				if($sender_picture){
					$sender_picture = base_url().$sender_picture;
				}else{
					$sender_picture = base_url()."asset/img/dummy.svg";
				}
				$this->db->trans_commit();
				
				#unread count of sender
				$unreadcount	=	$this->db->query("SELECT count(distinct(sender_id)) from `med_message` where receiver_id = '".$input['receiver_id']."' and is_read = 0")->num_rows();

				#Send Push Notification
				$keys = array("message_id"=>$this->db->insert_id(),'thread_id'=>$input['thread_id'],'main_id'=>$input['sender_id'],'unread_sender'=>$unreadcount);
				$message = "A new message received";
				$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$input['receiver_id']."'")->result_array();
				foreach($tokens as $token){
					if(strlen($token['device_token'])>30){
						if($token['device_type']==1){
							applePush($token['device_token'],$message,1,2,$keys);
						}else{
							andiPush($token['device_token'],$message,1,2,$keys);
						}
					}
				}
				return ['sender_picture'=>$sender_picture,'message_id'=>$this->db->insert_id()];
			}
		}else{
			return 0;
		}
	}
	
	public function blockFreeTrial()
	{
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$this->db->query("UPDATE `med_companies` SET `status`='0' WHERE `company_id`='".$company_id."'");
		return true;
	}
	
	// archive/unarchive user
	
	public function archive_unarchive($input)
	{
		if($input['type']==1){
			$this->db->query("UPDATE `med_employees` SET `is_active`='0' WHERE `employeeid`='".$input['id']."'");
		}else{
			$this->load->helper('common_helper');
			$company_id= $this->session->userdata()['userdata']['company_id'];
			if(activePlanLimit($company_id,1))
			{
				$this->db->query("UPDATE `med_employees` SET `is_active`='1' WHERE `employeeid`='".$input['id']."'");
			}else{
				return;
			}
		}
		return $input['type'];
	}
	
	
	// get library record
	
	public function get_records($input)
	{
		$year			=	$input['year'];
		$month			=	$input['month'];
		$client			=	$input['client'];
		$month_name		=	date('F', mktime(0, 0, 0, $month, 10));
		$client_name 	= 	getAnythingData("med_clients","client_id",$client,"firstname");
		
		$string	=	'<div id="content"><div class="col-md-12 top-20 padding-0"><div class="col-md-12"> <div class="panel"> <div class="panel-heading"> <h3 class="">'.$client_name.' ( '.$month_name.', '.$year.' )</h3></div><div class="panel-body"><div class="responsive-table"><table id="table-grid" class="table table-striped display" width="100%" cellspacing="0"><thead><tr><th width="30%">Client Name</th><th width="30%">Daily Care Record</th><th width="30%">Medication Record</th></tr></thead><tbody><tr><td>'.$client_name.'</td><td><a href="'.base_url("admin/ajax/get_daily_care_record?client=".$client."&year=".$year."&month=".$month."").'" style="cursor:pointer;"><img src='.base_url("images/pdf.png").' height="15" width="15" >&nbsp;Click here to download</a></td><td><a href="'.base_url("admin/ajax/get_medication_record?client=".$client."&year=".$year."&month=".$month."").'" style="cursor:pointer;"><img src='.base_url("images/pdf.png").' height="15" width="15" >&nbsp;Click here to download</a></td></tr></tbody></table></div></div></div></div></div></div>';
		
		return $string;
	}
	
	
	// get_daily_care_record
	
	public function get_daily_care_record($input)
	{		
		$year	=	$input['year'];		
		if(strlen($input['month'])>1){
			$month	=	$input['month'];
		}else{
			$month	=	'0'.$input['month'];
		}
		$month_name		=	date('F', mktime(0, 0, 0, $month, 10));
		$client	=	$input['client'];
		$client_name 	= 	getAnythingData("med_clients","client_id",$client,"firstname");
		
		$cond	=	$year.'-'.$month;
		$timezone = $this->session->userdata('timezone');
		
		$data  = $this->db->query("SELECT DISTINCT(DATE(med_daily_records.`date`)) AS `date`,TIME(med_daily_records.`date`) AS `time`,med_daily_records.client_id,med_daily_records.employeeid,med_employees.employee_number,med_employees.employee_name, med_daily_records.date as `fulldate` from  med_daily_records JOIN med_employees ON med_employees.employeeid=med_daily_records.employeeid  where med_daily_records.client_id = ".$client." and DATE_FORMAT(date, '%Y-%m') = '".$cond."' order by med_daily_records.date DESC")->result_array();
		
		$final = [];
		
	    for($i=0;$i<count($data);$i++){
			$answers = $this->db->query("SELECT med_daily_records.record_id,med_daily_records.answer,med_daily_records_questions.question from  med_daily_records JOIN med_daily_records_questions ON med_daily_records_questions.question_id=med_daily_records.question_id  where med_daily_records.client_id = ".$client." AND `date`='".$data[$i]['fulldate']."'")->result_array();
			$ansString = ""; 
			//$ansString .= "<ul>";
			for($a=0;$a<count($answers);$a++){
				$ansString .= rtrim(ucfirst($answers[$a]['answer']),'.').". ";
			}
			$final[] = array(
							'date' => convertUtcToLocal("d/m/Y",$data[$i]['date']." ".$data[$i]['time'],$timezone),
							'time' => convertUtcToLocal("h:iA",$data[$i]['date']." ".$data[$i]['time'],$timezone),
							'employee_name'=>$data[$i]['employee_name'],
							'employee_number'=>$data[$i]['employee_number'],
							'answer' => $ansString 
						);
		}
		
		// Load pdf library
        require_once(APPPATH.'libraries/vendor/autoload.php');
		$dompdf = new  Dompdf\Dompdf();
		$title = 'Daily Care Record   '.$client_name.'( '.$month_name.' , '.$year.' ) ';
		
		$pdf = '<div class="col-md-12"><center><h4>'.$title.'</h4></center></div>';
		$pdf .='<table width="100%">
					<thead style="font-size:15px;">
						<tr>
                            <th width="5%" class="color-black">#</th>
                            <th width="8%" class="color-black">Date</th>
                            <th width="8%" class="color-black">Time</th>
							<th width="15%" class="color-black">Staff Name</th>
                            <th width="10%" class="color-black">Staff ID</th>
                            <th width="54%" class="color-black">Daily Care Record</th>
                        </tr>
54                    </thead>
                    <tbody style="font-size:12px;font-family:Arial;">';
					
					for($i=0;$i<count($final);$i++){
						if ($i%2 == 0) $pdf .= '<tr style="background-color:#d6d6d6;margin-left:10px;">';
						else $pdf .= '<tr style="background-color:#f6f6f6;margin-left:10px;">';
						$pdf .='<td>'.($i+1).'</td>';
						$pdf .='<td>'.$final[$i]['date'].'</td>';
						$pdf .='<td>'.$final[$i]['time'].'</td>';
						$pdf .='<td>'.$final[$i]['employee_name'].'</td>';
						$pdf .='<td>'.$final[$i]['employee_number'].'</td>';
						$pdf .='<td>'.$final[$i]['answer'].'</td>';
						$pdf .='</tr>';
					}
		$pdf .= '</tbody>';
		$pdf .= '</table>';
		$dompdf->load_html($pdf, 'UTF-8');
		$dompdf->set_paper('a4', 'landscape');
		$dompdf->render();
		$dompdf->stream("sample.pdf", array("Attachment" => false));
	}	
	
	// get_medication_record
	
	public function get_medication_record($input)
	{		
		$year	=	$input['year'];		
		if(strlen($input['month'])>1){
			$month	=	$input['month'];
		}else{
			$month	=	'0'.$input['month'];
		}
		$month_name		=	date('F', mktime(0, 0, 0, $month, 10));
		$client	=	$input['client'];
		$client_name 	= 	getAnythingData("med_clients","client_id",$client,"firstname");
		
		$cond	=	$year.'-'.$month;
		$timezone = $this->session->userdata('timezone');
		
		$data  = $this->db->query("SELECT med_medication_chart.* from med_medication_chart where med_medication_chart.client_id='".$client."' and DATE_FORMAT(medication_datetime, '%Y-%m') = '".$cond."' order by med_medication_chart.medication_datetime DESC")->result_array();
				
		$final = [];
		$status = ['-Na-','Administer','Prepared','Prompt','Other'];
	    for($i=0;$i<count($data);$i++){
			$final[] = array(
							'date' => convertUtcToLocal("d/m/Y",$data[$i]['medication_datetime'],$timezone),
							'time' => convertUtcToLocal("h:iA",$data[$i]['medication_datetime'],$timezone),
							'medicine' => $data[$i]['medicine'],
							'status' => $status[$data[$i]['status']],
							'employee_name' => $data[$i]['employee_name'],
							'employee_number' => $data[$i]['employee_number'],
							'detail' => $data[$i]['detail']
						);
		}
		
		// Load pdf library
        require_once(APPPATH.'libraries/vendor/autoload.php');
		$dompdf = new  Dompdf\Dompdf();
		$title = 'Medication Record   '.$client_name.'( '.$month_name.' , '.$year.' ) ';
		
		$pdf = '<div class="col-md-12"><center><h4>'.$title.'</h4></center></div>';
		$pdf .='<table width="100%">
					<thead style="font-size:15px;">
						<tr>
                            <th width="5%" class="color-black">#</th>
                            <th width="8%" class="color-black">Date</th>
                            <th width="8%" class="color-black">Time</th>
							<th width="10%" class="color-black">Medicine</th>
							<th width="8%" class="color-black">Status</th>
							<th width="15%" class="color-black">Staff Name</th>
                            <th width="10%" class="color-black">Staff ID</th>
                            <th width="36%" class="color-black">Detail</th>
                        </tr>
                    </thead>
                    <tbody style="font-size:12px;">';
					
					for($i=0;$i<count($final);$i++){
						if ($i%2 == 0) $pdf .= '<tr style="background-color:#d6d6d6;margin-left:10px;text-align: justify;">';
						else $pdf .= '<tr style="background-color:#f6f6f6;margin-left:10px;text-align: justify;">';
						$pdf .='<td>'.($i+1).'</td>';
						$pdf .='<td>'.$final[$i]['date'].'</td>';
						$pdf .='<td>'.$final[$i]['time'].'</td>';
						$pdf .='<td>'.$final[$i]['medicine'].'</td>';
						$pdf .='<td>'.$final[$i]['status'].'</td>';
						$pdf .='<td>'.$final[$i]['employee_name'].'</td>';
						$pdf .='<td>'.$final[$i]['employee_number'].'</td>';
						if($data[$i]['status']==1){
							$pdf .='<td>-</td>';
						}else{
							$pdf .='<td>'.$final[$i]['detail'].'</td>';
						}
						$pdf .='</tr>';
					}
					
		$pdf .= '</tbody>';
		$pdf .= '</table>';
		$dompdf->load_html($pdf, 'UTF-8');
		$dompdf->set_paper('a4', 'landscape');
		$dompdf->render();
		$dompdf->stream("sample.pdf", array("Attachment" => false));
	}
	
	# delete employee
	
	public function delete_employee($input)
	{
		$emp_id	=	$input['emp_id'];
		
		# get employee
		$emp_type =	$input['emp_type'];
		
		if($emp_type == 1){		# carer
			$this->db->query("delete from med_activities_history where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_carer_notifications where carer_id = '".$emp_id."'");
			$this->db->query("delete from med_concerns where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_concern_replies where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_daily_records where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_data_usage where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_employees_authentication where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_medication_chart where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_message where ( sender_id = '".$emp_id."' or receiver_id = '".$emp_id."' )");
			$this->db->query("delete from med_notifications where carer_id = '".$emp_id."'");
			$this->db->query("delete from med_schedule where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_employees where employeeid = '".$emp_id."'");
		}elseif($emp_type == 2){	# manager
			$this->db->query("delete from med_activities_history where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_carer_notifications where authority_id = '".$emp_id."'");
			$this->db->query("delete from med_concerns where supervisor_id = '".$emp_id."'");
			$this->db->query("delete from med_concern_replies where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_daily_records where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_data_usage where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_employees_authentication where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_medication_chart where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_message where ( sender_id = '".$emp_id."' or receiver_id = '".$emp_id."' )");
			$this->db->query("delete from med_notifications where authority_id = '".$emp_id."'");
			$this->db->query("delete from med_employees where employeeid = '".$emp_id."'");
		}else{	# supervisor
			$this->db->query("delete from med_activities_history where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_carer_notifications where authority_id = '".$emp_id."'");
			$this->db->query("delete from med_concerns where supervisor_id = '".$emp_id."'");
			$this->db->query("delete from med_concern_replies where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_daily_records where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_data_usage where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_employees_authentication where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_medication_chart where employeeid = '".$emp_id."'");
			$this->db->query("delete from med_message where ( sender_id = '".$emp_id."' or receiver_id = '".$emp_id."' )");
			$this->db->query("delete from med_notifications where authority_id = '".$emp_id."'");
			$this->db->query("delete from med_employees where employeeid = '".$emp_id."'");
		}
		return 200;
	}
	
	# delete schedule
	
	public function delete_schedule($input)
	{
		$sch_id	=	$input['sch_id'];
		
		$this->db->query("delete from med_schedule where schedule_id = '".$sch_id."'");
		$this->db->query("delete from med_activities_history where section = 6 and main_id = '".$sch_id."'");
		$this->db->query("delete from med_carer_notifications where ( notification_type = 1 or notification_type = 2 ) and table_id = '".$sch_id."'");
		$this->db->query("delete from med_notifications where notification_type = 1 and filter = 5 and table_id = '".$sch_id."'");
		
		return 200;
	}
	
	# get_schedule_info
	
	public function get_schedule_info($input)
	{
		$sch_id	=	$input['schedule_id'];
		
		$get_sch	=	$this->db->query("SELECT med_schedule.*,CONCAT(med_clients.firstname,' ',med_clients.lastname) as client_name,med_clients.client_picture,med_employees.employee_name,med_employees.employee_number,med_employees.employee_picture FROM med_schedule JOIN med_clients ON med_clients.client_id=med_schedule.client_id JOIN med_employees ON med_employees.employeeid=med_schedule.employeeid WHERE med_schedule.schedule_id='".$sch_id."'")->row_array();
		$msg ='';
		if(count($get_sch)){
			
			if(!empty($get_sch['client_picture'])){
				$profilepic = '<img src="'.base_url().$get_sch['client_picture'].'" width="140" height="140" border="0" class="img-circle" onerror="this.src=\''.base_url().'asset/img/dummy.svg\'" >';
			}else{
				$profilepic = '<img src="'.base_url().'asset/img/dummy.svg" width="140" height="140" border="0" class="img-circle">';
			}
			
			$msg = '<div class="modal fade" id="schedule'.$get_sch['schedule_id'].'" tabindex="-1" role="dialog" aria-hidden="true"> <div class="modal-dialog"> <div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">View Schedule</h4>	</div> <div class="modal-body"> <center>'.$profilepic.'<h3 class="media-heading">'.$get_sch['client_name'].'</h3> </center> <hr> <center> <p class="text-left"><strong>Employee Name: </strong><br>'.$get_sch['employee_name'].'</p><p class="text-left"><strong>Start Date Time: </strong><br>'.date("d/m/Y H:i A",strtotime($get_sch['start_datetime'])).'</p><p class="text-left"><strong>End Date Time: </strong><br>'.date("d/m/Y H:i A",strtotime($get_sch['end_datetime'])).'</p><br></center></div><div class="modal-footer"><button type="button" class="btn btn-default" onclick="edit_schedule_view('.$get_sch['schedule_id'].','.$get_sch['company_id'].')">Edit Schedule</button><button type="button" class="btn btn-danger" onclick="delete_schedule('.$get_sch['schedule_id'].')">Delete Schedule</button></div></div></div></div>';
			return array('code'=>500,'msg'=>$msg);
		}else{
			return array('code'=>500,'msg'=>'Something went wrong');
		}
	}
	
	# get_schedule_info
	
	public function edit_schedule_view($input)
	{
		$schedule_id	=	$input['schedule_id'];
		$company_id		=	$input['company_id'];
		
		$this->load->model('common/Common');
        $data['clients'] = $this->Common->get_ClientList($company_id);
        $data['carer'] = $this->Common->get_CarerList($company_id);
		
		$data['schedule']	=	$this->db->query("SELECT med_schedule.*,CONCAT(med_clients.firstname,' ',med_clients.lastname) as client_name,med_clients.client_picture,med_employees.employee_name,med_employees.employee_number,med_employees.employee_picture FROM med_schedule JOIN med_clients ON med_clients.client_id=med_schedule.client_id JOIN med_employees ON med_employees.employeeid=med_schedule.employeeid WHERE med_schedule.schedule_id='".$schedule_id."'")->row_array();		
		$data['carerid'] = $input['carerid'];	
		return $data;
	}
	
	# delete client
	
	public function delete_client($input)
	{
		$client_id	=	$input['client_id'];
		
		$this->db->query("delete from med_care_program where client_id = '".$client_id."'");
		$this->db->query("delete from med_clients where client_id = '".$client_id."'");		
		#daily care record		
		$this->db->query("delete from med_daily_records where client_id = '".$client_id."'");
		$this->db->query("delete from med_activities_history where main_id = '".$client_id."' and section = 2");	
		$this->db->query("delete from med_medication_chart where client_id = '".$client_id."'");
		$this->db->query("delete from med_activities_history where main_id = '".$client_id."' and section = 1");
		$this->db->query("delete from med_carer_notifications where main_id = '".$client_id."' and notification_type = 1");
		$this->db->query("delete from med_carer_notifications where main_id = '".$client_id."' and notification_type = 2");
		$this->db->query("delete from med_carer_notifications where main_id = '".$client_id."' and notification_type = 3");
		$this->db->query("delete from med_carer_notifications where table_id = '".$client_id."' and notification_type = 4");
		$this->db->query("delete from med_schedule where client_id = '".$client_id."'");
		
		return 200;
	}	

	public function check_old_password($data){
		$eid 	= $data['eid'];
		$pass	= $data['oldPass'];	
		$res 	= $this->db->query("SELECT * FROM med_employees WHERE employee_password='".crypt($pass,APP_SALT)."' AND employeeid='".$eid."'")->row_array();  
		return count($res);
	}

	public function update_concern($data){
		if($data['concern_message']==""){
			echo "Please enter message.";			
		}else{
			$employeeid = $this->userdata['employeeid'];
			$this->db->query("UPDATE med_concerns SET `closed_by`='".$employeeid."',`closed_at`='".today()[0]."',status=1 WHERE concern_id='".$data['concernid']."'");
			
			$this->db->query("UPDATE med_notifications SET `status`='0',filter=2 WHERE table_id='".$data['concernid']."' AND carer_id='".$data['employee_id']."' AND notification_type='3'");

			$this->db->insert('med_concern_replies',array("concern_id"=>$data['concernid'],"employeeid"=>$employeeid,"reply"=>$data['concern_message'],"reply_at"=>today()[0]));

			$this->db->insert('med_carer_notifications',array("notification_type"=>5, "authority_id"=>$employeeid, "carer_id"=>$data['employee_id'],"table_id"=>$data['concernid'],"main_id"=>$data['concernid'],"created_at"=>today()[0]));

			$keys = array("main_id"=>$data['concernid']);
			$message = "New reply received on your concern";
			
			$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$data['employee_id']."'")->result_array();
			foreach($tokens as $token){
				if(strlen($token['device_token'])>30){
					if($token['device_type']==1){
						applePush($token['device_token'],$message,1,3,$keys);
					}else{
						andiPush($token['device_token'],$message,1,3,$keys);
					}
				}
			}
			echo 1;
		}

	}
}