<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_clients extends CI_Model {
	
	public function new_client($input){
		$am				= $input['am_time_care_program'];
		$am_type		= $input['am_time_care_program_type'];
		$am_time		= $input['am_time_care_program_time'];
		$am_dose		= $input['am_time_care_program_dose'];
		$am_detail		= $input['am_time_care_program_detail'];
		$noon			= $input['noon_time_care_program'];
		$noon_type		= $input['noon_time_care_program_type'];
		$noon_time		= $input['noon_time_care_program_time'];
		$noon_dose		= $input['noon_time_care_program_dose'];
		$noon_detail	= $input['noon_time_care_program_detail'];
		$evening		= $input['tea_time_care_program'];
		$evening_type	= $input['tea_time_care_program_type'];
		$evening_time	= $input['tea_time_care_program_time'];
		$evening_dose	= $input['tea_time_care_program_dose'];
		$evening_detail	= $input['tea_time_care_program_detail'];
		$night			= $input['night_time_care_program'];
		$night_type		= $input['night_time_care_program_type'];
		$night_time		= $input['night_time_care_program_time'];
		$night_dose		= $input['night_time_care_program_dose'];
		$night_detail	= $input['night_time_care_program_detail'];
		unset($input['am_time_care_program']);
		unset($input['am_time_care_program_time']);
		unset($input['am_time_care_program_type']);
		unset($input['am_time_care_program_dose']);
		unset($input['am_time_care_program_detail']);
		unset($input['noon_time_care_program']);
		unset($input['noon_time_care_program_type']);
		unset($input['noon_time_care_program_time']);
		unset($input['noon_time_care_program_dose']);
		unset($input['noon_time_care_program_detail']);
		unset($input['tea_time_care_program']);
		unset($input['tea_time_care_program_type']);
		unset($input['tea_time_care_program_time']);
		unset($input['tea_time_care_program_dose']);
		unset($input['tea_time_care_program_detail']);
		unset($input['night_time_care_program']);
		unset($input['night_time_care_program_type']);
		unset($input['night_time_care_program_time']);
		unset($input['night_time_care_program_dose']);
		unset($input['night_time_care_program_detail']);
		unset($input['submit']);
		$input['created_at']		= today()[0];
		$input['updated_at']		= today()[0];
		@$location = get_location(@$input['address']);
		$input['latitude']	= $location['lat'] ? $location['lat'] : 0;
		$input['longitude']	= $location['lng'] ? $location['lng'] : 0;
		$input['client_picture']	= '';
		$config['allowed_types'] 	= '*';
		$config['max_size'] 		= '1024';
		$date 						= date("Y-m");
		
		$check_email = $this->db->query("select client_id from med_clients where email = '".$input['email']."'")->row_array();
		if(count($check_email)==0){
			if (!empty($_FILES['client_picture']['name'])){
				$path	=  './media/client/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_CPP-".rand(1000,9999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('client_picture')){
					return array('status' =>400,'msg' =>"Picture uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					//$input['client_picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
					$this->load->library('image_lib');
					$path = ltrim($path,"./");

					$picture_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $path.$picture_data['file_name']; //get original image
					$config['maintain_ratio'] = TRUE;
					$config['create_thumb'] = false;
					$config['height'] = 160;
					$this->image_lib->initialize($config);
					if (!$this->image_lib->resize()) {
						$card = $path.$picture_data['file_name'];
						if (file_exists($card)) {
							unlink($card);
						}
						return array('status' =>400,'msg' =>"Picture uploading error: ".strip_tags($this->upload->display_errors()));
						exit;
					}else{
						$input['client_picture'] = ltrim($path,"./").$picture_data['file_name'];
					}
					$this->image_lib->clear();
				}
			}
			$this->db->trans_start();
			$this->db->insert('med_clients',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$client_id = $this->db->insert_id();
				$am_data		= [];
				$noon_data		= [];
				$evening_data	= [];
				$night_data		= [];
				for($a=0;$a<count($am);$a++){
					if($am[$a]){
						$am_data[] = ['medicine'=>$am[$a],'dose'=>$am_dose[$a],'time'=>$am_time[$a],'type'=>$am_type[$a],'detail'=>$am_detail[$a]];
					}
				}
				for($n=0;$n<count($noon);$n++){
					if($noon[$n]){
						$noon_data[] = ['medicine'=>$noon[$n],'dose'=>$noon_dose[$n],'time'=>$noon_time[$n],'type'=>$noon_type[$n],'detail'=>$noon_detail[$n]];
					}
	
				}
				for($e=0;$e<count($evening);$e++){
					if($evening[$e]){
						$evening_data[] = ['medicine'=>$evening[$e],'dose'=>$evening_dose[$e],'time'=>$evening_time[$e],'type'=>$evening_type[$e],'detail'=>$evening_detail[$e]];
					}
	
				}
				for($nt=0;$nt<count($night);$nt++){
					if($night[$nt]){
						$night_data[] = ['medicine'=>$night[$nt],'dose'=>$night_dose[$nt],'time'=>$night_time[$nt],'type'=>$night_type[$nt],'detail'=>$night_detail[$nt]];
					}
				}
				$this->db->insert('med_care_program',array('client_id'=>$client_id,'am_time_care_program'=>serialize($am_data),'noon_time_care_program'=>serialize($noon_data),'tea_time_care_program'=>serialize($evening_data),'night_time_care_program'=>serialize($night_data),'program_startdate'=>today()[0],'isactive'=>1));
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Client profile created');
			}
		}else{
			return array('status' => 409,'msg' =>'Account already exists');
		}
	}
	public function update_care_program($input){		
		$am				= @$input['am_time_care_program'];
		$am_type		= @$input['am_time_care_program_type'];
		$am_time		= @$input['am_time_care_program_time'];
		$am_dose		= @$input['am_time_care_program_dose'];
		$am_detail		= @$input['am_time_care_program_detail'];
		$noon			= @$input['noon_time_care_program'];
		$noon_type		= @$input['noon_time_care_program_type'];
		$noon_time		= @$input['noon_time_care_program_time'];
		$noon_dose		= @$input['noon_time_care_program_dose'];
		$noon_detail	= @$input['noon_time_care_program_detail'];
		$evening		= @$input['tea_time_care_program'];
		$evening_type	= @$input['tea_time_care_program_type'];
		$evening_time	= @$input['tea_time_care_program_time'];
		$evening_dose	= @$input['tea_time_care_program_dose'];
		$evening_detail	= @$input['tea_time_care_program_detail'];
		$night			= @$input['night_time_care_program'];
		$night_type		= @$input['night_time_care_program_type'];
		$night_time		= @$input['night_time_care_program_time'];
		$night_dose		= @$input['night_time_care_program_dose'];
		$night_detail	= @$input['night_time_care_program_detail'];
		unset($input['submit']);
		$this->db->trans_start();
		$am_data		= [];
		$noon_data		= [];
		$evening_data	= [];
		$night_data		= [];
		for($a=0;$a<count($am);$a++){
			if($am[$a]){
				$am_data[] = ['medicine'=>$am[$a],'dose'=>$am_dose[$a],'time'=>$am_time[$a],'type'=>$am_type[$a],'detail'=>$am_detail[$a]];
			}
		}
		for($n=0;$n<count($noon);$n++){
			if($noon[$n]){
				$noon_data[] = ['medicine'=>$noon[$n],'dose'=>$noon_dose[$n],'time'=>$noon_time[$n],'type'=>$noon_type[$n],'detail'=>$noon_detail[$n]];
			}

		}
		for($e=0;$e<count($evening);$e++){
			if($evening[$e]){
				$evening_data[] = ['medicine'=>$evening[$e],'dose'=>$evening_dose[$e],'time'=>$evening_time[$e],'type'=>$evening_type[$e],'detail'=>$evening_detail[$e]];
			}

		}
		for($nt=0;$nt<count($night);$nt++){
			if($night[$nt]){
				$night_data[] = ['medicine'=>$night[$nt],'dose'=>$night_dose[$nt],'time'=>$night_time[$nt],'type'=>$night_type[$nt],'detail'=>$night_detail[$nt]];
			}
		}
		$notiEmp_Id=$this->session->userdata()['userdata']['employeeid'];
		if($input['program_id']){
			$this->db->where(array('program_id'=>$input['program_id']))->update('med_care_program',array('am_time_care_program'=>serialize($am_data),'noon_time_care_program'=>serialize($noon_data),'tea_time_care_program'=>serialize($evening_data),'night_time_care_program'=>serialize($night_data),'program_updated'=>today()[0],'isactive'=>1,'noti_send'=>1,'employeeid'=>$notiEmp_Id));
			
		}else{
			$this->db->insert('med_care_program',array('client_id'=>$input['client_id'],'am_time_care_program'=>serialize($am_data),'noon_time_care_program'=>serialize($noon_data),'tea_time_care_program'=>serialize($evening_data),'night_time_care_program'=>serialize($night_data),'program_startdate'=>today()[0],'isactive'=>1,'employeeid'=>$notiEmp_Id));
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return array('status' => 500,'msg' => 'Internal server error. Please try again!');
		}else{
			$this->db->trans_commit();			
			//$this->db->where(array('client_id' => $input['client_id']))->update('med_clients',array('comments'=>$input['comments']));

			/* #Get all carer
			$company_id 		 = $this->session->userdata()['userdata']['company_id'];
			$input['employeeid'] = $this->session->userdata()['userdata']['employeeid'];
			$carer  = $this->db->query("SELECT employeeid FROM med_employees WHERE employee_type=1 AND company_id='".$company_id."'")->result_array();
				
			foreach($carer as $user){
				
				## Notification feed
				$this->db->insert('med_carer_notifications',array("notification_type"=>4, "authority_id"=>$input['employeeid'], "carer_id"=>$user['employeeid'],"table_id"=>$input['client_id'],"main_id"=>$input['program_id'],"created_at"=>today()[0]));
				
				#Send Push Notification
				$keys = array("client_id"=>$input['client_id']);
				$message = "Program of care updated";
				$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$user['employeeid']."'")->result_array();
				foreach($tokens as $token){
					if(strlen($token['device_token'])>30){
						if($token['device_type']==1){
							applePush($token['device_token'],$message,1,6,$keys);  #section 6 client program update
						}else{
							andiPush($token['device_token'],$message,1,6,$keys);
						}
					}
				}
			}*/
			return array('status' => 200,'msg' =>'Program of care updated successfully.');
		}
	}
	
	public function view_profile($client_id,$company_id){
		$data = $this->db->select('*')->from('med_clients')->where(array('client_id'=>$client_id,'company_id'=>$company_id))->get()->row_array();
		if (count($data)){
			return array('data'=>$data,'status' => 200,'msg' =>'');
		}else{
			return array('status' => 400,'msg' =>'Account not found!');
		}
	}
	
	public function update_profile($input){
		
		$input['client_picture']	= getAnythingData("med_clients","client_id",$input['client_id'],'client_picture');
		$location = get_location($input['address']);
		$input['latitude']	= $location['lat'] ? $location['lat'] : 0;
		$input['longitude']	= $location['lng'] ? $location['lng'] : 0;
		$config['allowed_types'] 	= '*';
		$config['max_size'] 		= '1024';
		$date 						= date("Y-m");
		if (!empty($_FILES['client_picture']['name'])){
			$path	=  './media/client/'.$date."/";
			if (!is_dir($path)){
				mkdir($path , 0777);
				$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
				$fp = fopen($path."index.html","wb"); 
				fwrite($fp,$content); 
				fclose($fp); 
			}
			$config['file_name']	= date("Ymd")."_CPP-".rand(1000,9999).'-'.time();
			$config['upload_path']	= $path;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('client_picture')){
				return array('status' =>400,'msg' =>"Picture uploading error: ".strip_tags($this->upload->display_errors()));
				exit;
			}else{
				$input['client_picture'] ? unlink('./'.$input['client_picture']) : "";
				$input['client_picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
			}
		}
		$this->db->trans_start();
		$this->db->where(array('client_id'=>$input['client_id'],'company_id'=>$input['company_id']))->update('med_clients',array('firstname'=>$input['firstname'],'lastname'=>$input['lastname'],'email'=>$input['email'],'phone_number'=>$input['phone_number'],'dob'=>$input['dob'],'gender'=>$input['gender'],'address'=>$input['address'],'postcode'=>$input['postcode'],'client_picture'=>$input['client_picture'],'emergency_contact'=>$input['emergency_contact'],'personal_information'=>$input['personal_information'],'comments'=>$input['comments'],'latitude'=>$input['latitude'],'longitude'=>$input['longitude'],'emergency_contact_name'=>@$input['emergency_contact_name'],'emergency_contact_relation'=>@$input['emergency_contact_relation'],'doctor_name'=>@$input['doctor_name'],'doctor_address'=>@$input['doctor_address'],'doctor_contact_number'=>@$input['doctor_contact_number'],'allergies'=>@$input['allergies'],'additional_information'=>@$input['additional_information'],'noti_send'=>1,'updated_at'=>today()[0]));
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return array('status' => 500,'msg' => 'Internal server error. Please try again!');
		}else{
			$this->db->trans_commit();
			#Get all carer
			/*$input['employeeid'] = $this->session->userdata()['userdata']['employeeid'];
			$carer  = $this->db->query("SELECT employeeid FROM med_employees WHERE employee_type=1 AND company_id='".$input['company_id']."'")->result_array();
			foreach($carer as $user){
				
				## Notification feed
				$this->db->insert('med_carer_notifications',array("notification_type"=>3, "authority_id"=>$input['employeeid'], "carer_id"=>$user['employeeid'],"table_id"=>$input['client_id'],"main_id"=>$input['client_id'],"created_at"=>today()[0]));
				
				#Send Push Notification
				$keys = array("client_id"=>$input['client_id']);
				$message = $input['firstname'].' '.$input['lastname']." updated his profile";
				$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$user['employeeid']."'")->result_array();
					foreach($tokens as $token){
						if(strlen($token['device_token'])>30){
						if($token['device_type']==1){
							applePush($token['device_token'],$message,1,4,$keys);
						}else{
							andiPush($token['device_token'],$message,1,4,$keys);
						}
					}
				}
			}*/
			return array('status' => 200,'msg' =>'User account is updated.');
		}
				
			
	}
	
	public function marReport($input){
		$data  = $this->db->select("med_medication_chart.*,med_clients.firstname,med_clients.lastname,med_clients.client_picture,med_employees.employee_name,med_employees.employee_number,med_employees.employee_picture")->from('med_medication_chart')->where(array("med_medication_chart.client_id"=>$input['client_id'], 'med_medication_chart.medication_datetime>='=>$input['start'],'med_medication_chart.medication_datetime<='=>$input['end']))->join("med_clients","med_clients.client_id=med_medication_chart.client_id and med_clients.company_id=".$input['company_id']."")->join("med_employees","med_employees.employeeid=med_medication_chart.employeeid")->order_by('med_medication_chart.medication_datetime','DESC')->get()->result_array();
		$status = array("","Admire","Refused","Prompt","Damaged/Lost");
		$status = ['-Na-','Administer','Refused','Prompt','Damage/Lost'];
		for($i=0;$i<count($data);$i++){
			$data[$i]['client_picture'] != '' ? $data[$i]['client_picture'] = base_url().$data[$i]['client_picture'] : '';
			$data[$i]['employee_picture'] != '' ? $data[$i]['employee_picture'] = base_url().$data[$i]['employee_picture'] : '';
			$data[$i]['status'] = $status[$data[$i]['status']];
		}
		return $data;
	}


	
	
}