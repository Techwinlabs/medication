<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_library extends CI_Model {
    
    public function get_all_clients(){
		$company_id = $this->session->userdata()['userdata']['company_id'];        
		return $this->db->select('client_id, firstname, lastname, postcode')->from('med_clients')->where(array('company_id'=>$company_id))->get()->result_array();
	}
    
}