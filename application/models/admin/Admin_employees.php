<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_employees extends CI_Model {
	
	public function select_allEmployees(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$employeeid = $this->session->userdata()['userdata']['employeeid'];

		$data['carer'] = $this->db->select('employeeid, company_id, employee_type, employee_number, employee_name, employee_email, employee_country_code, employee_mobile, employee_picture, employee_designation, created_at,is_active')->from('med_employees')->where(array('company_id'=>$company_id,'employeeid!='=>$employeeid,'employee_type'=>1))->order_by('employeeid','DESC')->get()->result_array();

		$data['manager'] = $this->db->select('employeeid, company_id, employee_type, employee_number, employee_name, employee_email, employee_country_code, employee_mobile, employee_picture, employee_designation, created_at,is_active,addedBy_role_type')->from('med_employees')->where(array('company_id'=>$company_id,'employeeid!='=>$employeeid,'employee_type'=>2))->order_by('employeeid','DESC')->get()->result_array();

		$data['supervisor'] = $this->db->select('employeeid, company_id, employee_type, employee_number, employee_name, employee_email, employee_country_code, employee_mobile, employee_picture, employee_designation, created_at,is_active')->from('med_employees')->where(array('company_id'=>$company_id,'employeeid!='=>$employeeid,'employee_type'=>3))->order_by('employeeid','DESC')->get()->result_array();
		
		return $data;
	}
	
	public function admin_new_employee($input){
		
		$check = $this->db->select('employee_email')->from('med_employees')->where(array('employee_email'=>$input['employee_email'],'company_id'=>$input['company_id']))->get()->row();
		if($check==''){
			$check = $this->db->select('employee_email')->from('med_employees')->where(array('employee_number'=>$input['employee_number'],'company_id'=>$input['company_id']))->get()->num_rows();
			if ($check){
				return array('status' => 400,'msg' =>$input['employee_number'].' employee number already exist.');
			}else{
				
				unset($input['submit']);
				$input['employee_type']	= $input['employee_designation'];
				$input['created_at']	= today()[0];
				$input['updated_at']	= today()[0];
				$input['employee_picture']	= '';
				$config['allowed_types'] 	= '*';
				$config['max_size'] 		= '1024';
				$date 						= date("Y-m");
				if (!empty($_FILES['employee_picture']['name'])){
					$path	=  './media/profile/'.$date."/";
					if (!is_dir($path)){
						mkdir($path , 0777);
						$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
						$fp = fopen($path."index.html","wb");
						fwrite($fp,$content); 
						fclose($fp); 
					}
					$config['file_name']	= date("Ymd")."_PP-".rand(1000,9999).'-'.time();
					$config['upload_path']	= $path;
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('employee_picture')){
						return array('status' =>400,'msg' =>"Profile picture uploading error: ".strip_tags($this->upload->display_errors()));
						exit;
					}else{
						$this->load->library('image_lib');
						$path = ltrim($path,"./");

						$picture_data = $this->upload->data();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $path.$picture_data['file_name']; //get original image
						$config['maintain_ratio'] = TRUE;
						$config['create_thumb'] = false;
						$config['height'] = 160;
						$this->image_lib->initialize($config);
						if (!$this->image_lib->resize()) {
							$card = $path.$picture_data['file_name'];
							if (file_exists($card)) {
								unlink($card);
							}
							return array('status' =>400,'msg' =>"Picture uploading error: ".strip_tags($this->upload->display_errors()));
							exit;
						}else{
							$input['employee_picture'] = ltrim($path,"./").$picture_data['file_name'];
						}
						$this->image_lib->clear();
					}
				}
				
				$rand = randomCode();
				$rand1 = rand(14567,98789);
				
				$input['employee_password']	= crypt($rand,APP_SALT);
				$input['employee_pin']		= crypt($rand1,APP_SALT);
				if(isset($input['section_roles'])){
					$input['section_roles']		= implode(',',@$input['section_roles']);
				}else{
					$input['section_roles']		= '';
				}
				
				$this->db->trans_start();
				$this->db->insert('med_employees',$input);
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'msg' => 'Internal server error. Please try again!');
				}else{
					$this->db->trans_commit();					
					$this->load->library('email');
					$config['protocol']    = 'smtp';
					$config['smtp_host']    = 'smtp.1and1.com';
					$config['smtp_port']    = '587';
					$config['smtp_timeout'] = '7';
					$config['smtp_user']    = 'noreply@dailycarerecords.com';
					$config['smtp_pass']    = 'Dailycarerecords!@#123';
					$config['charset']    = 'utf-8';
					$config['newline']    = "\r\n";
					$this->email->initialize($config);
					// setting mailtype
					$this -> email -> set_mailtype("html");
					$email_message = "";
					$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
					$email_message .= "<div>Hi, ".$input['employee_name']."</div>" ;
					$email_message .= "<div>Please find your Medication app login credential listed below:</div>" ;
					if($input['employee_type']==2 || $input['employee_type']==3){
						$email_message .= "<div><b>Admin Panel Credential:</div>";
						$email_message .= "<div>Admin Panel URL: ".base_url()."</div>" ;
						$email_message .= "<div>User Name : ".$input['employee_email']."</div>" ;
						$email_message .= "<div>Password : ".$rand."</div>" ;
					}else{
						$email_message .= "<div><b>Mobile App Credential:</div>" ;
						$email_message .= "<div>User Name : ".$input['employee_email']."</div>" ;
						$email_message .= "<div>Password : ".$rand1."</div>" ;
						$email_message .= "<div>Please use these credential for Medication account. After successful login we recommend you to change your password.</div>" ;
					}
					$this->email->from('noreply@dailycarerecords.com', 'DailyCareRecords Support Team');
					$this->email->to($input['employee_email']);
					$this->email->subject('Medication App login credential');
					$this->email->message($email_message);
					$this->email->send();
					echo $this->email->print_debugger();
					return array('status' => 200,'msg' =>'Account is created. Password has sent to employeee email. If email not received at your inbox please check spam folder.');
				}
			}
		}else{
			return array('status' => 400,'msg' =>'Account already registered with '.$input['employee_email']);
		}
	}
	
	
	public function admin_update_employee($input){
		
		$check = $this->db->select('employee_email')->from('med_employees')->where(array('employeeid!='=>$input['employeeid'],'employee_email'=>$input['employee_email'],'company_id'=>$input['company_id']))->get()->row_array();
		if(count($check)==0){
			unset($input['submit']);
			$input['employee_type']	= $input['employee_designation'];
			$input['created_at']	= today()[0];
			$input['updated_at']	= today()[0];
			$input['employee_picture']	= '';
			$config['allowed_types'] 	= '*';
			$config['max_size'] 		= '1024';
			$date 						= date("Y-m");
			if (!empty($_FILES['employee_picture']['name'])){
				$path	=  './media/profile/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content);
					fclose($fp);
				}
				$config['file_name']	= date("Ymd")."_PP-".rand(1000,9999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('employee_picture')){
					return array('status' =>400,'msg' =>"Profile picture uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					$input['employee_picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
				}
			}else{
				$input['employee_picture']	=	$input['employee_picture_hidden'];
			}
			$employeeid = $input['employeeid'];
			unset($input['employeeid']);
			unset($input['employee_picture_hidden']);
			$input['section_roles']		= implode(',',$input['section_roles']);
			$this->db->trans_start();
			$this->db->where('employeeid', $employeeid);
			$this->db->update('med_employees',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Account updated successfully.');
			}
		}else{
			return array('status' => 400,'msg' =>'Account already exists with '.$input['employee_email']);
		}		
	}
	
	public function view_userprofile($id){
		$data = $this->db->select('*')->from('med_employees')->where(array('employeeid'=>$id))->get()->row_array();
		if (count($data)){
			$data['employee_picture'] != '' ? $data['employee_picture'] = base_url().$data['employee_picture'] : '';
			return array('data'=>$data,'status' => 200,'msg' =>'');
			
		}else{
			return array('status' => 400,'msg' =>'User account not exist!');
		}
	}
	
	public function get_profile($id){
		$data = $this->db->select('*')->from('med_employees')->where(array('employeeid'=>$id))->get()->row_array();
		return $data;
	}
	
	public function edit_userprofile($input){
		
		$check = $this->db->select('employee_email')->from('med_employees')->where(array('employee_email'=>$input['employee_email'],'employeeid!='=>$input['employeeid']))->get()->row();
		if($check==''){
			$check = $this->db->select('employee_email')->from('med_employees')->where(array('employee_email'=>$input['employee_email'],'employeeid!='=>$input['employeeid']))->get()->row();
			if($check){
				$check = $this->db->select('employeeid')->from('med_employees')->where(array('employee_mobile'=>$input['employee_mobile'],'employee_country_code'=>$input['employee_country_code']))->where(array('employeeid!='=>$input['employeeid']))->get()->num_rows();
				if ($check){
					return array('status' => 400,'msg' =>'Account already registered with '.$input['employee_mobile'])." mobile number";
				}else{
					unset($input['submit']);
					$input['employee_picture']	= getusersdata($input['employeeid'],'employee_picture');
					$config['allowed_types'] 	= '*';
					$config['max_size'] 		= '500';
					$date 						= date("Y-m");
					
					if (!empty($_FILES['employee_picture']['name'])){
						$path	=  './media/profile/'.$date."/";
						if (!is_dir($path)){
							mkdir($path , 0777);
							$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
							$fp = fopen($path."index.html","wb"); 
							fwrite($fp,$content); 
							fclose($fp); 
						}
						$config['file_name']	= date("Ymd")."_PP-".rand(1000,9999).'-'.time();
						$config['upload_path']	= $path;
						$this->upload->initialize($config);
						if (!$this->upload->do_upload('employee_picture')){
							return array('status' =>400,'msg' =>"Profile picture uploading error: ".strip_tags($this->upload->display_errors()));
							exit;
						}else{
							$input['employee_picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
						}
					}
					$this->db->trans_start();
					$this->db->where(array('employeeid'=>$input['employeeid']))->update('med_employees',array('employee_name'=>$input['employee_name'],'employee_picture'=>$input['employee_picture'],'employee_designation'=>$input['employee_designation'],'employee_email'=>$input['employee_email'],'employee_country_code'=>$input['employee_country_code'],'employee_mobile'=>$input['employee_mobile'],'updated_at'=>today()[0]));
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						return array('status' => 500,'msg' => 'Internal server error. Please try again!');
					}else{
						$this->db->trans_commit();
						return array('status' => 200,'msg' =>'User account is updated.');
					}
				}
			}else{
				
			}
		}else{
			return array('status' => 400,'msg' =>'Account already registered with '.$input['employee_email']);
		}
	}
	
	
	public function UpdateAdminProfile($input){
		
		$check = $this->db->select('employee_email')->from('med_employees')->where(array('employee_email'=>$input['employee_email'],'employeeid!='=>$input['employeeid']))->get()->row();
		
		if($check==''){
		
			$check = $this->db->select('employeeid')->from('med_employees')->where(array('employee_mobile'=>$input['employee_mobile'],'employee_country_code'=>$input['employee_country_code']))->where(array('employeeid!='=>$input['employeeid']))->get()->num_rows();
			if ($check){
				return array('status' => 400,'msg' =>'Account already registered with '.$input['employee_mobile'])." mobile number";
			}else{
				unset($input['submit']);
				$input['employee_picture']	= getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_picture');
				$config['allowed_types'] 	= '*';
				$config['max_size'] 		= '500';
				$date 						= date("Y-m");
				
				if (!empty($_FILES['employee_picture']['name'])){
					$path	=  './media/profile/'.$date."/";
					if (!is_dir($path)){
						mkdir($path , 0777);
						$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
						$fp = fopen($path."index.html","wb"); 
						fwrite($fp,$content);
						fclose($fp);
					}
					$config['file_name']	= date("Ymd")."_PP-".rand(1000,9999).'-'.time();
					$config['upload_path']	= $path;
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('employee_picture')){
						return array('status' =>400,'msg' =>"Profile picture uploading error: ".strip_tags($this->upload->display_errors()));
						exit;
					}else{
						$input['employee_picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
					}
				}
				$this->db->trans_start();
				$this->db->where(array('employeeid'=>$input['employeeid']))->update('med_employees',array('employee_name'=>$input['employee_name'],'employee_picture'=>$input['employee_picture'],'employee_email'=>$input['employee_email'],'employee_country_code'=>$input['employee_country_code'],'employee_mobile'=>$input['employee_mobile'],'employee_number'=>$input['employee_number'],'updated_at'=>today()[0]));
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'msg' => 'Internal server error. Please try again!');
				}else{
					$this->db->trans_commit();
					return array('status' => 200,'msg' =>'User account is updated.');
				}
			}
		
		}else{
			return array('status' => 400,'msg' =>'Account already registered with '.$input['employee_email']);
		}
	}
	
	public function ChangePassword($input){
		$employeeid = $input['employeeid'];
		$pass = $input['oldpassword'];
		$newpass = $input['newpassword'];
		$confirm = $input['confirmpassword'];
		$data =  $this->db->query("SELECT employee_password FROM `med_employees` WHERE employeeid='".$employeeid."'")->row_array();
		if (count($data)) {
			$hashed_password = $data['employee_password'];
			if (!hash_equals($hashed_password, crypt($pass,APP_SALT))) {
			  return array('status' => 400,'msg' =>'Wrong old password');
			}else{
				if ($pass == $newpass){
					return array('status' => 400,'msg' =>'New password must be different then old password!');
				}else{
					if($newpass != $confirm){
						return array('status' => 400,'msg' =>'Confirm password not matched!');
					}else{
						$this->db->trans_start();
						$this->db->where(array('employeeid'=>$employeeid))->update('med_employees',array('employee_password'=>crypt($newpass,APP_SALT),'first_login'=>0,'updated_at'=>today()[0]));
						if ($this->db->trans_status() === FALSE){
							$this->db->trans_rollback();
							return array('status' => 500,'msg' => 'Internal server error. Please try again!');
						}else{
							$this->db->trans_commit();
							return array('status' => 200,'msg' =>'Password changed successfully.');
						}
					}
				}
			}
		} else {
			return array('status' => 400,'msg' =>'Account not exist.');
		}
	}
}