<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_timesheet extends CI_Model {

	public function loadCarer(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
        $data = $this->db->query("SELECT `employeeid`, `employee_name`,`employee_number` FROM `med_employees` WHERE `company_id`='".$company_id."' AND `is_active`=1 AND `employee_type`=1")->result_array();
        
        if(count($data)){
            $carer = "";
            $carer .='<select class="form-control selectpicker" data-live-search="true" id="load_carer" >';
            $carer .='<option value="">-Select carer-</option>';
            for($i=0;$i<count($data);$i++){
               $carer .="<option data-tokens='".$data[$i]['employee_name']."' value='".$data[$i]['employeeid']."'>".$data[$i]['employee_name']."(".$data[$i]['employee_number'].")</option>";
            }
            $carer .= "</select>"; 
           echo $carer;
        }else{
            return "<option>-No carer found-</option>";
        }
	}
    
    
    public function getTimeSheet($input){
        $timezone = $this->session->userdata('timezone');
        $company_id	= $this->session->userdata()['userdata']['company_id'];

	    $data = $this->db->query("SELECT DISTINCT(DATE(start_datetime)) as `date`,(SELECT `employee_name` FROM `med_employees` WHERE `employeeid`='".$input['carerid']."') as employee_name,(SELECT `employee_number` FROM `med_employees` WHERE `employeeid`='".$input['carerid']."') as employee_number FROM med_schedule WHERE employeeid='".$input['carerid']."' and date(created_at) between '".$input['startdate']."' and '".$input['enddate']."' ORDER BY start_datetime DESC")->result_array();
        for($i=0;$i<count($data);$i++){
            $date = $data[$i]['date'];
            $schedule = $this->db->query("SELECT med_schedule.schedule_id,  med_schedule.employeeid,  med_schedule.client_id,  med_schedule.start_datetime,  med_schedule.end_datetime,  med_schedule.secheduled_by, med_schedule.status,IFNULL(TIMESTAMPDIFF(MINUTE, med_schedule.`start_datetime`, med_schedule.`end_datetime`),0) as`minutes` ,CONCAT(med_clients.firstname,' ',med_clients.lastname) as client_name FROM med_schedule JOIN med_clients ON med_clients.client_id=med_schedule.client_id  WHERE med_schedule.employeeid='".$input['carerid']."' AND DATE(med_schedule.start_datetime)='".$date."' ORDER BY schedule_id ASC")->result_array();
            $totalmints = 0;
            $workedmints = 0;
            for($x=0;$x<count($schedule);$x++){
                $schedule[$x]['totalhrs'] = getHourAndMinutsDiff($schedule[$x]['start_datetime'],$schedule[$x]['end_datetime']);
                $totalmints = $totalmints+$schedule[$x]['minutes'];
                if($schedule[$x]['status']==1){
                    $workedmints = $workedmints+$schedule[$x]['minutes'];
                }
            }
			$data[$i]['date'] = date("d/m/Y",strtotime($data[$i]['date']));
            $data[$i]['schedule'] = $schedule;
            $data[$i]['totalhours'] = date("H:i", mktime(0,$totalmints));
            $data[$i]['workedhours'] = date("H:i", mktime(0,$workedmints));
            $data[$i]['carerid'] = $input['carerid'];
        }
       

    //    print "<pre>"; print_r($data); exit;
        return $data;
	}

}