<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_chat extends CI_Model {
	
	public function getChatInbox(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$employeeid = $this->session->userdata()['userdata']['employeeid'];
		$managers = $this->db->select("employeeid")->from("med_employees")->where(array("company_id"=>$company_id,'employeeid!='=>$employeeid))->get()->result_array();
		$data = array();
		for($i=0;$i<count($managers);$i++){
			$manager_id         = $managers[$i]['employeeid'];
			$employee_name	    = getAnythingData("med_employees","employeeid",$manager_id,'employee_name');
			$employee_picture	= getAnythingData("med_employees","employeeid",$manager_id,'employee_picture');
			
			if($employee_picture){
				$employee_picture = base_url().$employee_picture;
			}
			
			$chat =  $this->db->query("SELECT `message`,sent_date, IFNULL((select count(message_id) from med_message where sender_id=$manager_id and receiver_id=".$employeeid." and is_read=0),0) as new_messages FROM `med_message` WHERE ((`sender_id`=$manager_id AND `receiver_id`=".$employeeid." AND receiver_trash!=0) OR ( `sender_id`=".$employeeid." AND `receiver_id`=$manager_id  AND sender_trash!=0 )) and `receiver_trash`!=0 and `isactive`=1 ORDER BY message_id desc limit 1")->row_array();
						
			if(count($chat)==0){
				$chat = ["message"=>"","sent_date"=>"","new_messages"=>"0"];
			}
			$chat['employee_name'] = $employee_name;
			$chat['receiver_id'] = $manager_id;
			$chat['employee_picture'] = $employee_picture;
			$data[] = $chat;
		}
		
        $data = array_sort_by_column($data,'sent_date');
        return $data;
    }
	
	public function viewChat($receiver_id,$message_id=0){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$employeeid = $this->session->userdata()['userdata']['employeeid'];
		$receiver_company_id	 = getAnythingData("med_employees","employeeid",$receiver_id,'company_id');
		if($company_id = $receiver_company_id){
			$greater_then = "";
			if ($message_id){
				$greater_then = " AND message_id>$message_id";
			}
			$data  = $this->db->query("SELECT message_id,sender_id,thread_id,message,sent_date, is_read FROM med_message WHERE ((sender_id=".$receiver_id." AND receiver_id=".$employeeid." AND receiver_trash!=0 ) OR ( sender_id=".$employeeid." AND receiver_id=".$receiver_id."  AND sender_trash!=0)) and isactive=1 $greater_then ORDER BY message_id DESC")->result_array();
			
			$data = array_reverse($data);
			## Sender INFO

			$sender_picture	 = getAnythingData("med_employees","employeeid",$employeeid,'employee_picture');
			if($sender_picture){
				$sender_picture = base_url().$sender_picture;
			}else{
				$sender_picture = base_url()."asset/img/dummy.svg";
			}
			$data['sender_data'] = ['sender_id'=>$employeeid,'sender_name'=>getAnythingData("med_employees","employeeid",$employeeid,'employee_name'),'sender_picture'=>$sender_picture];
			
			## Receiver info
			$receiver_picture	 = getAnythingData("med_employees","employeeid",$receiver_id,'employee_picture');
			if($receiver_picture){
				$receiver_picture = base_url().$receiver_picture;
			}else{
				$receiver_picture = base_url()."asset/img/dummy.svg";
			}
			$data['receiver_data'] = ['receiver_id'=>$receiver_id,'receiver_name'=>getAnythingData("med_employees","employeeid",$receiver_id,'employee_name'),'receiver_picture'=>$receiver_picture];
			
			## Set Read
			$this->db->where(array('receiver_id'=>$employeeid,'sender_id'=>$receiver_id))->update('med_message',array('is_read'=>1));
			$data['valid'] = 1;
		}else{
			$data['valid'] = 0;
		}
		return $data;
	}
	
	public function deleteChat($receiver_id,$message_id=0){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$employeeid = $this->session->userdata()['userdata']['employeeid'];
		$receiver_company_id = getAnythingData("med_employees","employeeid",$receiver_id,'company_id');
		if($company_id = $receiver_company_id){
			$this->db->query("update med_message set sender_trash = 0 where sender_id = '".$employeeid."' and receiver_id=".$receiver_id."");
			$this->db->query("update med_message set receiver_trash = 0 where receiver_id = '".$employeeid."' and sender_id=".$receiver_id."");
			$data['valid'] = 1;
		}else{
			$data['valid'] = 0;
		}
		return $data;
	}	
}