<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * App Name       -    	Medication
 * author         -     Hem Thakur
 * created        -     24/01/2018
**/		
class AppBackend extends CI_Model {
	
	
	################################################
	############ COMMON SECTION ####################
	################################################
	
	#Push Notification section
	# section = 1 => Schedule Section, 
	# section = 2 => Message Section 
	# section = 3 => Concern section 

	/*
	* Request Authentication
	*/
	public function ht_check_auth_client(){
		$auth_key	= $this->input->get_request_header('Auth-Key', TRUE);
		if($auth_key == APP_AUTH_KEY_SECRET){
			return true;
        } else {
            return json_output(400,array('status' => 400,'message' => 'Unauthorized.','method'=>$this->uri->segment(3)));
        }
    }
	/*
	* Search Company
	*/
	public function ht_search_company($input)
    {	
		$data 	= $this->db->select('company_id,company_name')->from('med_companies')->WHERE(array('status'=>'1'))->group_start()->like('company_name',$input['keyword'])->group_end()->get()->result_array();
		return array('status' => 200,'message' => 'Search result.','method'=>'search_company','data'=>$data);
	}
	/*
	* Employee Login
	*/
	public function ht_employee_login($input)
    {	
		
		//$data  = $this->db->query(" SELECT employeeid, company_id, employee_type, employee_country_code, employee_number, employee_name, employee_email, employee_pin, employee_mobile, employee_picture, employee_designation,first_login from med_employees where employee_email='".$input['employee_number']."'")->row_array();	
		//if($data == ""){
		//	return array('status' =>203,'message' => 'Account not found.','method'=>'employee_login');
      //  } 
		$data  = $this->db->query(" SELECT e.`employeeid`, e.`company_id`, e.`employee_type`, e.`employee_country_code`, e.`employee_number`, e.`employee_name`, e.`employee_email`, e.`employee_pin`, e.`employee_mobile`, e.`employee_picture`, e.`employee_designation`, e.`first_login`, c.`status` as 'company_status',e.is_active from med_employees e JOIN med_companies c ON  e.`company_id` = c.`company_id` where employee_email='".$input['employee_number']."'")->row_array();
		
		if($data == ""){
			return array('status' =>203,'message' => 'Account not found.','method'=>'employee_login');
		}else if($data['company_status'] == '2' ){
			return array('status' =>203,'message' => 'Access Denied.','method'=>'employee_login');
        }elseif($data['is_active']==0){
			return array('status' =>203,'message' => 'Access Denied. Please contact Owner.','method'=>'employee_login');
		}else {
			if(isset($input['employee_password'])){
				$hashed_password = $data['employee_pin'];
				if (!hash_equals($hashed_password, crypt($input['employee_password'],APP_SALT))) {
				   return array('status' => 203,'message' => 'Wrong password.','method'=>'employee_login');
				   exit;
				}
			}
			unset($data['employee_password']);
			$employeeid	= $data['employeeid'];
			$token = crypt((substr(md5(rand()), 0, 7)),APP_SALT);
			$this->db->trans_start();
						
			$token1 		=	$this->db->query("select id from med_employees_authentication where device_token = '".$input['device_token']."'")->row_array();
			if(count($token1)){
				$this->db->query("UPDATE `med_employees_authentication` set `employeeid`='".$employeeid."',token='".$token."',device_type='".$input['device_type']."',login_at='".today()[0]."' where device_token='".$input['device_token']."'");
			}else{
				$this->db->insert('med_employees_authentication',array('employeeid'=>$employeeid,'token'=>$token,'device_type'=>$input['device_type'],'device_token'=>$input['device_token'],'login_at'=>today()[0]));
			}
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' => 'Internal server error.','method'=>'employee_login');
			} else {
				$this->db->trans_commit();
				if(isset($input['timezone'])){
					if($input['timezone']==""){
						$input['timezone'] = "UTC";
					}
					$this->db->where(array('employeeid'=>$data['employeeid']))->update('med_employees',array('timezone'=>$input['timezone'],'first_login'=>0,'updated_at'=>today()[0]));
				}
				$data['employee_picture'] != '' ? $data['employee_picture'] = base_url().$data['employee_picture'] : '';
				$data['token'] = $token;
				return array('status' => 200,'message' => 'Successfully login.','method'=>'employee_login','data'=>$data);
			}
		}
	}
	/*
	* Forgot Password
	*/
	public function ht_forgot_password($input)
    {
		$data  = $this->db->select('*')->from('med_employees')->where(array('employee_email'=>$input['email'],'is_active'=>'1'))->get()->row();
		if($data == ""){
			return array('status' => 400,'message' => 'Account not found.','method'=>'forgot_password');
        } else { 
			$random		= rand(12345,99999);
			$password 	= crypt($random,APP_SALT);
			$this->db->trans_start();
			$this->db->where(array('employee_email'=>$input['email']))->update('med_employees',array('employee_pin'=>$password,'updated_at'=>today()[0]));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' => 'Internal server error.');
			} else {
				$this->db->trans_commit();
				$this->load->library('email');
				$config['protocol']    = 'smtp';
				$config['smtp_host']    = 'smtp.1and1.com';
				$config['smtp_port']    = '587';
				$config['smtp_timeout'] = '7';
				$config['smtp_user']    = 'noreply@dailycarerecords.com';
				$config['smtp_pass']    = 'Dailycarerecords!@#123';
				$config['charset']    = 'utf-8';
				$config['newline']    = "\r\n";
				$this->email->initialize($config);
				$this->email->from('noreply@dailycarerecords.com', 'DailyCareRecords Support Team');
				$this->email->to($input['email']);
				$this->email->subject('Medication App temporary passowrd');
				$this->email->set_mailtype("html");
				$email_message = "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
				$email_message .= "<div>Temporary login password: ".$random."</div>" ;
				$email_message .= "<div>Please use this passowrd for login to your Medication app. After successful login we recommend you to change your password.</div>" ;
				$email_message .= "<div>Thanks</div>" ;
				$this->email->message($email_message);
				$this->email->send();
				return array('status' => 200,'message' => 'A temporary password has sent at your email.If email not received at your inbox please check spam folder.','method'=>'forgot_password');
			}
		}
    }
	/*
	* Log out
	*/
    public function ht_logout()
    {
		$employeeid = 	$this->input->get_request_header('employeeid', TRUE);
        $token     	= 	$this->input->get_request_header('token', TRUE);
        return array('status' => 200,'message' => 'Successfully logout.','method'=>'logout');
    }
	/*
	* Authenticate User
	*/
    public function ht_auth()
    {
        $employeeid	= $this->input->get_request_header('employeeid', TRUE);
        $token		= $this->input->get_request_header('token', TRUE);
        $q  = $this->db->select('id')->from('med_employees_authentication')->where(array('employeeid'=>$employeeid,'token'=>$token))->get()->num_rows();
        if($q){
            return array('id'=>$employeeid,'status' => 200,'message' => 'Authorized.','method'=>$this->uri->segment(3));
        } else {
            return json_output(401,array('status' => 401,'message' => 'Unauthorized.','method'=>$this->uri->segment(3)));
        }
    }
	/*
	* View Profile
	*/
	public function ht_view_profile($input)
	{
		$data = $this->db->select('employeeid, company_id, employee_type, employee_number, employee_name, employee_email, employee_country_code, employee_mobile, employee_picture, employee_designation')->from('med_employees')->where(array('employeeid'=>$input['employeeid']))->get()->row();
		$data->employee_picture != '' ? $data->employee_picture = base_url().$data->employee_picture : '';
		return array('status' => 200,'message' =>'Profile detail.','method'=>'view_profile','data'=>$data);
	}
	/*
	* Update Profile
	*/
	public function ht_update_profile($input)
	{	
		
		$input['employee_email']		= empty($input['employee_email']) ? getAnythingData("med_employees","employeeid",$input['employeeid'],"employee_email") : $input['employee_email'];
		
		$check= $this->db->query("SELECT employeeid FROM med_employees WHERE employee_email='".$input['employee_email']."' AND employeeid!=".$input['employeeid']."")->num_rows();
		
		if($check){
			return array('status' => 400,'message' =>'Email id already used.','method'=>'update_profile');
		}else{
			
			$input['employee_country_code']	= empty($input['employee_country_code']) ? getAnythingData("med_employees","employeeid",$input['employeeid'],"employee_country_code") : $input['employee_country_code'];
			$input['employee_mobile']		= empty($input['employee_mobile']) ? getAnythingData("med_employees","employeeid",$input['employeeid'],"employee_mobile") : $input['employee_mobile'];
			
			$check= $this->db->query("SELECT employeeid FROM med_employees WHERE employee_country_code='".$input['employee_country_code']."' AND employee_mobile='".$input['employee_mobile']."' AND employeeid!=".$input['employeeid']."")->num_rows();
	
			
			if($check){
				return array('status' => 400,'message' =>'Mobile number already used.','method'=>'update_profile');
			}else{
				$input['employee_picture']		= getAnythingData("med_employees","employeeid",$input['employeeid'],"employee_picture");
				$input['gender']				= empty($input['gender']) ? getAnythingData("med_employees","employeeid",$input['employeeid'],"gender") : $input['gender'];
				$date = date("Y-m");
				$config['allowed_types']= '*';
				if (!empty($_FILES['employee_picture']['name'])){
					$this->load->library('image_lib');
					$path	=  './media/profile/'.$date."/";
					if (!is_dir($path)){
						mkdir($path , 0777);
						$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
						$fp = fopen($path."index.html","wb"); 
						fwrite($fp,$content); 
						fclose($fp); 
					}
					$config['file_name']	= date("Ymd")."_EPP-".rand(100000,999999).'-'.time();
					$config['upload_path']	= $path;
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('employee_picture')){
						return array('status' =>400,'message' =>"File uploading error: ".strip_tags($this->upload->display_errors()),'method'=>$this->uri->segment(3));
						exit;
					}else{
						$input['employee_picture'] !="" ? unlink("./".$input['employee_picture']) : '';
						## Resize image
						$uploaded_picture_data = $this->upload->data();
						//$config['image_library'] = 'gd2';
						//$config['source_image'] = $path.$uploaded_picture_data['file_name']; //get original image
						//$config['maintain_ratio'] = TRUE;
						//$config['create_thumb'] = false;
						//$config['height'] = 200;
						//$this->image_lib->initialize($config);
						//if (!$this->image_lib->resize()) {
						//	$pic = $path.$uploaded_picture_data['file_name'];
						//	if (file_exists($pic)) {
						//		unlink($pic);
						//	}
						//	return array('status' =>400,'method'=>'update_profile','message' =>"Picture resizing error: ".strip_tags($this->image_lib->display_errors()));
						//	exit;
						//}else{
							$input['employee_picture'] = ltrim($path,"./").$uploaded_picture_data['file_name'];
						//}
						//$this->image_lib->clear();
					}
				}
				$this->db->trans_start();
				$this->db->where(array('employeeid'=>$input['employeeid']))->update('med_employees',array('employee_name'=>$input['employee_name'],'employee_picture'=>$input['employee_picture'],'updated_at'=>today()[0],'employee_mobile'=>$input['employee_mobile'],'employee_country_code'=>$input['employee_country_code'],'employee_email'=>$input['employee_email'],'gender'=>$input['gender']));
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'message' =>'Internal server error.','method'=>'update_profile');
				} else {
					$this->db->trans_commit();
		
					## Activity History
					$this->db->insert("med_activities_history",array("employeeid"=>$input['employeeid'],"section"=>5,"activity"=>"You update your profile.","main_id"=>$input['employeeid'],"activity_at"=>today()[0]));

					##--- Return updated params in responde ---##
					$data = $this->db->select('employee_name,employee_picture,employee_mobile')->from('med_employees')->where(array('employeeid'=>$input['employeeid']))->get()->row_array();
					$data['employee_picture'] 	=	base_url().$data['employee_picture'];
					return array('status' => 200,'message' =>'Profile updated.','data'=>$data,'method'=>'update_profile');
				}
			}
		}
	}
	/*
	* Change Password
	*/
	public function ht_change_password($input)
	{	
		$data  = $this->db->select('employee_pin')->from('med_employees')->where(array('employeeid'=>$input['employeeid']))->get()->row();
		if($data == ""){
			return array('status' => 400,'message' => 'Account not found.','method'=>'change_password');
        } else {
			$oldpassword = $data->employee_pin;
			if (hash_equals($oldpassword, crypt($input['oldpassword'],APP_SALT))) {
				if ($input['oldpassword'] !=$input['newpassword']){
					if(strlen($input['newpassword']) < 5 ){
						return array('status' => 200,'message' =>'Password must be at least of 5 digits.','method'=>'change_password');
					}
					$password 	= crypt($input['newpassword'],APP_SALT);
					$this->db->trans_start();
					$this->db->where(array('employeeid'=>$input['employeeid']))->update('med_employees',array('employee_pin'=>$password,'updated_at'=>today()[0]));
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						return array('status' => 500,'message' => 'Internal server error.');
					} else{
						$this->db->trans_commit();
						## Activity History
						$this->db->insert("med_activities_history",array("employeeid"=>$input['employeeid'],"section"=>5,"activity"=>"You change your password.","main_id"=>$input['employeeid'],"activity_at"=>today()[0]));
						return array('status' => 200,'message' =>'Password changed successfully.','method'=>'change_password');
					}
				}else{
					return array('status' => 400,'message' => 'New password must be different then old password.','method'=>'change_password');
				}
			} else {
               return array('status' => 400,'message' => 'Wrong old password.','method'=>'change_password');
            }
        }
	}
	
	#################################################
	############ EMPLOYEE SECTION ###################
	#################################################
	/*
	* Search Clients
	*/
	public function ht_search_clients($input)
	{
		$keyword = $input['search_keyword'];
		if (is_numeric($keyword)){
			$search  = " and a.postcode LIKE '%".$input['search_keyword']."%'";
		}elseif(empty($keyword)){
			$search = "";
		}else{
			$word = explode(" ",$input['search_keyword']);
			if(!empty($word[1])){
				$search  = " and (a.firstname LIKE '%".$word[0]."%' || a.firstname LIKE '%".$word[1]."%' || a.lastname LIKE '%".$word[0]."%' || a.lastname LIKE '%".$word[1]."%') ";
			}else{
				$search  = " and (a.firstname LIKE '%".$word[0]."%' || a.lastname LIKE '%".$word[0]."%') ";
			}
		}
		$company_id = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$data  = $this->db->query("SELECT client_id, firstname, lastname, dob, gender, `address`, postcode, IF(client_picture='','',CONCAT('".base_url()."','',client_picture)) as client_picture, emergency_contact, personal_information, latitude, longitude,if((select carer_notification_id from med_carer_notifications where main_id = a.client_id and carer_id = '".$input['employeeid']."' and notification_type = 3 and is_read = 0 order by carer_notification_id desc limit 1),1,0) as badge from med_clients as a where company_id='".$company_id."' $search ")->result_array();
		
		return array('status' =>200,'message' =>'Search data.','data'=>$data,'method'=>'search_clients');
	}
	/*
	* Get Suggested Clients
	*/
	public function ht_get_suggestedClients($input)
	{
		$company_id = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$data  = $this->db->select("client_id, firstname, lastname, dob, gender, address, postcode, client_picture, emergency_contact, personal_information, latitude, longitude,ROUND((6371 * acos(cos(radians(".$input['latitude'].")) * cos( radians(latitude)) * cos(radians(latitude)-radians(".$input['latitude'].")) + sin(radians(".$input['latitude'].")) * sin(radians(latitude)))),2)  AS distance,if((select carer_notification_id from med_carer_notifications where main_id = a.client_id and carer_id = '".$input['employeeid']."' and notification_type = 3 and is_read = 0 order by carer_notification_id desc limit 1),1,0) as badge")->from('med_clients as a')->where("company_id=".$company_id)->having('distance <= 5')->get()->result_array();
		for($i=0;$i<count($data);$i++){
			$data[$i]['client_picture'] ? $data[$i]['client_picture'] = base_url().$data[$i]['client_picture'] : "";
		}
		return array('status' =>200,'message' =>'Suggestions.','data'=>$data,'method'=>'get_suggestedClients');
	}
	/*
	*  Get Client Care Program
	*/
	public function ht_get_ClientCareProgram($input)
	{	
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$client_company_id = getAnythingData("med_clients","client_id",$input['client_id'],'company_id');
		if($company_id==$client_company_id){
			$data  = $this->db->select("program_id, client_id, am_time_care_program, noon_time_care_program, tea_time_care_program, night_time_care_program, program_startdate")->from('med_care_program')->where(array("client_id"=>$input['client_id']))->get()->row_array();
			if(count($data)){
				$comments = getAnythingData("med_clients","client_id",$input['client_id'],'comments');
				$data['am_time_care_program'] = unserialize($data['am_time_care_program']);
				$data['noon_time_care_program']=	unserialize($data['noon_time_care_program']);
				$data['tea_time_care_program'] = unserialize($data['tea_time_care_program']);
				$data['night_time_care_program'] = unserialize($data['night_time_care_program']);
				
				$key = array("am_time_care_program","noon_time_care_program","tea_time_care_program","night_time_care_program");
				for($i=0;$i<4;$i++){
					$program = $data[$key[$i]];
					//print_r($program); exit;
					if(count($program)){
						for($x=0;$x<count($program);$x++){
							$medicine = $program[$x]['medicine'];
							$check  = $this->db->select('status,IFNULL(medication_datetime,"") as medication_datetime,IFNULL(message,"") as message')->from('med_medication_chart')->where(array('client_id'=>$input['client_id'],'care_time'=>($i+1),'medicine'=>$medicine))->group_start()->like('medication_datetime',$input['date'])->group_end()->get()->row();
							if($check){
								$data[$key[$i]][$x]['status'] = $check->status;
								$data[$key[$i]][$x]['time_given']	=	$check->medication_datetime;																		$data[$key[$i]][$x]['message']	=	$check->message;

							}else{
								$data[$key[$i]][$x]['status'] = '0';
								$data[$key[$i]][$x]['time_given']	=	'';
								$data[$key[$i]][$x]['message']	=	'';
							}
						}
					}
				}
				return array('status' =>200,'message' =>'Care program.','data'=>$data,'comments'=>$comments,'method'=>'get_ClientCareProgram');
			}else{
				return array('status' => 400,'message' => 'Care program not updated yet.','method'=>'get_ClientCareProgram');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'get_ClientCareProgram');
		}
	}
	/*
	* Enter New Medication
	*/
	public function ht_enter_newMedication($input)
	{
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$client_company_id = getAnythingData("med_clients","client_id",$input['client_id'],'company_id');
		
		if($company_id==$client_company_id){
			$program_id = getAnythingData("med_care_program","client_id",$input['client_id'],'program_id');
			if($program_id == $input['program_id']){
				
				$check  = $this->db->select('medication_chart_id')->from('med_medication_chart')->where(array('client_id'=>$input['client_id'],'program_id'=>$input['program_id'],'care_time'=>$input['care_time'],'medicine'=>$input['medicine']))->group_start()->like('medication_datetime',date("Y-m-d"))->group_end()->get()->row();
				if(count($check)){
					return array('status' => 400,'message' => 'Medication chart already entered for this medicine','method'=>'enter_newMedication');
				} else {
					$input['medication_datetime'] = $input['date']." ".date("H:i:s");
					
					$input['firstname'] 		=	getAnythingData("med_clients","client_id",$input['client_id'],'firstname');
					$input['lastname'] 			= 	getAnythingData("med_clients","client_id",$input['client_id'],'lastname');
					$input['employee_name'] 	= 	getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_name');
					$input['employee_number'] 	= 	getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_number');							
					
					unset($input['date']);
					$this->db->trans_start();
					$this->db->insert("med_medication_chart",$input);
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						return array('status' => 500,'message' => 'Internal server error.');
					}else{
						$medicationid = $this->db->insert_id();
						$this->db->trans_commit();
						## Activity History
						$client_name = getAnythingData("med_clients","client_id",$input['client_id'],'firstname')." ".getAnythingData("med_clients","client_id",$input['client_id'],'lastname');
						$this->db->insert("med_activities_history",array("employeeid"=>$input['employeeid'],"section"=>1,"activity"=>"You gave medication to ".$client_name,"main_id"=>$input['client_id'],"inserted_id"=>$medicationid ,"activity_at"=>today()[0]));
						return array('status' => 200,'message' =>'Medication chart updated.','method'=>'enter_newMedication');
					}
				}
			}else{
				return array('status' => 400,'message' => 'Invalid care program!','method'=>'enter_newMedication');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'enter_newMedication');
		}
	}
	/*
	*  Get Client Care Program
	*/
	public function ht_view_ClientsMedications($input)
	{
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$client_company_id = getAnythingData("med_clients","client_id",$input['client_id'],'company_id');
		if($company_id==$client_company_id){
			
			if($input['start']=='all' && $input['end']=='all'){
				$daterange = "";
			}else{
				$daterange = " AND med_medication_chart.medication_datetime>= '".$input['start']." 00:00:00' AND med_medication_chart.medication_datetime<= '".$input['end']." 23:59:59'";
			}
			$page		= $input['page'];
			$limit		= 30;
			$start		= ($page - 1)* $limit; 
			$totalrows 	= $this->db->query("SELECT medication_chart_id from  med_medication_chart WHERE med_medication_chart.client_id='".$input['client_id']."' $daterange")->num_rows();

			$totalpages = ceil($totalrows/$limit);
			$previous	=  $totalpages > 1 ? "1" : "0";
			$next		=  $page < $totalpages ? "1" : "0";
			
			$data  = $this->db->query("SELECT med_medication_chart.*,IF(med_clients.client_picture='','',CONCAT('".base_url()."','',med_clients.client_picture)) as client_picture,IF(med_employees.employee_picture='','',CONCAT('".base_url()."','',med_employees.employee_picture)) as employee_picture from med_medication_chart join med_clients ON med_clients.client_id=med_medication_chart.client_id join med_employees ON med_employees.employeeid=med_medication_chart.employeeid where med_medication_chart.client_id='".$input['client_id']."' $daterange order by med_medication_chart.medication_datetime DESC limit $start ,$limit")->result_array();

			return array('status' =>200,'message' =>'Medication Cart data.','data'=>$data,'method'=>'view_ClientsMedications','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
			
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'view_ClientsMedications');
		}
	}
	/*
	*  Get Daily Records Questions
	*/
	public function ht_get_dailyRecordsQuestions($input)
	{	
		$data  = $this->db->select("*")->from('med_daily_records_questions')->where(array("isactive"=>1))->order_by('question_id','ASC')->get()->result_array();
		for($i=0;$i<count($data);$i++){
			$data[$i]['answer'] = "";
		}
		return array('status' =>200,'message' =>'Daily records questions.','data'=>$data,'method'=>'get_dailyRecordsQuestions');
	}
	/*
	* Add Daile Record
	*/
	public function ht_add_DailyRecords($input)
	{	
		$question_answer	= json_decode($input['question_answer'],TRUE);
		unset($input['question_answer']);
		$input['date'] = date("Y-m-d H:i:s");
		for($i=0;$i<count($question_answer);$i++){
			$input['question_id']	= $question_answer[$i]['question_id'];
			$input['answer'] 		= $question_answer[$i]['answer'];
			
			$input['firstname'] 		=	getAnythingData("med_clients","client_id",$input['client_id'],'firstname');
			$input['lastname'] 			= 	getAnythingData("med_clients","client_id",$input['client_id'],'lastname');
			$input['employee_name'] 	= 	getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_name');
			$input['employee_number'] 	= 	getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_number');			
			
			$this->db->insert("med_daily_records",$input);
		}
		## Activity History
		$client_name = getAnythingData("med_clients","client_id",$input['client_id'],'firstname')." ".getAnythingData("med_clients","client_id",$input['client_id'],'lastname');
		$this->db->insert("med_activities_history",array("employeeid"=>$input['employeeid'],"section"=>2,"activity"=>"You have submitted the daily records of ".$client_name,"main_id"=>$input['client_id'],"activity_at"=>today()[0]));

		return array('status' => 200,'message' =>'Daily record added.','method'=>'add_DailyRecords');
	}
	/*
	*  View Daily Records of employee
	*/
	public function ht_view_DailyRecords($input)
	{	
		
		if($input['start']!='all' && $input['end']!='all'){
			$daterange 	=" AND DATE(date)>='".$input['start']."' AND DATE(date)<= '".$input['end']."'";
		}else{
			$daterange = "";
		}
		$page		= $input['page'];
		$limit		= 30;
		$start		= ($page - 1)* $limit; 
		
		$totalrows 	= $this->db->query("SELECT record_id from med_daily_records WHERE employeeid=".$input['employeeid']." and client_id=".$input['client_id']." $daterange ")->num_rows();

		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0"; 
		$next		=  $page < $totalpages ? "1" : "0";
		
		$data  = $this->db->query("SELECT med_daily_records.*,med_daily_records_questions.question from  med_daily_records JOIN med_daily_records_questions ON med_daily_records_questions.question_id=med_daily_records.question_id where med_daily_records.employeeid=".$input['employeeid']." AND med_daily_records.client_id = ".$input['client_id']." $daterange order by med_daily_records.date DESC limit $start, $limit")->result_array();

		//	print_R($this->db->last_query()); exit;
		return array('status' =>200,'message' =>'Daily recored data.','data'=>$data,'method'=>'view_DailyRecords','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}

	/*
	*  View Daily Records of employee
	*/
	public function ht_view_DailyRecordsData($input)
	{	
		
		if($input['start']!='all' && $input['end']!='all'){
			$daterange 	=" AND DATE(date)>='".$input['start']."' AND DATE(date)<= '".$input['end']."'";
		}else{
			$daterange = "";
		}
		$page		= $input['page'];
		$limit		= 10;
		$start		= ($page - 1)* $limit; 
		
		//$totalrows 	= $this->db->query("SELECT record_id from med_daily_records WHERE employeeid=".$input['employeeid']." and client_id=".$input['client_id']." $daterange ")->num_rows();
		$totalrows 	= $this->db->query("SELECT DISTINCT(DATE(`date`)) AS `date` from med_daily_records WHERE employeeid=".$input['employeeid']." and client_id=".$input['client_id']." $daterange ")->num_rows();


		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0";
		$next		=  $page < $totalpages ? "1" : "0";		
		
		$data  = $this->db->query("SELECT  DISTINCT(DATE(med_daily_records.`date`)) AS `date`,TIME(med_daily_records.`date`) AS `time`,med_daily_records.client_id,med_daily_records.postcode,med_daily_records.employeeid,med_daily_records.firstname,med_daily_records.lastname ,med_daily_records.date as `fulldate`,med_daily_records.employeeid as `carer_id`,med_daily_records.employee_number as carer_number, med_daily_records.employee_name as carer_name FROM  med_daily_records where med_daily_records.employeeid=".$input['employeeid']." AND med_daily_records.client_id = ".$input['client_id']." $daterange order by med_daily_records.date DESC limit $start, $limit")->result_array();

		for($i=0;$i<count($data);$i++){
			$data[$i]['question_answers'] = $this->db->query("SELECT med_daily_records.record_id,med_daily_records.answer,med_daily_records_questions.question from  med_daily_records JOIN med_daily_records_questions ON med_daily_records_questions.question_id=med_daily_records.question_id  where med_daily_records.employeeid=".$input['employeeid']." AND med_daily_records.client_id = ".$input['client_id']." AND `date`='".$data[$i]['fulldate']."'")->result_array(); 

		}

		//$data  = $this->db->query("SELECT med_daily_records.*,med_daily_records_questions.question,med_clients.firstname,med_clients.lastname from  med_daily_records JOIN med_daily_records_questions ON med_daily_records_questions.question_id=med_daily_records.question_id JOIN med_clients ON med_clients.client_id=med_daily_records.client_id where med_daily_records.employeeid=".$input['employeeid']." AND med_daily_records.client_id = ".$input['client_id']." $daterange order by med_daily_records.date DESC limit $start, $limit")->result_array();

		//	print_R($this->db->last_query()); exit;
		return array('status' =>200,'message' =>'Daily recored data.','data'=>$data,'method'=>'view_DailyRecordsData','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}

	public function ht_view_DailyRecordsManagerSection($input)
	{	
		
		if($input['start']!='all' && $input['end']!='all'){
			$daterange 	=" AND DATE(date)>=".$input['start']." AND DATE(date)<= ".$input['end'];
		}else{
			$daterange = "";
		}
		$page		= $input['page'];
		$limit		= 30;
		$start		= ($page - 1)* $limit; 
		
		$totalrows 	= $this->db->query("SELECT record_id from med_daily_records WHERE client_id=".$input['client_id']." $daterange ")->num_rows();


		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0"; 
		$next		=  $page < $totalpages ? "1" : "0";
		
		$data  = $this->db->query("SELECT med_daily_records.*,med_daily_records_questions.question from  med_daily_records JOIN med_daily_records_questions ON med_daily_records_questions.question_id=med_daily_records.question_id where med_daily_records.client_id = ".$input['client_id']." $daterange order by med_daily_records.date DESC limit $start, $limit")->result_array();

		return array('status' =>200,'message' =>'Daily recored data.','data'=>$data,'method'=>'view_DailyRecordsManagerSction','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}


	/*
	*  View Daily Records Manager Section
	*/
	public function ht_view_DailyRecordsManagerSctionData($input)
	{	
		
		if($input['start']!='all' && $input['end']!='all'){
			$daterange 	=" AND DATE(date)>='".$input['start']."' AND DATE(date)<= '".$input['end']."'";
		}else{
			$daterange = "";
		}
		$page		= $input['page'];
		$limit		= 10;
		$start		= ($page - 1)* $limit;
				
		$totalrows 	= $this->db->query("SELECT DISTINCT(DATE(med_daily_records.`date`)) AS `date` from med_daily_records WHERE client_id=".$input['client_id']." $daterange ")->num_rows();

		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0";
		$next		=  $page < $totalpages ? "1" : "0";
		
		$data  = $this->db->query("SELECT DISTINCT(DATE(med_daily_records.`date`)) AS `date`,TIME(med_daily_records.`date`) AS `time`,med_daily_records.client_id,med_daily_records.postcode,med_daily_records.employeeid,med_daily_records.firstname,med_daily_records.lastname,med_daily_records.employee_name,med_daily_records.employee_number,med_daily_records.date as `fulldate` from  med_daily_records where med_daily_records.client_id = ".$input['client_id']." $daterange order by med_daily_records.date DESC limit $start, $limit")->result_array();
		
		for($i=0;$i<count($data);$i++){
			$data[$i]['question_answers'] = $this->db->query("SELECT med_daily_records.record_id,med_daily_records.answer,med_daily_records_questions.question from  med_daily_records JOIN med_daily_records_questions ON med_daily_records_questions.question_id=med_daily_records.question_id  where med_daily_records.client_id = ".$input['client_id']." AND `date`='".$data[$i]['fulldate']."'")->result_array();

		}
		return array('status' =>200,'message' =>'Daily recored data.','data'=>$data,'method'=>'view_DailyRecordsManagerSctionData','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}
	/* 
	*  Get Message Inbox
	*/
	public function ht_get_MessageInbox($input)
	{
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$timezone	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'timezone');
		//$managers = $this->db->select("employeeid")->from("med_employees")->where(array("company_id"=>$company_id,'employeeid!='=>$input['employeeid']))->get()->result_array();
		$managers = $this->db->query("SELECT employeeid from med_employees where company_id='".$company_id."' AND employeeid!='".$input['employeeid']."' AND (employee_type = 2 || employee_type = 3 )" )->result_array();
		$data = array();
		// get hour diff between utc and timezone
		$diff	=	getHoursMin($timezone);	
		for($i=0;$i<count($managers);$i++){
			$manager_id = $managers[$i]['employeeid'];
			$employee_name	 = getAnythingData("med_employees","employeeid",$manager_id,'employee_name');
			$employee_picture	 = getAnythingData("med_employees","employeeid",$manager_id,'employee_picture');
			
			if($employee_picture){
				$employee_picture = base_url().$employee_picture;
			}
			$chat =  $this->db->query("SELECT message,CONVERT_TZ (sent_date, '+0:00','".$diff."') as sent_date, IFNULL((select count(message_id) from med_message where sender_id=$manager_id and receiver_id=".$input['employeeid']." and is_read=0),0) as new_messages FROM med_message WHERE ((sender_id=$manager_id AND receiver_id=".$input['employeeid'].") OR ( sender_id=".$input['employeeid']." AND receiver_id=$manager_id )) and (receiver_trash!=0 AND sender_trash!=0) and isactive=1 ORDER BY message_id desc limit 1")->row_array();
			
			if(count($chat)==0){
				$chat = ["message"=>"","sent_date"=>"","new_messages"=>"0"];
			}
			$chat['employee_name'] = $employee_name;
			$chat['receiver_id'] = $manager_id;
			$chat['employee_picture'] = $employee_picture;
			$data[] = $chat;
		}
		$data = array_sort_by_column($data,'sent_date');
		$this->db->query("UPDATE badge SET count=0 WHERE employeeid='".$input['employeeid']."'");
		return array('status' =>200,'message' =>'Inbox','data'=>$data,'method'=>'get_MessageInbox');
	}

	/*
	*  View Messages
	*/
	public function ht_view_Messages($input)
	{
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');		
		$receiver_company_id	 = getAnythingData("med_employees","employeeid",$input['receiver_id'],'company_id');
		if($company_id = $receiver_company_id){
			$timezone	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'timezone');
			// get hour diff between utc and timezone
			$diff	=	getHoursMin($timezone);
			
			$page		= $input['page'];
			$limit		= 100;
			$start		= ($page - 1)* $limit; 
			$totalrows 	= $this->db->query("SELECT message_id FROM med_message WHERE ((sender_id=".$input['receiver_id']." AND receiver_id=".$input['employeeid']." AND receiver_trash!=0 ) OR ( sender_id=".$input['employeeid']." AND receiver_id=".$input['receiver_id']." AND sender_trash!=0)) and isactive=1")->num_rows();
			$totalpages = ceil($totalrows/$limit);
			$previous	=  $totalpages > 1 ? "1" : "0"; 
			$next		=  $page < $totalpages ? "1" : "0";
			$data  = $this->db->query("SELECT message_id,sender_id,thread_id,message,CONVERT_TZ (sent_date, '+0:00','".$diff."') as sent_date, is_read FROM med_message WHERE ((sender_id=".$input['receiver_id']." AND receiver_id=".$input['employeeid']." AND receiver_trash!=0 ) OR ( sender_id=".$input['employeeid']." AND receiver_id=".$input['receiver_id']." AND sender_trash!=0)) and isactive=1 ORDER BY message_id desc limit $start, $limit")->result_array();
			$employee_picture	 = getAnythingData("med_employees","employeeid",$input['receiver_id'],'employee_picture');
			if($employee_picture){
				$employee_picture = base_url().$employee_picture;
			}
			## Receiver info
			$receiver_data = ['receiver_id'=>$input['receiver_id'],'employee_name'=>getAnythingData("med_employees","employeeid",$input['receiver_id'],'employee_name'),'employee_picture'=>$employee_picture];
			
			## Set Read
			$this->db->where(array('receiver_id'=>$input['employeeid'],'sender_id'=>$input['receiver_id']))->update('med_message',array('is_read'=>1));
			
			return array('status' =>200,'message' =>'Chat history.','receiver_data'=>$receiver_data,'data'=>$data,'method'=>'view_Messages','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'view_Messages');
		}
	}
	/*
	*  View Messages
	*/
	public function ht_refreshChat($input)
	{
		$timezone	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'timezone');
		
		// get hour diff between utc and timezone
		$diff	=	getHoursMin($timezone);	
		
		$data  = $this->db->query("SELECT message_id,sender_id,thread_id,message,CONVERT_TZ (sent_date, '+0:00','".$diff."') as sent_date, is_read FROM med_message WHERE ((sender_id=".$input['receiver_id']." AND receiver_id=".$input['employeeid']." and receiver_trash!=0) OR ( sender_id=".$input['employeeid']." AND receiver_id=".$input['receiver_id']." and sender_trash!=0)) and isactive=1 AND message_id >".$input['last_message_id']." ORDER BY message_id desc")->result_array();
		$employee_picture	 = getAnythingData("med_employees","employeeid",$input['receiver_id'],'employee_picture');
		if($employee_picture){
			$employee_picture = base_url().$employee_picture;
		}
		
		## Set Read
		$this->db->where(array('receiver_id'=>$input['employeeid'],'sender_id'=>$input['receiver_id']))->update('med_message',array('is_read'=>1));
		## Receiver info
		$receiver_data = ['receiver_id'=>$input['receiver_id'],'employee_name'=>getAnythingData("med_employees","employeeid",$input['receiver_id'],'employee_name'),'employee_picture'=>$employee_picture];
		return array('status' =>200,'message' =>'New messages.','receiver_data'=>$receiver_data,'data'=>$data,'method'=>'refreshChat');
	}
	/*
	* Send New Message
	*/
	public function ht_send_newMessage($input)
	{
		$company_id	 = getAnythingData("med_employees","employeeid",$input['sender_id'],'company_id');
		$receiver_company_id	 = getAnythingData("med_employees","employeeid",$input['receiver_id'],'company_id');
		if($company_id = $receiver_company_id && $input['sender_id']!=$input['receiver_id']){
			$input['thread_id'] = getThreadId($input['sender_id'],$input['receiver_id']);
			$input['sent_date'] = today()[0];
			$input['company_id'] = $company_id;
			$this->db->trans_start();
			$this->db->insert('med_message',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'send_newMessage');
			} else {
				$message_id = $this->db->insert_id();
				$this->db->trans_commit();
				
				#unread count of sender
				$unreadcount	=	$this->db->query("SELECT distinct(sender_id) from `med_message` where receiver_id = '".$input['receiver_id']."' and is_read = 0")->num_rows();

				#Send Push Notification
				/*$keys = array("message_id"=>$message_id,'thread_id'=>$input['thread_id'],'main_id'=>$input['sender_id'],'unread_sender'=>$unreadcount);
				$message = "A new message received";
				$badge = getBadge($input['receiver_id']);
				$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type,employeeid FROM `med_employees_authentication` WHERE `employeeid`='".$input['receiver_id']."'")->result_array();
				foreach($tokens as $token){
					if(strlen($token['device_token'])>30){
						if($token['device_type']==1){
							//applePush($token['device_token'],$message,$badge,2,$keys);
						}else{
							//andiPush($token['device_token'],$message,$badge,2,$keys);
						}
					}
				}*/
				return array('status' => 200,'thread_id'=>(string)$input['thread_id'],'message_id'=>(string)$message_id,'message' =>'Message sent.','method'=>'send_newMessage');
			}
		}else{
			return array('status' =>400,'message' => 'Unauthorized access!','method'=>'send_newMessage');
		}
	}
	/*
	* Clear Chat
	*/
	public function ht_clear_Chat($input)
	{
		$this->db->trans_start();
		$this->db->query("UPDATE med_message SET sender_trash=0 WHERE thread_id=".$input['thread_id']." AND sender_id=".$input['employeeid']."");
		$this->db->query("UPDATE med_message SET is_read=1,receiver_trash=0 WHERE thread_id=".$input['thread_id']." AND receiver_id=".$input['employeeid']."");
		$this->db->where(array('thread_id'=>$input['thread_id'],'sender_trash'=>0,'receiver_trash'=>0))->delete('med_message');
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return array('status' => 500,'message' =>'Internal server error.','method'=>'clear_Chat');
		} else {
			$this->db->trans_commit();
			return array('status' => 200,'message' =>'Chat cleared.','method'=>'clear_Chat');
		}
	}
	/*
	* Delete Single Message
	*/
	public function ht_delete_SingleMessage($input)
	{	
		$check = $this->db->select("sender_id,receiver_id")->from("med_message")->where(array('message_id'=>$input['message_id']))->get()->row();
		if($check){
			if($input['employeeid'] == $check->sender_id || $input['employeeid'] == $check->receiver_id){
				$this->db->trans_start();
				if($input['employeeid'] == $check->sender_id){
					$this->db->query("UPDATE med_message SET sender_trash=0 WHERE message_id=".$input['message_id']." AND sender_id=".$input['employeeid']."");
				}else{
					$this->db->query("UPDATE med_message SET is_read=1,receiver_trash=0 WHERE message_id=".$input['message_id']." AND receiver_id=".$input['employeeid']."");
				}
				$this->db->where(array('message_id'=>$input['message_id'],'sender_trash'=>0,'receiver_trash'=>0))->delete('med_message');
				
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'message' =>'Internal server error.','method'=>'delete_SingleMessage');
				} else {
					$this->db->trans_commit();
					return array('status' => 200,'message' =>'Message deleted.','method'=>'delete_SingleMessage');
				}
			}else{
				return array('status' =>400,'message' => 'Unauthorized access!','method'=>'delete_SingleMessage');
			}
		}else{
			return array('status' =>400,'message' => 'Message already deleted','method'=>'delete_SingleMessage');
		}
	}
	
	################################################
	############## - MANAGER SECTION - #############
	################################################
	
	/*
	*  Get All Employees 
	*/
	public function ht_get_allStaffMembers($input)
	{	
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$employee_type	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_type');
		if($employee_type == 2 || $employee_type == 3){ ## = 2 Admin
			$search = "";
			if ($input['search']){
				$search = " AND employee_name LIKE '%".$input['search']."%'";
			}
			$page		= $input['page'];
			$limit		= 30;
			$start		= ($page - 1)* $limit; 
			$totalrows 	= $this->db->query("SELECT employeeid FROM med_employees WHERE company_id='".$company_id."' and is_active=1 AND employeeid!=".$input['employeeid']." $search")->num_rows();
			$totalpages = ceil($totalrows/$limit);
			$previous	=  $totalpages > 1 ? "1" : "0"; 
			$next		=  $page < $totalpages ? "1" : "0";
			$data  = $this->db->query("SELECT employeeid, company_id, employee_type, employee_number, employee_name, employee_email, employee_country_code, employee_mobile, IF(employee_picture='','',CONCAT('".base_url()."','',employee_picture)) as employee_picture, employee_designation FROM med_employees WHERE company_id=".$company_id." and is_active=1 AND employeeid!=".$input['employeeid']." $search ORDER BY employee_name ASC limit $start, $limit")->result_array();
			for($i=0;$i<count($data);$i++){
				$data[$i]['designation'] = getAnythingData("med_designations","designation_id",$data[$i]['employee_designation'],'designation');
			}
			return array('status' =>200,'message' =>'All staff members list','data'=>$data,'method'=>'get_allStaffMembers','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'get_allStaffMembers');
		}
	}
	/*
	* Get Designations list
	*/
	public function ht_get_designationsList($input)
	{
	
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$data = $this->db->select('designation_id,designation')->from('med_designations')->where(array('company_id'=>$company_id))->order_by('designation','ASC')->get()->result_array();
		return array('status' =>200,'message' => 'Designations list','data'=>$data,'method'=>'get_designationsList');
	}
	/*
	* Get Country Code
	*/
	public function ht_get_CountryCode()
	{	
		$data = $this->db->select('countries_id,countries_name, country_code, countries_iso_code')->from('med_countries')->order_by('countries_name','ASC')->get()->result_array();
		for($i=0;$i<count($data);$i++){
			$data[$i]['flag'] = base_url().'asset/flags/'.strtolower($data[$i]['countries_iso_code']).'.png';
		}
		return array('status' => 200,'message' => 'Countries code list','method'=>'get_CountryCode','data'=>$data);
	}
		
	/*
	* Check Email Free
	*/
	public function ht_checkEmailIsFree($input)
	{
	
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$check = $this->db->select('employee_email')->from('med_employees')->where(array('employee_email'=>$input['employee_email']))->get()->num_rows();
		if($check){
			return array('status' =>200,'message' => '0','method'=>'checkEmailIsFree');
		}else{
			return array('status' => 200,'message' =>'1','method'=>'checkEmailIsFree');
		}
	}
	/*
	* Check Employee Number is Free
	*/
	public function ht_checkEmployeeNumberIsFree($input)
	{	
	
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$check = $this->db->select('employee_email')->from('med_employees')->where(array('employee_number'=>$input['employee_number'],'company_id'=>$company_id))->get()->num_rows();
		if($check){
			return array('status' =>200,'message' => '0','method'=>'checkEmployeeNumberIsFree');
		}else{
			return array('status' => 200,'message' =>'1','method'=>'checkEmployeeNumberIsFree');
		}
	}
	
		/*
	*  Add New Staff Member 
	*/
	public function ht_add_newStaffMember($input)
	{
		$input['company_id']	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		if(activePlanLimit($input['company_id'],1)){
			unset($input['employeeid']);
			$input['employee_type']	= $input['employee_designation'];
			$input['created_at']	= today()[0];
			$input['updated_at']	= today()[0];
			$input['employee_picture']	= '';
			$config['allowed_types'] 	= '*';
			$date 						= date("Y-m");
			
			if (!empty($_FILES['employee_picture']['name'])){
				$this->load->library('image_lib');
				$path	=  './media/profile/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_PP-".rand(1000,9999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('employee_picture')){
					return array('status' =>400,'msg' =>"Profile picture uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					## Resize image
					$uploaded_picture_data = $this->upload->data();
					////$config['image_library'] = 'gd2';
					//$config['source_image'] = $path.$uploaded_picture_data['file_name']; //get original image
					//$config['maintain_ratio'] = TRUE;
					//$config['create_thumb'] = false;
					//$config['height'] = 200;
					//$this->image_lib->initialize($config);
					//if (!$this->image_lib->resize()) {
					//	$pic = $path.$uploaded_picture_data['file_name'];
					//	if (file_exists($pic)) {
					//		unlink($pic);
					//	}
					//	return array('status' =>400,'method'=>'add_newStaffMember','message' =>"Picture resizing error: ".strip_tags($this->image_lib->display_errors()));
					//	exit;
					//}else{
					$input['employee_picture'] = ltrim($path,"./").$uploaded_picture_data['file_name'];
					//$this->image_lib->clear();
				}
			}
			$rand = rand(14567,99999);
			$rand1 = randomCode();
			$input['employee_pin']		= crypt($rand,APP_SALT);
			$input['employee_password']	= crypt($rand1,APP_SALT);
			$this->db->trans_start();
			$this->db->insert('med_employees',$input);
			$emp_id = $this->db->insert_id();
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'add_newStaffMember');
			}else{
				$this->db->trans_commit();
				$this->load->library('email');
				$config['protocol']    = 'smtp';
				$config['smtp_host']    = 'smtp.1and1.com';
				$config['smtp_port']    = '587';
				$config['smtp_timeout'] = '7';
				$config['smtp_user']    = 'noreply@dailycarerecords.com';
				$config['smtp_pass']    = 'Dailycarerecords!@#123';
				$config['charset']    = 'utf-8';
				$config['newline']    = "\r\n";
				$this->email->initialize($config);
				$this->email->from('noreply@dailycarerecords.com', 'DailyCareRecords Support Team');
				$this->email->to($input['employee_email']);
				$this->email->subject('Medication App login credential');
				$this->email->set_mailtype("html");
				$email_message = "";
				$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
				$email_message .= "<div>Hi, ".$input['employee_name']."</div>" ;
				$email_message .= "<div>Please find your Medication login credential listed below:</div>" ;
				$email_message .= "<br>" ;
				
				if($input['employee_type'] == 2 || $input['employee_type'] == 3){
					$email_message .= "<div>ADMIN PANEL</div>" ;
					$email_message .= "<div>Admin Panel Link : ".base_url()."admin</div>" ;
					$email_message .= "<div>Username : ".$input['employee_number']."</div>" ;
					$email_message .= "<div>Password : ".$rand1."</div>" ;
					$email_message .= "<br>" ;
				}
				
				$email_message .= "<div>APP</div>" ;
				$email_message .= "<div>Employee Code : ".$input['employee_number']."</div>" ;
				$email_message .= "<div>Password : ".$rand."</div>" ;
				$email_message .= "<div>Please use these credential for Medication app account. After successful login we recommend you to change your password.</div>" ;
				$email_message .= "<div>Thanks</div>" ;
				$this->email->message($email_message);
				$this->email->send();
				return array('status' => 200,'message' =>'Employee added successfully and password has sent to employee email','data'=>array('employeeid'=>(string)$emp_id,'employee_name'=>$input['employee_name']),'method'=>'add_newStaffMember');
			}
		}else{
			return array('status' => 400,'message' =>'Maximum users allocated! Please call support to increase allocation.','method'=>'add_newClient');
		}
	}
	
	/*
	* Get Staff member info
	*/
	public function ht_get_staffMemberInfo($input)
	{	
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$employee_type	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_type');
		$staff_company_id	 = getAnythingData("med_employees","employeeid",$input['staff_id'],'company_id');
		
		if ($company_id == $staff_company_id){
			$data = $this->db->select('employeeid, company_id, employee_type, employee_number, employee_name, employee_email, employee_country_code,(select countries_name from med_countries where country_code = employee_country_code) as country_name, employee_mobile, employee_picture, employee_designation, gender')->from('med_employees')->where(array('employeeid'=>$input['staff_id']))->get()->row();
			$data->employee_picture != '' ? $data->employee_picture = base_url().$data->employee_picture : '';
			$data->designation = getAnythingData("med_designations","designation_id",$data->employee_designation,'designation');
			
			return array('status' => 200,'message' =>'Staff member information.','method'=>'get_staffMemberInfo','data'=>$data);
		}else{
			return array('status' =>400,'message' => 'Unauthorized access!','method'=>'get_staffMemberInfo');
		}
	}
	/*
	*  Update staff member information 
	*/
	public function ht_update_staffMemberInfo($input)
	{
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$employee_type	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_type');
		$staff_company_id	 = getAnythingData("med_employees","employeeid",$input['staff_id'],'company_id');
		
		if ($company_id == $staff_company_id){
			unset($input['employeeid']);
			$input['employee_picture']	= getAnythingData("med_employees","employeeid",$input['staff_id'],'employee_picture');
			$config['allowed_types'] 	= '*';
			$date 						= date("Y-m");
			
			if (!empty($_FILES['employee_picture']['name'])){
				$path	=  './media/profile/'.$date."/";
				$this->load->library('image_lib');
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_PP-".rand(1000,9999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('employee_picture')){
					return array('status' =>400,'msg' =>"Profile picture uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					$input['employee_picture'] !="" ? unlink("./".$input['employee_picture']) : '';
					## Resize image
					$uploaded_picture_data = $this->upload->data();
					//$config['image_library'] = 'gd2';
					//$config['source_image'] = $path.$uploaded_picture_data['file_name']; //get original image
					//$config['maintain_ratio'] = TRUE;
					//$config['create_thumb'] = false;
					//$config['height'] = 200;
					//$this->image_lib->initialize($config);
					//if (!$this->image_lib->resize()) {
					//	$pic = $path.$uploaded_picture_data['file_name'];
					//	if (file_exists($pic)) {
					//		unlink($pic);
					//	}
					//	return array('status' =>400,'method'=>'update_staffMemberInfo','message' =>"Picture resizing error: ".strip_tags($this->image_lib->display_errors()));
					//	exit;
					//}else{
						$input['employee_picture'] = ltrim($path,"./").$uploaded_picture_data['file_name'];
					//$this->image_lib->clear();
				}
			}
			$rand = rand(11111111,99999999);
			$input['employee_password']		= crypt($rand,APP_SALT); 
			$this->db->trans_start();
			$this->db->where(array('employeeid'=>$input['staff_id']))->update('med_employees',array('employee_type'=>$input['employee_designation'],'employee_name'=>$input['employee_name'],'employee_picture'=>$input['employee_picture'],'employee_number'=>$input['employee_number'],'employee_email'=>$input['employee_email'],'employee_country_code'=>$input['employee_country_code'],'employee_mobile'=>$input['employee_mobile'],'employee_designation'=>$input['employee_designation'],'gender'=>$input['gender'],'updated_at'=>today()[0]));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'update_staffMemberInfo');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Information updated successfully','method'=>'update_staffMemberInfo');
			}
		}else{
			return array('status' =>400,'message' => 'Unauthorized access!','method'=>'update_staffMemberInfo');
		}
	}
	/*
	*  Get All Clients 
	*/
	public function ht_get_allClients($input)
	{	
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$employee_type	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_type');
		
		if($employee_type == 2 || $employee_type == 3){ ## = 2 Admin
			$search = "";
			if ($input['search']){
				$search = " AND (firstname LIKE '%".$input['search']."%' OR lastname LIKE '%".$input['search']."%' OR postcode LIKE '%".$input['search']."%')";
			}
			$notin = "";
			if($input['section']!=1){
				$clients = $this->db->query("SELECT GROUP_CONCAT(`client_id`) as `client_id` FROM `med_schedule` WHERE `company_id`='".$company_id."' AND DATE(`start_datetime`)>='".$input['section']."' AND DATE(end_datetime)<='".$input['section']."'")->row_array();
				if($clients['client_id']!=NULL){
					$notin = " AND client_id NOT IN(".$clients['client_id'].")";
				}
			}
			$page		= $input['page'];
			$limit		= 30;
			$start		= ($page - 1)* $limit; 
			$totalrows 	= $this->db->query("SELECT client_id FROM med_clients WHERE company_id=".$company_id." $notin $search ")->num_rows();
			$totalpages = ceil($totalrows/$limit);
			$previous	=  $totalpages > 1 ? "1" : "0"; 
			$next		=  $page < $totalpages ? "1" : "0";
			$data  = $this->db->query("SELECT * FROM med_clients WHERE company_id=".$company_id." $notin $search ORDER BY client_id DESC limit $start, $limit")->result_array();
			for($i=0;$i<count($data);$i++){
				$data[$i]['client_picture'] ? $data[$i]['client_picture'] = base_url().$data[$i]['client_picture'] : '';
			}
			return array('status' =>200,'message' =>'All clients list','data'=>$data,'method'=>'get_allClients','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'get_allClients');
		}
	}
	
	/*
	*  Add New Client
	*/
	public function ht_add_newClient($input)
	{	
		$input['company_id'] = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		if(activePlanLimit($input['company_id'],2)){
			$am_data 		= json_decode($input['am_time_care_program'],TRUE);
			$noon_data 		= json_decode($input['noon_time_care_program'],TRUE);
			$evening_data 	= json_decode($input['tea_time_care_program'],TRUE);
			$night_data 	= json_decode($input['night_time_care_program'],TRUE);
			unset($input['am_time_care_program']);
			unset($input['noon_time_care_program']);
			unset($input['tea_time_care_program']);
			unset($input['night_time_care_program']);
			unset($input['employeeid']);
			$input['created_at']	= today()[0];
			$input['updated_at']	= today()[0];
			$location = get_location($input['address']);
			$input['latitude']	= $location['lat'] ? $location['lat'] : 0;
			$input['longitude']	= $location['lng'] ? $location['lng'] : 0;
			$input['client_picture']	= '';
			$config['allowed_types'] 	= '*';
			$date 						= date("Y-m");
			if (!empty($_FILES['client_picture']['name'])){
				$this->load->library('image_lib');
				$path	=  './media/client/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_CPP-".rand(1000,9999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('client_picture')){
					return array('status'=>400,'method'=>'add_newClient','message'=>"Picture uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					## Resize image
					$uploaded_picture_data = $this->upload->data();
					//$config['image_library'] = 'gd2';
					//$config['source_image'] = $path.$uploaded_picture_data['file_name']; //get original image
					//$config['maintain_ratio'] = TRUE;
					//$config['create_thumb'] = false;
					//$config['height'] = 200;
					//$this->image_lib->initialize($config);
					//if (!$this->image_lib->resize()) {
					//	$pic = $path.$uploaded_picture_data['file_name'];
					//	if (file_exists($pic)) {
					//		unlink($pic);
					//	}
					//	return array('status' =>400,'method'=>'add_newClient','message' =>"Picture resizing error: ".strip_tags($this->image_lib->display_errors()));
					//	exit;
					//}else{
					$input['client_picture'] = ltrim($path,"./").$uploaded_picture_data['file_name'];
					//$this->image_lib->clear();
				}
			}
			$this->db->trans_start();
			$this->db->insert('med_clients',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'add_newClient');
			}else{
				$client_id = $this->db->insert_id();
				$this->db->insert('med_care_program',array('client_id'=>$client_id,'am_time_care_program'=>serialize($am_data),'noon_time_care_program'=>serialize($noon_data),'tea_time_care_program'=>serialize($evening_data),'night_time_care_program'=>serialize($night_data),'program_startdate'=>today()[0],'isactive'=>1));
				
				$this->db->trans_commit();
				
				return array('status' => 200,'message' =>'Profile created successfully.','method'=>'add_newClient');
			}
		}else{
			return array('status' => 400,'message' =>'Maximum users allocated! Please call support to increase allocation.','method'=>'add_newClient');
		}
	}
	
	/*
	*  View Client Profile
	*/
	public function ht_view_ClientProfile($input)
	{
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$client_company_id	 = getAnythingData("med_clients","client_id",$input['client_id'],'company_id');
		
		# check user type if carer
		
		$usertype = getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_type');
		if($usertype==1){
			$this->db->query("UPDATE `med_carer_notifications` set is_read = 1 WHERE `carer_id`='".$input['employeeid']."' and main_id = '".$input['client_id']."' and notification_type = 3");
		}
		
		if($company_id == $client_company_id){
			
			$data  = $this->db->query("SELECT * FROM med_clients WHERE client_id=".$input['client_id']."")->row_array();
			if(count($data)){
				$data['client_picture'] ? $data['client_picture'] = base_url().$data['client_picture'] : '';
				$poc  = $this->db->select("program_id, client_id, am_time_care_program, noon_time_care_program, tea_time_care_program, night_time_care_program, program_startdate")->from('med_care_program')->where(array("client_id"=>$input['client_id']))->get()->row_array();
				if(count($poc)){
					$data['am_time_care_program'] 	= unserialize($poc['am_time_care_program']);
					$data['noon_time_care_program']	= unserialize($poc['noon_time_care_program']);
					$data['tea_time_care_program']	= unserialize($poc['tea_time_care_program']);
					$data['night_time_care_program']= unserialize($poc['night_time_care_program']);
				}else{
					$data['am_time_care_program']	= [];
					$data['noon_time_care_program']	= [];
					$data['tea_time_care_program']	= [];
					$data['night_time_care_program']= [];
				}
			}
			return array('status' =>200,'message' =>'Client profile','data'=>$data,'method'=>'view_ClientProfile',);
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'view_ClientProfile');
		}
	}
	/*
	*  Update Client Profile
	*/
	public function ht_update_ClientProfile($input)
	{
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$client_company_id	 = getAnythingData("med_clients","client_id",$input['client_id'],'company_id');
		
		if($company_id == $client_company_id){
			$location = @get_location($input['address']);
			$input['latitude']	= $location['lat'] ? $location['lat'] : 0;
			$input['longitude']	= $location['lng'] ? $location['lng'] : 0;
			$input['client_picture']	= getAnythingData("med_clients","client_id",$input['client_id'],'client_picture');
			$config['allowed_types'] 	= '*';
			$date 						= date("Y-m");
			if (!empty($_FILES['client_picture']['name'])){
				$this->load->library('image_lib');
				$path	=  './media/client/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_CPP-".rand(1000,9999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('client_picture')){
					return array('status'=>400,'method'=>'update_ClientProfile','message'=>"Picture uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					$input['client_picture'] ? unlink('./'.$input['client_picture']) :'';
					## Resize image
					$uploaded_picture_data = $this->upload->data();
					//$config['image_library'] = 'gd2';
					//$config['source_image'] = $path.$uploaded_picture_data['file_name']; //get original image
					//$config['maintain_ratio'] = TRUE;
					//$config['create_thumb'] = false;
					//$config['height'] = 200;
					//$this->image_lib->initialize($config);
					//if (!$this->image_lib->resize()) {
					//	$pic = $path.$uploaded_picture_data['file_name'];
					//	if (file_exists($pic)) {
					//		unlink($pic);
					//	}
					//	return array('status' =>400,'method'=>'update_ClientProfile','message' =>"Picture resizing error: ".strip_tags($this->image_lib->display_errors()));
					//	exit;
					//}else{
					$input['client_picture'] = ltrim($path,"./").$uploaded_picture_data['file_name'];
					//$this->image_lib->clear();
				}
			}
			$this->db->trans_start();
			$this->db->where(array('client_id'=>$input['client_id']))->update('med_clients',array('postcode'=>$input['postcode'],'firstname'=>$input['firstname'],'lastname'=>$input['lastname'],'dob'=>$input['dob'],'address'=>$input['address'],'address'=>$input['address'],'emergency_contact'=>$input['emergency_contact'],'gender'=>$input['gender'],'client_picture'=>$input['client_picture'],'personal_information'=>@$input['personal_information'],'comments'=>@$input['comments'],'emergency_contact_name'=>@$input['emergency_contact_name'],'emergency_contact_relation'=>@$input['emergency_contact_relation'],'doctor_name'=>@$input['doctor_name'],'doctor_address'=>@$input['doctor_address'],'doctor_contact_number'=>@$input['doctor_contact_number'],'allergies'=>@$input['allergies'],'additional_information'=>@$input['additional_information'],'updated_at'=>today()[0],'email'=>@$input['email'],'phone_number'=>@$input['phone_number']));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'update_ClientProfile');
			}else{
				$this->db->trans_commit();
				
				#Get all carer
				$carer  = $this->db->query("SELECT employeeid FROM med_employees WHERE employee_type=1 AND company_id='".$company_id."'")->result_array();
				foreach($carer as $user){
					
					## Notification feed
					$this->db->insert('med_carer_notifications',array("notification_type"=>3, "authority_id"=>$input['employeeid'], "carer_id"=>$user['employeeid'],"table_id"=>$input['client_id'],"main_id"=>$input['client_id'],"created_at"=>today()[0]));
					
					#Send Push Notification
					$keys = array("client_id"=>$input['client_id']);
					$message = $input['firstname'].' '.$input['lastname']." updated his profile";
					$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$user['employeeid']."'")->result_array();
						foreach($tokens as $token){
							if(strlen($token['device_token'])>30){
							if($token['device_type']==1){
								applePush($token['device_token'],$message,1,4,$keys);
							}else{
								andiPush($token['device_token'],$message,1,4,$keys);
							}
						}
					}
				}
				return array('status' => 200,'message' =>'Profile updated successfully.','method'=>'update_ClientProfile');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'update_ClientProfile');
		}
	}
	
	/*
	*  Update Client Care program
	*/
	public function ht_update_ClientCareProgram($input)
	{
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$client_company_id	 = getAnythingData("med_clients","client_id",$input['client_id'],'company_id');
		$client_id	 	= getAnythingData("med_care_program","program_id",$input['program_id'],'client_id');
		if($company_id == $client_company_id && $client_id == $input['client_id']){
			$am_data 		= json_decode(@$input['am_time_care_program'],TRUE);
			$noon_data 		= json_decode(@$input['noon_time_care_program'],TRUE);
			$evening_data 	= json_decode(@$input['tea_time_care_program'],TRUE);
			$night_data 	= json_decode(@$input['night_time_care_program'],TRUE);
			$this->db->trans_start();
			$this->db->where(array('program_id'=>$input['program_id']))->update('med_care_program',array('am_time_care_program'=>serialize($am_data),'noon_time_care_program'=>serialize($noon_data),'tea_time_care_program'=>serialize($evening_data),'night_time_care_program'=>serialize($night_data),'program_updated'=>today()[0]));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'update_ClientCareProgram');
			}else{
				$this->db->trans_commit();
				
				#Get all carer
				$carer  = $this->db->query("SELECT employeeid FROM med_employees WHERE employee_type=1 AND company_id='".$company_id."'")->result_array();
				
				foreach($carer as $user){
					
					## Notification feed
					$this->db->insert('med_carer_notifications',array("notification_type"=>4, "authority_id"=>$input['employeeid'], "carer_id"=>$user['employeeid'],"table_id"=>$input['client_id'],"main_id"=>$input['program_id'],"created_at"=>today()[0]));
					
					#Send Push Notification
					$keys = array("client_id"=>$input['client_id']);
					$message = "Program of care updated";
					$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$user['employeeid']."'")->result_array();
					foreach($tokens as $token){
						if(strlen($token['device_token'])>30){
							if($token['device_type']==1){
								applePush($token['device_token'],$message,1,6,$keys);  #section 6 client program update
							}else{
								andiPush($token['device_token'],$message,1,6,$keys);
							}
						}
					}
				}
				return array('status' => 200,'message' =>'Program of care updated successfully.','method'=>'update_ClientCareProgram');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'update_ClientCareProgram');
		}
	}

	/*
	*  Get Manage Message Inbox
	*/
	public function ht_get_ManagerMessageInbox($input)
	{
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$timezone	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'timezone');
		// get hour diff between utc and timezone
		$diff	=	getHoursMin($timezone);
		$page		= $input['page'];
		$limit		= 30;
		$start		= ($page - 1)* $limit; 
		$totalrows 	= $this->db->query("select med_message.message_id from med_message JOIN (select max(sent_date) as sent_date,thread_id from med_message group by thread_id) as latest on med_message.sent_date=latest.sent_date and med_message.thread_id=latest.thread_id WHERE (( med_message.receiver_id=".$input['employeeid']." AND med_message.receiver_trash!=0) OR ( med_message.sender_id=".$input['employeeid']." AND med_message.sender_trash!=0))")->num_rows();
		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0"; 
		$next		=  $page < $totalpages ? "1" : "0";
		$data = $this->db->query("select med_message.message_id, med_message.thread_id, med_message.sender_id, med_message.receiver_id, med_message.message, CONVERT_TZ (med_message.sent_date, '+0:00','".$diff."') as `sent_date` from med_message JOIN (select max(sent_date) as sent_date,thread_id from med_message group by thread_id) as latest on med_message.sent_date=latest.sent_date and med_message.thread_id=latest.thread_id WHERE (( med_message.receiver_id=".$input['employeeid']." AND med_message.receiver_trash!=0) OR ( med_message.sender_id=".$input['employeeid']." AND med_message.sender_trash!=0)) ORDER BY med_message.sent_date desc LIMIT $start, $limit")->result_array();
		if(count($data)>0){
			for($i=0;$i<count($data);$i++){
				if ($data[$i]['sender_id']==$input['employeeid']){
					unset($data[$i]['sender_id']);
				}else{
					$data[$i]['receiver_id'] = $data[$i]['sender_id'];
					unset($data[$i]['sender_id']);
				}
				$employee_picture	 = getAnythingData("med_employees","employeeid",$data[$i]['receiver_id'],'employee_picture');
				if($employee_picture){
					$employee_picture = base_url().$employee_picture;
				}
				$data[$i]['employee_name'] = getAnythingData("med_employees","employeeid",$data[$i]['receiver_id'],'employee_name'); 
				$data[$i]['employee_picture'] = $employee_picture;
				$data[$i]['new_messages'] = $this->db->query("select count(message_id) as new_message from med_message where receiver_id=".$input['employeeid']." AND thread_id='".$data[$i]['thread_id']."' and is_read=0")->row_array()['new_message'];
				unset($data[$i]['thread_id']);
				unset($data[$i]['message_id']);
			}
		}
		
		//print_R($this->db->last_query()); exit;
		return array('status' =>200,'message' =>'Inbox','data'=>$data,'method'=>'get_ManagerMessageInbox','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}
	
	/*
	*  Date wise My schedule count - Employee
	*/
	public function ht_get_MyDayWiseScheduleCount($input)
	{	
		$company_id	=	getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$totaldays	=	date("t",strtotime($input['date']));
		$month		=	date("m",strtotime($input['date']));
		$year		=	date("Y",strtotime($input['date']));
		$data = array();
		for($d=1; $d<=$totaldays; $d++){
			$time=mktime(12, 0, 0, $month, $d, $year);
			$date=date('Y-m-d', $time);
			$count = $this->db->query("SELECT schedule_id FROM med_schedule WHERE company_id='".$company_id."' AND employeeid='".$input['employeeid']."' AND DATE(start_datetime)='".$date."'")->num_rows();
			$data[] = ['date'=>$date,'count'=>(string)$count];
		}
		return array('status' =>200,'message' =>'My Schedule count','data'=>$data,'method'=>'get_MyDayWiseScheduleCount');
	}
	/*
	*  Date wise schedule count - Employee
	*/
	public function ht_get_MyDayWiseSchedule($input)
	{
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$data = $this->db->query("SELECT med_schedule.*,CONCAT(med_clients.firstname,' ',med_clients.lastname) as client_name,IF(med_clients.client_picture='','',CONCAT('".base_url()."','',med_clients.client_picture)) as client_picture,med_clients.postcode as client_postcode,med_employees.employee_name,med_employees.employee_number,IF(med_employees.employee_picture='','',CONCAT('".base_url()."','',med_employees.employee_picture)) as employee_picture FROM med_schedule JOIN med_clients ON med_clients.client_id=med_schedule.client_id JOIN med_employees ON med_employees.employeeid=med_schedule.employeeid WHERE med_schedule.company_id='".$company_id."' AND  med_employees.employeeid='".$input['employeeid']."' AND  DATE(med_schedule.start_datetime)='".$input['date']."' ORDER BY med_schedule.start_datetime ASC")->result_array();
		return array('status' =>200,'message' =>'My Schedule data','data'=>$data,'method'=>'MyDayWiseSchedule');
	}
	
	/*
	*  Date wise schedule count - Manager
	*/
	public function ht_get_DayWiseScheduleCount($input)
	{
		$company_id	=	getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$totaldays	=	date("t",strtotime($input['date'])); 
		$month		=	date("m",strtotime($input['date']));
		$year		=	date("Y",strtotime($input['date']));
		$data = array();
		for($d=1; $d<=$totaldays; $d++){
			$time=mktime(12, 0, 0, $month, $d, $year);
			$date=date('Y-m-d', $time);
			$count = $this->db->query("SELECT schedule_id FROM med_schedule WHERE company_id='".$company_id."' AND DATE(start_datetime)='".$date."'")->num_rows();
			$data[] = ['date'=>$date,'count'=>(string)$count];
		}
		return array('status' =>200,'message' =>'Schedule count','data'=>$data,'method'=>'get_DayWiseScheduleCount');
	}
	/*
	*  Date wise schedule list - Manager
	*/
	public function ht_get_DayWiseSchedule($input)
	{
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$page		= $input['page'];
		$limit		= 30;
		$start		= ($page - 1)* $limit; 
		$totalrows 	= $this->db->query("SELECT schedule_id FROM med_schedule WHERE company_id='".$company_id."' AND DATE(start_datetime)='".$input['date']."'")->num_rows();
		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0";
		$next		=  $page < $totalpages ? "1" : "0";
		$data = $this->db->query("SELECT med_schedule.*,CONCAT(med_clients.firstname,' ',med_clients.lastname) as client_name,IF(med_clients.client_picture='','',CONCAT('".base_url()."','',med_clients.client_picture)) as client_picture,med_employees.employee_name,med_employees.employee_number,IF(med_employees.employee_picture='','',CONCAT('".base_url()."','',med_employees.employee_picture)) as employee_picture FROM med_schedule JOIN med_clients ON med_clients.client_id=med_schedule.client_id JOIN med_employees ON med_employees.employeeid=med_schedule.employeeid WHERE med_schedule.company_id='".$company_id."' AND DATE(med_schedule.start_datetime)='".$input['date']."' ORDER BY med_schedule.start_datetime ASC limit $start, $limit")->result_array();
		return array('status' =>200,'message' =>'Schedule data','data'=>$data,'method'=>'get_DayWiseSchedule','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}
	/*
	*  Date wise schedule of perticuler Carer - Manager Section
	*/
	public function ht_get_DayScheduleOfCarer($input)
	{	
		$company_id	 		= getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$carer_company_id	= getAnythingData("med_employees","employeeid",$input['carerid'],'company_id');
		if ($company_id==$carer_company_id){
			$data = $this->db->query("SELECT med_schedule.*,CONCAT(med_clients.firstname,' ',med_clients.lastname) as client_name,IF(med_clients.client_picture='','',CONCAT('".base_url()."','',med_clients.client_picture)) as client_picture,med_employees.employee_name,med_employees.employee_number,IF(med_employees.employee_picture='','',CONCAT('".base_url()."','',med_employees.employee_picture)) as employee_picture FROM med_schedule JOIN med_clients ON med_clients.client_id=med_schedule.client_id JOIN med_employees ON med_employees.employeeid=med_schedule.employeeid WHERE med_schedule.company_id='".$company_id."' AND  med_employees.employeeid='".$input['carerid']."' AND  DATE(med_schedule.start_datetime)='".$input['date']."' ORDER BY schedule_id DESC")->result_array();
			return array('status' =>200,'message' =>'Carer schedules','data'=>$data,'method'=>'get_DayScheduleOfCarer');
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'get_DayScheduleOfCarer');
		}
	}
	
	/*
	*  Create New Schedule
	*/
	public function ht_create_NewSchedule($input)
	{
		$input['company_id'] 	= getAnythingData("med_employees","employeeid",$input['secheduled_by'],'company_id');
		$input['created_at']	= today()[0];
		$input['updated_at']	= today()[0];
		$this->db->trans_start();
		$this->db->insert('med_schedule',$input);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return array('status' => 500,'message' =>'Internal server error.','method'=>'create_NewSchedule');
		}else{
			$schedule_id = $this->db->insert_id();
			$this->db->trans_commit();
			
			## Notification feed
			//$this->db->insert('med_carer_notifications',array("notification_type"=>1, "authority_id"=>$input['secheduled_by'], "carer_id"=>$input['employeeid'],"table_id"=>$schedule_id,"main_id"=>$input['client_id'],"created_at"=>today()[0]));

			#Send Push Notification
			// $keys = array("schedule_id"=>$schedule_id,'main_id'=>$input['start_datetime']);
			// $message = "A new schedule fixed";
			// $tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$input['employeeid']."'")->result_array();
			// foreach($tokens as $token){
			// 	if(strlen($token['device_token'])>30){
			// 		if($token['device_type']==1){
			// 			applePush($token['device_token'],$message,1,1,$keys);
			// 		}else{
			// 			andiPush($token['device_token'],$message,1,1,$keys);
			// 		}
			// 	}
			// }
			return array('status' => 200,'message' =>'Schedule created','method'=>'create_NewSchedule');
		}
	}
	/*
	*  Update Schedule
	*/
	public function ht_update_Schedule($input)
	{
		$company_id = getAnythingData("med_employees","employeeid",$input['secheduled_by'],'company_id');
		$check = $this->db->query("SELECT schedule_id FROM med_schedule WHERE company_id='".$company_id."' AND schedule_id='".$input['schedule_id']."'")->num_rows();
		if($check){
			$this->db->trans_start();
			$this->db->query("UPDATE med_schedule SET employeeid='".$input['employeeid']."',client_id='".$input['client_id']."',start_datetime='".$input['start_datetime']."',end_datetime='".$input['end_datetime']."' WHERE schedule_id='".$input['schedule_id']."'");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'update_Schedule');
			}else{
				$this->db->trans_commit();
				
				## Notification feed
				/*$this->db->insert('med_carer_notifications',array("notification_type"=>2, "authority_id"=>$input['secheduled_by'], "carer_id"=>$input['employeeid'],"table_id"=>$input['schedule_id'],"main_id"=>$input['client_id'],"created_at"=>today()[0]));

				#Send Push Notification
				$keys = array("schedule_id"=>(int)$input['schedule_id'],'main_id'=>$input['start_datetime']);
				$message = "Schedule fixed";
				$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$input['employeeid']."'")->result_array();
				foreach($tokens as $token){
					if(strlen($token['device_token'])>30){
						if($token['device_type']==1){
							applePush($token['device_token'],$message,1,3,$keys);
						}else{
							andiPush($token['device_token'],$message,1,3,$keys);
						}
					}
				}*/

				return array('status' => 200,'message' =>'Schedule updated.','method'=>'update_Schedule');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'update_Schedule');
		}
	}
	/*
	*  Delete Schedule
	*/
	public function ht_delete_Schedule($input)
	{	
		$company_id = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$check = $this->db->query("SELECT schedule_id FROM med_schedule WHERE company_id='".$company_id."' AND schedule_id='".$input['schedule_id']."'")->num_rows();
		if($check){
			$this->db->trans_start();
			$this->db->query("DELETE FROM `med_schedule` WHERE `schedule_id`='".$input['schedule_id']."'");
			$this->db->query("delete from med_carer_notifications where ( notification_type = 1 or notification_type = 2) and table_id = '".$input['schedule_id']."'");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'delete_Schedule');
			}else{
				$client_id = $this->db->insert_id();
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Schedule deleted.','method'=>'delete_Schedule');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'delete_Schedule');
		}
	}
	/*
	*  Get User By user type - Manager
	*/
	public function ht_get_UserByUserTypes($input)
	{	
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$page		= $input['page'];
		$limit		= 30;
		$start		= ($page - 1)* $limit; 
		if($input['user_type']){
			$totalrows 	= $this->db->query("SELECT employeeid FROM med_employees WHERE company_id='".$company_id."' and is_active=1 AND employeeid!=".$input['employeeid']." AND employee_type='".$input['user_type']."'")->num_rows();
			$totalpages = ceil($totalrows/$limit);
			$previous	=  $totalpages > 1 ? "1" : "0";
			$next		=  $page < $totalpages ? "1" : "0";

			$data  = $this->db->query("SELECT employeeid, company_id, employee_type, employee_number, employee_name, employee_email, employee_country_code, employee_mobile, employee_picture, employee_designation FROM med_employees WHERE company_id=".$company_id." and is_active=1 AND employeeid!=".$input['employeeid']." AND employee_type='".$input['user_type']."' ORDER BY employee_name ASC  limit $start, $limit")->result_array();
			for($i=0;$i<count($data);$i++){
				$data[$i]['employee_picture'] ? $data[$i]['employee_picture'] = base_url().$data[$i]['employee_picture'] : '';
				$data[$i]['designation'] = getAnythingData("med_designations","designation_id",$data[$i]['employee_designation'],'designation');
			}
		}else{
			$totalrows 	= $this->db->query("SELECT `outside_user_id` FROM `med_outside_user` WHERE `company_id`='".$company_id."'")->num_rows();
			$totalpages = ceil($totalrows/$limit);
			$previous	=  $totalpages > 1 ? "1" : "0"; 
			$next		=  $page < $totalpages ? "1" : "0";

			$data = $this->db->query("SELECT `outside_user_id`, `user_name`, `email`, `access_grants` FROM `med_outside_user` WHERE `company_id`='".$company_id."' ORDER BY user_name ASC")->result_array();
			for($i=0;$i<count($data);$i++){
				$data[$i]['accessable_users'] = [];
				if($data[$i]['access_grants']){
					$data[$i]['accessable_users'] = $this->db->query("SELECT employeeid,employee_number, employee_name, employee_email FROM med_employees WHERE employeeid IN(".$data[$i]['access_grants'].") ORDER BY employee_name ASC limit $start, $limit")->result_array();
				}
			}
		}	
		return array('status' =>200,'message' =>'List','data'=>$data,'method'=>'get_UserByUserTypes','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}

	/*
	*  Add out side user
	*/
	public function ht_add_OutSideUser($input)
	{	
		$input['company_id']	= getAnythingData("med_employees","employeeid",$input['added_by'],'company_id');
		$input['created_at']	= today()[0];
		$input['updated_at']	= today()[0];
		$input['access_grants'] = rtrim($input['access_grants'],',');
		$check = $this->db->query("SELECT `outside_user_id` FROM `med_outside_user` WHERE `email`='".$input['email']."' AND  `company_id`='".$input['company_id']."'")->num_rows();
		if($check){
			return array('status' => 400,'message' => 'User already exist with email id you have entered!','method'=>'create_NewSchedule');
		}else{
			$random				= randomCode();
			$input['password'] 	= crypt($random,APP_SALT);
			$this->db->trans_start();
			$this->db->insert('med_outside_user',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'create_NewSchedule');
			}else{
				$this->db->trans_commit();
				$this->load->library('email');
				$config['protocol']    = 'smtp';
				$config['smtp_host']    = 'smtp.1and1.com';
				$config['smtp_port']    = '587';
				$config['smtp_timeout'] = '7';
				$config['smtp_user']    = 'noreply@dailycarerecords.com';
				$config['smtp_pass']    = 'Dailycarerecords!@#123';
				$config['charset']    = 'utf-8';
				$config['newline']    = "\r\n";
				$this->email->initialize($config);
				$this->email->from('noreply@dailycarerecords.com', 'DailyCareRecords Support Team');
				$this->email->to($input['email']);
				$this->email->subject('Medication App temporary passowrd');
				$this->email->set_mailtype("html");
				$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
				$email_message .= "<div>Temporary login password: ".$random."</div>" ;
				$email_message .= "<div>Please use this passowrd for login to your Medication app spaciel access.</div>" ;
				$email_message .= "<div>Thanks</div>" ;
				$this->email->message($email_message);
				$this->email->send();
				return array('status' => 200,'message' =>'User created','method'=>'create_NewSchedule');
			}
		}
	}
	/*
	*  Update out side user
	*/
	public function ht_update_OutSideUser($input)
	{
		$input['company_id'] = getAnythingData("med_employees","employeeid",$input['added_by'],'company_id');
		$input['updated_at']	= today()[0];
		$check = $this->db->query("SELECT `outside_user_id` FROM `med_outside_user` WHERE `email`='".$input['email']."' AND  `company_id`='".$input['company_id']."' AND outside_user_id!='".$input['outside_user_id']."'")->num_rows();
		if($check){
			return array('status' => 400,'message' => 'User already exist with email id you have entered!','method'=>'update_OutSideUser');
		}else{
			$this->db->trans_start();
			$this->db->query("UPDATE `med_outside_user` SET `user_name`='".$input['user_name']."',`email`='".$input['email']."',`access_grants`='".$input['access_grants']."',`added_by`='".$input['added_by']."',`updated_at`=NOW() WHERE outside_user_id='".$input['outside_user_id']."'");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'update_OutSideUser');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Updated successfully','method'=>'update_OutSideUser');
			}
		}
	}
	/*
	*  Delete out side user
	*/
	public function ht_delete_outSideUser($input)
	{	
		$input['company_id'] = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$check = $this->db->query("SELECT `outside_user_id` FROM `med_outside_user` WHERE `company_id`='".$input['company_id']."' AND outside_user_id='".$input['outside_user_id']."'")->num_rows();
		if($check){
			$this->db->trans_start();
			$this->db->query("DELETE FROM `med_outside_user` WHERE `outside_user_id`='".$input['outside_user_id']."'");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'delete_outSideUser');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'User deleted','method'=>'delete_outSideUser');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'delete_outSideUser');
		}
	}

	/*
	*  Get Supervisor Role and services
	*/
	public function ht_get_SupervisorRoleAndServices($input)
	{	
		$companyid	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$s_companyid = getAnythingData("med_employees","employeeid",$input['supervisorid'],'company_id');
		
		if($companyid == $s_companyid){
			$data = $this->db->query("SELECT `employeeid`,`employee_number`,`employee_name`,`employee_picture`,`staff_to_supervise`,`access`,`service_on_off` FROM `med_employees` WHERE `employeeid`='".$input['supervisorid']."'")->row_array();
			if(!empty($data['staff_to_supervise'])){
				$data['staff_to_supervise'] = $this->db->query("SELECT `employeeid`,`employee_number`,`employee_name` FROM `med_employees` WHERE `employeeid` IN (".$data['staff_to_supervise'].")")->result_array();
			}else{
				$data = [];
			}
			return array('status' =>200,'message' =>'Data','data'=>$data,'method'=>'get_SupervisorRoleAndServices');

		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'get_SupervisorRoleAndServices');
		}
	}

	/*
	*  Get List of carer those are not Supervised by any AND already added by selected Supervisor
	*/
	public function ht_get_listOfCarerToSupervise($input)
	{	
		$companyid	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$s_companyid = getAnythingData("med_employees","employeeid",$input['supervisorid'],'company_id');
		
		if($companyid == $s_companyid){

			$check = $this->db->query("SELECT `staff_to_supervise` FROM `med_employees` WHERE `employeeid`='".$input['supervisorid']."'")->row_array();
			if(!empty($check['staff_to_supervise'])){
				$data = $this->db->query("SELECT `employeeid`,`employee_number`,`employee_name`,IF(employeeid >0, '1', '1') as `status` FROM `med_employees` WHERE `employeeid` IN (".$check['staff_to_supervise'].") ORDER BY employee_number ASC")->result_array();
			}else{
				$data = [];
			}

			$check =$this->db->query("SELECT GROUP_CONCAT(`staff_to_supervise`) as `employeeid` FROM `med_employees` WHERE `company_id`='".$companyid."' AND `staff_to_supervise`!='' AND employee_type='1'")->row_array();
			$notin = 0;
			if($check['employeeid']!=''){
				$notin = $check['employeeid'];
			}
			$data1 = $this->db->query("SELECT `employeeid`,`employee_number`,`employee_name`,IF(employeeid >0, '0', '0') as `status` FROM med_employees WHERE company_id='".$companyid."' AND employeeid NOT IN(".$notin.") AND employee_type='1' ORDER BY employee_name ASC")->result_array();
			return array('status' =>200,'message' =>'Carer list','data'=>array_merge($data,$data1),'method'=>'get_listOfCarerToSupervise ');
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'get_SupervisorRoleAndServices');
		}
	}

	/*
	*  Add/update Role
	*/
	public function ht_addUpdate_Role($input)
	{	
		$companyid = getAnythingData("med_employees","employeeid",$input['added_by'],'company_id');
		$emp_companyid = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
	
		if($companyid == $emp_companyid){
			$this->db->trans_start();
			$this->db->query("UPDATE `med_employees` SET `staff_to_supervise`='".$input['staff_to_supervise']."',`access`='".$input['access']."',`service_on_off`='1',`updated_at`=NOW() WHERE `employeeid`='".$input['employeeid']."'");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'addUpdate_Role');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Role added','method'=>'addUpdate_Role');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'addUpdate_Role');
		}
	}
	/*
	*  Delete Role
	*/
	public function ht_delete_Role($input)
	{	
		$companyid = getAnythingData("med_employees","employeeid",$input['added_by'],'company_id');
		$emp_companyid = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		
		if($companyid ==$emp_companyid){
			$this->db->trans_start();
			$this->db->query("UPDATE `med_employees` SET `role_name`='',`staff_to_supervise`='',`access`='',`service_on_off`='0',`updated_at`=NOW() WHERE `employeeid`='".$input['employeeid']."'");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'delete_Role');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Role deleted','method'=>'delete_Role');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'delete_Role');
		}
	}
	/*
	*  Start / Stop Role Service Access
	*/
	public function ht_startStopRoleServiceAccess($input)
	{	
		$companyid = getAnythingData("med_employees","employeeid",$input['added_by'],'company_id');
		$emp_companyid = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		
		if($companyid ==$emp_companyid){
			$service_on_off = getAnythingData("med_employees","employeeid",$input['employeeid'],'service_on_off');
			
			if($service_on_off==1){
				$service_on_off = 2;
				$msg = "Service access stoped.";
			}else{
				$service_on_off = 1;
				$msg = "Service access started.";
			}
			$this->db->trans_start();
			$this->db->query("UPDATE `med_employees` SET `service_on_off`='".$service_on_off."',`updated_at`=NOW() WHERE `employeeid`='".$input['employeeid']."'");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'startStopRoleServiceAccess');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'message' =>$msg,'service_on_off'=>(string)$service_on_off,'method'=>'starStopRoleServiceAccess');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'startStopRoleServiceAccess');
		}
	}

	/*
	*  Get Already Fixed Schedule dates of Carers/employees
	*/
	public function ht_getAlreadyFixedSchedules($input)
	{	
		$company_id	 = getAnythingData("med_employees","employeeid",$input['managerid'],'company_id');
		$carer_company_id	 = getAnythingData("med_employees","employeeid",$input['carerid'],'company_id');
		if ($company_id == $carer_company_id){
			$data = $this->db->query("SELECT `start_datetime`,`end_datetime` FROM `med_schedule` WHERE `employeeid`='".$input['carerid']."' AND  DATE(`start_datetime`)>='".date("Y-m-d")."'")->result_array();
			return array('status' =>200,'message' =>'Scheduled dates','data'=>$data,'method'=>'getAlreadyFixedSchedules');
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'getAlreadyFixedSchedules');
		}
	}
	/*
	*  Get List of Carers those schedules not fixed in periculer days
	*/
	public function ht_getListOfCarerAvailableForSchedule($input)
	{	$notin = "";
		$check = $this->db->query("SELECT GROUP_CONCAT(`employeeid`) as employeeid  FROM `med_schedule` WHERE ('".urldecode($input['start'])."' BETWEEN `start_datetime` AND `end_datetime`) Or ('".urldecode($input['end'])."'BETWEEN `start_datetime` AND `end_datetime`)")->row_array();
		if ($check['employeeid']!=null){
			$notin = " AND employeeid NOT IN(".$check['employeeid'].")";
		}
		$company_id	 = getAnythingData("med_employees","employeeid",$input['managerid'],'company_id');
		
		$data  = $this->db->query("SELECT employeeid, employee_number, employee_name FROM med_employees WHERE company_id=".$company_id." and is_active=1 AND employeeid!=".$input['managerid']." AND employee_type='1' $notin ORDER BY employee_name ASC")->result_array();
		
		return array('status' =>200,'message' =>'Carer List','data'=>$data,'method'=>'getListOfCarerAvailableForSchedule');
	}
	/*
	*  Update Schedule
	*/
	public function ht_updateScheduleStatus($input)
	{	
		$check = $this->db->query("SELECT `schedule_id`,`status`,start_datetime FROM med_schedule WHERE employeeid='".$input['employeeid']."' AND schedule_id='".$input['schedule_id']."'")->row_array();
		if(count($check)){
			$this->db->trans_start();
			if($input['status']==2){
				$status = 1;
				if ($check['status']){
					$status = 0;
				}
			}else{
				$status = $input['status'];
				if($check['status']==1 && $status==1){
					return array('status' => 200,'message' =>'Status already updated.','method'=>'updateScheduleStatus');
				}
			}
			$this->db->query("UPDATE med_schedule SET status='".$status."',updated_at='".today()[0]."' WHERE schedule_id='".$input['schedule_id']."'");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'updateScheduleStatus');
			}else{
				$this->db->trans_commit();
				if($status==1){
				## Activity History
					$this->db->insert("med_activities_history",array("employeeid"=>$input['employeeid'],"section"=>6,"activity"=>"You have updated schedule status","main_id"=>$input['schedule_id'],"activity_at"=>today()[0]));
				}else{
					$this->db->query("DELETE from med_activities_history WHERE employeeid= '".$input['employeeid']."' AND section=6 AND activity='You have updated schedule status' AND main_id='".$input['schedule_id']."'");
				}

				return array('status' => 200,'message' =>'Status updated.','method'=>'updateScheduleStatus');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'updateScheduleStatus');
		}
	}

	/*
	*  Get Timesheet records of employee
	*/
	public function ht_get_TimeSheetRecords($input)
	{
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$carer_company_id	 = getAnythingData("med_employees","employeeid",$input['carerid'],'company_id');

		if ($company_id== $carer_company_id){
			$page		= $input['page'];
			$limit		= 10;
			$start		= ($page - 1)* $limit; 
			$totalrows 	= $this->db->query("SELECT  DISTINCT(DATE(start_datetime)) FROM med_schedule WHERE employeeid='".$input['carerid']."'")->num_rows();
			$totalpages = ceil($totalrows/$limit);
			$previous	=  $totalpages > 1 ? "1" : "0"; 
			$next		=  $page < $totalpages ? "1" : "0";
			$data = $this->db->query("SELECT DISTINCT(DATE(start_datetime)) as `date`,(SELECT `employee_name` FROM `med_employees` WHERE `employeeid`='".$input['carerid']."') as employee_name,(SELECT `employee_number` FROM `med_employees` WHERE `employeeid`='".$input['carerid']."') as employee_number FROM med_schedule WHERE employeeid='".$input['carerid']."' ORDER BY start_datetime DESC limit $start, $limit")->result_array();
		
			for($i=0;$i<count($data);$i++){
				$date = $data[$i]['date'];
				$schedule = $this->db->query("SELECT med_schedule.schedule_id,  med_schedule.employeeid,  med_schedule.client_id,  med_schedule.start_datetime,  med_schedule.end_datetime,  med_schedule.secheduled_by, med_schedule.status,IFNULL(TIMESTAMPDIFF(MINUTE, med_schedule.`start_datetime`, med_schedule.`end_datetime`),0) as`minutes` ,CONCAT(med_clients.firstname,' ',med_clients.lastname) as client_name FROM med_schedule JOIN med_clients ON med_clients.client_id=med_schedule.client_id  WHERE med_schedule.employeeid='".$input['carerid']."' AND DATE(med_schedule.start_datetime)='".$date."' ORDER BY schedule_id ASC")->result_array();
				$totalmints = 0;
				$workedmints = 0;
				for($x=0;$x<count($schedule);$x++){
					$schedule[$x]['totalhrs'] = getHourAndMinutsDiff($schedule[$x]['start_datetime'],$schedule[$x]['end_datetime']);
					$totalmints = $totalmints+$schedule[$x]['minutes'];
					if($schedule[$x]['status']==1){
						$workedmints = $workedmints+$schedule[$x]['minutes'];
					}
				}
				$data[$i]['schedule'] = $schedule;
				$data[$i]['totalhours'] = date("H:i", mktime(0,$totalmints));
				$data[$i]['workedhours'] = date("H:i", mktime(0,$workedmints));
			}

			return array('status' =>200,'message' =>'Time sheet data','data'=>$data,'method'=>'get_TimeSheetRecords','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'get_TimeSheetRecords');
		}
	}

	/*
	*  Add new concerns 
	*/
	public function ht_add_NewConcern($input)
	{
		$input['company_id']	= getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$input['open_at']	= today()[0];
		$check = $this->db->query("SELECT `employeeid` FROM `med_employees` WHERE FIND_IN_SET(".$input['employeeid'].",`staff_to_supervise`)")->row_array();
		if(count($check)){
			$input['supervisor_id'] = $check['employeeid'];
		}else{
			$input['supervisor_id'] = '47'; // This is static for now
		}

		$this->db->trans_start();
		$this->db->insert('med_concerns',$input);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return array('status' => 500,'message' =>'Internal server error.','method'=>'add_NewConcern');
		}else{
			$concern_id = $this->db->insert_id();
			$this->db->trans_commit();
			
			/*if($input['concern_type'] == 0){
				## Notification feed
				$this->db->insert('med_notifications',array("notification_type"=>3, "authority_id"=>'0', "carer_id"=>$input['employeeid'],"table_id"=>$concern_id,"filter"=>1,"created_at"=>today()[0]));
				## get all managers
				$all_managers = $this->db->query("select employeeid from med_employees where company_id = '".$input['company_id']."' and ( employee_type = 2 or employee_type = 3 )")->result_array();
				if(count($all_managers)){
					for($i=0;$i<count($all_managers);$i++){
						
						$check_read = $this->db->query("select badge from med_authority_notification_read where authority_id = '".$all_managers[$i]['employeeid']."'")->row_array();
						$badge = 1;
						if(count($check_read)){
							$this->db->query("update med_authority_notification_read set badge = badge + 1 where authority_id = '".$all_managers[$i]['employeeid']."'");
							$badge = $check_read['badge']+1;
						}else{
							$this->db->query("insert into med_authority_notification_read (authority_id,badge) values ('".$all_managers[$i]['employeeid']."',1)");
						}
						
						#Send Push Notification
						$keys = array("concern_id"=>$concern_id);
						$message = "A new concern submitted";
					
						$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$all_managers[$i]['employeeid']."'")->result_array();
						foreach($tokens as $token){
							if(strlen($token['device_token'])>30){
								if($token['device_type']==1){
									applePush1($token['device_token'],$message,(int)$badge,3,$keys);
								}else{
									andiPush($token['device_token'],$message,(int)$badge,3,$keys);
								}
							}
						}
					}
				}
			}else{
				## Notification feed
				$this->db->insert('med_notifications',array("notification_type"=>3, "authority_id"=>'', "carer_id"=>$input['employeeid'],"table_id"=>$concern_id,"filter"=>1,"created_at"=>today()[0]));
				## get all managers
				$all_managers = $this->db->query("select employeeid from med_employees where company_id = '".$input['company_id']."' and ( employee_type = 2 or employee_type = 3 )")->result_array();
				
				if(count($all_managers)){
					for($i=0;$i<count($all_managers);$i++){
						
						$check_read = $this->db->query("select badge from med_authority_notification_read where authority_id = '".$all_managers[$i]['employeeid']."'")->row_array();
						$badge = 1;
						if(count($check_read)){
							$this->db->query("update med_authority_notification_read set badge = badge + 1 where authority_id = '".$all_managers[$i]['employeeid']."'");
							$badge = $check_read['badge']+1;
						}else{
							$this->db->query("insert into med_authority_notification_read (authority_id,badge) values ('".$all_managers[$i]['employeeid']."',1)");
						}
						
						#Send Push Notification
						$keys = array("concern_id"=>$concern_id);
						$message = "A new concern submitted";
						
						$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$all_managers[$i]['employeeid']."'")->result_array();
						foreach($tokens as $token){
							if(strlen($token['device_token'])>30){
								if($token['device_type']==1){
									applePush1($token['device_token'],$message,(int)$badge,3,$keys,1,1);
								}else{
									andiPush($token['device_token'],$message,(int)$badge,3,$keys,1);
								}
							}
						}
					}
				}
			}*/
			
			#Activity History
			$this->db->insert("med_activities_history",array("employeeid"=>$input['employeeid'],"section"=>3,"activity"=>"You have submitted new concern,","main_id"=>$concern_id,"activity_at"=>today()[0]));

			return array('status' => 200,'message' =>'Concern submitted successfully','method'=>'add_NewConcern');
		}
	}
	/*
	*  Update concerns
	*/
	public function ht_update_Concern($input)
	{
		$check = $this->db->query("SELECT `supervisor_id` FROM `med_concerns` WHERE `employeeid`='".$input['employeeid']."' AND `concern_id`='".$input['concern_id']."'")->row_array();
		if(count($check)){
			$this->db->trans_start();
			$this->db->query("UPDATE med_concerns SET `subject`='".$input['subject']."',`concern`='".$input['concern']."',`open_at`='".today()[0]."' WHERE concern_id='".$input['concern_id']."'");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'update_Concern');
			}else{
				$this->db->trans_commit();

				/*if($input['concern_type'] == 0){## get all managers
					## Notification feed
					$this->db->insert('med_notifications',array("notification_type"=>3, "authority_id"=>'', "carer_id"=>$input['employeeid'],"table_id"=>$concern_id,"filter"=>1,"created_at"=>today()[0]));
					$all_managers = $this->db->query("select employeeid from med_employees where company_id = '".$input['company_id']."' and ( employee_type = 2 or employee_type = 3 )")->result_array();
				
					if(count($all_managers)){
						for($i=0;$i<count($all_managers);$i++){
						
							$check_read = $this->db->query("select badge from med_authority_notification_read where authority_id = '".$all_managers[$i]['employeeid']."'")->row_array();
							$badge = 1;
							if(count($check_read)){
								$this->db->query("update med_authority_notification_read set badge = badge + 1 where authority_id = '".$all_managers[$i]['employeeid']."'");
								$badge = $check_read['badge']+1;
							}else{
								$this->db->query("insert into med_authority_notification_read (authority_id,badge) values ('".$all_managers[$i]['employeeid']."',1)");
							}
							
							#Send Push Notification
							$keys = array("concern_id"=>$concern_id);
							$message = "A new concern submitted";
							
							$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$all_managers[$i]['employeeid']."'")->result_array();
							foreach($tokens as $token){
								if(strlen($token['device_token'])>30){
									if($token['device_type']==1){
										//applePush1($token['device_token'],$message,(int)$badge,3,$keys);
									}else{
										//andiPush($token['device_token'],$message,(int)$badge,3,$keys);
									}
								}
							}
						}
					}					
				}else{
					## Notification feed
					$this->db->insert('med_notifications',array("notification_type"=>3, "authority_id"=>$all_managers[$i]['employeeid'], "carer_id"=>$input['employeeid'],"table_id"=>$concern_id,"filter"=>1,"created_at"=>today()[0]));
					## get all managers
					$all_managers = $this->db->query("select employeeid from med_employees where company_id = '".$input['company_id']."' and ( employee_type = 2 or employee_type = 3 )")->result_array();
					
					if(count($all_managers)){
						for($i=0;$i<count($all_managers);$i++){
						
							$check_read = $this->db->query("select badge from med_authority_notification_read where authority_id = '".$all_managers[$i]['employeeid']."'")->row_array();
							$badge = 1;
							if(count($check_read)){
								$this->db->query("update med_authority_notification_read set badge = badge + 1 where authority_id = '".$all_managers[$i]['employeeid']."'");
								$badge = $check_read['badge']+1;
							}else{
								$this->db->query("insert into med_authority_notification_read (authority_id,badge) values ('".$all_managers[$i]['employeeid']."',1)");
							}
							
							#Send Push Notification
							$keys = array("concern_id"=>$concern_id);
							$message = "A new concern submitted";
							
							$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$all_managers[$i]['employeeid']."'")->result_array();
							foreach($tokens as $token){
								if(strlen($token['device_token'])>30){
									if($token['device_type']==1){
										//applePush1($token['device_token'],$message,(int)$badge,3,$keys,1,1);
									}else{
										//andiPush($token['device_token'],$message,(int)$badge,3,$keys,1);
									}
								}
							}
						}
					}
				}*/
				
				#Activity History
				$this->db->insert("med_activities_history",array("employeeid"=>$input['employeeid'],"section"=>3,"activity"=>"You have updated a concern,","main_id"=>$input['concern_id'],"activity_at"=>today()[0]));
				return array('status' => 200,'message' =>'Concern updated.','method'=>'update_Concern');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'update_Concern');
		}
	}

	/*
	*  Update concerns
	*/
	public function ht_udate_statusOrReplyConcern($input)
	{
		$company_id	= getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$check = $this->db->query("SELECT `company_id`,`employeeid` FROM `med_concerns` WHERE `concern_id`='".$input['concern_id']."'")->row_array();
		if($company_id == $check ['company_id']){
			
			$this->db->trans_start();
			if($input['status']>0){
				$this->db->query("UPDATE med_concerns SET `closed_by`='".$input['employeeid']."',`closed_at`='".today()[0]."',status=1 WHERE concern_id='".$input['concern_id']."'");
				$this->db->query("UPDATE med_notifications SET `status`='0',filter=2 WHERE table_id='".$input['concern_id']."' AND carer_id='".$check['employeeid']."' AND notification_type='3'");
			}
			if(!empty($input['reply'])){
				$this->db->insert('med_concern_replies',array("concern_id"=>$input['concern_id'],"employeeid"=>$input['employeeid'],"reply"=>$input['reply'],"reply_at"=>today()[0]));
			}

			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'udate_statusOrReplyConcern');
			}else{
				$this->db->trans_commit();
				
				
				if ($input['employeeid']!=$check['employeeid']){
					$this->db->insert('med_carer_notifications',array("notification_type"=>5, "authority_id"=>$input['employeeid'], "carer_id"=>$check['employeeid'],"table_id"=>$input['concern_id'],"main_id"=>$input['concern_id'],"created_at"=>today()[0]));
				}

				#Send Push Notification
				/*if ($input['employeeid']!=$check['employeeid']){
					$keys = array("main_id"=>$input['concern_id']);
					if($input['status']>0){
						$message = "Your concern resolved";
					}else{
						$message = "New reply received on your concern";
					}
					$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$check['employeeid']."'")->result_array();
					foreach($tokens as $token){
						if(strlen($token['device_token'])>30){
							if($token['device_type']==1){
								//applePush($token['device_token'],$message,1,3,$keys);
							}else{
								//andiPush($token['device_token'],$message,1,3,$keys);
							}
						}
					}
				}*/
				#Activity History
				if ($input['employeeid']==$check['employeeid']){
					if($input['status']>0){
						$msg = "You updated your concern to resolved";
					}else{
						$msg = "You repled to your concern";
					}
					
					$this->db->insert("med_activities_history",array("employeeid"=>$input['employeeid'],"section"=>3,"activity"=>$msg,"main_id"=>$input['concern_id'],"activity_at"=>today()[0]));
				}
				return array('status' => 200,'message' =>'Updated successfully.','method'=>'udate_statusOrReplyConcern');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'udate_statusOrReplyConcern');
		}
	}

	/*
	*  Get My all concerns Carer
	*/
	public function ht_get_myAllConcerns($input)
	{
	
		$page		= $input['page'];
		$limit		= 10;
		$start		= ($page - 1)* $limit; 
		$totalrows 	= $this->db->query("SELECT `concern_id` FROM `med_concerns` WHERE `employeeid`='".$input['carerid']."'")->num_rows();
		$totalpages = ceil($totalrows/$limit);
		$previous	= $totalpages > 1 ? "1" : "0"; 
		$next		= $page < $totalpages ? "1" : "0";
		$data 		= $this->db->query("SELECT med_concerns.`concern_id`, med_concerns.`employeeid`,med_concerns.`supervisor_id`,med_concerns.`subject`, med_concerns.`concern`,med_concerns.`closed_by`,med_concerns.`open_at`,med_concerns.`closed_at`, med_concerns.`status`,med_employees.employee_name,med_employees.employee_number,med_employees.employee_picture FROM  `med_concerns` JOIN med_employees ON med_employees.employeeid=med_concerns.supervisor_id WHERE med_concerns.`employeeid`='".$input['carerid']."' ORDER BY med_concerns.`status` ASC, med_concerns.`concern_id` DESC LIMIT $start,$limit")->result_array();
	
		for($i=0;$i<count($data);$i++){
			$data[$i]['closed_by'] = getAnythingData("med_employees","employeeid",$data[$i]['closed_by'],'employee_name');
			$data[$i]['employee_picture'] ? $data[$i]['employee_picture'] = base_url().$data[$i]['employee_picture']: "";
		}
		return array('status' =>200,'message' =>'Concerns','data'=>$data,'method'=>'get_myAllConcerns','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);		
	}

	/*
	*  View Concern Detail with replies
	*/
	public function ht_viewConcernDetailWithReplies($input)
	{	
		$company_id	= getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$data 		= $this->db->query("SELECT med_concerns.`concern_id`, med_concerns.company_id, med_concerns.`employeeid`,med_concerns.`supervisor_id`,med_concerns.`subject`, med_concerns.`concern`,med_concerns.`closed_by`,med_concerns.`open_at`,med_concerns.`closed_at`, med_concerns.`status` FROM  `med_concerns` WHERE med_concerns.`concern_id`='".$input['concern_id']."'")->row_array();
		if($company_id==$data['company_id']){

			$page		= $input['page'];
			$limit		= 20;
			$start		= ($page - 1)* $limit; 
			$totalrows 	= $this->db->query("SELECT `reply_id` FROM `med_concern_replies` WHERE `concern_id`='".$input['concern_id']."'")->num_rows();
			$totalpages = ceil($totalrows/$limit);
			$previous	= $totalpages > 1 ? "1" : "0"; 
			$next		= $page < $totalpages ? "1" : "0";
			$replies 	= $this->db->query("SELECT med_concern_replies.`reply_id`, med_concern_replies.`concern_id`, med_concern_replies.`employeeid`, med_concern_replies.`reply`, med_concern_replies.`reply_at`,med_employees.employee_name,med_employees.employee_number,med_employees.employee_picture FROM `med_concern_replies` JOIN med_employees ON med_concern_replies.`employeeid`=med_employees.`employeeid` WHERE med_concern_replies.`concern_id`='".$input['concern_id']."' ORDER BY med_concern_replies.`reply_id` DESC LIMIT $start,$limit")->result_array();
			for($i=0;$i<count($replies);$i++){
				$replies[$i]['employee_picture'] ? $replies[$i]['employee_picture'] = base_url().$replies[$i]['employee_picture']: "";
			}
			$data['replies'] = $replies;
			return array('status' =>200,'message' =>'Replies','data'=>$data,'method'=>'viewConcernDetailWithReplies','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);

		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'viewConcernDetailWithReplies');
		}
		
	}

	/*
	*  Get Supervisor Notificatons
	*/
	public function ht_get_SupervisorNotifications($input)
	{	

		$filter = ["","and filter=1","and filter=2","and filter=3","and filter=4","and filter=5"];

		$page		= $input['page'];
		$limit		= 20;
		$start		= ($page - 1)* $limit; 
		$totalrows 	= $this->db->query("SELECT `notifcation_id` FROM `med_notifications` WHERE `authority_id`='".$input['employeeid']."' ".$filter[$input['filter']]."")->num_rows();
		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0"; 
		$next		=  $page < $totalpages ? "1" : "0";
		$data = $this->db->query("SELECT `notifcation_id`, `notification_type`, `authority_id`, `carer_id`, `table_id`, `status`, `forworded_by`, `created_at` FROM `med_notifications` WHERE `authority_id`='".$input['employeeid']."' ".$filter[$input['filter']]." ORDER BY notifcation_id desc limit $start,$limit")->result_array();
		
		for($i=0;$i<count($data);$i++){
			if ($data[$i]['notification_type']==1){
				$data[$i]['notification']=[];
			}elseif($data[$i]['notification_type']==2){
				$data[$i]['notification']=[];
			}elseif($data[$i]['notification_type']==3){
				$concern_id = $data[$i]['table_id'];

				$data[$i]['notification'] = $this->db->query("SELECT med_concerns.`concern_id`, med_concerns.`employeeid`,med_concerns.`subject`, med_concerns.`concern`,med_concerns.`closed_by`,med_concerns.`open_at`,med_concerns.`closed_at`, med_concerns.`status`,med_employees.employee_name,med_employees.employee_number FROM  `med_concerns` JOIN med_employees ON med_employees.employeeid=med_concerns.employeeid WHERE med_concerns.`concern_id`='".$concern_id."'")->row_array();
				$data[$i]['notification']['closed_by'] = getAnythingData("med_employees","employeeid",$data[$i]['notification']['closed_by'],'employee_number');
			}
		}
		
		return array('status' =>200,'message' =>'Notfications List','data'=>$data,'method'=>'get_SupervisorNotifications','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}
	
	
	/**
	*
	* get unread notification count
	*
	*/

	public function ht_unread_notification_count($input)
	{
		$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$usertype	= getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_type');
		if($usertype==1){
			$count= $this->db->query("SELECT `carer_notification_id` FROM `med_carer_notifications` WHERE `carer_id`='".$input['employeeid']."' and is_read = 0")->num_rows();
		}else{			
			$count = $this->db->query("SELECT `badge` FROM `med_authority_notification_read` WHERE `authority_id` ='".$input['employeeid']."'")->row_array()['badge'];
			if($count == ''){
				$count = 0;
			}
		}
		$data[] = ['count'=>(string)$count];
		return array('status' =>200,'message' =>'Unread notification count','data'=>$data,'method'=>'unread_notification_count');
	}
	
	/**
	*
	* get unread message count
	*
	*/
	
	public function ht_unread_message_count($input)
	{
		$unreadcount	=	$this->db->query("SELECT count((message_id)) as count from `med_message` where receiver_id = '".$input['employeeid']."' and is_read = 0")->row_array()['count'];
		$data[] = ['count'=>(string)$unreadcount];
		return array('status' =>200,'message' =>'Unread message count','data'=>$data,'method'=>'unread_message_count');
	}
	
	
	/*
	*  Get Manager Notificatons
	*/
	public function ht_get_ManagerNotifications($input)
	{
		$this->db->query("update med_authority_notification_read set badge = 0 where authority_id = '".$input['employeeid']."'");
		
		$employee_type = getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_type');
		$section_roles = getAnythingData("med_employees","employeeid",$input['employeeid'],'section_roles');
		
		if ($input['section']==1){
			
			$timezone	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'timezone');
			$company_id	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
			// get hour diff between utc and timezone
			$diff	=	getHoursMin($timezone);
			$filter = ["","filter=1","filter=2","filter=3","filter=4","filter=5"];
			
			if($input['filter']==3){
				$fil	=	"(filter = 1 or filter = 2)";
			}else{
				if($input['filter'] == 4 || $input['filter'] == 5){
					if($employee_type == 2){
						$fil	=	$filter[$input['filter']];
					}else{
						$role_arr		=	explode(',',$section_roles);
						if(in_array($input['filter'],$role_arr)){
							$fil	=	$filter[$input['filter']];
						}else{
							return array('status' => 400,'message' => 'Unauthorized access!','method'=>'archive_User');
						}
					}
				}else{
					$fil	=	$filter[$input['filter']];
				}
			}
			
			$page		= $input['page'];
			$limit		= 20;
			$start		= ($page - 1)* $limit;
			$totalrows 	= $this->db->query("SELECT `notifcation_id` FROM `med_notifications` WHERE ".$fil."")->num_rows();
			$totalpages = ceil($totalrows/$limit);
			$previous	=  $totalpages > 1 ? "1" : "0";
			$next		=  $page < $totalpages ? "1" : "0";
			$data = $this->db->query("SELECT `notifcation_id`, `notification_type`, `authority_id`, `carer_id`, `table_id`, `status`, `forworded_by`, `created_at` FROM `med_notifications` where ".$fil." ORDER BY notifcation_id desc limit $start,$limit")->result_array();
			
			for($i=0;$i<count($data);$i++){
				if ($data[$i]['notification_type']==1){
					$schedule_id = $data[$i]['table_id'];
					$data[$i]['notification']=$this->db->query("SELECT a.`schedule_id`, a.`employeeid`, (select employee_name from med_employees where employeeid = a.employeeid ) as employee_name,(select employee_number from med_employees where employeeid = a.employeeid ) as employee_number, (select CONCAT(firstname,' ',lastname) from med_clients where client_id = a.client_id ) as client_name,a.`client_id`,CONVERT_TZ (a.`start_datetime`, '+0:00','".$diff."') as `start_datetime`,CONVERT_TZ (a.`end_datetime`, '+0:00','".$diff."') as `end_datetime`,CONVERT_TZ (a.`created_at`, '+0:00','".$diff."') as `created_at` FROM  `med_schedule` as a WHERE a.schedule_id = '".$schedule_id."'")->row_array();
				}elseif($data[$i]['notification_type']==2){
					$schedule_id = $data[$i]['table_id'];
					$data[$i]['notification']=$this->db->query("SELECT a.`schedule_id`, a.`employeeid`, (select employee_name from med_employees where employeeid = a.employeeid ) as employee_name,(select employee_number from med_employees where employeeid = a.employeeid ) as employee_number, (select CONCAT(firstname,' ',lastname) from med_clients where client_id = a.client_id ) as client_name,a.`client_id`,CONVERT_TZ (a.`start_datetime`, '+0:00','".$diff."') as `start_datetime`,CONVERT_TZ (a.`end_datetime`, '+0:00','".$diff."') as `end_datetime`,CONVERT_TZ (a.`created_at`, '+0:00','".$diff."') as `created_at` FROM  `med_schedule` as a WHERE a.schedule_id = '".$schedule_id."'")->row_array();
				}elseif($data[$i]['notification_type']==3){
					$concern_id = $data[$i]['table_id'];
					$data[$i]['notification'] = $this->db->query("SELECT med_concerns.`concern_id`, med_concerns.`employeeid`,med_concerns.`subject`, med_concerns.`concern`,med_concerns.`closed_by`,med_concerns.`open_at`,med_concerns.`closed_at`, IFNULL((select reply from med_concern_replies where concern_id = med_concerns.`concern_id` order by reply_id desc limit 1),0) as message,med_concerns.`status`,med_employees.employee_name,med_employees.employee_number FROM  `med_concerns` JOIN med_employees ON med_employees.employeeid=med_concerns.employeeid  WHERE med_concerns.`concern_id`='".$concern_id."' order by med_concerns.`closed_at` desc")->row_array();
					$data[$i]['notification']['closed_by_emp_number'] = getAnythingData("med_employees","employeeid",$data[$i]['notification']['closed_by'],'employee_number');
					$data[$i]['notification']['closed_by_id'] = $data[$i]['notification']['closed_by'];
					$data[$i]['notification']['closed_by'] = getAnythingData("med_employees","employeeid",$data[$i]['notification']['closed_by'],'employee_name');
				}
			}
		}else{
			$data = $this->db->query("SELECT med_employees.`employeeid`,med_employees.employee_name,med_employees.employee_number,med_employees.employee_picture,(SELECT count(`notifcation_id`) FROM `med_notifications` WHERE med_notifications.`authority_id`=med_employees.`employeeid` AND med_notifications.status=1 ) AS notification_count FROM `med_employees` JOIN med_notifications ON med_notifications.`authority_id` = med_employees.`employeeid` WHERE  med_employees.`employee_type`=3")->result_array();
			for($i=0;$i<count($data);$i++){
				$data[$i]['employee_picture'] ? $data[$i]['employee_picture'] = base_url().$data[$i]['employee_picture'] : ""; 
			}
		}
		return array('status' =>200,'message' =>'Notfications List','data'=>$data,'method'=>'get_ManagerNotifications','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}

	/*
	*  View Suervisor Notification By manager
	*/
	public function ht_view_SupervisorNotifications($input)
	{

		$filter = ["","and filter=1","and filter=2","and filter=3","and filter=4","and filter=5"];

		$page		= $input['page'];
		$limit		= 20;
		$start		= ($page - 1)* $limit;
		$totalrows 	= $this->db->query("SELECT `notifcation_id` FROM `med_notifications` WHERE `authority_id`='".$input['supervisorid']."' ".$filter[$input['filter']]."")->num_rows();
		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0"; 
		$next		=  $page < $totalpages ? "1" : "0";
		$data = $this->db->query("SELECT `notifcation_id`, `notification_type`, `authority_id`, `carer_id`, `table_id`, `status`, `forworded_by`, `created_at` FROM `med_notifications` WHERE `authority_id`='".$input['supervisorid']."' ".$filter[$input['filter']]." ORDER BY notifcation_id desc limit $start,$limit")->result_array();
		
		for($i=0;$i<count($data);$i++){
			if ($data[$i]['notification_type']==1){
				$data[$i]['notification']=[];
			}elseif($data[$i]['notification_type']==2){
				$data[$i]['notification']=[];
			}elseif($data[$i]['notification_type']==3){
				$concern_id = $data[$i]['table_id'];
				$data[$i]['notification'] = $this->db->query("SELECT med_concerns.`concern_id`, med_concerns.`employeeid`,med_concerns.`subject`, med_concerns.`concern`,med_concerns.`closed_by`,med_concerns.`open_at`,med_concerns.`closed_at`, med_concerns.`status`,med_employees.employee_name,med_employees.employee_number FROM  `med_concerns` JOIN med_employees ON med_employees.employeeid=med_concerns.employeeid WHERE med_concerns.`concern_id`='".$concern_id."'")->row_array();
				$data[$i]['notification']['closed_by'] = $data[$i]['notification']['closed_by'];
				$data[$i]['notification']['closed_by'] = getAnythingData("med_employees","employeeid",$data[$i]['notification']['closed_by'],'employee_number');
			}
		}
		
		return array('status' =>200,'message' =>'Notfications List','data'=>$data,'method'=>'view_SupervisorNotifications','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}

	/*
	*  Get Activity History of carer
	*/
	public function ht_get_activityHistoryOfCarer($input)
	{
		$timezone	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'timezone');
		// get hour diff between utc and timezone
		$diff	=	getHoursMin($timezone);
		$page		= $input['page'];
		$limit		= 30;
		$start		= ($page - 1)* $limit; 
		$totalrows 	= $this->db->query("SELECT `history_id` FROM `med_activities_history` WHERE `employeeid`='".$input['employeeid']."'")->num_rows();
		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0"; 
		$next		=  $page < $totalpages ? "1" : "0";
		$data = $this->db->query("SELECT `history_id`, `employeeid`, `section`, `activity`, `main_id`,(select CONCAT(firstname,' ',lastname) from med_clients where client_id = a.main_id) as name ,(select postcode from med_clients where client_id = a.main_id) as postcode ,`inserted_id`,CONVERT_TZ (activity_at, '+0:00','".$diff."') as activity_at FROM `med_activities_history` as a WHERE employeeid='".$input['employeeid']."' ORDER BY history_id DESC limit $start,$limit")->result_array();
		return array('status' =>200,'message' =>'Activity history','data'=>$data,'method'=>'get_activityHistoryOfCarer','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}

	
	/*
	*  Get staff member wise data usage
	*/

	public function ht_get_dataUsagesByStaffMember($input)
	{	
		$company_id	= getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$page		= $input['page'];
		$limit		= 30;
		$start		= ($page - 1)* $limit; 
		if($input['staffids']=='all'){
			$condition = " AND `employee_type`='1'";
		}else{
			$condition =   " AND `employeeid` IN(".$input['staffids'].")";
		}
		$totalrows 	= $this->db->query("SELECT `employeeid` FROM `med_employees` WHERE `company_id`='".$company_id."' $condition ")->num_rows();
		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0";
		$next		=  $page < $totalpages ? "1" : "0";
		//$data = $this->db->query("SELECT `employeeid`,employee_number,employee_name,IF(employee_picture='','',CONCAT('".base_url()."','',employee_picture)) as employee_picture,IFNULL((SELECT SUM(`data`) FROM `med_data_usage` WHERE `employeeid`=a.employeeid AND date(`date`)='".$input['date']."'),0) as data FROM `med_employees` a WHERE `company_id`='".$company_id."' AND `employee_type`='1' ORDER BY data DESC limit $start,$limit")->result_array();
		
		$data = $this->db->query("SELECT `employeeid`,employee_number,employee_name,IF(employee_picture='','',CONCAT('".base_url()."','',employee_picture)) as employee_picture,IFNULL((SELECT SUM(`data`) FROM `med_data_usage` WHERE `employeeid`=a.employeeid AND date(`date`) > date(NOW() - INTERVAL 1 MONTH) ),0) as data FROM `med_employees` a WHERE `company_id`='".$company_id."' $condition ORDER BY data DESC limit $start,$limit")->result_array();
		for($i=0;$i<count($data);$i++){
			$usages = $this->db->query("SELECT DISTINCT(DATE(`date`)) AS `date` FROM `med_data_usage` WHERE `employeeid`='".$data[$i]['employeeid']."' AND date(`date`) > date(NOW() - INTERVAL 1 MONTH) ")->result_array();
			for($x=0;$x<count($usages);$x++){
				$usages[$x]['data']= $this->db->query("SELECT SUM(`data`) AS `data` FROM `med_data_usage` WHERE `employeeid`='".$data[$i]['employeeid']."' AND DATE(`date`)='".$usages[$x]['date']."'")->row_array()['data'];
			}
			$data[$i]['data_usages'] = $usages;
		}

		$totalusage = $this->db->query("SELECT IFNULL(SUM(`data`),0) as `data` FROM `med_data_usage` WHERE `company_id`='".$company_id."' AND date(`date`) BETWEEN '".$input['start']."' AND '".$input['end']."' ")->row_array()['data'];
		return array('status' =>200,'message' =>'Data usage history','data'=>$data,'total_data_usage'=>$totalusage,'method'=>'get_dataUsagesByStaffMember','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}

	/*
	*  Insert data usage
	*/

	public function ht_insert_dataUsage($input)
	{	
		$input['company_id']	= getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$input['user_type']	= getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_type');
		if($input['company_id']){
			$input['date']	= today()[0];
			if($input['data']>0){
				$this->db->insert('med_data_usage',$input);
			}
		}
		return array('status' => 200,'message' =>'Inserted','method'=>'insert_dataUsage');
	}

	/*
	*  Get My Data usage
	*/

	public function ht_get_MyDataUsages($input)
	{
		
		$timezone	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'timezone');
		// get hour diff between utc and timezone
		$diff	=	getHoursMin($timezone);
		$page		= $input['page'];
		$limit		= 10;
		$start		= ($page - 1)* $limit; 
		$totalrows 	= $this->db->query("SELECT DISTINCT(date(`date`)) as `date` FROM `med_data_usage` WHERE `employeeid`='".$input['employeeid']."'")->num_rows();
		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0"; 
		$next		=  $page < $totalpages ? "1" : "0";
		$data = $this->db->query("SELECT DISTINCT(date(`date`)) as `date` FROM `med_data_usage` WHERE `employeeid`='".$input['employeeid']."' ORDER BY date DESC limit $start,$limit")->result_array();
		for($i=0;$i<count($data);$i++){
			$data[$i]['data_usages'] = $this->db->query("SELECT `data`,CONVERT_TZ (date, '+0:00','".$diff."') as `date` FROM `med_data_usage` WHERE `employeeid`='".$input['employeeid']."' AND DATE(`date`)='".$data[$i]['date']."'")->result_array();
			$data[$i]['total_data'] = $this->db->query("SELECT IFNULL(SUM(`data`),0) as `data` FROM `med_data_usage` WHERE `employeeid`='".$input['employeeid']."' AND DATE(`date`)='".$data[$i]['date']."' and user_type='1' ")->row_array()['data'];
		}
		return array('status' =>200,'message' =>'Data usage history','data'=>$data,'method'=>'get_MyDataUsages','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}
	
	/*
	*  Get Company accesses
	*/

	public function ht_get_company_accesses($input)
	{
		$company_id	= getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
		$data = $this->db->query("SELECT `company_id`,`billing_plan`, `billing_start_date` FROM `med_companies` WHERE `company_id`='".$company_id."'")->row_array();
		$access = "1";
		if($data['billing_start_date']=="" && $data['billing_plan']==0){
			$access = "1";
		}elseif($data['billing_start_date']!="" && $data['billing_plan']==0){
			$max = strtotime(date("Y-m-d H:i:s",strtotime("+ 30 days",strtotime($data['billing_start_date']))));
			$now = strtotime(date("Y-m-d H:i:s"));
			$days =  round(($max-$now)/(60 * 60 * 24));
			if($days>0){
				$access = "1";
			}else{
				$access = "0";
			}
		}
		return array('status' =>200,'message' =>'Company access','access'=>$access,'method'=>'get_company_accesses');
	}
	
	
	/*
	*  update section roles
	*/
	public function ht_update_section_access($input)
	{
		$input['updated_at']	= today()[0];
		$employees_arr	=	explode(',',$input['employess']);
		if(count($employees_arr)>0){
			for($i=0;$i<count($employees_arr);$i++){
				$this->db->query("UPDATE `med_employees` SET `section_roles`='".$input['accesses']."',`updated_at`=NOW() WHERE employeeid='".$employees_arr[$i]."'");
				
				#Send Push Notification
				$keys = array("employee_id"=>$employees_arr[$i]);
				$message = "Your Access Level are updated";
				$tokens = $this->db->query("SELECT distinct(`device_token`) as device_token,device_type FROM `med_employees_authentication` WHERE `employeeid`='".$employees_arr[$i]."'")->result_array();
				foreach($tokens as $token){
					if(strlen($token['device_token'])>30){
						if($token['device_type']==1){
							applePush($token['device_token'],$message,1,7,$keys);
						}else{
							andiPush($token['device_token'],$message,1,7,$keys);
						}
					}
				}
				
			}
			return array('status' => 200,'message' =>'Updated successfully','method'=>'update_section_access');
		}
	}
	
	
	/*
	*  Get Section accesses
	*/

	public function ht_get_section_accesses($input)
	{
		$employee_type	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_type');
		if($employee_type==3){
			$data = $this->db->query("SELECT `section_roles` FROM `med_employees` WHERE `employeeid`='".$input['employeeid']."'")->row_array();		
			return array('status' =>200,'message' =>'Section access','access'=>$data,'method'=>'get_section_accesses');
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'get_section_accesses');
		}		
	}
	
	
	/*
	*  Archive User
	*/
	public function ht_archive_User($input)
	{
		$employee_type	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_type');
		if($employee_type==2){
			$checkmanager_comp = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
			if($checkmanager_comp==$input['company_id']){
				$user_comp = getAnythingData("med_employees","employeeid",$input['userid'],'company_id');
				if($checkmanager_comp==$user_comp){
					$this->db->trans_start();
					$this->db->query("Update med_employees SET is_active = 0 WHERE `employeeid`='".$input['userid']."'");
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						return array('status' => 500,'message' =>'Internal server error.','method'=>'archive_User');
					}else{
						$this->db->trans_commit();
						return array('status' => 200,'message' =>'User Archived','method'=>'archive_User');
					}
				}else{
					return array('status' => 400,'message' => 'Unauthorized access!','method'=>'archive_User');
				}
			}else{
				return array('status' => 400,'message' => 'Unauthorized access!','method'=>'archive_User');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'archive_User');
		}
	}
	
	
	/*
	*  Un Archive User
	*/
	public function ht_unarchive_User($input)
	{
		$employee_type	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_type');
		if($employee_type==2){
			$checkmanager_comp = getAnythingData("med_employees","employeeid",$input['employeeid'],'company_id');
			if($checkmanager_comp==$input['company_id']){
				$user_comp = getAnythingData("med_employees","employeeid",$input['userid'],'company_id');
				if($checkmanager_comp==$user_comp){
					$this->db->trans_start();
					$this->db->query("Update med_employees SET is_active = 1 WHERE `employeeid`='".$input['userid']."'");
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						return array('status' => 500,'message' =>'Internal server error.','method'=>'archive_User');
					}else{
						$this->db->trans_commit();
						return array('status' => 200,'message' =>'User Unarchived','method'=>'archive_User');
					}
				}else{
					return array('status' => 400,'message' => 'Unauthorized access!','method'=>'archive_User');
				}
			}else{
				return array('status' => 400,'message' => 'Unauthorized access!','method'=>'archive_User');
			}
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'archive_User');
		}
	}
	
	/*
	*  Un Archive User
	*/
	public function ht_get_supervisor_accesses($input)
	{
		$employee_type	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_type');
		if($employee_type==2){
			$data = $this->db->query("SELECT `section_roles` FROM `med_employees` WHERE `employeeid`='".$input['userid']."'")->row_array();		
			return array('status' =>200,'message' =>'Section access','access'=>$data,'method'=>'get_section_accesses');			
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'get_supervisor_accesses');
		}
	}
	
	
	/*
	*  Get Carer Notificatons
	*/
	public function ht_get_CarerNotifications($input)
	{
		$employee_type	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_type');
		if($employee_type==1){
			
			#set isread
			
			//$this->db->query("update med_carer_notifications set is_read = 1 where carer_id = '".$input['employeeid']."'");
			
			$timezone	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'timezone');
			// get hour diff between utc and timezone
			$diff	=	getHoursMin($timezone);
			$lastdate = date('Y-m-d H:i:s', strtotime('-30 days'));
			$page		= $input['page'];
			$limit		= 20;
			$start		= ($page - 1)* $limit;
			$totalrows 	= $this->db->query("SELECT `carer_notification_id` FROM `med_carer_notifications` WHERE `carer_id`='".$input['employeeid']."'  AND created_at>= '".$lastdate."' AND (notification_type=3 OR notification_type=4)")->num_rows();
			$totalpages = ceil($totalrows/$limit);
			$previous	=  $totalpages > 1 ? "1" : "0"; 
			$next		=  $page < $totalpages ? "1" : "0";
			$data = $this->db->query("SELECT `carer_notification_id`, `notification_type`, `authority_id`, `carer_id`, `table_id`, `main_id`, `is_read`, CONVERT_TZ (created_at, '+0:00','".$diff."') as `created_at` FROM `med_carer_notifications` WHERE `carer_id`='".$input['employeeid']."'  AND created_at >= '".$lastdate."' AND  (notification_type=3 OR notification_type=4) ORDER BY carer_notification_id desc limit $start,$limit")->result_array();
				
			for($i=0;$i<count($data);$i++){
				if ($data[$i]['notification_type']==1){
					$schedule_id = $data[$i]['table_id'];
					$data[$i]['notification']=$this->db->query("SELECT a.`schedule_id`, a.`employeeid`, (select employee_name from med_employees where employeeid = a.employeeid ) as employee_name,(select employee_number from med_employees where employeeid = a.employeeid ) as employee_number, (select CONCAT(firstname,' ',lastname) from med_clients where client_id = a.client_id ) as client_name,a.`client_id`,CONVERT_TZ (a.`start_datetime`, '+0:00','".$diff."') as `start_datetime`,CONVERT_TZ (a.`end_datetime`, '+0:00','".$diff."') as `end_datetime`,CONVERT_TZ (a.`created_at`, '+0:00','".$diff."') as `created_at`,CONCAT('New schedule added with ',(select CONCAT(firstname,' ',lastname) from med_clients where client_id = a.client_id )) as message FROM  `med_schedule` as a WHERE a.schedule_id = '".$schedule_id."'")->row_array();
				}elseif($data[$i]['notification_type']==2){
					$schedule_id = $data[$i]['table_id'];
					$data[$i]['notification']=$this->db->query("SELECT a.`schedule_id`, a.`employeeid`, (select employee_name from med_employees where employeeid = a.employeeid ) as employee_name,(select employee_number from med_employees where employeeid = a.employeeid ) as employee_number, (select CONCAT(firstname,' ',lastname) from med_clients where client_id = a.client_id ) as client_name,a.`client_id`,CONVERT_TZ (a.`start_datetime`, '+0:00','".$diff."') as `start_datetime`,CONVERT_TZ (a.`end_datetime`, '+0:00','".$diff."') as `end_datetime`,CONVERT_TZ (a.`created_at`, '+0:00','".$diff."') as `created_at`,CONCAT('Your schedule with ',(select CONCAT(firstname,' ',lastname) from med_clients where client_id = a.client_id ) , ' is updated') as message FROM  `med_schedule` as a WHERE a.schedule_id = '".$schedule_id."'")->row_array();
				}elseif($data[$i]['notification_type']==3){
					$client_id = $data[$i]['table_id'];
					$data[$i]['notification'] = $this->db->query("SELECT client_id,firstname,lastname,postcode,CONCAT(firstname,' ',lastname ,' profile is updated') as message from med_clients where client_id = '".$data[$i]['main_id']."'")->row_array();
				}elseif($data[$i]['notification_type']==4){
					$client_id = $data[$i]['table_id'];
					$data[$i]['notification'] = $this->db->query("SELECT client_id,firstname,lastname,postcode,CONCAT('Program of care is updated for  ',firstname,' ',lastname ) as message from med_clients where client_id = '".$data[$i]['main_id']."'")->row_array();
				}elseif($data[$i]['notification_type']==5){
					$concernid = $data[$i]['table_id'];
					$client_name	=	@$this->db->query("select CONCAT(firstname,' ',lastname) as name from med_clients where client_id = (select client_id from med_concerns where concern_id = '".$concernid."')")->row_array()['name'];
					$data[$i]['notification'] = $this->db->query("SELECT (select subject from med_concerns where concern_id = '".$concernid."') as subject,reply,CONCAT('Your concern with ".$client_name." is resolved','') as message from med_concern_replies where concern_id = '".$concernid."' order by reply_id desc limit 1")->row_array();
				}
			}
			return array('status' =>200,'message' =>'Notfications List','data'=>$data,'method'=>'get_CarerNotifications','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'get_CarerNotifications');
		}
	}
	
	/*
	*  Un Archive User
	*/
	public function ht_Set_CarerNotificationRead($input)
	{
		$employee_type	 = getAnythingData("med_employees","employeeid",$input['employeeid'],'employee_type');
		if($employee_type==1){
			$this->db->query("update med_carer_notifications set is_read = 1 where carer_id = '".$input['employeeid']."' and carer_notification_id='".$input['carer_notification_id']."'");
			return array('status' => 200,'message' => 'Successfull','method'=>'Set_CarerNotificationRead');
		}else{
			return array('status' => 400,'message' => 'Unauthorized access!','method'=>'Set_CarerNotificationRead');
		}
	}
	
}