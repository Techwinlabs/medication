<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ADMIN_Controller extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->userdata = $this->session->userdata('userdata');
		$this->session->set_flashdata('segment', explode('/', $this->uri->uri_string()));
		if ($this->session->userdata('status') != 'ADMIN') {
			redirect(base_url().'admin');
		}
		

		#--Update session data---#
		$data =  $this->db->query("SELECT * FROM `med_employees` WHERE employeeid='".$this->userdata['employeeid']."'")->row_array();
		unset($data['employee_password']);
		$bilingstart = getAnythingData("med_companies","company_id",$data['company_id'],'billing_start_date');
		$data['employee_picture'] != '' ? $data['employee_picture'] = base_url().$data['employee_picture'] : '';
		$billing_plan = getAnythingData("med_companies","company_id",$data['company_id'],'billing_plan');
		$data['billing_plan'] = $billing_plan;
		$session = [
			'userdata' => $data,
			'status' => "ADMIN",
			'trial' =>date("M d,, Y H:i:s",strtotime("+ 30 days",strtotime($bilingstart))),
			'access' =>1,
			'billing_plan' =>$data['billing_plan'],
		];
		$status = getAnythingData('med_companies','company_id',$data['company_id'],'status');
		$session['company_status']	=	$status;
		$this->session->set_userdata($session);
		#--get coompany info --#
		$this->load->model('super/Super_companies');
		$companyDetails = $this->Super_companies->viewCompany_profile($this->userdata);
		$this->userdata["company_name"] = $companyDetails["company_name"];
	}

	public function load_view($template_name, $vars = array(), $return = FALSE)
	{
		//if($this->session->userdata('access')==0){
		//	redirect(base_url().'admin/payment');
		//}
		
		if($return):
			$content  = $this->load->view('admin/header', $vars, $return);
			$content  = $this->load->view('admin/'.$template_name, $vars, $return);
			return $content;
		else:
			$content  = $this->load->view('admin/header', $vars, $return);
			$content  = $this->load->view('admin/'.$template_name, $vars, $return);
			$content  = $this->load->view('admin/footer');
		endif;
    }
	
}