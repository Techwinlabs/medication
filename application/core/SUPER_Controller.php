<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SUPER_Controller extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->userdata = $this->session->userdata('superdata');
		$this->session->set_flashdata('segment', explode('/', $this->uri->uri_string()));
		if ($this->session->userdata('superstatus') != 'SUPER') {
			redirect(base_url().'super');
		}
	}

	public function load_view($template_name, $vars = array(), $return = FALSE)
	{
		if($return):
			$content  = $this->load->view('super/header', $vars, $return);
			$content  = $this->load->view('super/'.$template_name, $vars, $return);
			return $content;
		else:
			$content  = $this->load->view('super/header', $vars, $return);
			$content  = $this->load->view('super/'.$template_name, $vars, $return);
		endif;
    }
	
	
	
	
}
