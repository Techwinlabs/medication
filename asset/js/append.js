/* Custom functions */
function addMoreAMMedicine(){
	var a1 = 0;
	var a2 = 0;
	var a3 = 0;
	var a4 = 0;
	$('[name="am_time_care_program[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a1	=	1;
			return false;
		}
	});
	$('[name="am_time_care_program_type[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a2	=	1;
			return false;
		}
	});
	$('[name="am_time_care_program_time[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a3	=	1;
			return false;
		}
	});
	$('[name="am_time_care_program_dose[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a4	=	1;
			return false;
		}
	});

	if(a1==1 || a2==1 || a3==1 || a4==1){
		alert('Please fill previous fields first.');
	}else{
		var x = Math.round(Math.random()*1000) + 1;
		var wrapper         = jQuery(".apendAm"); //Fields wrapper
		jQuery(wrapper).append('<div class="col-md-12 mar-box"><span style="font-size:25px; color:red" title="Click to remove medicine" class="icons icon-close remove_field"></span><div class="col-md-12 form-group"> <label class="col-sm-3 control-label text-right">Medicine Name</label><div class="col-md-8"> <input type="text" name="am_time_care_program[]" class="form-control default"></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Medicine Type</label><div class="col-md-8"> <input type="text" name="am_time_care_program_type[]" class="form-control default" placeholder="eg. Pill, Cream, Injection etc"></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Dose</label><div class="col-md-8"> <input type="text" name="am_time_care_program_dose[]" class="form-control default" placeholder=""></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Medicine Time</label><div class="col-md-8"> <input type="time" name="am_time_care_program_time[]" class="form-control default"></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Other Detail</label><div class="col-md-8"> <input type="text" name="am_time_care_program_detail[]" class="form-control default"></div></div></div>');
		jQuery(wrapper).on("click",".remove_field", function(e){ //user click on remove text
			jQuery(this).parent('div').remove();
		});
	}
	
}

function addMoreNoonMedicine(){
	var a1 = 0;
	var a2 = 0;
	var a3 = 0;
	var a4 = 0;
	$('[name="noon_time_care_program[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a1	=	1;
			return false;
		}
	});
	$('[name="noon_time_care_program_type[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a2	=	1;
			return false;
		}
	});
	$('[name="noon_time_care_program_time[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a3	=	1;
			return false;
		}
	});
	$('[name="noon_time_care_program_dose[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a4	=	1;
			return false;
		}
	});

	if(a1==1 || a2==1 || a3==1 || a4==1){
		alert('Please fill previous fields first.');
	}else{
		var x = Math.round(Math.random()*1000) + 1;
		var wrapper         = jQuery(".apendNoon"); //Fields wrapper
		jQuery(wrapper).append('<div class="col-md-12 mar-box"><span style="font-size:25px; color:red" title="Click to remove medicine" class="icons icon-close remove_field"></span><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Medicine Name</label><div class="col-md-8"> <input type="text" name="noon_time_care_program[]" class="form-control default"></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Medicine Type</label><div class="col-md-8"> <input type="text" name="noon_time_care_program_type[]" class="form-control default" placeholder="eg. Pill, Cream, Injection etc"></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Dose</label><div class="col-md-8"> <input type="text" name="noon_time_care_program_dose[]" class="form-control default" placeholder=""></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Medicine Time</label><div class="col-md-8"> <input type="time" name="noon_time_care_program_time[]" class="form-control default"></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Other Detail</label><div class="col-md-8"> <input type="text" name="noon_time_care_program_detail[]" class="form-control default"></div></div></div>');
		jQuery(wrapper).on("click",".remove_field", function(e){ //user click on remove text
			jQuery(this).parent('div').remove();
		});
	}
}

function addMoreEveingMedicine(){
	var a1 = 0;
	var a2 = 0;
	var a3 = 0;
	var a4 = 0;
	$('[name="tea_time_care_program[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a1	=	1;
			return false;
		}
	});
	$('[name="tea_time_care_program_type[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a2	=	1;
			return false;
		}
	});
	$('[name="tea_time_care_program_time[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a3	=	1;
			return false;
		}
	});
	$('[name="tea_time_care_program_dose[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a4	=	1;
			return false;
		}
	});
	
	if(a1==1 || a2==1 || a3==1 || a4==1){
		alert('Please fill previous fields first.');
	}else{
		var x = Math.round(Math.random()*1000) + 1;
		var wrapper         = jQuery(".apendEvening"); //Fields wrapper
		jQuery(wrapper).append('<div class="col-md-12 mar-box"><span style="font-size:25px; color:red" title="Click to remove medicine" class="icons icon-close remove_field"></span><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Medicine Name</label><div class="col-md-8"> <input type="text" name="tea_time_care_program[]" class="form-control default"></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Medicine Type</label><div class="col-md-8"> <input type="text" name="tea_time_care_program_type[]" class="form-control default" placeholder="eg. Pill, Cream, Injection etc"></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Dose</label><div class="col-md-8"> <input type="text" name="tea_time_care_program_dose[]" class="form-control default" placeholder=""></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Medicine Time</label><div class="col-md-8"> <input type="time" name="tea_time_care_program_time[]" class="form-control default"></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Other Detail</label><div class="col-md-8"> <input type="text" name="tea_time_care_program_detail[]" class="form-control default"></div></div></div>');
		jQuery(wrapper).on("click",".remove_field", function(e){ //user click on remove text
			jQuery(this).parent('div').remove();
		});
	}
}

function addMoreNightMedicine(){
	var a1 = 0;
	var a2 = 0;
	var a3 = 0;
	var a4 = 0;
	$('[name="night_time_care_program[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a1	=	1;
			return false;
		}
	});
	$('[name="night_time_care_program_type[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a2	=	1;
			return false;
		}
	});
	$('[name="night_time_care_program_time[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a3	=	1;
			return false;
		}
	});
	$('[name="night_time_care_program_dose[]"]').each(function(){
		var am=($(this).val());
		if(am==""){
			a4	=	1;
			return false;
		}
	});

	if(a1==1 || a2==1 || a3==1 || a4==1){
		alert('Please fill previous fields first.');
	}else{
		var x = Math.round(Math.random()*1000) + 1;
		var wrapper         = jQuery(".apendNight"); //Fields wrapper
		jQuery(wrapper).append('<div class="col-md-12 mar-box"><span style="font-size:25px; color:red" title="Click to remove medicine" class="icons icon-close remove_field"></span><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Medicine Name</label><div class="col-md-8"> <input type="text" name="night_time_care_program[]" class="form-control default"></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Medicine Type</label><div class="col-md-8"> <input type="text" name="night_time_care_program_type[]" class="form-control default" placeholder="eg. Pill, Cream, Injection etc"></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Dose</label><div class="col-md-8"> <input type="text" name="night_time_care_program_dose[]" class="form-control default" placeholder=""></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Medicine Time</label><div class="col-md-8"> <input type="time" name="night_time_care_program_time[]" class="form-control default"></div></div><div class="form-group col-md-12"> <label class="col-sm-3 control-label text-right">Other Detail</label><div class="col-md-8"> <input type="text" name="night_time_care_program_detail[]" class="form-control default"></div></div></div>');
		jQuery(wrapper).on("click",".remove_field", function(e){ //user click on remove text
			jQuery(this).parent('div').remove();
		});
	}					
}

function goBack() {
    window.history.back();
}

/*Ajax Section*/

function care_program(i){
	$.ajax({
        url: '../admin/ajax/care_program',
		method: 'post',
		data:{'id':i},
		success: function(data) {
			$('#POC').html(data);
			$('#poc_mode').modal('show');
		},
		error: function ( data )
        {
            alert('Internal server error!. Please try after some time.');
        }
	});
}

function searchMAR(i){
	var s = $("#startdate").val();
	var e = $("#enddate").val();
	if( (s=='' && e!='') || (s!='' && e=='')){
		alert("Please select a correct date range!");
	}else{
		$.ajax({
			url: '../../admin/ajax/searchMAR',
			method: 'post',
			data:{'client_id':i,'start':s,'end':e},
			success: function(data) {
				
				$('#MAR').html(data);
				$('#mar-table').DataTable({
					"order": [],
					dom: 'lBfrtip',
				});
				
			},
			error: function ( data )
			{
				alert(data.responseText)
			}
		});
	}
}

function filterDailyRecords(i){
	var s = $("#startdate").val();
	var e = $("#enddate").val();
	if( (s=='' && e!='') || (s!='' && e=='')){
		alert("Please select a correct date range!");
	}else{
		$.ajax({
			url: '../../admin/ajax/filterDailyRecords',
			method: 'post',
			data:{'client_id':i,'start':s,'end':e},
			success: function(data) {
				$('#dailyrecords').html(data);
				$('#table-grid').DataTable({
					"order": [],
					dom: 'lBfrtip',
				} );
			},
			error: function ( data )
			{
				alert(data.responseText)
			}
		});
	}
}


function getScheduleByDate(){
	var s = $("#startdate").val();
	if (s == ""){
		location.reload();
	}else{
		$.ajax({
			url: '../admin/ajax/getScheduleByDate',
			method: 'post',
			data:{'start':s},
			success: function(data) {
				if(data == 1){
					alert("No schedule found for selected date range!");
				}else{
					$('#day_wise_schedule').html(data);
					$(".confirm").popConfirm();
					$('#schedule_table').DataTable();
			
				}
			},
			error: function ( data )
			{
				alert(data.responseText)
			}
		});
	}
}

/* View Profile Emplyee */

function viewProfile_emp(tid,eid,manage=0){

	if(manage == 1){
		var url = '../admin/ajax/viewProfile_emp';
	}else{
		var url = '../../admin/ajax/viewProfile_emp';
	}
	$.ajax({
		url: url,
		method: 'post',
		data:{'employeeid':eid,'tid':tid},
		success: function(data) {
			if(data == 1){
				alert("Something went wrong! Please refresh the page.");
			}else{
				$("#load_profile").html(data);
				$("#"+tid).modal('show');
			}
		},
		error: function ( data )
		{
			alert(data.responseText)
		}
	});
	
}
/* View Profile Emplyee */

function viewProfile_client(tid,eid){
	
	$.ajax({
		url: '../admin/ajax/viewProfile_client',
		method: 'post',
		data:{'client_id':eid,'tid':tid},
		success: function(data) {
			if(data == 1){
				alert("Something went wrong! Please refresh the page.");
			}else{
				$("#load_profile").html(data);
				$("#profile_client"+tid).modal('show');
			}
		},
		error: function ( data )
		{
			alert(data.responseText)
		}
	});
	
}

/* View Company Profile */

function viewCompany_profile(cid){
	$.ajax({
		url: '../super/ajax/viewCompany_profile',
		method: 'post',
		data:{'company_id':cid},
		success: function(data) {
			if(data == 1){
				alert("Something went wrong! Please refresh the page.");
			}else{
				$("#load_profile").html(data);
				$("#profile_company"+cid).modal('show');
			}
		},
		error: function ( data )
		{
			alert(data.responseText)
		}
	});
}

/* Send new Message */

function sendNewMessage(receiver_id){
	var message = $("#newmessage").val();
	if($.trim(message) == ""){
		alert("Error: Message can't be empty!");
		 $("#newmessage").val('');
	}else{
		$.ajax({
			url: '../../../admin/ajax/sendNewMessage',
			method: 'post',
			data:{'message':message,'receiver_id':receiver_id},
			global: false,
			success: function(data) {
				
				if(data == 1){
					alert("Something went wrong! Please refresh the page.");
				}else{
					$("#append_message").append(data);
					$('#chatlist').scrollTop($('#chatlist')[0].scrollHeight);
					$("#newmessage").val('');
				}
			},
			error: function ( data )
			{
				alert(data.responseText);
			}
		});
	}
	
}

// Get new messages

function loadNewMessages(receiver_id){
	var message_id = $("#last_message").val(); 
	$.ajax({
		url: '../../../admin/ajax/loadNewMessages',
		method: 'post',
		data:{'message_id':message_id,'receiver_id':receiver_id},
		global: false,
		success: function(data) {
			if(data != 1){
				$("#append_message").html(data);
				$('#chatlist').scrollTop($('#chatlist')[0].scrollHeight);
			}
		},
		error: function ( data )
		{
			alert(data.responseText)
		}
	});
}
function loadCarer(){
	var check = $("#load_carer").val();
	if(check=='0'){
		$.ajax({
			url: '../admin/ajax/loadCarer',
			method: 'post',
			success: function(data) {
				$("#carer_triger").html(data);
				$('.selectpicker').selectpicker();
				
			},
			error: function ( data )
			{
				alert(data.responseText);
			}
		});
	}
	
}
function loadCarerForTimesheet(){
	var check = $("#load_carer").val();
	if(check=='0'){
		$.ajax({
			url: '../admin/ajax/loadCarerForTimesheet',
			method: 'post',
			success: function(data) {
				$("#carer_triger").html(data);
				$('.selectpicker').selectpicker();
			},
			error: function ( data )
			{
				alert(data.responseText);
			}
		});
	}
	
}

function getDataUsage(){
	var start = $("#startdate").val();
	var end = $("#enddate").val();
	var check = $("#load_carer").val();
	if(check=='0' || check=="" || start=="" || end==""){
		alert("All fields are required");
	} else if(check < 1){
		alert("Please select a carer first.");
	}else{
		$.ajax({
			url: '../admin/ajax/getDataUsage',
			method: 'post',
			data:{ 'carer_id':check,'start':start,'end':end },
			success: function(data) {
				$("#data_usage").html(data);
			},
			error: function ( data )
			{
				alert(data.responseText);
			}
		});
	}
}
function getTimeSheet(){
	var check 		= $("#load_carer").val();
	var startdate 	= $("#startdate").val();
	var enddate 	= $("#enddate").val();
	
	if(startdate==""){
			alert("Select start date.");
	}else{
		if(enddate ==""){
			alert("Select end date.");
		} else if((Date.parse(startdate)) > (Date.parse(enddate))){
		
			alert("End date can't be less then start date.");
			
		}else{
			if(check=='0' || check=="" ){
				alert("Select carer from dropdwon.");
			}else{
				$.ajax({
					url: '../admin/ajax/getTimeSheet',
					method: 'post',
					data:{ 'carerid':check ,'startdate' : startdate , 'enddate' : enddate },
					success: function(data) {
						$("#time_sheets").html(data);
					},
					error: function ( data )
					{
						alert(data.responseText)
					}
				});
			}
		}
	}
}

function getfilteredNotifications(i){
	$.ajax({
		url: '../admin/ajax/getfilteredNotifications',
		method: 'post',
		data:{ 'filter':i },
		success: function(data) {
			$("#view_notifications").html(data);
		},
		error: function ( data )
		{
			alert(data.responseText)
		}
	});
}

// archive user

function archive_unarchive(id,type) {
    $.ajax({
		url: '../admin/ajax/archive_unarchive',
		method: 'post',
		data:{ 'id':id , 'type' : type },
		success: function(data) {
			if(data !="error"){	
				if (data==1) {
					$('#archive'+id).html('<a href="" onclick="archive_unarchive('+id+',2)" class="confirm" title="<b>Are you sure to unarchive!</b>"><i class="fa fa-undo"></i></a>');
					jQuery(".confirm").popConfirm();
				}else{
					$('#archive'+id).html('<a href="" onclick="archive_unarchive('+id+',1)" class="confirm" title="<b>Are you sure to archive!</b>"><i class="fa fa-archive"></i></a>');
					jQuery(".confirm").popConfirm();
				}
			}else{
				alert("Maximum users allocated! Please call support to increase allocation.");
			}
			$("#view_notifications").html(data);
		},
		error: function ( data )
		{
			console.log(data.responseText)
		}
	});
}



/* delete employee */

function delete_employee(row_id,emp_id,type){
	$.ajax({
		url: '../admin/ajax/delete_employee',
		method: 'post',
		data:{'emp_id':emp_id,'emp_type':type},
		success: function(data) {
			if(data == 1){
				if (type == 1) {
                    $('#emp'+row_id+'').fadeOut(1200).css({'background-color':'#f2dede'});
                }else if (type == 2) {
                    $('#man'+row_id+'').fadeOut(1200).css({'background-color':'#f2dede'});
                }else{
					$('#sup'+row_id+'').fadeOut(1200).css({'background-color':'#f2dede'});
				}
			}else{
				alert("Something went wrong! Please refresh the page.");
			}
		},
		error: function ( data )
		{
			console.log(data.responseText)
		}
	});	
}

/* delete employee */

function super_delete_employee(row_id,emp_id,type){
	$.ajax({
		url: '../ajax/super_delete_employee',
		method: 'post',
		data:{'emp_id':emp_id,'emp_type':type},
		success: function(data) {
			if(data == 1){
				if (type == 1) {
                    $('#emp'+row_id+'').fadeOut(1200).css({'background-color':'#f2dede'});
                }else if (type == 2) {
                    $('#man'+row_id+'').fadeOut(1200).css({'background-color':'#f2dede'});
                }else{
					$('#sup'+row_id+'').fadeOut(1200).css({'background-color':'#f2dede'});
				}
			}else{
				alert("Something went wrong! Please refresh the page.");
			}
		},
		error: function ( data )
		{
			console.log(data.responseText)
		}
	});	
}


/* delete client */

function delete_client(row_id,client_id){
	$.ajax({
		url: '../admin/ajax/delete_client',
		method: 'post',
		data:{'client_id':client_id},
		success: function(data) {
			if(data == 1){
				console.log(data);
				$('#client_'+row_id+'').fadeOut(1200).css({'background-color':'#f2dede'});
			}else{
				alert("Something went wrong! Please refresh the page.");
			}
		},
		error: function ( data )
		{
			console.log(data.responseText)
		}
	});	
}

/* delete schedule */

function delete_schedule(sch_id){
	if (confirm("Are you sure you want to delete this schedule ?")){
		$.ajax({
			url: '../admin/ajax/delete_schedule',
			method: 'post',
			data:{'sch_id':sch_id},
			success: function(data) {
				if(data == 1){
					window.location.reload();
				}else{
					alert("Something went wrong! Please refresh the page.");
				}
			},
			error: function ( data )
			{
				console.log(data.responseText)
			}
		});
	}
}

/* add schedule */

function add_schedule(){
	var start 		= $('#sechedule_date_start').val();
	var end   		= $('#sechedule_date_end').val();
	var employeeid 	= $('#employeeid').val();
	var client_id	= $('#client_id').val();
	
	if (start != '' && end !='' && employeeid != '' && client_id !='' ) {
		if(start<end){	
			if (moment(start, 'YYYY-MM-DD HH:mm', true).isValid() && moment(end, 'YYYY-MM-DD HH:mm', true).isValid()) {
				$.ajax({
					url: 'new_schedule',
					method: 'post',
					data:{'start':start,'end':end,'employeeid':employeeid,'client_id':client_id},
					success: function(data) {
						if(data == 1){
							window.location.reload();
						}else{
							alert(data);
						}
					},
					error: function ( data )
					{
						console.log(data.responseText)
					}
				});
			}else{
				alert('Incorrect date & time format');
			}
		}else{
			alert("End date should  be grater than start date.");
		}
	}else{
		alert('please fill all the details');
	}
	
}

/* update schedule */

function update_schedule(id){
	var start 		= $('#edit_sechedule_date_start').val();
	var end   		= $('#edit_sechedule_date_end').val();
	var employeeid 	= $('#edit_employeeid').val();
	var client_id	= $('#edit_client_id').val();
	if (start != '' && end !='' && employeeid != '' && client_id !='' ) {
		
		if(start<end){
			if (moment(start, 'YYYY-MM-DD HH:mm', true).isValid() && moment(end, 'YYYY-MM-DD HH:mm', true).isValid()) {
				$.ajax({
					url: 'update_schedule/'+id,
					method: 'post',
					data:{'start':start,'end':end,'employeeid':employeeid,'client_id':client_id,'schedule_id':id},
					success: function(data) {
						if(data == 1){
							window.location.reload();
						}else{
							alert(data);
							$('#error_div').html(data);
						}
					},
					error: function ( data )
					{
						console.log(data.responseText)
					}
				});
			}else{
				alert('Incorrect date & time format');
			}
		}else{
			alert("End date should  be grater than start date.");
		}
	}else{
		alert('please fill all the details');
	}
}


/* get_schedule_info */

function get_schedule_info(id){
	$.ajax({
		url: 'ajax/get_schedule_info',
		method: 'post',
		data:{'schedule_id':id},
		success: function(data) {
			if(data){
				$('#view_schedule').html(data);
				$('#schedule'+id).modal('show');
			}
		},
		error: function ( data )
		{
			alert(data.responseText)
		}
	});
}

/* edit_schedule_view */

function edit_schedule_view(sid,cid,s_date=0,e_date=0,carerid=''){
	$.ajax({
		url: 'ajax/edit_schedule_view',
		method: 'post',
		data:{'schedule_id':sid,'company_id':cid,'carerid':carerid},
		success: function(data) {
			if(data){
				$('#schedule'+sid).modal('hide');
				$('#edit_schedule_view').html(data);
				if (s_date !=0 && e_date != 0) {
					$('#edit_sechedule_date_start').val(s_date);
					$('#edit_sechedule_date_end').val(e_date);
				}
				$('#edit_schedule'+sid).modal('show');
			}
		},
		error: function ( data )
		{
			alert(data.responseText)
		}
	});
}

function resolveCon(i,eid){
	$.ajax({
		url: 'ajax/reply_concern',
		method: 'post',
		data:{'concernid':i,'employee_id':eid},
		success: function(data) {
			if(data){
				$('#reply_concern_modal').html(data);
				$('#reply_concern').modal('show');
			}
		},
		error: function ( data )
		{

			alert(data.responseText)
		}
	});
}

function updateConcern(){
	var cid = 	$("#concern_id").val();
	var eid = 	$("#employee_id").val();
	var cm  = 	$("#concern_message").val();
	$.ajax({
		url: 'ajax/update_concern',
		method: 'post',
		data:{'concernid':cid,'concern_message':cm,'employee_id':eid},
		success: function(data) {
			if(data==1){
				window.location.reload();
			}else{
				alert(data);
			}
		},
		error: function ( data )
		{
			alert(data.responseText)
		}
	});
}
